/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.command

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import spock.lang.Specification;

class JavaCommandTest extends Specification {

	def "this test class doesn't know a resource class"(){
		
		when :
		
			Class<?>  clazz = this.getClass().getClassLoader().loadClass("org.squashtest.installer.command.test.TestObject");
			
		then :
			thrown(ClassNotFoundException)
		
	}
	
	/*
	 * well because surefire would have tried to load it (fruitlessly) the .class we want to load does not exist yet. 
	 * We create it at runtime, by simply copy the bytecode file and rename it to .class.
	 * 
	 * Still, the best way would be to prevent surefire from trying to load it.
	 * 
	 */
	def "should load and execute a resource class"(){
		
		given : "stupid trick, see comment above"
			
			URL bytesUrl = this.getClass().getClassLoader()
				.getResource("runtime-classes/org/squashtest/installer/command/test/TestObject.class.bytes");
				
			URL destUrl = new URL(bytesUrl.toString().replace(".bytes", ""));
			
			File byteFile = new File(bytesUrl.toURI());
			File destFile = new File(destUrl.toURI());
			
			FileUtils.copyFile(byteFile, destFile);
				
		
		and : "class url"
		
			URL classUrl = this.getClass().getClassLoader().getResource("runtime-classes/");
		
		and : "temp output file"
			
			File file = File.createTempFile("test", "temp"); 
			
		and : "configure the command"
			JavaCommand command = new JavaCommand();
			command.addToClassPath(classUrl);
			command.setMainClassName("org.squashtest.installer.command.test.TestObject")
			
			command.addArg file.getCanonicalPath()
			command.addArg "this test" 
			command.addArg "is undoubtfully"
			command.addArg "successful"
			
		when :
		
			command.execute()
			def readOut = new BufferedReader(new FileReader(file))

			
		then :
		try{
			readOut.readLine()=="this test"
			readOut.readLine()=="is undoubtfully"
			readOut.readLine()=="successful"
			readOut.close()
		}finally{
			destFile.delete();
		}
		
	}
	
}
