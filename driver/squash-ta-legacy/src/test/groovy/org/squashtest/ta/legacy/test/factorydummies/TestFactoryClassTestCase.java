/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.test.factorydummies;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.instructions.TestInstructionVisitor;

public class TestFactoryClassTestCase extends AbstractCatsTestCase{
	private static final class DummyInstruction implements TestInstruction {
		@Override
		public void visit(TestInstructionVisitor visitor) {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public String toText() {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void setText(String text) {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException("TODO");
		}
	}

	public static final TestInstruction SETUP1_INSTR=new DummyInstruction();
	public static final TestInstruction SETUP2_INSTR=new DummyInstruction();
	public static final TestInstruction TEARDOWN1_INSTR=new DummyInstruction();
	public static final TestInstruction TEARDOWN2_INSTR=new DummyInstruction();
	public static final TestInstruction TEST_INSTR=new DummyInstruction();
	public static final TestInstruction SETUP_SNARE_INSTR=new DummyInstruction();
	public static final TestInstruction TEARDOWN_SNARE_INSTR=new DummyInstruction();
	
	@Before
	public void setup1(){
		getCallListener().addInstruction(SETUP1_INSTR);
	}
	
	@Before
	public void setup2(){
		getCallListener().addInstruction(SETUP2_INSTR);
	}
	
	@After
	public void teardown1(){
		getCallListener().addInstruction(TEARDOWN1_INSTR);
	}
	
	@After
	public void teardown2(){
		getCallListener().addInstruction(TEARDOWN2_INSTR);
	}

	//nothing in there, but a noop test.
	@Test
	public void testDummy(){
		getCallListener().addInstruction(TEST_INSTR);
	}
	
	@BeforeClass
	public void setUpSnare(){
		//this method should be ignored by the test factory (it applies at ecosystem level)
		getCallListener().addInstruction(SETUP_SNARE_INSTR);
	}
	
	@AfterClass
	public void tearDownSnare(){
		//this method should be ignored by the test factory (it applies at ecosystem level)
		getCallListener().addInstruction(TEARDOWN_SNARE_INSTR);
	}
}
