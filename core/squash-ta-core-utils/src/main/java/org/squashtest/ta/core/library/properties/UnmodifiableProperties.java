/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.library.properties;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Collection;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Read-only version of the {@link Properties} type.
 * @author edegenetais
 *
 */
@SuppressWarnings("serial")
public class UnmodifiableProperties extends Properties {
	
	public UnmodifiableProperties(Properties dataSource) {
		super.putAll(dataSource);
	}
	
	@Override
	public synchronized void clear() {
		throw new UnsupportedOperationException("Immutable object");
	}

	@Override
	public Set<java.util.Map.Entry<Object, Object>> entrySet() {
		return Collections.unmodifiableSet(super.entrySet());
	}

	@Override
	public Set<Object> keySet() {
		return Collections.unmodifiableSet(super.keySet());
	}

	@Override
	public synchronized void load(InputStream inStream) throws IOException {
		throw new UnsupportedOperationException("Immutable object");
	}

	@Override
	public synchronized void load(Reader reader) throws IOException {
		throw new UnsupportedOperationException("Immutable object");
	}

	@Override
	public synchronized void loadFromXML(InputStream in) throws IOException,
			InvalidPropertiesFormatException {
		throw new UnsupportedOperationException("Immutable object");
	}

	@Override
	public synchronized Object put(Object key, Object value) {
		throw new UnsupportedOperationException("Immutable object");
	}

	@Override
	public synchronized void putAll(Map<? extends Object, ? extends Object> t) {
		throw new UnsupportedOperationException("Immutable object");
	}
	
	@Override
	public synchronized Object remove(Object key) {
		throw new UnsupportedOperationException("Immutable object");
	}
	
	@Override
	public synchronized Object setProperty(String key, String value) {
		throw new UnsupportedOperationException("Immutable object");
	}
	
	@Override
	public Collection<Object> values() {
		throw new UnsupportedOperationException("Immutable object");
	}
}
