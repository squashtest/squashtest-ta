/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java;

/**
 * Exception thrown when some error happends during the bundle-specific class-loading process.
 * @author edegenetais
 *
 */
@SuppressWarnings("serial")
public class JavaBundleClassLoadingException extends RuntimeException {

	public JavaBundleClassLoadingException() {
		// TODO Auto-generated constructor stub
	}

	public JavaBundleClassLoadingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JavaBundleClassLoadingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public JavaBundleClassLoadingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
