/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.converters;

import java.io.File
import java.util.Properties

import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository
import org.squashtest.ta.soapui.converters.FileToSoapUI
import org.squashtest.ta.soapui.resources.SoapUIScript
import org.squashtest.ta.framework.exception.BadDataException;

import static org.junit.Assert.*

import spock.lang.Specification

class FileToSoapUITest extends Specification {

	def "the soapUI file must be correctly formated"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/soapui/converters/correct-soapui-project.xml")
			File file = new File(url.toURI())
			FileResource myResource = new FileResource(file)
			FileToSoapUI converter = new FileToSoapUI();
		when :
			SoapUIScript mySoapUIScript = converter.convert(myResource)
			
		then :
			mySoapUIScript != null
			notThrown(BadDataException) 
	}
	
	def "the soapUI file makes the validation crash"(){
		
			given :
				URL url = getClass().getClassLoader().getResource("org/squashtest/ta/soapui/converters/incorrect-soapui-project.xml")
				File file = new File(url.toURI())
				FileResource myResource = new FileResource(file)
				FileToSoapUI converter = new FileToSoapUI();
			when :
				SoapUIScript mySoapUIScript = converter.convert(myResource)
				
			then :
				thrown(BadDataException)
		}
}
