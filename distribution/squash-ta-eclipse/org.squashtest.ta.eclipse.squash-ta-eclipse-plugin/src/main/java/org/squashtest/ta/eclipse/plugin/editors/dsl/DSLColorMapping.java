/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.editors.dsl;

import org.eclipse.swt.graphics.RGB;

/**
 * Defining base colors for the DSL elements.
 * @author edegenetais
 *
 */
public enum DSLColorMapping {
	DSL_COMMENT(new RGB(128,128,128)),
	MACRO_CALL(new RGB(128, 80, 0)),
	PHASE_MARKER(new RGB(80,128,0)),
	COMPONENT_TYPE(new RGB(128, 0, 0)),
	INLINE_RESOURCE(new RGB(0, 0, 190)),
	DEFAULT(new RGB(0, 0, 0)),
	TAG(new RGB(0, 0, 128));
	
	private RGB color;
	private DSLColorMapping(RGB color){
		this.color=color;
	}
	/**
	 * Get the {@link RGB} color specification for this color mapping.
	 * @return the {@link RGB} object.
	 */
	public RGB getRGB(){
		return color;
	}
}
