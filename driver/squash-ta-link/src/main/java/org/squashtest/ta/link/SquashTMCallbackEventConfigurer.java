/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.link;

import java.io.File;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.api.execution.client.ClientFactory;
import org.squashtest.ta.api.execution.client.StatusUpdate;
import org.squashtest.ta.framework.facade.Configurer;
import org.squashtest.ta.framework.facade.Engine;


/**
 * {@link Configurer} implementation to configure the handling of status update
 * events for callback to Squash TM through the RESTful execution update
 * services.
 * 
 * @author edegenetais
 * 
 */
public class SquashTMCallbackEventConfigurer implements Configurer {
	private static final Logger LOGGER = LoggerFactory.getLogger(SquashTMCallbackEventConfigurer.class);

	/**
	 * Endpoint URL for the destination service.
	 */
	private URL endpointURL;

	/**
	 * External id attributed to the present automated test execution.
	 */
	private String executionExternalId;

	/**
	 * Name of the job used to serve execution requests.
	 */
	private String jobName;

	/**
	 * Name of the server (server host name would be a good candidate in most
	 * cases, but any unique (in your context) name will do).
	 */
	private String hostName;
	
	/**
	 * Configuration file which contains the mapping callback url / credentials for post
	 */
	private File endpointLoginConfFile; 

	/** For testability */
	private ClientFactory clientFactory=new ClientFactory();
	@Override
	public void apply(Engine engine) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Configuring events for client "+endpointURL+" on execution "+jobName+"#"+executionExternalId+"@"+hostName);
		}
		if(endpointURL==null){
			throw new IllegalArgumentException("The mandatory enpointURL parameter is null");
		}
		StatusUpdate updateService = clientFactory.getClient(endpointURL,endpointLoginConfFile);
		RestTestStatusUpdateListener testStatusListener = new RestTestStatusUpdateListener(
				updateService, hostName, jobName, executionExternalId);
		RestTestSuiteStatusUpdateListener testSuiteStatusListener = new RestTestSuiteStatusUpdateListener(
				updateService, hostName, jobName, executionExternalId);
		engine.addEventListener(testStatusListener);
		engine.addEventListener(testSuiteStatusListener);
	}

	public URL getEndpointURL() {
		return endpointURL;
	}

	public void setEndpointURL(URL endpointURL) {
		this.endpointURL = endpointURL;
	}

	public String getExecutionExternalId() {
		return executionExternalId;
	}

	public void setExecutionExternalId(String executionExternalId) {
		this.executionExternalId = executionExternalId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostName() {
		return hostName;
	}

	public File getEndpointLoginConfFile() {
		return endpointLoginConfFile;
	}

	public void setEndpointLoginConfFile(File endpointLoginConfFile) {
		this.endpointLoginConfFile = endpointLoginConfFile;
	}

}
