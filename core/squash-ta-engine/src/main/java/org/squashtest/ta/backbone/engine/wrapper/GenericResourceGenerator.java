/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.wrapper;

import java.util.Properties;

import org.squashtest.ta.framework.test.result.ResourceGenerator;

public class GenericResourceGenerator implements ResourceGenerator {
	
	private String name;
	private String generatorType;
	private Properties config;

	public String getName() {
		return name;
	}
	
	public String getGeneratorType() {
		return generatorType;
	}

	public Properties getConfiguration() {
		return config;
	}
	
	public GenericResourceGenerator(String name, String generatorType, Properties config){
		this.name=name;
		this.generatorType=generatorType;
		this.config=config;
	}

}
