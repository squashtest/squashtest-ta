/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.factory;

import java.lang.reflect.Method;

import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.legacy.exception.MalformedCatsClassException;

public class LegacyClassTestFactory {
	public Test buildTest(Method method, AbstractCatsTestCase instance){
		if(method.getReturnType()!=Void.TYPE){
			throw new MalformedCatsClassException("Test method "+method.toGenericString()+" should have void return type.");
		}
		if(method.getParameterTypes().length>0){
			throw new MalformedCatsClassException("Test method "+method.toGenericString()+" should take no arguments.");
		}
		throw new UnsupportedOperationException("TODO");
	}
}
