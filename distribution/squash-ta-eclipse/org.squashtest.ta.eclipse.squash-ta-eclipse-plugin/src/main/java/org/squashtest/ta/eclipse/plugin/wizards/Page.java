/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * Base page behavior with error message LIST and global page status management, 
 * and global page validation management.
 * 
 * @author edegenetais
 *
 */
public abstract class Page extends WizardPage {

	private Map<String,String> messages=new HashMap<String, String>();
	
	private List<PageRule> pageRules=new ArrayList<PageRule>();
	
	public Page(String pageName) {
		super(pageName);
	}
	
	public Page(String pageName, String title, ImageDescriptor titleImage) {
		super(pageName, title, titleImage);
	}

	protected Composite createPageUIContainer(Composite parent, int nbColumns) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = nbColumns;
		layout.verticalSpacing = 9;
		return container;
	}

	protected void addLabel(Composite uiContainer, String text) {
		Label label = new Label(uiContainer, SWT.NULL);
		GridData gd= new GridData(GridData.FILL);
		label.setLayoutData(gd);
		label.setText("&"+text+":");
	}

	protected void addLabel(Composite uiContainer, String text, int horizontalSpan) {
		Label label = new Label(uiContainer, SWT.NULL);
		GridData gd= new GridData(GridData.FILL);
		gd.horizontalSpan = horizontalSpan;
		label.setLayoutData(gd);
		label.setText("&"+text+":");
	}
	
	protected Text createInput(Composite uiContainer) {
		Text input=new Text(uiContainer,SWT.BORDER | SWT.SINGLE);
		GridData gd= new GridData(GridData.FILL_HORIZONTAL);
		input.setLayoutData(gd);
		return input;
	}
	
	protected Button createCheck(Composite uiContainer){
		Button checkButton = new Button(uiContainer, SWT.CHECK);
		GridData gd= new GridData(GridData.FILL);
		checkButton.setLayoutData(gd);
		return checkButton;
	}
	
	protected Combo createCombo(Composite uiContainer, String [] listData) {
		Combo readOnlyCombo = new Combo(uiContainer, SWT.READ_ONLY);
		readOnlyCombo.setItems(listData);
		GridData gd= new GridData(GridData.FILL);
		readOnlyCombo.setLayoutData(gd);
		return readOnlyCombo;
	}
	

	protected void addPageRule(PageRule rule){
		pageRules.add(rule);
	}
	
	@Override
	public boolean isPageComplete() {
		boolean isPageComplete=super.isPageComplete();
		if(isPageComplete){
			for(PageRule rule:pageRules){
				if(rule.check()){
					messages.remove(rule.getId());
				}else{
					messages.put(rule.getId(), rule.getMessage());
					isPageComplete=false;					
				}
			}
		}
	return isPageComplete;	}
	
	/**
	 * Update the status of a check rule.
	 * @param ruleId unique id for the rule.
	 * @param message error message.
	 */
	protected void updateStatus(String ruleId,String message) {
		if(message==null){
			messages.remove(ruleId);
		}else{
			messages.put(ruleId, message);
		}
		if(messages.isEmpty()){
			setErrorMessage(null);
			setPageComplete(true);
		}else{
			StringBuilder messageBuilder=new StringBuilder();
			for(String msg:messages.values()){
				messageBuilder.append(msg).append("\n");
			}
			setErrorMessage(messageBuilder.toString());
			setPageComplete(false);
		}
	}

}