/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.filechecker.converters;

import java.io.File;
import java.net.URL;

import org.junit.Test;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;


public class FileToFFFQueriesConverterTest {

	@Test
	public void testConvert() {
		// Successful convert
		URL resourceUrl = Thread.currentThread().getContextClassLoader().getResource("filechecker/xpath/queries/FFF_queries.xml");
		FileResource resource = new FileResource(new File(resourceUrl.getPath()));
		FileToFFFQueriesConverter converters = new FileToFFFQueriesConverter();
		converters.convert(resource);
	}
	
	@Test (expected = InstructionRuntimeException.class)
	public void testConvert2() {
		// Fail with a not existing file
		FileResource resource = new FileResource(new File("notExistingFile.xml"));
		FileToFFFQueriesConverter converters = new FileToFFFQueriesConverter();
		converters.convert(resource);
	}
	
}
