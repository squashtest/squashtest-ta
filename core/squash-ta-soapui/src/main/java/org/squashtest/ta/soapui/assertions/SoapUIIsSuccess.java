/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.soapui.assertions;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.soapui.resources.SoapUIResult;

/**
 * This assertion checks that a Selenium test succeeded. If the assertion fails.
 * The exception thrown contains the names of the failed selenium junit tests
 * @author fgaillard
 *
 */
@EngineComponent("success")
public class SoapUIIsSuccess implements UnaryAssertion<SoapUIResult>{

	private static final Logger LOGGER=LoggerFactory.getLogger(SoapUIIsSuccess.class);
	
	private SoapUIResult soapUIResult;
	
	private static final String LF = System.getProperty("line.separator");
	
	@Override
	public void setActualResult(SoapUIResult actual) {
		this.soapUIResult=actual;
	}

	/**
	 * For this assertion, no configuration is necessary. Any injected resource will be ignored.
	 */
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		if(LOGGER.isWarnEnabled() && configuration.size()>0){
			LOGGER.warn("Ignoring "+configuration.size()+" useless configuration resources (none is expected)");
		}
	}

	@Override
	public void test() throws AssertionFailedException {
		ArrayList<ResourceAndContext> context = new ArrayList<ResourceAndContext>();
		if(soapUIResult.getSoapUITestsStatus() != GeneralStatus.SUCCESS){
			//We retrieve the SOAP UI tests that failed
			List<String> soapUIFailures = soapUIResult.getFailures();
			StringBuffer failedTestNames = new StringBuffer();
			for (String failure : soapUIFailures) {
				failedTestNames.append(failure);
				failedTestNames.append(LF);
			}
			File messageReport = soapUIResult.getExecutionReport();
			FileResource reportResource=new FileResource(messageReport).copy();
			ResourceAndContext reportContext=new ResourceAndContext();
			reportContext.resource=reportResource;
			reportContext.metadata=new ExecutionReportResourceMetadata(SoapUIIsSuccess.class,new Properties(),FileResource.class,"SOAP_UIReport");
			context.add(reportContext);
			throw new AssertionFailedException("SoapUI test failed. Following are the test cases that failed:\n" + failedTestNames.toString(), soapUIResult, context);
		}
	}
}
