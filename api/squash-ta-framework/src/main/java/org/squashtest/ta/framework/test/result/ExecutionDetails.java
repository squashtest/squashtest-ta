/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.framework.test.result;

import java.util.Collection;

import org.squashtest.ta.framework.test.instructions.TestInstruction;

/**
 * An ExecutionDetails is mainly relevant when a given instruction fails. If so an ExecutionDetails will shipped along a failed {@link TestResult}. 
 * 
 * @author bsiri
 *
 */

public interface ExecutionDetails{

	/**
	 * @return the GeneralStatus of the instruction when the test halted.
	 */
	GeneralStatus getStatus();
	
	/** 
	 * @return a short description of the instruction.
	 */
	InstructionType instructionType();
	
	/**
	 * 
	 * @return the index of the instruction within its own phase + 1;
	 */
	int instructionNumberInPhase();
	
	/** 
	 * @return the absolute index of the instruction + 1;
	 */
	int instructionNumber();
	
	/**
	 * @return the textual representation of the Instruction (see {@link TestInstruction})
	 */
	String instructionAsText();
	
	/**
	 * 
	 * @return the exception that triggered the instruction failure, or null if everything went fine.
	 */
	Exception caughtException();
		
	/**
	 * When the GlobalStatus is ERROR or FAILURE, the resources used by the instruction will be supplied here, along with some metadata.
	 * When the GlobalStatus is SUCCESS or NOT_RUN, the list is empty.
	 * 
	 *  @return
	 */
	Collection<ResourceAndContext> getResourcesAndContext();
}
