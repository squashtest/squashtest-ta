/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java

import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.TempUnzipper;

import spock.lang.Specification;

class JavaCodeBundleClassLoaderTest extends Specification{
	def testee
	def sourceDir
	
	def setup(){
		sourceDir=new TempUnzipper().unzipInTemp(getClass().getResourceAsStream("compiledClasses.zip"))
		testee=new JavaCodeBundleClassLoader(new File(sourceDir,"compiledClasses"))
	}
	
	def cleanup(){
		new TempUnzipper().clean(sourceDir)
	}
	
	def "must load our classes properly"(){
		when:
			def classObject=testee.loadClass("com.example.subpackage.Source3")
		then:
			notThrown(ClassNotFoundException)
			"com.example.subpackage.Source3".equals(classObject.getName())
	}
	
	def "class loading must be isolated from main application"(){
		given:
			def controlClassObject
			try{
				controlClassObject=getClass().getClassLoader().loadClass("com.example.subpackage.Source3")
			}catch(ClassNotFoundException e){
				controlClassObject=null
			}
		when:
			def classObject=testee.loadClass("com.example.subpackage.Source3")
		then:
			notThrown(ClassNotFoundException)
			!classObject.equals(controlClassObject)
	}
	
	def "class loading must be isolated between instances"(){
		given:
			def secondInstance=new JavaCodeBundleClassLoader(new File(sourceDir,"compiledClasses"))
		and:
			def secondClassObject=secondInstance.loadClass("com.example.subpackage.Source3")
		when:
			def classObject=testee.loadClass("com.example.subpackage.Source3")
		then:
			notThrown(ClassNotFoundException)
			!classObject.equals(secondClassObject)
	}
	
	def "when looking for a resource, should find it if root package of the bundle"(){
		when:
			def resourceURL=testee.getResource("/noise.Tofilter")
		then:
			resourceURL!=null
			BinaryData data=new BinaryData(resourceURL)
			"base of package".equals(new String(data.toByteArray()))
	}
	
	def "when looking for a resource, should find it if subpackage of the bundle"(){
		when:
			def resourceURL=testee.getResource("/com/noise.Tofilter")
		then:
			resourceURL!=null
			BinaryData data=new BinaryData(resourceURL)
			"subpackage".equals(new String(data.toByteArray()))
	}
}
