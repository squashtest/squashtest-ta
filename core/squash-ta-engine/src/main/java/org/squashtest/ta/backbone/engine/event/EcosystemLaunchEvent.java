/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.event;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;

public class EcosystemLaunchEvent extends
		EcosystemStatusUpdateBase {
	
	private String ecosystemName;
	private int nbTests;
	
	public EcosystemLaunchEvent(Ecosystem ecosystem) {
		ecosystemName=ecosystem.getName();
		nbTests=ecosystem.getTestPopulation().size();
	}
	
	@Override
	public EcosystemResult getPayload() {
		return new LaunchResult();
	}

	@Override
	public void addContext(ContextSource manager) {}
	
	private final class LaunchResult implements EcosystemResult {
		private Date startTime=new Date();

		@Override
		public Date startTime() {
			return startTime;
		}

		@Override
		public GeneralStatus getStatus() {
			return GeneralStatus.RUNNING;
		}

		@Override
		public String getName() {
			return ecosystemName;
		}

		@Override
		public Date endTime() {
			return null;
		}

		@Override
		public void cleanUp() {/*cleanup what?*/}

		@Override
		public int getTotalWarning() {
			return 0;
		}

		@Override
		public int getTotalTests() {
			return nbTests;
		}

		@Override
		public int getTotalSuccess() {
			return 0;
		}

		@Override
		public int getTotalPassed() {
			return 0;
		}

		@Override
		public int getTotalNotRun() {
			return 0;
		}

		@Override
		public int getTotalNotPassed() {
			return 0;
		}

		@Override
		public int getTotalFailures() {
			return 0;
		}

		@Override
		public int getTotalErrors() {
			return 0;
		}

		@Override
		public List<? extends TestResult> getSubpartResults() {
			return Collections.emptyList();
		}

		@Override
		public TestResult getSetupResult() {
			return null;
		}
		
		@Override
		public TestResult getTearDownResult() {
			return null;
		}
	}
}