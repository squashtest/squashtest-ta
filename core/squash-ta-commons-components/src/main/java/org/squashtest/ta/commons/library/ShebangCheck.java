/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Checks that a given file wears a given shebang. The shebang is defined as an
 * initial <code>#!<id></code> line, where <code><id></code> is a free string
 * identifier.
 * 
 * @author edegenetais
 * 
 */
public class ShebangCheck {
	
	private static final String SHEBANG_MARK = "#!";

	private static final Logger LOGGER=LoggerFactory.getLogger(ShebangCheck.class);
	
	/** Content of the target shebang (without the #! prefix). */
	private String shebang;
	
	/**
	 * @param shebang the shebang to check.
	 */
	public ShebangCheck(String shebang){
		if(shebang==null){
			throw new IllegalArgumentException("shebang id cannot be null.");
		}
		this.shebang=shebang;
	}
	
	/**
	 * Reads the file to look the shebang up.
	 * @param fileLocation url of the target file.
	 * @return <code>true</code> if the file wears your shebang, <code>false</code> if not.
	 * @throws IOException if the configuration URL cannot be opened.
	 */
	public boolean hasShebang(URL fileLocation) throws IOException{
		InputStream readingStream = fileLocation.openStream();
		boolean hasShebang = false;
		BufferedReader br = new BufferedReader(new InputStreamReader(
				readingStream));
		try {
			String line=br.readLine();
			if(line!=null && line.startsWith(SHEBANG_MARK)){
				hasShebang=shebang.equals(line.substring(SHEBANG_MARK.length()));
			}
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				LOGGER.warn(
						"Error while closing reading stream after shebang check",
						e);
			}
		}
		return hasShebang;
	}
}
