/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.legacy.factory.LegacyClassTestSuiteFactory;
import org.squashtest.ta.legacy.init.LegacyTestClassesLoader;
import org.squashtest.ta.legacy.init.LegacyWorkspaceBrowser;

/**
 * This mojo is a maven way to start the squash TA engine to execute legacy
 * (CATS) tests. The mojo itself will not compile the test classes (please use
 * the maven test compiler mojo for this), but will trigger the following
 * process:
 * 
 * @author edegenetais
 * 
 * @goal legacy-run
 * @requiresDependencyResolution test
 * @phase integration-test
 */
public class LegacySquashTAMojo extends AbstractSquashTaMojo {

	

	/**
	 * Where to find test classes.
	 * 
	 * @parameter expression="${project.build.testOutputDirectory}"
	 */
	private File testClassRepository;
	
	
	
	/**
	 * @parameter expression="${project.build.directory}"
	 */
	private File buildDirectory;
	
	/**
	 * @parameter expression="${project.name}"
	 */
	private String projectName;
	
	private void init() {
		if(getOutputDirectory()==null){
			setOutputDirectory(new File(buildDirectory,"/squashTA"));
		}
	}

	@Override
	public SuiteResult executeImpl() throws MojoExecutionException, MojoFailureException {
		init();
		try {
			Set<Class<AbstractCatsTestCase>> testClasses = lookupCatTestClasses();
			if(getLogger().isDebugEnabled()){
				getLogger().debug("Found test classes:" + testClasses);
			}
			
			if(getOutputDirectory().exists()){
				new FileTree().clean(getOutputDirectory());
			}
			getOutputDirectory().mkdirs();
			
			getLogger().info("Translating test classes");
			LegacyClassTestSuiteFactory factory=new LegacyClassTestSuiteFactory(projectName);
			TestSuite suite=factory.buildTestSuite(testClasses);
			
			getLogger().info("Executing tests");
			TestWorkspaceBrowser browser=new LegacyWorkspaceBrowser(this.testClassRepository);
			Engine engine=new EngineLoader().load();
			SuiteResult result=engine.execute(suite, browser);
			
			return result;
		} catch (MalformedURLException e) {
			throw new MojoExecutionException("Test classes loading failed: test compilation directory unreadable.", e);
		} catch (IOException e) {
			throw new MojoExecutionException("Failed report writing", e);
		}
	}

	private Set<Class<AbstractCatsTestCase>> lookupCatTestClasses() throws MojoExecutionException,
			MalformedURLException {
		getLogger().info("Looking up CATS legacy test classes.");

		Set<String> testPackageList = buildTestPackageList();

		//we need to add the test output directory to the classpath.
		getLogger().debug("Test classes compiled in the following location: "+testClassRepository);
		URL testClassesURL = testClassRepository.toURI().toURL();
		URLClassLoader cl = new URLClassLoader(new URL[] { testClassesURL },getClass().getClassLoader());
		Thread.currentThread().setContextClassLoader(cl);
		
		LegacyTestClassesLoader testLoader = new LegacyTestClassesLoader();

		Set<Class<AbstractCatsTestCase>> testClasses = testLoader
				.enumerateTestClasses(testPackageList);
		
		if(getLogger().isInfoEnabled()){
			getLogger().info("Found "+testClasses.size()+" test classes.");
		}
		return testClasses;
	}

	private Set<String> buildTestPackageList() throws MojoExecutionException {
		Set<String> testPackageList = new HashSet<String>();
		try {
			FileTree testClassTree = new FileTree();
			List<File> testClassDirectories = testClassTree.enumerate(
					testClassRepository, EnumerationMode.DIRECTORIES_ONLY);
			String canonicalTestClassesDirectory = testClassRepository
					.getCanonicalPath();
			int prefixLength = canonicalTestClassesDirectory.length();
			for (File packageDir : testClassDirectories) {
				if (packageDir.listFiles(new FileFilter() {

					@Override
					public boolean accept(File pathname) {
						return pathname.isFile();
					}
				}).length > 0) {
					String canonicalPackageDirectory = packageDir
							.getCanonicalPath();
					if (canonicalPackageDirectory
							.startsWith(canonicalTestClassesDirectory)) {
						String packageSubDir = canonicalPackageDirectory
								.substring(prefixLength + 1);
						String packageName = packageSubDir.replaceAll(
								"[/,\\\\]", "\\.");
						testPackageList.add(packageName);
					} else {
						throw new MojoExecutionException(
								"Legacy CATS test classes enumeration failed because of canonical root '"
										+ canonicalTestClassesDirectory
										+ "' and package path '"
										+ canonicalPackageDirectory
										+ "' mismatch, please report this to the development team.");
					}
				}
			}
		} catch (IOException ioe) {
			throw new MojoExecutionException(
					"Legacy CATS test classes enumeration failed.", ioe);
		}
		return testPackageList;
	}

	public void setTestClasses(File testClasses) {
		this.testClassRepository = testClasses;
	}

}
