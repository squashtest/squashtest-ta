/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.main;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

import org.squashtest.installer.tools.FileUtils;
import org.squashtest.installer.command.CommandRunner;
import org.squashtest.installer.context.Context;
import org.squashtest.installer.tools.FileLocatorUtils;
import org.squashtest.installer.tools.PlaceHolderFiller;
import org.squashtest.installer.tools.ZipUtils;

/**
 * <p>Will prepare a TA-oriented Eclipse workspace with preconfigured user preferences</p>
 * <p>arguments :</p>
 * 	<ul>
 * 		<li>-ta-directory : the home directory of Squash TA.</li>
 * 		<li>-dest : the place where the workspace will be installed.</li>
 * 	</ul>
 * 
 * <p>The process is the following :</p>
 * 
 * <ol>
 * 	<li>install the workspace skeleton</li>
 * 	<li>install a temporary working directory</li>
 * 	<li>setup of the user prefs template (replace placeholders)</li>
 * 	<li>call the import preference IBM Ant task (the plugin must be installed in the /plugins directory at Eclipse's location</li>
 * 	<li>cleanup</li>
 * </ol>
 * 
 * 
 * 
 * <p>Note that it uses an important classpath resource : the workspace_template. It's an empty eclipse workspace.
 * Due to build constraints, the .metadata folder had to be rename 'metadata' (the dot disappeared). So as part of 
 * the installation process the class have to rename it. Make sure that both statements are true.</p> 
 * 
 * 
 * @author bsiri
 *
 */

public class WorkspaceInstaller {

	Context context;
	
	FileLocatorUtils locatorUtils;
	PlaceHolderFiller filler;

	
	public WorkspaceInstaller(Context context) throws IOException, URISyntaxException{
		
		this.context=context;
		locatorUtils = new FileLocatorUtils(context);
		filler = new PlaceHolderFiller(context);
	}
	
	/*
	 * Stage 1  : install the eclipse workspace skeletton at the specified location.
	 * Be Aware that it needs a renaming operation for silly reasons, see the class javadoc for why.
	 * 
	 */
	public void installTemplate() throws URISyntaxException, IOException{
		//blunt copy of the resource		
		InputStream stream = locatorUtils.getStreamFromJar(context.getProperty("WORKSPACE_TEMPLATE_NAME"));
		ZipUtils.copyDirFromStream(stream, context.getOutputDirectory());
	}
	
	/*
	 * stage 2 : install a working directory.
	 */
	public void installWorkingDirectory() throws IOException, URISyntaxException{
		
		//create the temporary directory
		File workDirectory = mkTempDirectory();
		context.setWorkingDirectory(workDirectory);
		
		//get the resources from classpath
		InputStream inStream = locatorUtils.getStreamFromJar(context.getProperty("IMPORT_DATA_DIR"));
		ZipUtils.copyDirFromStream(inStream, context.getWorkingDirectory());
	}

	/*
	 * stage 3 : replace the placeholders.
	 */
	public void configurePreferences() throws IOException{
		//configure the preferences
		configurePlaceHolderFiller(context.getProperty("USER_PREFERENCES_TEMPLATE"), 
									context.getProperty("CONFIGURED_USER_PREFERENCES"));
		filler.process();
		
		//configure the ant build
		configurePlaceHolderFiller(context.getProperty("ANT_BUILD_TEMPLATE"), 
									context.getProperty("CONFIGURED_ANT_BUILD"));
		filler.process();
	
	}
	
	/*
	 * stage 4 : import the preferences 
	 */
	public void importPreferences() throws URISyntaxException, IOException{		

		installAdditionalResources();
		
		InstallCommandFactory commandFactory = new InstallCommandFactory(context);
		CommandRunner runner = commandFactory.makeRunner();
		runner.runCommands();

			
	}
	
	
	//todo : move it as a Command like in stage 4 too ?
	private void installAdditionalResources() throws URISyntaxException, IOException{
		File pluginDir = new File(locatorUtils.findEclipseDirectory(), "plugins/");
		File jarFile = new File(context.getWorkingDirectory(), context.getProperty("PLUGIN_JAR_NAME"));
		
		File jarInPluginDir = new File(pluginDir, jarFile.getName());
		
		if (! jarInPluginDir.exists()){
			FileUtils.copyFile(jarFile, jarInPluginDir);
		}
		
	}
	

	
	/* a temp file is not a temp directory, so we must perform the following : */
	private File mkTempDirectory() throws IOException{
		
		File temp = File.createTempFile("ta_install", "temp");
		
		boolean deleteSuccess=temp.delete();
		boolean mkdirSuccess=temp.mkdir();
		
		if (! (deleteSuccess && mkdirSuccess)){
			throw new RuntimeException("squash-ta-workspace-installer : could not create temporary directory.");
		}
		
		return temp;
		
	}
	
	private void configurePlaceHolderFiller(String templateName, String outputName) throws IOException{
		File template = new File(context.getWorkingDirectory(), templateName);
		File output = new File(context.getWorkingDirectory(), outputName);
		output.createNewFile();
		
		filler.setInputFile(template);
		filler.setOutputFile(output);
		
	}
	
	public static void main(String... args){
		/*
		 * 
		 * todo :
		 * 
		 * parse args
		 * instantiate the generator
		 * install template
		 * create working directory
		 * replace the placeholders in the ta_user_profile_template
		 * call the migrator invoker
		 * cleanup
		 * 
		 */


		try{
			
			String taPath = getOpt("-ta-directory", args);
			String userPath = getOpt("-dest", args);
			
			if (taPath==null || userPath == null){
				printUsage();
				System.exit(0); //we can go harsh here since the app hadn't started yet.
			}
			
			Context context = initContext(taPath, userPath);
			if (context.getOutputDirectory().exists()){
				System.out.println("TA workspace installer : warning : the destination directory alreayd exists. Some problems may arise because of this. In case you experience troubles" +
						"later on try to remove that directory and try again.");
			}
			
			WorkspaceInstaller installer = new WorkspaceInstaller(context);
			
			installer.installTemplate();
			installer.installWorkingDirectory();
			installer.configurePreferences();
			installer.importPreferences();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	/*
	 * parses the command line, looking for options. It will return it's value if found, null if not found. Wont crash
	 * when random string are passed along the arg line as long as a valid options always are paired with their value.
	 * 
	 * @param option the option we look for 
	 * @param args the command line 
	 * @return the value if found, null if not found
	 * @throws IllegalArgumentException if an option was found but its value was not.
	 */
	private static String getOpt(String option, String... args){
		Iterator<String> iter = Arrays.asList(args).iterator();
		
		while(iter.hasNext()){
			String buffer = iter.next();
			if (buffer.equals(option)){
				if (!iter.hasNext()){
					throw new IllegalArgumentException("option "+option+" has incorrect value");
				}else{
					return iter.next();
				}
			}
		}
		
		return null;
		
	}
	
	private static Context initContext(String squashPath, String outputPath) throws IOException, URISyntaxException{
		Properties properties = new Properties();
		InputStream stream = WorkspaceInstaller.class.getClassLoader().getResourceAsStream("org.squashtest.installer.filenames.properties");
		properties.load(stream);
		
		Context context = new Context();		
		context.setSquashDirectory(new File(squashPath));
		context.setOutputDirectory(new File(outputPath));
		context.setProperties(properties);
		
		return context;
	}
	
	private static void printUsage(){
		System.out.println("usage : java -jar org.squashtest.installer.main.WorkspaceInstaller -ta-directory <ta installation path> -dest <output path>");
		System.out.println("info : the output directory will be created if it doesn't exist already");
		System.out.println("tips : do not forget the quotes (\"\") if specified paths contains spaces");		
	}
	
}
