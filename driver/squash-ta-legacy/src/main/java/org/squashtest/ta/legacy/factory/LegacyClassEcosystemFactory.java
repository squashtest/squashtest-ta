/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.factory;

import java.lang.reflect.Method;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.Environment;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.legacy.exception.MalformedCatsClassException;

/**
 * Ecosystem factory based on CATS test case class analysis.Please note that
 * this object is stateful, and is therefore <strong>NOT thread-safe</strong>.
 * 
 * @author edegenetais
 * 
 */
public class LegacyClassEcosystemFactory {
	private LegacyClassTestFactory factory=new LegacyClassTestFactory();
	/**
	 * Build an ecosystem equivalent to the CATS test class.
	 * @param testClass the test class to translate.
	 * @return the ecosystem that executes as the test class would have.
	 */
	public Ecosystem buildEcosystem(Class<AbstractCatsTestCase> testClass){
		try {
			Ecosystem ecosystem=new Ecosystem();
			ecosystem.setName(testClass.getName());
			
			Method[] methods=testClass.getMethods();
			AbstractCatsTestCase testCase=testClass.newInstance();
			
			Test setup=new Test();
			setup.setName("ecosystemSetup");
			Test teardown=new Test();
			teardown.setName("ecosystemTearDown");
			for(Method method:methods){
				if(method.isAnnotationPresent(org.junit.Test.class)){
					//test methods become ecosystem tests
					Test test=factory.buildTest(method, testCase);
					ecosystem.getTestPopulation().add(test);
				}else if(method.isAnnotationPresent(BeforeClass.class)){
					//Before class methods go into ecosystem setup
					Test setupStep=factory.buildTest(method, testCase);
					setup.addToTests(setupStep.getTests());
				}else if(method.isAnnotationPresent(AfterClass.class)){
					//After class methods go into ecosystem setup
					Test tearDownStep=factory.buildTest(method, testCase);
					teardown.addToTests(tearDownStep.getTests());
				}
			}
			
			Environment environment = new Environment();
			environment.setSetUp(setup);
			environment.setTearDown(teardown);
			ecosystem.setEnvironment(environment);
			
			return ecosystem;
			
		} catch (InstantiationException e) {
			throw new BrokenTestException("Test class instance could not be created", e);
		} catch (IllegalAccessException e) {
			throw new MalformedCatsClassException("Test class "+testClass.getName()+" should have a public noarg constructor.",e);
		}
	}
}
