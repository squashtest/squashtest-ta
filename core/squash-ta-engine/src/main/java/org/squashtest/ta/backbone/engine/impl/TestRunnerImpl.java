/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.InstructionFlowStrategy;
import org.squashtest.ta.backbone.engine.TestRunner;
import org.squashtest.ta.backbone.engine.event.TestCompletionEvent;
import org.squashtest.ta.backbone.engine.event.TestStatusUpdateBase;
import org.squashtest.ta.backbone.engine.wrapper.LocalContext;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.test.DefaultTestResult;
import org.squashtest.ta.backbone.tools.ElementUtils;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.Phase;
import org.squashtest.ta.framework.test.result.TestResult;

public class TestRunnerImpl implements TestRunner, InternalTestRunner{

	private static final Logger LOGGER = LoggerFactory.getLogger(TestRunnerImpl.class);

	private static final ElementUtils ELEMENT_UTILS=new ElementUtils();
	
	private ContextManager manager;
	private Test test;
	private InstructionRunnerFactory runnerFactory;
	
	private LocalContext localContext;
	
	private Map<ResourceName,ResourceWrapper> ecosystemResources;
	
	private Map<ResourceName, ResourceWrapper> testResources = new HashMap<ResourceName, ResourceWrapper>();
	
	private InstructionFlowStrategy setupExecStrategy = new DefaultSetupExecutionStrategy();
	
	private InstructionFlowStrategy testExecStrategy = new DefaultTestExecutionStrategy();
	
	private InstructionFlowStrategy tearDownExecStrategy = new DefaultTeardownExecutionStrategy();
	
	public void setContextManager(ContextManager manager) {
		this.manager=manager;
	}
	
	public void setInstructionRunnerFactory(InstructionRunnerFactory factory){
		runnerFactory = factory;
	}

	public void setTest(Test test) {
		this.test=test;
	}
	
	public Test getTest(){
		return test;
	}

	
	public TestResult runTest() {
		
		try{
			
		ecosystemResources=manager.getEcosystemResources();
		
		LOGGER.debug("Ecosystem resources engaged.");
		
		Date startTime = new Date();
		
		DefaultTestResult result=new DefaultTestResult();
		result.setName(test.getName());
		
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Beginning execution of test "+test.getName());
		}
		
		manager.postEvent(new TestLaunchEvent(test.getName()));
		
		runPhase(result, test.getSetup(), Phase.SETUP, setupExecStrategy);
		
		runPhase(result, test.getTests(), Phase.TEST, testExecStrategy);
		
		runPhase(result, test.getTeardown(), Phase.TEARDOWN, tearDownExecStrategy);
		

		Date endTime = new Date();
		
		result.setStartTime(startTime);
		result.setEndTime(endTime);
		
		LOGGER.debug("Test execution complete.");
		
		cleanUp();
		
		LOGGER.debug("Test context cleanup complete.");
		
		manager.postEvent(new TestCompletionEvent(result));
		
		return result;
		
		}finally{
			ecosystemResources=null;
			LOGGER.debug("Ecosystem resources released.");
		}
	}

	private void runPhase(DefaultTestResult result,
			Iterator<TestInstruction> iterator, Phase phase, InstructionFlowStrategy instructionFlowStrategy) {
		boolean iterate = instructionFlowStrategy.beginGroupExecution(result
				.getStatus()) && iterator.hasNext();
		while (iterate) {

			TestInstruction currentInstruction = iterator.next();
			InstructionRunner runner = runnerFactory
					.getRunner(currentInstruction);
			runner.setTestRunner(this);

			ExecutionDetails executionDetails = runner.run();

			GeneralStatus newStatus=instructionFlowStrategy.aggregate(result.getStatus(), executionDetails.getStatus());
			
			if(instructionFlowStrategy.continueExecution(executionDetails.getStatus())){
				result.updateWith(executionDetails, newStatus);
				iterate=iterator.hasNext();
			}else{
				Integer localContextId=(localContext==null)?null:localContext.getContextIdentifier();
				result.fail(executionDetails, newStatus, localContextId, phase);
				iterate=false;
			}
			
		}
	}


	/* ************************ InternalTestRunner impl *********************/
	

	public ResourceWrapper getResourceFromCache(ResourceName resourceName) {
		ResourceWrapper resource;
		switch(resourceName.getScope()){
		case SCOPE_ECOSYSTEM:
			resource=ecosystemResources.get(resourceName);
			break;
		case SCOPE_TEST:
			resource=testResources.get(resourceName);
			break;
		case SCOPE_TEMPORARY:
			resource=localContext.getResource(resourceName);
			break;
		default:
			//just for an easier diagnosis in case we forget something in the future...
			throw new UnsupportedOperationException("The "+resourceName.getScope()+" scope is not supported");
		}
		return resource;
	}

	public void addResourceToCache(ResourceWrapper resource) {
		switch(resource.getName().getScope()){
		case SCOPE_ECOSYSTEM:
			ELEMENT_UTILS.checkUniqueness(ecosystemResources.keySet(), resource.getName());
			try{
				ecosystemResources.put(resource.getName(), resource);
			}catch(UnsupportedOperationException e){
				throw new BrokenTestException("Detected attempt to write to the ecosystem scope during ecosystem Run Tests Phase.", e);
			}
			break;
		case SCOPE_TEST:
			ELEMENT_UTILS.checkUniqueness(testResources.keySet(), resource.getName());
			testResources.put(resource.getName(), resource);
			break;
		case SCOPE_TEMPORARY:
			if(localContext==null){
				throw new BrokenTestException("Using a temporary storage before the first context initialisation.");
			}
			localContext.addResource(resource);
			break;
		default:
			//just for an easier diagnosis in case we forget something in the future...
			throw new UnsupportedOperationException("The "+resource.getName().getScope()+" scope is not supported");
		}
	}

	public ContextManager getContextManager() {
		return manager;
	}
	
	@Override
	public LocalContext getLocalContext() {
		return localContext;
	}

	@Override
	public void setLocalContext(LocalContext context) {
		localContext=context;
	}
	
	/* ************************* private stuffs **************************** */
	
	private void cleanUp(){
		for (ResourceWrapper resource : testResources.values()){
			resource.cleanUp();
		}
		testResources.clear();
		if(localContext!=null){
			localContext.cleanUp();
		}
	}

	private static class TestLaunchEvent extends TestStatusUpdateBase {
		
		public TestLaunchEvent(String testName) {
			this.testName=testName;
		}
		
		private final class RunningTestResult implements TestResult {
			private Date startTime=new Date();

			@Override
			public Date startTime() {
				return startTime;
			}

			@Override
			public GeneralStatus getStatus() {
				return GeneralStatus.RUNNING;
			}

			@Override
			public String getName() {
				return testName;
			}

			@Override
			public Date endTime() {
				return null;
			}

			@Override
			public void cleanUp() {/*clean what?*/}

			@Override
			public int getTotalInstructions() {
				return 0;
			}

			@Override
			public int getTotalCommands() {
				return 0;
			}

			@Override
			public int getTotalAssertions() {
				return 0;
			}

			@Override
			public ExecutionDetails getFailureReport() {
				return null;
			}
		}
		private String ecosystemName;
		private String testName;
		@Override
		public TestResult getPayload() {
			return new RunningTestResult();
		}
		@Override
		public String getEcosystemName() {
			return ecosystemName;
		}
		public void setEcosystemName(String ecosystemName) {
			this.ecosystemName = ecosystemName;
		}
	}
}
