/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.shell;

/**
 * Checks that the exit code from the {@link ShellResult} is as expected.
 * 
 * @author edegenetais
 * 
 */
public class ExitCodeCheck {
	/** success exit code is 0 */
	private static final int SUCCESS = 0;
	/** expected exit code */
	private int expected;

	/**
	 * Default check=success check.
	 */
	public ExitCodeCheck() {
		this.expected = SUCCESS;
	}

	/**
	 * Create a check for an expected non-zero exit code (for error scenarios).
	 * 
	 * @param expectedCode
	 */
	public ExitCodeCheck(int expectedCode) {
		this.expected = expectedCode;
	}

	/**
	 * Checks the actual exit code from the shell execution result.
	 * @param actual the shell execution result.
	 * @return <code>true</code> if the exit code is as expected, <code>false</code> if it is different.
	 */
	public boolean check(ShellResult actual){
		return (actual.getExitValue()==expected);
	}
}
