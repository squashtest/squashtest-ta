/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.init;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.squashtest.ta.backbone.exception.EngineInitException;
import org.squashtest.ta.core.tools.io.BinaryData;

public abstract class ScannerComponentLoader {

	public static final String TEMPLATE_ENCODING = "UTF-8";

	private static final String EXCLUDE_ENTRY_TAIL_STRING = "\"/>";
	private static final String EXCLUDE_ENTRY_STUB = "<context:exclude-filter type=\"regex\" expression=\"";
	private static final String END_VARNAME = "\\}";
	private static final String BEGIN_VARNAME = "\\$\\{";
	private static final String EXCLUDE_FRAGMENT_VARNAME = BEGIN_VARNAME+"excludeFilterFragment"+END_VARNAME;
	private static final String BASE_PACKAGE_VARNAME = BEGIN_VARNAME+"basePackageList"+END_VARNAME;
	
	
	/**
	 * Context factory: this is used as a means to decouple the loader and the actual spring context class during unit testing.
	 */
	static class ContextFactory{
		public ApplicationContext createContext(final Resource configurationData){
			return new GenericXmlApplicationContext(configurationData);
		}
	}
	protected ContextFactory factory = new ContextFactory();

	public ScannerComponentLoader() {
		super();
	}

	/**
	 * Defines a specialized warning message for template input stream closing exceptions.
	 * @return
	 */
	protected abstract String getTemplateCloseErrorMsg();

	/**
	 * Utility method to change a list of strings into a comma separated list in a {@link StringBuilder}.
	 * @param targetStringBuilder target builder.
	 * @param stringSet value set.
	 */
	protected void setToList(StringBuilder targetStringBuilder, Set<String> stringSet) {
		for(String basePackageName:stringSet){
			targetStringBuilder.append(basePackageName).append(",");
		}
		if(targetStringBuilder.length()>0){
			targetStringBuilder.setLength(targetStringBuilder.length()-1);
		}
	}

	/**
	 * Method to generate a configuration from a template and variable values.
	 * @param configurationTemplate the template <strong>contents</strong>.
	 * @param basePackages list of base packages to insert.
	 * @param excludedElements list of excluded elements to insert.
	 * @return the configuration contents as a string.
	 */
	protected String generateConfigurationFromTemplate(String configurationTemplate,
			Set<String> basePackages, Set<String> excludedElements) {
				StringBuilder variableValue=new StringBuilder();
				
				setToList(variableValue, basePackages);
				String configurationString=configurationTemplate.replaceAll(BASE_PACKAGE_VARNAME, variableValue.toString());
				variableValue.setLength(0);
				
				for(String excludedRegex:excludedElements){
					variableValue.append(EXCLUDE_ENTRY_STUB).append(excludedRegex).append(EXCLUDE_ENTRY_TAIL_STRING);
					variableValue.append("\n");
				}
				if(variableValue.length()>0){
					variableValue.setLength(variableValue.length()-1);
				}		
				configurationString=configurationString.replaceAll(EXCLUDE_FRAGMENT_VARNAME, variableValue.toString());
				return configurationString;
			}

	/**
	 * Loads the targeted configuration context.
	 * @param templateResourceName name of the classpath resource for the template.
	 * @return the configuration template as a string.
	 */
	protected String loadConfigurationTemplate(String templateResourceName) {
		URL configurationTemplateURL = getClass().getResource(
				templateResourceName);
		if(configurationTemplateURL==null){
			throw new EngineInitException(templateResourceName+" not found");
		}
		
		String configurationTemplate;
		try {
			BinaryData templateResource=new BinaryData(configurationTemplateURL,getTemplateCloseErrorMsg());
			configurationTemplate=new String(templateResource.toByteArray(),TEMPLATE_ENCODING);
		} catch (IOException ioe) {
			throw new EngineInitException(
					"Failed to load configuration template.", ioe);
		}
		return configurationTemplate;
	}

	/**
	 * Loads the given configuration into a spring context.
	 * @param configurationString the configuration definition XML as a string.
	 * @param computedConfigurationResourceName The resource name to use for this configuration.
	 * @return the loaded Spring application context.
	 * @throws UnsupportedEncodingException
	 */
	protected ApplicationContext loadGeneratedConfiguration(String configurationString, String computedConfigurationResourceName)
			throws UnsupportedEncodingException {
				ByteArrayResource configurationResource=new ByteArrayResource(configurationString.getBytes(TEMPLATE_ENCODING),computedConfigurationResourceName);
				return factory.createContext(configurationResource);
			}

}