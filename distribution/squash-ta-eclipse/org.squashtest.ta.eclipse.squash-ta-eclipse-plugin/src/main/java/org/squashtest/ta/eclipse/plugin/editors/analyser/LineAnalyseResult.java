/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.editors.analyser;

import java.util.ArrayList;
import java.util.List;

import org.squashtest.ta.eclipse.plugin.model.DSLClause;
import org.squashtest.ta.eclipse.plugin.model.DSLHeadClause;
import org.squashtest.ta.eclipse.plugin.model.DSLInstruction;
import org.squashtest.ta.eclipse.plugin.model.TaInstructionFactory;

public class LineAnalyseResult {
	
	private static final int FIRST_ELEMENT = 0;
	
	private LineIdentifier line;
	private int initialOffset;
	
	private int elementIndex=-1;
	List<LineElement> lineElementList = new ArrayList<LineElement>();
	private String searchedStartingString = "";
	private boolean isSearchedAtTheBeginning;
	private DSLHeadClause headClause;
	private boolean isFirstElementHeadKeyWord;
	private boolean isSearchedPartOfFirstElement;
	private List<DSLClause> possibleClauseList = new ArrayList<DSLClause>();
	
	
	
	/**
	 * Partial constructor
	 *  
	 * @param line The {@link LineIdentifier} corresponding to the analyzed line 
	 * @param initialOffset The cursor offset at the beginning of the analyze
	 */
	public LineAnalyseResult(LineIdentifier line, int initialOffset, List<LineElement> elements) {
		super();
		this.line = line;
		this.initialOffset = initialOffset;
		this.lineElementList = elements;
		compute();
	}

	private void compute() {
		// set is search at the beginning of the line && set is search part of first element 
		if(lineElementList.size()>0){
			LineElement elt = lineElementList.get(FIRST_ELEMENT);
			if(elt.contains(initialOffset)){
				elementIndex = 1;
				isSearchedPartOfFirstElement = true;
				if(elt.getBeginOffset()==line.getBeginOffset()){
					isSearchedAtTheBeginning=true;
				}
				setSearched(elt);
			}
			headClause = DSLHeadClause.get(elt.getString());
			if(headClause!= null){
				isFirstElementHeadKeyWord = true;
			}
		} else {
			isSearchedPartOfFirstElement=true;
			if(line.getBeginOffset() == initialOffset  ){
				isSearchedAtTheBeginning=true;
			
			}
		} 
		
		
		
		// We start at index = 1 as index = 0 has already be treated.
		if(headClause!=null){
			DSLInstruction instruction = TaInstructionFactory.getInstruction(headClause);
			possibleClauseList = new ArrayList<DSLClause>(instruction.getPossibleClause());
			DSLClause dslClause;
			for (int index = 1; index < lineElementList.size(); index++) {
				LineElement lineElement = lineElementList.get(index);
				if(lineElement.contains(initialOffset)){
					setSearched(lineElement);
					elementIndex = index;
				}else{
					dslClause = DSLClause.get(lineElement.getString());
					if(dslClause!=null ){
						possibleClauseList.remove(dslClause);
					}
				}
			}
		}
	}
	
	private void setSearched(LineElement elt){
		searchedStartingString = elt.getString().substring(0,initialOffset-elt.getBeginOffset());
	}

	/**
	 * This method return the identifier of the analysed line.  
	 * 
	 * @return The {@link LineIdentifier}
	 */
	public LineIdentifier getLine() {
		return line;
	}
	
	/**
	 * This method return the offset of the cursor position at the begin of the analyze.
	 * 
	 * @return The initial cursor offset position
	 */
	public int getInitialOffSet() {
		return initialOffset;
	}
	
	public String getSearched() {
		return searchedStartingString;
	}
	
	public boolean isSearchAtTheBeginning() {
		
		return isSearchedAtTheBeginning;
	}
	
	public boolean isSearchedPartOfFirstElement() {
		return isSearchedPartOfFirstElement; 
	}
	
	public boolean isFirstElementHeadKeyWord() {
		return isFirstElementHeadKeyWord;
	}

	public DSLHeadClause getHeadClause() {
		return headClause;
	}

	public List<DSLClause> getPossibleClauseList() {
		return possibleClauseList;
	}
	
}
