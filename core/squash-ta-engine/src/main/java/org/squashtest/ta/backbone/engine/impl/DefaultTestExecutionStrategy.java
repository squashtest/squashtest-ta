/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;

import org.squashtest.ta.backbone.engine.InstructionFlowStrategy;
import org.squashtest.ta.framework.test.result.GeneralStatus;

/**
 * This is the default strategy used for the test phase execution.
 * @author edegenetais
 *
 */
public class DefaultTestExecutionStrategy implements InstructionFlowStrategy{

	@Override
	public boolean continueExecution(GeneralStatus instructionStatus) {
		//Instruction group execution stops at first non passing instruction status.
		return instructionStatus.isPassed();
	}

	@Override
	public GeneralStatus aggregate(GeneralStatus aggregateStatus,
			GeneralStatus instructionStatus) {
		return aggregateStatus.mostSevereStatus(instructionStatus);
	}

	@Override
	public boolean beginGroupExecution(GeneralStatus aggregateStatus) {
		return aggregateStatus.isPassed();
	}

}
