/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.library.properties;

import spock.lang.Specification;

public class PropertiesKeySetTest extends Specification {

	
	def "should add a key only once"(){
		given :
			def set = new PropertiesKeySet()
		
		when :
			set.addUnique("bob")
			set.addUnique("bob")
		
		then :
			set.keys == ["bob"]
	}
	
	def "should add keys only once"(){
		given :
			def set = new PropertiesKeySet()
		
		when :
			set.addUnique(["bob", "bob", "mike", "mike"])
		
		then :
			set.keys as Set == ["bob", "mike"] as Set
		
	}
	
	def "should remove all the keys"(){
		given :
			def set = new PropertiesKeySet(["bob", "mike", "robert"])
		
		when :
			set.removeAll(["bob", "mike"])
		
		then :
			set.keys == ["robert"]
		
	}
	
	def "should say that the set contains all the arguments"(){
		
		given :
			def set = new PropertiesKeySet(["aaa", "bbb", "ccc", "ddd"])
		
		when :
			def res = set.containsAll(["aaa", "bbb"])
		
		then :
			res == true
		
	}
	
	def "should say that the set does not contain all the arguments"(){
		
		given :
			def set = new PropertiesKeySet(["aaa", "bbb", "ccc", "ddd"])
		
		when :
			def res = set.containsAll(["aaa", "ccc", "eee"])
		
		then :
			res == false
	}
	
	def "should say that the set contains one of the arguments"(){
		given :
			def set = new PropertiesKeySet(["aaa", "bbb", "ccc", "ddd"])
		
		when :
			def res = set.containsOne(["aaa", "fff", "eee"])
		
		then :
			res == true
	}
	
	def "should say that the set does not contain any of the arguments"(){
		given :
			def set = new PropertiesKeySet(["aaa", "bbb", "ccc", "ddd"])
		
		when :
			def res = set.containsOne(["ggg", "fff", "eee"])
		
		then :
			res == false
	}
	
	def "should say that the set contains exactly that"(){
		
		given :
			def set = new PropertiesKeySet(["aaa", "bbb", "ccc", "ddd"]) 	
		
		when :
			def res = set.containsExactly(["aaa", "bbb", "ccc", "ddd"])
			
		then :
			res == true
		
	}
	
	def "should say that the set does not contain exactly that (1)"(){
		given :
			def set = new PropertiesKeySet(["aaa", "bbb", "ccc", "ddd"]) 	
		
		when :
			def res = set.containsExactly(["bbb", "ccc", "ddd"])
			
		then :
			res == false
		
	}
	
	def "should say that the set does not contain exactly that (2)"(){
		given :
			def set = new PropertiesKeySet(["bbb", "ccc", "ddd"])
		
		when :
			def res = set.containsExactly(["aaa", "bbb", "ccc", "ddd"])
			
		then :
			res == false
		
	}
	
	def "should say that the set does not contain exactly that (3)"(){
		given :
			def set = new PropertiesKeySet(["aaa", "bbb", "ccc"])
		
		when :
			def res = set.containsExactly(["bbb", "ccc", "ddd"])
			
		then :
			res == false
		
	}
	
	def "should find no common keys when disjoined"(){
		given:
			def set=new PropertiesKeySet(["aaa","bbb","ccc"])
		and:
			def prop=new Properties()
			prop.setProperty("ddd", "dummy")
			prop.setProperty("zorro", "il le signe � la pointe de l'�p�e")
		when:
			def common=set.getCommonKeys(prop)
		then:
			common.isEmpty()
	}
	
	def "should find existing common keys"(){
		given:
			def set=new PropertiesKeySet(["aaa","bbb","ccc"])
		and:
			def prop=new Properties()
			prop.setProperty("bbb", "got that one too, hombre! Any problem with you?")
		when:
			def common=set.getCommonKeys(prop)
		then:
			common.size()==1
			common.contains("bbb")
	}
}
