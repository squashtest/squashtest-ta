/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.init;

import java.io.File;
import java.util.List;

import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;

/**
 * Workspace browser to expose legacy resources and targets to the engine.
 * @author edegenetais
 *
 */
public class LegacyWorkspaceBrowser implements TestWorkspaceBrowser {

	@Override
	public List<File> getTargetsDefinitions() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("TODO");
	}

	@Override
	public List<File> getRepositoriesDefinitions() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("TODO");
	}

	@Override
	public ResourceRepository getDefaultResourceRepository() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("TODO");
	}

}
