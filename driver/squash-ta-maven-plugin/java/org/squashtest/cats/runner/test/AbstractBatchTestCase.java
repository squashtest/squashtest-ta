/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.test;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * This class is a facade to integrate legacy CATS batch tests classes into the
 * new squashTA execution framework.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractBatchTestCase extends AbstractCatsTestCase {

    /**
     * Runs a SSH command, reading the connection parameters from the specified
     * configuration file. If the exit status is 0 (= "success"), checks whether
     * the contents of the output and error streams match the patterns
     * specified.
     * 
     * @param configFile SSH configuration file (contains host, port, username
     *            and password)
     * @param command Command to execute
     * @param outputStreamPattern Pattern to match against the output stream. If
     *            <code>null</code> or empty, the output stream is not checked.
     * @param errorStreamPattern Pattern to match against the error stream. If
     *            <code>null</code> or empty, the error stream is not checked.
     * @throws IOException
     */
    protected void runAndCheckBatchTestCase(String configFile, String command, String outputStreamPattern,
                    String errorStreamPattern) throws URISyntaxException, IOException, InterruptedException {
    	throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
    }

}
