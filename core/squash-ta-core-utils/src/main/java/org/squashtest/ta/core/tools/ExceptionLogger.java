/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Because code duplication of tedious tasks like exception reporting deserves an adequate treatment.
 * 
 * <p>It will log things to a specified logger, and will create exceptions. A default exception type must be supplied 
 * to the constructor, so that it knows what exception to use for default logging methods. </p>
 * 
 * <p> 
 * The supplied exception type must :
 * <ul>
 * 	<li>extend {@link RuntimeException}</li>
 * 	<li>must not be a non static inner class of something else,</li>
 * 	<li>define a constructor with String as first argument and {@link Throwable} as second,</li>
 * 	<li>the said constructor must be public,</li>
 * </ul>
 * </p>
 * 
 * @author bsiri
 *
 */
public class ExceptionLogger {

	private static final Logger OWN_LOGGER = LoggerFactory.getLogger(ExceptionLogger.class);
	
	
	private final Logger delegateLogger;
	private final Class<? extends RuntimeException> defaultException;
	private final Constructor<? extends RuntimeException> exConstructor;
	private final Constructor<? extends RuntimeException> exConstructorNoCause;
	
	public ExceptionLogger(Logger logger, Class<? extends RuntimeException> defaultExceptionClass){
		
		if (logger == null){
			throw new IllegalArgumentException("Cannot create ExceptionLogger : supplied logger is null");
		}
		this.delegateLogger = logger;
		this.defaultException=defaultExceptionClass;
		
		Constructor<? extends RuntimeException>causeConstructor;
		try{
			causeConstructor=defaultExceptionClass.getConstructor(String.class, Throwable.class);
		}
		 catch(NoSuchMethodException e ){
			 causeConstructor=null;
		 }
		 exConstructor=causeConstructor;
		 
		 Constructor<? extends RuntimeException> noCauseConstructor;
		 try{
				noCauseConstructor=defaultExceptionClass.getConstructor(String.class);
		 }catch(NoSuchMethodException e ){
			 if(exConstructor==null){
				 throw new IllegalArgumentException("Cannot create ExceptionLogger : the default exception class '"+
						 defaultExceptionClass.getName()+"' defines no public constructor(String, Throwable)",e);
			 }else{
				 noCauseConstructor=null;
			 }
		 }
		 exConstructorNoCause=noCauseConstructor;	 
	}
	
	public ExceptionLogger(Class<?> ownerClass, Class<? extends RuntimeException> defaultExceptionClass){
		this(LoggerFactory.getLogger(ownerClass), defaultExceptionClass);
	}

	
	// ************************** bulk methods *********************** 
	
	
	public RuntimeException errAndThrow(String message, Throwable cause){
		delegateLogger.error(message, cause);
		if(cause==null){
			return newExceptionInstance(message);
		}else{
			return newExceptionInstance(message, cause);
		}
	}
	
	public RuntimeException errAndThrow(String message){
		delegateLogger.error(message);
		return newExceptionInstance(message);
	}
	
	public void simpleWarn(String message, Throwable cause){
		if (delegateLogger.isWarnEnabled()){
			delegateLogger.warn(message, cause);
		}
	}

	// ******************** private stuffs ***********

	private RuntimeException newExceptionInstance(String message, Throwable cause){
		try {
			RuntimeException exception;
			if(exConstructor==null){
				exception=exConstructorNoCause.newInstance(message);
				exception.initCause(cause);
			}else{
				exception=exConstructor.newInstance(message, cause);
			}
			return exception;
		} 
		catch (IllegalArgumentException e) {
			String mess = "ExceptionLogger : cannot create default exception, supplied argument message '"
				+message+"' or exception cause '"+cause.getClass().getName()+"' were not suitable.";
			
			OWN_LOGGER.error(mess);
			
			throw new ExceptionCreationException(mess, e);			
		} 
		catch (InstantiationException e) {
			String mess = "ExceptionLogger : cannot create default exception : the default exception class '"
				+defaultException.getClass().getName()+"' cannot be instanciated.";
			
				OWN_LOGGER.error(mess);
			
			throw new ExceptionCreationException(mess, e);
		} 
		catch (IllegalAccessException e) {
			String mess = "ExceptionLogger : cannot create default exception : constructor for class '"
				+defaultException.getClass().getName()+"' is not accessible.";
			
				OWN_LOGGER.error(mess);
			
			throw new ExceptionCreationException(mess, e);
		} 
		catch (InvocationTargetException e) {
			String mess = "ExceptionLogger : an exception occured while creating instance of '"
				+defaultException.getClass().getName()+"' using '"+message+"' and '"+cause.getClass().getName()+"'.";
			
				OWN_LOGGER.error(mess);
			
			throw new ExceptionCreationException(mess, e);
		}
	}
	
	private RuntimeException newExceptionInstance(String message){
		try {
			RuntimeException exception;
			if(exConstructorNoCause==null){
				exception=exConstructor.newInstance(message,null);
			}else{
				exception=exConstructorNoCause.newInstance(message);
			}
			return exception;
		} 
		catch (IllegalArgumentException e) {
			String mess = "ExceptionLogger : cannot create default exception, supplied argument message '"
				+message+"' is not suitable.";
			
			OWN_LOGGER.error(mess);
			
			throw new ExceptionCreationException(mess, e);			
		} 
		catch (InstantiationException e) {
			String mess = "ExceptionLogger : cannot create default exception : the default exception class '"
				+defaultException.getClass().getName()+"' cannot be instanciated.";
			
				OWN_LOGGER.error(mess);
			
			throw new ExceptionCreationException(mess, e);
		} 
		catch (IllegalAccessException e) {
			String mess = "ExceptionLogger : cannot create default exception : constructor for class '"
				+defaultException.getClass().getName()+"' is not accessible.";
			
				OWN_LOGGER.error(mess);
			
			throw new ExceptionCreationException(mess, e);
		} 
		catch (InvocationTargetException e) {
			String mess = "ExceptionLogger : an exception occured while creating instance of '"
				+defaultException.getClass().getName()+"' using '"+message+"'.";
			
				OWN_LOGGER.error(mess);
			
			throw new ExceptionCreationException(mess, e);
		}
	}
	/**
	 * Exception class for exception instantiation errors.
	 * @author edegenetais
	 *
	 */
	@SuppressWarnings("serial")
	private static class ExceptionCreationException extends RuntimeException{

		/**
		 * @param message
		 * @param cause
		 */
		public ExceptionCreationException(String message, Throwable cause) {
			super(message, cause);
		}
		
	}
}
