/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.squashtest.ta.framework.test.event.EcosystemStatusUpdate;
import org.squashtest.ta.framework.test.event.StatusUpdateEvent;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate;

import spock.lang.Specification;

/**
 * We test event dispatching here
 * @author edegenetais
 *
 */
class EventManagerImplTest extends Specification{
	EventManagerImpl testee
	StatusUpdateListener listener
	def setup(){
		testee=new EventManagerImpl()
		listener=Mock()
		testee.addEventListener(listener)

				//stop after 3 seconds anyway
		ScheduledExecutorService executor=Executors.newSingleThreadScheduledExecutor()
		executor.schedule(new Runnable(){
			void run() {
				testee.shutdown()
			};
		}, 3, TimeUnit.SECONDS)

	}
	
	def "added listeners should receive test udpate events as test events"(){
		given:
			TestStatusUpdate event=Mock()
			event.fire(_) >> {super.fire(it)}
		when:
			testee.postEvent(event)
			testee.run()//we need to be synchronous: only way to be sure we wait for event treatment before checking that events were received
		then:
			1 * listener.handle(event) >> {
				//stop as soon as we have received the event we expected
				testee.shutdown()
			}
	}
	
	def "added listeners should receive ecosystem udpate events as ecosystem events"(){
		given:
			EcosystemStatusUpdate event=Mock()
			event.fire(_) >> {super.fire(it)}
		when:
			testee.postEvent(event)
			testee.run()//we need to be synchronous: only way to be sure we wait for event treatment before checking that events were received
		then:
			1 * listener.handle(event) >> {
				//stop as soon as we have received the event we expected
				testee.shutdown()
			}
	}
	
	def "added listeners should receive suite udpate events as suite events"(){
		given:
			TestSuiteStatusUpdate event=Mock()
			event.fire(_) >> {super.fire(it)}
		when:
			testee.postEvent(event)
			testee.run()//we need to be synchronous: only way to be sure we wait for event treatment before checking that events were received
		then:
			1 * listener.handle(event) >> {
				//stop as soon as we have received the event we expected
				testee.shutdown()
			}
	}
	
}
