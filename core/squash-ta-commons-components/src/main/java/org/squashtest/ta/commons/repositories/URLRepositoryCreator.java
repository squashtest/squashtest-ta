/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.repositories;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.utils.authentication.EngineAuthenticator;
import org.squashtest.ta.commons.utils.authentication.URLBasedAuthenticator;
import org.squashtest.ta.core.templates.FileBasedCreator;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.RepositoryCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;

@EngineComponent("repo.creator.url")
public class URLRepositoryCreator extends FileBasedCreator implements RepositoryCreator<URLRepository> {

	protected static final Logger logger = LoggerFactory.getLogger(URLRepositoryCreator.class);

	private PropertiesBasedCreatorHelper helper = new PropertiesBasedCreatorHelper();
	
	public static final String TA_URL_BASE = "squashtest.ta.url.base";
	public static final String TA_URL_CACHE= "squashtest.ta.url.useCache";
	public static final String TA_URL_LOGIN = "squashtest.ta.url.login";
	public static final String TA_URL_PASSWORD = "squashtest.ta.url.password";
	
	private static final String[] KEYS = new String[]{
		TA_URL_BASE,
		TA_URL_CACHE,
		TA_URL_LOGIN,
		TA_URL_PASSWORD
	};
	
	public URLRepositoryCreator(){
		helper.setKeys(KEYS);
	}
	
	@Override
	public boolean canInstantiate(URL propertiesURL) {
		File file = getFileOrNull(propertiesURL);
		if (file==null){
			return false;
		}
		
		Properties properties;
		try {
			properties = helper.getEffectiveProperties(file);
		} catch (IOException e) {
			logger.warn("Could not read from URL "+propertiesURL);
			return false;
		}
		
		return checkBaseURLSyntax(properties);		
			
	}

	@Override
	public URLRepository createRepository(URL propertiesURL) {
		String strBaseUrl=null;
		try {
			File file = getFileOrFail(propertiesURL);
			Properties properties;

			properties = helper.getEffectiveProperties(file);

			// basic attributes settings
			strBaseUrl = properties.getProperty(TA_URL_BASE);
			URL baseURL = null;
			baseURL = new URL(strBaseUrl);
			boolean isCache = isUseCache(properties);

			// credentials settings
			String username = properties.getProperty(TA_URL_LOGIN);
			String password = properties.getProperty(TA_URL_PASSWORD);

			EngineAuthenticator authenticator = new URLBasedAuthenticator(
					username, password, baseURL);

			// hiding the credentials from the properties now
			properties = helper.anonymize(properties, 1, TA_URL_LOGIN,
					TA_URL_PASSWORD);

			return new URLRepository(properties, baseURL, isCache,
					authenticator);
		} catch (MalformedURLException e) {
			// should never happen : the validity checks were all performed in
			// the canInstantiate call.
			throw new BrokenTestException("URLRepositoryCreator : base url '"
					+ strBaseUrl + "' is no valid url", e);
		} catch (IOException e1) {
			throw new BrokenTestException(
					"Coulnd not read repository specification.", e1);
		} catch (URISyntaxException e) {
			throw new BrokenTestException("Definition URL was no valid URI", e);
		}
	}
	
	

	protected boolean isUseCache(Properties properties){
		String pptCache = properties.getProperty(TA_URL_CACHE);
		return ((pptCache!=null) && (pptCache.equalsIgnoreCase("yes")));
	}
	

	protected boolean checkBaseURLSyntax(Properties properties){
		try {
			String strBaseURL = properties.getProperty(TA_URL_BASE);	
			new URL(strBaseURL);//checks URL syntax
			return true;
		} catch (MalformedURLException e) {
			if (logger.isDebugEnabled()){
				logger.debug("URLRepositoryCreator : the base url is not a valid url");
			}
			return false;
		}
	}


}
