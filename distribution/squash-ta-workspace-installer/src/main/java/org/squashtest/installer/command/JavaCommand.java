/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.command;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;
import java.util.List;

import org.squashtest.installer.context.Context;

/**
 *  Will execute a given class with given args. 
 *  
 *  The target class must have a static void main.
 *  
 *  Provides methods to set class paths up.
 *  
 * 
 */

public class JavaCommand implements Command {
	
	private Context context;

	private List<URL> packageURLS = new LinkedList<URL>(); 
	private List<String> args = new LinkedList<String>();
	private String mainClassName; 
	
	private ClassLoader classLoader;
	private Class<?> mainClass;
	
	
	public void setContext(Context context){
		this.context=context;
	}
	
	public Context getContext(){
		return context;
	}
	
	public void addToClassPath(URL pckgURL){
		packageURLS.add(pckgURL);
	}
	
	/**
	 * add an unary argument
	 * @param args
	 */
	public void addArg(String arg){
		args.add(arg);
	}
	
	/**
	 * binary args (name value)
	 * 
	 * @param option
	 * @param value
	 */
	public void addArg(String option, String value){
		args.add(option);
		args.add(value);
	}
	
	/**
	 * REQUIRED : the fully qualified class to execute, who must have a public static void main method.
	 * 
	 * @param className
	 */
	public void setMainClassName(String className){
		mainClassName = className;
	}
	
	@Override
	public void execute() {
		if (mainClassName==null){
			throw new IllegalArgumentException("JavaCommand : main class name was not provided");
		}
		
		createClassLoader();
		getMainClass();
		executeMainClass();
		
	}

	private void createClassLoader(){
		URL[] urls = new URL[packageURLS.size()];
		urls=packageURLS.toArray(urls);
		classLoader= new URLClassLoader(urls, this.getClass().getClassLoader());
		
	}
	
	private void getMainClass(){
		try {
			mainClass = classLoader.loadClass(mainClassName);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("JavaCommand : main class name "+mainClassName+" does not exists. " +
					" Provide the class to the classpath and re-run the command.");
		}
	}
	
	
	private void executeMainClass() {
		try{
			Method mainMethod = mainClass.getDeclaredMethod("main", String[].class);
			String[] actualArgs = args.toArray(new String[0]);
			mainMethod.invoke(null, new Object[]{actualArgs});
		}catch(NoSuchMethodException nsme){
			throw new IllegalArgumentException("JavaCommand : main class name "+mainClassName+
					" have no public static void main(String... args) method", nsme);
		}catch(InvocationTargetException ite){
			throw new IllegalArgumentException("JavaCommand : main class name"+mainClassName+
					" main method is not static", ite);
		}catch(SecurityException se){
			throw new IllegalArgumentException("JavaCommand : main class name"+mainClassName+
					" is protected", se);
		}catch(IllegalArgumentException iae){
			throw new IllegalArgumentException("JavaCommand : main class name"+mainClassName+
					" main method only accept string arguments. Check carefully the type of those you provided.", iae);
		}catch(IllegalAccessException iae){
			throw new IllegalArgumentException("JavaCommand : main class name"+mainClassName+
					" main method rejected your arguments because :", iae);
		}
	}

}
