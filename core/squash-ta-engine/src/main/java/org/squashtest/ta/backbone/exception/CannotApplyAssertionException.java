/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.exception;

import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 * Exception thrown when an assertion cannot be applied because it is not found.
 * @author edegenetais
 *
 */
public class CannotApplyAssertionException extends BrokenTestException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8852007270223789235L;

	public CannotApplyAssertionException() {
		super();
	}

	public CannotApplyAssertionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CannotApplyAssertionException(String arg0) {
		super(arg0);
	}

}
