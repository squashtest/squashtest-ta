test :

LOAD conf/ppkFilter.properties AS ppk
CONVERT ppk TO properties(structured) AS ppk.properties
CONVERT ppk.properties TO conf.dbunit.ppk(from.properties) AS dbuPpk

LOAD datasets/actual.dataset.xml AS actual
CONVERT actual TO xml(structured) AS actual.xml
CONVERT actual.xml TO dataset.dbunit(dataset) AS actual.dbu

LOAD datasets/simple.dbu.xml AS expected
CONVERT expected TO xml(structured) AS expected.xml
CONVERT expected.xml TO dataset.dbunit(dataset) AS expected.dbu

ASSERT actual.dbu DOES equal THE expected.dbu USING dbuPpk