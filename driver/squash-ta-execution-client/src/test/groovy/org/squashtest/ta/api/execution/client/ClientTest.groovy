/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.api.execution.client
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.configuration.Configurable;
import org.apache.cxf.transport.http.ChunkedUtil;
import org.junit.Assert;
import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.TestSuiteExecutionStatus

import spock.lang.Specification;


class ClientTest extends Specification {
	ServerMock serverMock
	int port
	Thread serverThread
	
	StatusUpdate testee
	
	def setup(){
		def mini=1025
		port=mini
		ServerSocket serverSocket=null;
		while(serverSocket==null && port<10001){
			try{
				serverSocket=new ServerSocket(port);
			}catch(BindException be){
				port++;
				System.out.println((port-1)+"taken, trying "+port)
			}
		}
		if(serverSocket==null){
			Assert.fail("Server mock setup failed: tried all ports from "+mini+" to "+(port-1)+" without success.")
		}else{
			System.out.println("Got port "+port)
		}
		
		serverMock=new ServerMock(serverSocket)
		
		serverThread=new Thread(serverMock)
		serverThread.start()

		System.out.println(getClass().getResource("conf.properties").toURI());
		File confFile = new File(getClass().getResource("conf.properties").toURI());
		testee=new ClientFactory().getClient(new URL("http://localhost:"+port), confFile);
		
	}

	class ServerMock implements Runnable{
		private static final Pattern CONTENT_LENGTH_PATTERN=Pattern.compile("Content-Length: (\\d+)")
		ServerSocket srvSocket
		String query
		String content
		ServerMock(ServerSocket socket){
			this.srvSocket=socket
		}
		@Override
		public void run() {
			Socket socket=srvSocket.accept()
			InputStream socketIS=socket.getInputStream()
			InputStreamReader socketReader=new InputStreamReader(socketIS)
			StringBuilder request=new StringBuilder()
			int lastChar=socketReader.read()
			boolean header=true
			StringBuilder lineBuilder=new StringBuilder()
			Integer length=0
			while(lastChar>=0 && header){
				while(((char)lastChar)!='\n'){
					lineBuilder.append((char)lastChar)
					lastChar=socketReader.read()
				}
				String line=lineBuilder.toString()
				lineBuilder.setLength(0)
				System.out.println("[server] got:"+line)
				request.append(line).append('\n')
				def lengthMatcher = CONTENT_LENGTH_PATTERN.matcher(line)
				if(lengthMatcher.find()){
					length=Integer.parseInt(lengthMatcher.group(1))
				}
				if(!"".equals(line.trim())){
					lastChar=socketReader.read()
				}else{
					System.out.println("Last header line: stop reading!");
					header=false;
				}
			}

			System.out.println("Header is finished!");
			
			BufferedWriter responseWriter=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))
			
			if(length>0){
				char[] buffer=new char[length]
				int expected=length
				int chunkSize=Math.min(1024, (int)(length))
				int offset=0;
				while(chunkSize>0){
					int read=socketReader.read(buffer, offset, chunkSize)
					offset+=read
					chunkSize=Math.min(1024, length-offset)
				}
				content=new String(buffer)
			}
			
			responseWriter.write("HTTP/1.1 201 Created\n")
			responseWriter.write("\n")
			
			responseWriter.close()
			query=request.toString()
			System.out.println("Request served!");
		}
		
	}
	
	def cleanup(){
		if(serverMock!=null){
			serverMock.srvSocket.close()
		}
	}
	
	def "post test status update, expected protocol is implemented"(){
		given:
			TestExecutionStatus testStatus=new TestExecutionStatus()
			testStatus.setTestName("dummyTestName")
			testStatus.setTestGroupName("dummyTestGroup")
			def calendar = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris"))
			calendar.clear()
			calendar.set(1977, 01, 01, 10, 30);//very important event that day ;-)
			testStatus.setStartTime(calendar.getTime())
			testStatus.setStatus(ExecutionStatus.RUNNING)
			testStatus.setStatusMessage("a bit slow!")
		when:
			testee.updateTestExecutionStatus("testHost", "dummyJob", "testId", "dummyTestGroup", "dummyTestName", testStatus)
		then:
			serverThread.join(50000)
			Pattern urlChecker=Pattern.compile("POST /resultUpdate/testHost/dummyJob/testId/testStatus/dummyTestGroup/dummyTestName HTTP/\\d\\.\\d")
			def urlMatcher=urlChecker.matcher(serverMock.query)
			Assert.assertTrue("Real was:\n"+serverMock.query, urlMatcher.find())
			!urlMatcher.find()
			String expected="{\"testName\":\"dummyTestName\",\"testGroupName\":\"dummyTestGroup\","+	
			"\"startTime\":223637400000,\"endTime\":null,\"status\":\"RUNNING\",\"statusMessage\":\"a bit slow!\"}"
			expected.equals(serverMock.content)		
	}
	
	def "post test suite status update, expected protocol is implemented"(){
		given:
			TestSuiteExecutionStatus testStatus=new TestSuiteExecutionStatus()
			testStatus.setSuiteName("dummySuite")
			def calendar = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris"))
			calendar.clear()
			calendar.set(1977, 01, 01, 10, 30);//very important event that day ;-)
			testStatus.setStartTime(calendar.getTime())
			testStatus.setStatus(ExecutionStatus.SUCCESS)
		when:
			testee.updateTestSuiteExecutionStatus("testHost", "dummyJob", "testId", testStatus)
		then:
			serverThread.join(50000)
			Pattern urlChecker=Pattern.compile("POST /resultUpdate/testHost/dummyJob/testId/testSuiteStatus HTTP/\\d\\.\\d")
			def urlMatcher=urlChecker.matcher(serverMock.query)
			Assert.assertTrue("Real was:\n"+serverMock.query, urlMatcher.find())
			!urlMatcher.find()
			String expected="{\"suiteName\":\"dummySuite\","+
			"\"startTime\":223637400000,\"endTime\":null,\"status\":\"SUCCESS\"}"
			expected.equals(serverMock.content)
	}
}
