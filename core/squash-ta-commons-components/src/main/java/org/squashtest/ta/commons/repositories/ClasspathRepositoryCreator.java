/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.repositories;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.library.properties.PropertiesKeySet;
import org.squashtest.ta.core.templates.FileBasedCreator;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.RepositoryCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;

@EngineComponent("repo.creator.classpath")
public class ClasspathRepositoryCreator extends FileBasedCreator implements RepositoryCreator<ClasspathRepository> {

	protected static final Logger LOGGER=LoggerFactory.getLogger(ClasspathRepositoryCreator.class);
	
	public static final String TA_CLASSPATH_TAG = "squashtest.ta.classpath";
	
	private static final String[] KEYS = new String[]{TA_CLASSPATH_TAG};
	
	private PropertiesBasedCreatorHelper helper = new PropertiesBasedCreatorHelper();
	
	public ClasspathRepositoryCreator(){
		helper.setKeys(KEYS);
	}
	

	@Override
	public boolean canInstantiate(URL definitionURL) {

		//technical check
		File file = getFileOrNull(definitionURL);
		if (file==null){
			return false;		
		}
		
		//functional check
		try {
			return checkKeys(file);
		} catch (IOException e) {
			LOGGER.warn("Could not read from URL "+definitionURL);
			return false;
		}		
				
	}
	
	
	@Override
	public ClasspathRepository createRepository(URL definitionURL) {
		
		File file;
		try {
			file = getFileOrFail(definitionURL);
			Properties properties = helper.getEffectiveProperties(file);
			return new ClasspathRepository(properties);
		} catch (IOException e) {
			throw new BrokenTestException("Classpath repository specification cannot be read.", e);
		} catch (URISyntaxException e) {
			throw new BrokenTestException("Definition URL was no valid URI", e);
		}
	}


	
	
	private boolean checkKeys(File file) throws IOException{
		
		Properties properties = helper.getEffectiveProperties(file);
		PropertiesKeySet keySet = new PropertiesKeySet(properties);
		boolean test = keySet.containsExactly(KEYS);
		
		if (test){
			return true;
		}else{
			if (LOGGER.isDebugEnabled()){
				LOGGER.debug("ClasspathRepositoryCreator : cannot create repository '"+file.getPath()+"',"+
							" the supplied configuration should contain only the key '"+TA_CLASSPATH_TAG+"'");
			}
			return false;
		}
	}

	
	
}
