/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter.wildcard;

import java.util.List;

/**
 * Matches a path element name if it is exactly equal to the reference.
 * 
 * @author edegenetais
 * 
 */
public class LitteralMatcher extends AbstractMatcherBase {
	/** The name to look for. */
	private String targetName;

	/**
	 * Full initialization constructor.
	 * @param targetName the name to recognize.
	 */
	public LitteralMatcher(String targetName) {
		this.targetName = targetName;
	}

	@Override
	public MatchState matches(List<String> path) {
		MatchState newState;
		if (path.size() > 0) {
			List<String>leftOver=getSubPath(path, 1);
			if(targetName.equals(path.get(0))){
				newState=new MatchState(leftOver.isEmpty()?true:null, leftOver);
			}else{
				newState=new MatchState(false, leftOver);
			}
		} else {
			return new MatchState(false, emptyList());
		}
		return newState;
	}

	@Override
	public String toPattern() {
		return targetName;
	}
}
