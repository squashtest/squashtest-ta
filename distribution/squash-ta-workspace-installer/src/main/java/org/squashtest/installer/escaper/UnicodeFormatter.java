/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * Credits to the above for this class. I had to change it a bit but it's mostly their work.
 * http://docs.oracle.com/javase/tutorial/i18n/text/examples/UnicodeFormatter.java
 * 
 * 
 * bsiri 
 * 
 */
package org.squashtest.installer.escaper;

 
class UnicodeFormatter  {
 
   static public String byteToHex(byte b) {
      // Returns hex String representation of byte b
      char hexDigit[] = {
         '0', '1', '2', '3', '4', '5', '6', '7',
         '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
      };
      char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
      return new String(array);
   }
 
   static public String charToHex(char c) {
      // Returns hex String representation of char c
      byte hi = (byte) (c >>> 8);
      byte lo = (byte) (c & 0xff);
      return byteToHex(hi) + byteToHex(lo);
   }
   
   static public String escape(char c){
	   String prepend = "\\u";
	   if (isWithinAnsi(c)){
		   return new String(new char[]{c});
	   }else{
		   return prepend+charToHex(c);
	   }
   }
   
   private static boolean isWithinAnsi(char c){
	   return ((c > 0x1f) && c < (0x7F));
   }
 
} // class
