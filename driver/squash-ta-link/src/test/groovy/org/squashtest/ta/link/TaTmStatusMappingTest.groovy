/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.link

import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus;
import org.squashtest.ta.framework.test.result.GeneralStatus;

import spock.lang.Specification;
import spock.lang.Unroll;

class TaTmStatusMappingTest extends Specification{
	@Unroll("mapping GeneralStatus.#taStatus should return ExecutionStatus.#tmStatus")
	def "TA GeneralStatus to TM ExecutionStatus mapping"(){
		expect:
			TaTmStatusMapping.getMapping(taStatus).getTmStatus()==tmStatus
		where:
			taStatus				|	tmStatus
			GeneralStatus.ERROR		|	ExecutionStatus.ERROR
			GeneralStatus.FAIL		|	ExecutionStatus.FAILURE
			GeneralStatus.NOT_RUN	|	ExecutionStatus.ERROR
			GeneralStatus.RUNNING	|	ExecutionStatus.RUNNING
			GeneralStatus.SUCCESS	|	ExecutionStatus.SUCCESS
			GeneralStatus.WARNING	|	ExecutionStatus.WARNING
	}
}
