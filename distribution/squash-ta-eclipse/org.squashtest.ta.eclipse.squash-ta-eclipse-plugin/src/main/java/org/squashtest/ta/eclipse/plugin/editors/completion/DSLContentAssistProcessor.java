/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.editors.completion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateCompletionProcessor;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.text.templates.TemplateException;
import org.eclipse.jface.text.templates.TemplateProposal;
import org.eclipse.swt.graphics.Image;
import org.squashtest.ta.eclipse.plugin.Activator;
import org.squashtest.ta.eclipse.plugin.editors.analyser.LineAnalyseResult;
import org.squashtest.ta.eclipse.plugin.editors.analyser.LineAnalyser;
import org.squashtest.ta.eclipse.plugin.editors.dsl.DSLEditor;
import org.squashtest.ta.eclipse.plugin.editors.dsl.DSLTemplateEditorUI;

/**
 * Component used by the {@link DSLEditor} to compute auto-completion proposals.
 * @author edegenetais
 *
 */
public final class DSLContentAssistProcessor extends
		TemplateCompletionProcessor {
	
	
	
	private static final class ProposalComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			return ((TemplateProposal) o2).getRelevance() - ((TemplateProposal) o1).getRelevance();
		}
	}

	private static final Comparator fgProposalComparator= new ProposalComparator();
	
	private static final String DEFAULT_IMAGE= "$nl$/icons/template.gif"; //$NON-NLS-1$
	
	private String currentErrorMessage;
	private static final List<DSLCompletionPossibleType> ALL_COMPLETION_TYPE = Arrays.asList(DSLCompletionPossibleType.values());
	private String contextTypeId;
	
	@Override
	public String getErrorMessage() {
		return null;
	}

	@Override
	public IContextInformationValidator getContextInformationValidator() {
		return new YesContextInformationValidator();
	}

	@Override
	public ICompletionProposal[] computeCompletionProposals(ITextViewer editor, int carretPosition) {
		List<ICompletionProposal> proposalList=new ArrayList<ICompletionProposal>();
		currentErrorMessage = null;
		try {
			LineAnalyser analyser = new LineAnalyser();
			LineAnalyseResult result = analyser.parse(editor, carretPosition);
			List<DSLCompletionPossibleType> possibleType = computePossibleType(result);
			ComputeInstructionProposal prop = new ComputeInstructionProposal(result);
			for (DSLCompletionPossibleType dslCompletionPossibleType : possibleType) {
				proposalList.addAll(prop.getProposal(dslCompletionPossibleType));
				contextTypeId = dslCompletionPossibleType.getId();
				Map<String, List<ICompletionProposal>> templateProposalMap = computeTemplateCompletionProposal(editor, carretPosition, result.getSearched());
				proposalList.addAll(prop.getTemplateProposal(dslCompletionPossibleType, templateProposalMap));
			}
		} catch (Exception e) {
			e.printStackTrace();
			if(currentErrorMessage==null){
				currentErrorMessage = e.getMessage();
			}else{
				currentErrorMessage+="\n"+e.getMessage();
			}
		}
		return proposalList.toArray(new ICompletionProposal[proposalList.size()]);
	}

	@Override
	protected Template[] getTemplates(String contextTypeId) {
		return DSLTemplateEditorUI.getDefault().getTemplateStore().getTemplates();
	}

	@Override
	protected TemplateContextType getContextType(ITextViewer viewer,
			IRegion region) {
		return DSLTemplateEditorUI.getDefault().getContextTypeRegistry().getContextType(contextTypeId);
	}

	@Override
	protected Image getImage(Template template) {
		ImageRegistry registry= DSLTemplateEditorUI.getDefault().getImageRegistry();
		Image image= registry.get(DEFAULT_IMAGE);
		if (image == null) {
			ImageDescriptor desc= DSLTemplateEditorUI.imageDescriptorFromPlugin(Activator.PLUGIN_ID, DEFAULT_IMAGE); //$NON-NLS-1$
			registry.put(DEFAULT_IMAGE, desc);
			image= registry.get(DEFAULT_IMAGE);
		}
		return image;
	}
	
	private List<DSLCompletionPossibleType> computePossibleType(LineAnalyseResult result) {
		List<DSLCompletionPossibleType> possibleCompletionType = new ArrayList<DSLCompletionPossibleType>(ALL_COMPLETION_TYPE);
		if (!result.isSearchAtTheBeginning()){
			possibleCompletionType.remove(DSLCompletionPossibleType.PHASE);
			if(result.isSearchedPartOfFirstElement()){
				possibleCompletionType.remove(DSLCompletionPossibleType.KEY_WORD);
			}else{
				possibleCompletionType.remove(DSLCompletionPossibleType.HEAD_KEY_WORD);
			}
		}else{
			possibleCompletionType.remove(DSLCompletionPossibleType.KEY_WORD);
		}
		return possibleCompletionType;
	}
	
	/**
	 * Methos based on  computeCompletionProposals(ITextViewer viewer, int offset) from {@link TemplateCompletionProcessor}
	 * 
	 * @param viewer
	 * @param offset
	 * @return
	 */
	public Map<String, List<ICompletionProposal>> computeTemplateCompletionProposal(ITextViewer viewer, int offset, String searched){

		ITextSelection selection= (ITextSelection) viewer.getSelectionProvider().getSelection();

		// adjust offset to end of normalized selection
		if (selection.getOffset() == offset)
			offset= selection.getOffset() + selection.getLength();

		String prefix= extractPrefix(viewer, offset);
		Region region= new Region(offset - prefix.length(), prefix.length());
		TemplateContext context= createContext(viewer, region);
		
		Map<String, List<ICompletionProposal>> matches= new HashMap<String, List<ICompletionProposal>>();
		if (context != null){				

			context.setVariable("selection", selection.getText()); // name of the selection variables {line, word}_selection //$NON-NLS-1$

			Template[] templates= getTemplates(context.getContextType().getId());
			for (int i= 0; i < templates.length; i++) {
				Template template= templates[i];
				try {
					context.getContextType().validate(template.getPattern());
				} catch (TemplateException e) {
					continue;
				}
				String pattern = template.getPattern();
				String[] patternSplit = {};
				if (template.matches(prefix, context.getContextType().getId()) && template.getPattern().toUpperCase().startsWith(searched.toUpperCase())){
					patternSplit = pattern.split(" ");
					if( patternSplit.length > 0 ){
						int index = 0;
						boolean notFound = true;
						while (index < patternSplit.length && notFound) {
							String patternPiece = patternSplit[index];
							index++;
							if(!patternPiece.equals("")){
								notFound = false;
								List<ICompletionProposal> list = matches.get(patternPiece);
								if(list == null){
									list = new ArrayList<ICompletionProposal>();
									matches.put(patternPiece, list);
								}
								list.add( createProposal(template, context, (IRegion) region, getRelevance(template, prefix)));
							}
						}
						
					}
					
				}
			}
		}
		return matches;	
	}
	
}
