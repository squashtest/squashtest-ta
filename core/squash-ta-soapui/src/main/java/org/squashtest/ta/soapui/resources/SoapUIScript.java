/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.soapui.resources;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * CSV resource implementation.
 * @author fgaillard
 *
 */
@ResourceType("script.soapui")
public class SoapUIScript implements Resource<SoapUIScript> {

	private static final FileTree FILE_TREE = new FileTree();
	private File soapuiFile;
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public SoapUIScript(){}
	
	public SoapUIScript(File file){
		soapuiFile = file;
	}
	
	@Override
	public SoapUIScript copy() {
		try {
			File newFile = FILE_TREE.createTempCopyDestination(soapuiFile);
			FileUtils.copyFile(soapuiFile, newFile);
			return new SoapUIScript(newFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException("SOAPUI (resource) : could not copy file path '"+soapuiFile.getPath()+"', an error occured :", e);
		}
	}

	@Override
	public void cleanUp() {
		if (soapuiFile.exists()){
			soapuiFile.delete();
		}
	}

	public File getSoapUIFile() {
		return soapuiFile;
	}
}
