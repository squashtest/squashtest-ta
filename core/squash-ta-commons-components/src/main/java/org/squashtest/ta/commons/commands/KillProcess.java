/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.commands;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.resources.ProcessHandleResource;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;

/**
 * Command class to kill processes, mainly for ecosystem environment management.
 * 
 * @author edegenetais
 */
@EngineComponent("cleanup")
public class KillProcess implements Command<ProcessHandleResource, VoidTarget>{

	private static final Logger LOGGER = LoggerFactory.getLogger(KillProcess.class);
	
	/**
	 * The process to kill.
	 */
	private ProcessHandleResource processHandle;
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		if(LOGGER.isWarnEnabled() && configuration.size()>0){
			LOGGER.warn(configuration.size()+" configuration resources will be ignored (this command uses none).");
		}
	}

	@Override
	public void setTarget(VoidTarget target) {
		//ignored: the void target holds no information and is here as a place holder in the global engine scheme.
	}

	@Override
	public void setResource(ProcessHandleResource resource) {
		processHandle=resource;
	}

	@Override
	public FileResource apply() {
		/*
		 * We kill the process. 
		 */
		processHandle.cleanUp();
		FileResource fileResource=null;
		try{
			File exitFile=File.createTempFile(processHandle.getProcessHandle().getProcessName(), ".exit");
			exitFile.deleteOnExit();
			Integer returnValue = processHandle.getProcessHandle().returnValue();
			String exitCodeAsString = returnValue==null?"unknown":returnValue.toString();
			new SimpleLinesData(exitCodeAsString.getBytes()).write(exitFile);
			fileResource = new FileResource(exitFile);
		}catch(IOException ioe){
			LOGGER.warn("Failed to write exit code.");
		}
		return fileResource;
	}

	@Override
	public void cleanUp() {
		//noop: only memory resources held.
	}

}
