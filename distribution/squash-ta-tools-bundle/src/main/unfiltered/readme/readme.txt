
==================================================================================

Squash Test Automation version ${project.version} 

==================================================================================

Table of Content :
	0. Requirements
	I. Eclipse workspace post-installation settings
		I.1	Java configuration
		I.2	Maven Conviguration
		I.3	Encoding
		I.4	Sahi syntax coloring
		I.5	Run configurations (1)
	II Create a new Squash TA Project




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
0. Requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Squash TA requires : 

	- Java Runtime Environment 6 (jre6), or (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
	- A Java Development Kit 6 (jdk6) if you intend to develop your own plugins for Squash TA.
	- Apache Maven 3.0 or above (http://maven.apache.org/download.html)
	
But that is a bare minimal. Additionally you may want consider :

	- Eclipse Helios (http://www.eclipse.org/downloads/packages/release/helios/sr2). You may try more recent versions
	  of Eclipse but to our opinion the integration with Maven still needs fixes.
	  
Along with the following complemntary tools : 

	- Sahi version 3.5 (http://sourceforge.net/projects/sahi/files/)
	- Jailer version 4.0.6 (http://sourceforge.net/projects/jailer/files/)


You may download those part or all of the items above separately, or use the prepackaged toolbox (link available on
the squashtest web site). The toolbox includes the tools listed above, an Eclipse prepackaged with useful plugins 
and an installer that will install them as a coherent tool suite. 

The rest of this readme assumes that you are using Eclipse as your test edition platform.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
I. Eclipse workspace post-installation settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Eclipse needs slight tweaking in order to make it work with the technologies that support 
it. If you created a fresh new workspace from your own you will need to perform the following
operations. If you created a workspace using the installer when prompted to, most of the job 
has already be done, however it is wise to check again.

--

Start your Eclipse and please follow these instructions :

I.1 Java configuration
-----------------------

What : 
	This is the java platform that Eclipse should use to run the test engine.

Where : 
	Window > Preferences > Java > Installed JREs

Expected : 
	'${tools.jdk.name}' should be selected on the right part of the screen
	
How to :
 	If that is not the case,  click 'Add', then 'Standard VM', then 'Next'. Now choose the 
 	${tools.jdk.name} shipped with the toolbox using the file browser then click finish to 
 	close the dialog. Finally select the newly added jdk.
	 
	 
I.2 Maven configuration
-----------------------

What : 
	Basically Maven will run your tests by feeding the test engine with them.    

Where :
	Window > Preferences > Maven > Installations
	
Expected :
	The embedded Maven should be disabled
	The External : path\to\'${tools.maven.name}' should be selected
	
How to :
	If that's not the case, click 'Add'. Now choose the ${tools.maven.name} shipped with the 
	toolbox, then click 'OK'. Finally select the newly added Maven installation.


I.3 Encoding
------------

What :
	the UTF-8 encoding is a good standard for text storage that enhances cross platform 
	compatibility. Some tools understand UTF-8 only. You really should switch to UTF-8.

Where :
	Window > Preferences > General > Workspace
	
Expected : 
	the Text File Encoding should be set to 'other : UTF-8'
	
How to : 
	click on 'other', then select 'UTF-8'
	
	
	
I.4 Sahi syntax coloring
------------------------

What : 
	Will map the javascript syntaxic coloring to sahi scripts (.sah)

Where :
	Window > Preferences > Content Types, then unroll 'Text > Javascript Source File'
	
Expected :
	the 'File association' section should display '*.js' and '*.sah'
	
How to :
	Click 'Add', then enter '*.sah', then click 'OK'.
	


I.5 Run Configurations
----------------------

What :
	this is the procedure you call to tell Maven to get to work and run your tests. 

Where :
	Run > Run Configurations, unroll Maven Build.
	
Expected : 
	a Maven run configuration named 'Run all tests' and of which goals are clean integration-test
	
How to :
	* Click on 'Maven Build' and create a new run configuration. Set 'Name' to whatever pleases you 
	('run all tests' for instance).
	* Now for the field 'Base Directory', click on 'Variables', then 'project_loc' in the menu that 
	just showed, then click 'OK'.
	* Now for the field 'Goals', type in 'clean integration-test'.
	* You can ignore the other options and apply your modifications.

	
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
II. Create a new Squash TA Project
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This entry explains how to create a new project. 

NB : If you want download and synchronize with an existing 
project the procedure becomes depend on the tools used by your organization and are not part of this 
readme. You can find easily several how-to dedicated to those tools elsewhere on the internet 
(a quick look at google returns http://www.ibm.com/developerworks/opensource/library/os-ecl-subversion/ 
for SVN, for instance)


1/ Click on File > New > Other.

2/ Unroll 'Maven' in the tree dialog that just showed up, and select 'Maven Project'

3/ A dialog should appear. Leave the first screen untouched and click 'Next'

4/ This is the archetype selection screen. A Maven archetype is basically a project template. Click 
'Add Archetype' and fill the form with the following informations :   

	* Archetype group ID 	: org.squashtest.ta
	* Archetype Artifact ID : squash-ta-project-archetype
	* Archetype Version 	: ${project.version}
	* Repository URL		: http://repo.squashtest.org/maven2/releases/
	
then click 'OK'.

5/ Now select the archetype you just added : uncheck 'Show the last version of archetype only', then type in the filter bar 'org.squashtest.ta'. Select 
the squash TA archetype, version ${project.version} it just found. Click 'Next'.

6/ Configure your project. You may roughly consider that 'artifact Id' is the project name
and 'group ID' is a sort of superproject, or a label, that encompasses it and possibly others.
The package name feature is not used here since you won't have to write java.

7/ Finalize your project creation.



	