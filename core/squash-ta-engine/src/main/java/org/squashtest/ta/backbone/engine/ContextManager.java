/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine;


import java.util.Collection;
import java.util.Map;

import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.event.ContextualStatusUpdateEvent;
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler;
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler;
import org.squashtest.ta.backbone.exception.AmbiguousConversionException;
import org.squashtest.ta.backbone.exception.ElementNotFoundException;
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.test.instructions.ResourceName;

/**
 * <h3> general</h3>
 * <p>The ContextManager is a facade centralizing all resources, repositories, targets, converters
 * and commands that a test could need. It mostly delegates to more specialized classes like
 * {@link ResourceLoader} or {@link ResourceConverterManager}.
 * </p>
 *
 *	<h3>cache </h3>
 * <p>
 * 	A ContextManager will load its resources from the ResourceLoader, and sometimes a cache. When the cache is used the ContextManager 
 * must ensure that it returns a copy of the cached resource, not the resource itself. It helps preventing undesired resources alterations. 
 * </p>
 * 
 * @author bsiri
 *
 */
public interface ContextManager {
	
	/* ******************* lifecycle managment ********************** */
	
	/**
	 * will init the engine components that must be initialized
	 */
	void initAll();
	
	/**
	 * Will reset the engine component that need a reset
	 * 
	 */
	void resetAll();
	
	
	/**
	 * Will clean the engine components that require it
	 */
	void cleanupAll();
	
	
	/* ******************* adder methods **************************** */
	
	/**
	 * register a ResourceRepository.
	 * 
	 * @param repository
	 */
	void registerRepository(RepositoryWrapper repository);
	
	/**
	 * register a target (wrapped in a wrapper actually)
	 * 
	 * @param target
	 */
	void registerTarget(TargetWrapper target);
	
	void registerConverterDefinition(ConverterDefinition definition);
	
	void registerCommandDefinition(CommandDefinition definition); 
	
	void registerAssertionDefinition(BinaryAssertionDefinition definition);
	
	void registerAssertionDefinition(UnaryAssertionDefinition definition);
	
	
	/* ****************** remover methods ******************************* */
	
	
	/**
	 * unregister a ResourceRepository
	 * 
	 * @param repository
	 */
	void unregisterRepository(RepositoryWrapper repository);
	
	/**
	 * unregister a ResourceRepository
	 * 
	 * @param repository
	 */
	void unregisterRepository(String repositoryName);
	
	
	/**
	 * unregister a Target (actually a TargetWrapper)
	 * 
	 * @param target
	 */
	void unregisterTarget(TargetWrapper target);
	
	
	/**
	 * unregister a Target
	 * 
	 * @param target
	 */
	void unregisterTarget(String targetName);	
	
	/**
	 * unregister a ResourceConverterDefinition
	 * 
	 */
	void unregisterConverterDefinition(ConverterDefinition definition);
	
	void unregisterConverterDefinition(Nature input, Nature output, Nature category);
	
	void unregisterCommandDefinition(CommandDefinition definition);
	
	void unregisterCommandDefinition(Nature resourceNature, Nature targetNature, Nature category);
	
	//binary assertions
	void unregisterAssertionDefinition(BinaryAssertionDefinition definition);
	
	void unregisterAssertionDefinition(Nature actualNature, Nature expectedNature, Nature category);
	
	//unary assertions
	void unregisterAssertionDefinition(UnaryAssertionDefinition definition);	
	
	void unregisterAssertionDefinition(Nature actualNature, Nature category);
	
	/**
	 * given it's name, will return a Target
	 * 
	 * @param targetName
	 * @return
	 */
	TargetWrapper getTarget(String targetName);
	
	
	/* *************************** conversion tools ******************** */
	/**
	 * Given the informations contained in the settings, will return all the converters that fits to
	 * the description. Most of the parameters accept null as a wildcards, but the more precise you are
	 * the more the result will be precise. 
	 */
	Collection<ResourceConverterHandler> getConverters(ResourceConversionSettings settings);
	
	/**
	 * <p>Takes a resource and attempts to convert it into something else. Depending on what contains the settings,
	 * if may convert it to a specific resource type, or attempts to convert to the best guess according to the content.</p>
	 * 
	 * <p>If the conversion cannot be performed, an {@link ImpossibleConversionException} will be thrown if no converter
	 * was found corresponding to the description, of a {@link AmbiguousConversionException} if multiple were found.</p>
	 * 
	 * @param resource : the resource we want to convert
	 * @param settings : additional settings to guide the conversion.
	 * @return a converted resource, possibly of the same type, but not of the same instance.
	 * @throws ImpossibleConversionException, AmbiguousConversionException
	 */
	ResourceWrapper convertResource(ResourceWrapper resource, ResourceConversionSettings settings);
	
	
	/**
	 * <p>Given the appropriate settings, returns the appropriate command if found.</p>
	 * <p>If resource nature, target nature, or commandName is null, will return all the commands that match the non null parameters.</p>
	 * 
	 * @return a collection of commands, that may be empty. All commands returned will be loaded with their configuration, but neither with the Target nor the Resource.
	 */
	Collection<CommandHandler> getCommand(CommandFindSettings settings);
	
	
	/* ************************** assertion application ****************** */
	
	/**
	 * <p>Given the appropriate settings, returns the appropriate BINARY assertion if found.</p>
	 * <p>If actual result, expected result, or assertionName is null, will return all the assertion that match the non null parameters.</p>
	 * 
	 * @return a collection of assertion, that may be empty. All assertion returned will be loaded with their configuration, 
	 * but neither with the actual result or the expected result.
	 */
	Collection<BinaryAssertionHandler> getBinaryAssertion(AssertionFindSettings settings);	
	
	/**
	 * <p>Given the appropriate settings, returns the appropriate UNARY assertion if found.</p>
	 * <p>If actual result or assertionName is null, will return all the assertion that match the non null parameters.
	 * In any case, the expected result will be assumed to be {@link BinaryAssertion#UNARY_ASSERTION}</p>
	 * 
	 * @return a collection of assertion, that may be empty. All assertion returned will be loaded with their configuration, 
	 * but neither with the actual result or the expected result.
	 */
	Collection<UnaryAssertionHandler> getUnaryAssertion(AssertionFindSettings settings);
	
	/**
	 * Given it's name, the manager will query it's repositories looking for that resource.
	 * 
	 * @param resourceName
	 * @return a resource, most of the time a FileResource, but may be something else if a refined Resource was present in the 
	 * cache, and in any cases it's a copy of the actual resource.
	 * @throws ElementNotFoundException
	 */
	ResourceWrapper getResource(ResourceName resourceName);
	

	/**
	 * Given it's name, the manager will look for a resource from a particular Repository. It may enhance the speed search
	 * if the Resource is not cached yet.
	 * 
	 * @param resourceName
	 * @param repositoryName
	 * @return a resource, most of the time a FileResource, but may be something else if a refined Resource was present in the 
	 * cache, and in any cases it's a copy of the actual resource.
	 * @throws ElementNotFoundException
	 */
	ResourceWrapper getResource(ResourceName resourceName, String repositoryName);
	
	
	/**
	 * If loading the resource requires any settings, you can pass settings to configure the loading process.
	 * 
	 * @param settings : settings useful when building a resource. 
	 * @return a resource, most of the time a FileResource, but may be something else if a refined Resource was present in the 
	 * cache, and in any cases it's a copy of the actual resource.
	 * @throws ElementNotFoundException
	 */
	ResourceWrapper getResource(ResourceLoadingSettings settings); 
	
	/**
	 * @return a handle to ecosystem resources.
	 */
	Map<ResourceName,ResourceWrapper> getEcosystemResources();
	
	/**
	 * Method for context users to post status update events.
	 * @param event
	 */
	void postEvent(ContextualStatusUpdateEvent<?> event);
}
