/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.verify.cubic;

import java.util.HashMap;
import java.util.Map;

import org.cubictest.model.parameterization.Parameter;
import org.cubictest.model.parameterization.ParameterList;

/**
 * Extracts {@link Parameter} from a Cubic test {@link Test} and exposes them as a map of name - value pairs.
 * Code repackaged from the CATS squashTA pre-version.
 * 
 * @author gf
 * 
 */
public class ParameterListToMapCoercer {
	/**
	 * Retrieve the inputIndex-th value of each parameter and stores it under the parameter's header. If a parameter has
	 * no input for the given index, no entry is stored.
	 * 
	 * @param pParamsList
	 *            Some test parameters from a Cubic ".params" file
	 * @param pInputIndex
	 *            the index of each parameter input that should be used to populate the map.
	 * @return
	 */
	public Map<String, String> coerceToMap(ParameterList pParamsList, int pInputIndex) {
		Map<String, String> res = new HashMap<String, String>();

		for (Parameter param : pParamsList.getParameters()) {
			addInputToMap(param, pInputIndex, res);
		}

		return res;
	}

	private void addInputToMap(Parameter pParam, int pInputIndex, Map<String, String> pMap) {
		if (pInputIndex < pParam.getParameterInputs().size()) {
			String value = pParam.getParameterInput(pInputIndex);
			String key = pParam.getHeader();

			pMap.put(key, value);
		}
	}
}
