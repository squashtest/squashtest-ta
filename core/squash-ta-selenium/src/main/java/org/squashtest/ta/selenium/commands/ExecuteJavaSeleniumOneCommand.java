/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.commands;

import java.util.Collection;

import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.selenium.library.StandardJUnitExecutor;
import org.squashtest.ta.selenium.resources.SeleniumOneJavaTest;
import org.squashtest.ta.selenium.resources.SeleniumResult;

/**
 * <p><strong>Description : </strong>That command will simply run a Selenium 1 test. Actually, it mostly runs a JUnit Test but treats it 
 * as a Selenium Test. It doesn't need any Target, thus the target is VoidTarget.
 *  The result of the java test will be returned as a {@link SeleniumResult}.</p>
 *  
 *  <p><strong>Configuration</strong> : a FileResource which entries are comma separated pairs of <key:value> (note that column ':' is the separator)
 *  
 *  Available options :
 *  
 *  <ul>
 *  	<li>mainclass : the full class name of the main class. Optional, if not supplied it will use on the one set in the Resource instead.</li>
 *  </ul>
 *  
 *  
 *  </p>
 *  <p><strong>DSL example :</strong> <pre>EXECUTE execute WITH mytest.script.selenium USING $(mainclass : my.package.MyClass) AS my.selenium.result</pre></p>
 * 
 * 
 * 
 * @author bsiri
 *
 */


@EngineComponent("execute")
public class ExecuteJavaSeleniumOneCommand implements Command<SeleniumOneJavaTest, VoidTarget> {

	private StandardJUnitExecutor executor = new StandardJUnitExecutor();
	
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		executor.addConfiguration(configuration);
	}

	@Override
	public void setTarget(VoidTarget target) {
		executor.setTarget(target);
	}

	@Override
	public void setResource(SeleniumOneJavaTest resource) {
		executor.setResource(resource);
	}

	@Override
	public SeleniumResult apply() {
		return new SeleniumResult(executor.apply());
	}

	@Override
	public void cleanUp() {
		//nothing yet
	}

}
