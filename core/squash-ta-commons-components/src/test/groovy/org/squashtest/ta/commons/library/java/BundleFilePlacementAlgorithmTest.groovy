/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java

import javax.tools.JavaFileManager.Location;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;

import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.FileTree;

import spock.lang.Specification;

class BundleFilePlacementAlgorithmTest extends Specification{
	def testee
	def location

	def setup(){
		location=new FileTree().createTempDirectory()
		testee=new BundleFilePlacementAlgorithm()
	}
	
	def "files & data should go where we want'em"(){
		given:
			def expectedContent="content"
		and:
			def javaFile=testee.getElementFile(location, "toto.Toto", Kind.CLASS)
			def os=new FileOutputStream(javaFile)
		and:
			def expectedFile=new File(location,"toto.Toto.class")
		when:
			os.write(expectedContent.getBytes("utf-8"))
			os.close()
		then:
			expectedFile.exists()
			BinaryData data=new BinaryData(expectedFile)
			def actual=new String(data.toByteArray(),"utf-8")
			expectedContent.equals(actual)
	}
	
	def cleanup(){
		new FileTree().clean(location)
	}
}
