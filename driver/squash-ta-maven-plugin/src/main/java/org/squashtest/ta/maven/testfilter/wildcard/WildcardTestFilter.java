/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter.wildcard;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.LoggerFactory;

public class WildcardTestFilter implements FilenameFilter {

	private static final Pattern ABSOLUTE_WILDCARD_PATTERN = Pattern.compile("(?:[A-Za-z]:\\\\|/).*");
	private static final String PATH_SEPARATOR_REGEX = "[\\\\/]";
	/** All matchers in order. */
	private List<PathPartMatcher> matcherString;

	/**
	 * Full initialization constructor.
	 * 
	 * @param baseDir
	 *            base directory to use (in case the wildcard is relative).
	 * @param wildcard
	 *            the wildcard specification.
	 */
	public WildcardTestFilter(File baseDir, String wildcard) {
		matcherString = new LinkedList<PathPartMatcher>();
		String[] wildcardElements = wildcard.split(PATH_SEPARATOR_REGEX);
		boolean multiDirectoryPending = false;
		for (String element : wildcardElements) {
			if (element.equals("**")) {//case of ** pattern (must occupy a whole path segment)
				multiDirectoryPending=true;
			} else {
				if (element.indexOf('*') < 0) {//litteral name
					matcherString.add(new LitteralMatcher(element));
				} else {//name with stars in it
					matcherString.add(new JokerPartMatcher(element));
				}
				if(multiDirectoryPending){
					multiDirectoryPending=false;
					int lastElementIndex = matcherString.size()-1;
					PathPartMatcher lastAddedMatcher = matcherString.get(lastElementIndex);
					matcherString.add(lastElementIndex, new MultiDirectoryMatcher(lastAddedMatcher));
				}
			}
		}
		if(ABSOLUTE_WILDCARD_PATTERN.matcher(wildcard).matches()){
			LoggerFactory.getLogger(WildcardTestFilter.class).debug(wildcard+" recognized as absolute.");
		}else if(baseDir!=null){
			String baseDirPath=baseDir.getPath();
			String[] baseDirPathElements=baseDirPath.split(PATH_SEPARATOR_REGEX);
			int index=0;
			for(String element:baseDirPathElements){
				matcherString.add(index++, new LitteralMatcher(element));
			}
		}else{
			PathPartMatcher nextMatcher = matcherString.get(0);
			matcherString.add(0, new MultiDirectoryMatcher(nextMatcher));
		}
	}

	@Override
	public boolean accept(File dir, String name) {
		List<String> pathElements = new LinkedList<String>();
		pathElements.add(0, name);
		File currentFile = dir;
		while (currentFile != null) {
			pathElements.add(0, getWindozawareDirectoryName(currentFile));
			currentFile = currentFile.getParentFile();
		}
		MatchState state = MatchState.initialState(pathElements);
		Iterator<PathPartMatcher> matcherIterator = matcherString.iterator();
		while (state.getMatches() == null && matcherIterator.hasNext()) {
			if (state.getLeftOver().isEmpty()) {
				throw new IllegalStateException(
						"Match computation went through the whole path without reaching a determined result");
			}
			PathPartMatcher matcher = matcherIterator.next();
			state = matcher.matches(state.getLeftOver());
		}
		if (state.getMatches() == null) {
			// if you have run through all matchers without finishing, your path
			// is longer ==> this is a mismatch.
			if (state.getLeftOver().size() > 0) {
				List<String> emptyList = Collections.emptyList();
				state = new MatchState(false, emptyList);
			} else {
				reportComputationFailure(dir, name);
			}
		}
		return state.getMatches();
	}

	/**
	 * Add root name to the path to distinguish files that are not on the same drive on windows!
	 * @param currentFile
	 * @return
	 */
	protected String getWindozawareDirectoryName(File currentFile) {
		String name = currentFile.getName();
		if (name.length() == 0) {
			/*
			 * added for the case of a W$ filesystem root, bit fits in the *nix
			 * root case too.
			 */
			String path = currentFile.getPath();
			name = path.substring(0, path.length() - 1);
		}
		return name;
	}

	/**
	 * Method to throw an exception when the match cannot be decided. Should not happen, but a post match computation check was added to secure the algorithm.
	 * @param dir path to the parent directory of the file.
	 * @param name simple name of the file.
	 */
	private void reportComputationFailure(File dir, String name) {
		StringBuilder message = new StringBuilder(
				"Undetermined wilcard match for '/");
		for (PathPartMatcher matche : matcherString) {
			message.append(matche.toPattern()).append("/");
		}
		if (matcherString.size() > 0) {
			message.setLength(message.length() - 1);
		}
		message.append("' on file ").append(dir.getPath())
				.append(File.separator).append(name);
		throw new IllegalStateException(message.toString());
	}
}