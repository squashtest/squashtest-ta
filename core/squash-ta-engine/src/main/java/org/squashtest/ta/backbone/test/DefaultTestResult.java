/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.test;

import java.util.Collection;
import java.util.Date;

import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.Phase;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.TestResult;

public class DefaultTestResult implements TestResult {

	private String testName;
	private Date startTime;
	private Date endTime;
	
	private int totalCommands=0;
	private int totalAssertions=0;
	private int totalInstructions=0;
	
	private GeneralStatus testStatus=GeneralStatus.SUCCESS;
	
	private DefaultStatusSummary report=new DefaultStatusSummary();
	
	/* ************** API part **************** */
	
	public String getName() {
		return testName;
	}

	public Date startTime() {
		return startTime;
	}

	public Date endTime() {
		return endTime;
	}

	public int getTotalCommands() {
		return totalCommands;
	}

	public int getTotalAssertions() {
		return totalAssertions;
	}
	
	public int getTotalInstructions(){
		return totalInstructions;
	}

	public GeneralStatus getStatus() {
		return testStatus;
	}
	
	public ExecutionDetails getFailureReport() {
		return report.getFailureDetails();
	}
	
	/* ****************** setters and utility *************** */
	
	
	public void setName(String name){
		this.testName = name;
	}
	
	public void setStartTime(Date startTime){
		this.startTime = startTime;
	}
	
	public void setEndTime(Date endTime){
		this.endTime = endTime;
	}
	
	public void setTotalCommands(int total){
		totalCommands = total;
	}
	
	public void incrTotalCommands(){
		totalCommands++;
	}
	
	public void setTotalAssertions(int total){
		totalAssertions=total;
	}
	
	public void incrTotalAssertions(){
		totalAssertions++;
	}
	
	public void setTotalInstruction(int total){
		this.totalInstructions=total;
	}
		
	public void updateWith(ExecutionDetails details,GeneralStatus newStatus){
			if(details.instructionType()==InstructionType.ASSERT){
				incrTotalAssertions();
			}else if(details.instructionType()==InstructionType.COMMAND){
				incrTotalCommands();
			}
			testStatus=newStatus;
			totalInstructions++;
	}

	public void fail(ExecutionDetails details, GeneralStatus newStatus, Integer localContextId,Phase currentPhase) {
		testStatus=newStatus;
		report.fail(details,localContextId ,currentPhase);
	}
	
	@Override
	public void cleanUp() {
		if (getFailureReport() != null) {
			Collection<ResourceAndContext> failureContext = getFailureReport()
					.getResourcesAndContext();
			if (failureContext != null) {
				cleanupContext(failureContext);
			}
			if(getFailureReport().caughtException() instanceof AssertionFailedException){
				AssertionFailedException assertionFailedException = (AssertionFailedException)getFailureReport().caughtException();
				assertionFailedException.getActual().cleanUp();
				cleanupContext(assertionFailedException.getFailureContext());
			}
		}
	}

	private void cleanupContext(
			Collection<ResourceAndContext> failureContext) {
		for(ResourceAndContext context:failureContext){
			Resource<?> resource = context.resource;
			if(resource!=null){
				resource.cleanUp();
			}
		}
	}
}
