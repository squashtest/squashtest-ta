/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine;

import java.util.Collection;

import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;

public interface CommandManager {

	/**
	 * Will refuse to perform this operation if a factory having the same resource nature, target nature and name 
	 * is already present.
	 * 
	 * @param factory
	 */
	void addCommandDefinition(CommandDefinition factory);
	
	void removeCommandDefinition(CommandDefinition factory);
	
	void removeCommandDefinition(Nature resourceNature, Nature targetNature, Nature category);
	
	/**
	 * <p>
	 * 	Depending on the given parameter, will return a list of new command instances (with empty configuration).
	 * 	Each parameter accepts null, but the more the caller provides the more precise will be the result. 
	 *  Theoretically if the user supplies all the arguments the returned list shall be of size 0 of 1. 
	 * </p>
	 * 
	 * 
	 * @param resourceNature
	 * @param targetNature
	 * @param name
	 * @return a Collection of commands fitting the description (never null).
	 */
	Collection<CommandHandler> getAllCommands(Nature resourceNature, Nature targetNature, Nature category);
	
	Collection<CommandHandler> getAllCommandsByName(String resourceNatureName, String targetNatureName, String categoryName);
}
