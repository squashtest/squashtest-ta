/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter.wildcard;

import java.util.Collections;
import java.util.List;

/**
 * Some common behavior for {@link PathPartMatcher} implementations.
 * @author edegenetais
 *
 */
public abstract class AbstractMatcherBase implements PathPartMatcher{

	/**
	 * Pre-typed emptyList to simplify its use...
	 * @return
	 */
	protected List<String> emptyList() {
		List<String> emptyList = Collections.emptyList();
		//local here is created to work around some javac/ejc generic parsing quirk...
		return emptyList;//NOSONAR
	}

	/**
	 * Extract the leftover from the current path.
	 * @param path current path.
	 * @param offset number of elements treated by the operation.
	 * @return a sublist with the path left over for treatment by the opertation.
	 */
	protected List<String> getSubPath(List<String> path, int offset) {
		return path.subList(
				offset, path.size());
	}
	
	@Override
	public String toString() {
		return super.toString()+"{"+toPattern()+"}";
	}

}