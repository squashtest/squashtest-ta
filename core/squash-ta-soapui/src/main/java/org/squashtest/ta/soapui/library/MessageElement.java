/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.soapui.library;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eviware.soapui.model.testsuite.TestRunner.Status;

final class MessageElement implements Element {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageElement.class);
	private StringBuilder sb=new StringBuilder();
	@Override
	public Element add(String input, Map<String, Status> statusMap,
			Map<String, String> messageMap) {
		Element el;
		if(input.startsWith("ESCAPE:")){
			/*
			 * As we are unable to anticipate message contents, we need a crude escaping mechanisme to avoid
			 * construing some token-like message content as a token...
			 */
			String messageLine = input.substring("ESCAPE:".length());
			sb.append(messageLine).append("\n");
			el=this;
			LOGGER.debug("Message line: "+messageLine);
		}else if(input.startsWith("ENDMESSAGE:")){
			String testName = input.substring("ENDMESSAGE:".length());
			messageMap.put(testName, sb.toString());
			el=new IntervalElement();
			LOGGER.debug("Test name: "+testName);
		}else{
			sb.append(input).append("\n");
			el=this;
			LOGGER.debug("Message line: "+input);
		}
		return el;
	}
}