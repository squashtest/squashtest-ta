/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.framework.exception;

/**
 * Mother of all exceptions related to a test error due to misconfiguration or
 * misconception of the test.
 * 
 * 
 * @author bsiri
 * 
 */
public class BrokenTestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -809853483277368638L;

	public BrokenTestException() {
	}

	public BrokenTestException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public BrokenTestException(String arg0) {
		super(arg0);
	}

	public BrokenTestException(Throwable arg0) {
		super(arg0);
	}

}
