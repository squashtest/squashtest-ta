/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.ssh;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.connection.channel.direct.Session.Command;
import net.schmizz.sshj.transport.TransportException;
import net.schmizz.sshj.transport.verification.HostKeyVerifier;
import net.schmizz.sshj.userauth.UserAuthException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.shell.ShellResult;

/**
 * SSH Client code ported from CATS.
 * This class will become the SSH client, and only the SSH client.
 * Its other responsibilities will be splitted to other classes.
 * @author edegenetais
 * @author Agnes
 */
public class SquashSSHClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(SquashSSHClient.class);
    
    private HostKeyVerifier hostKeyVerifier= new DefaultHostKeyVerifier();

    private String hostname;
    
	private Integer port;
    
    private String username;
    
    private String password;
    
    private SSHClient sshClient=new SSHClient();
    
    /**
     * Constructor with complete initialization.
	 * @param hostname
	 * @param port
	 * @param username
	 * @param password
	 */
	public SquashSSHClient(String hostname, Integer port, String username,
			String password) {
		this.hostname = hostname;
		this.port = port;
		this.username = username;
		this.password = password;
		sshClient.addHostKeyVerifier(hostKeyVerifier);
	}
    
	/**
	 * Reset host key verifier.
	 * @param hostKeyVerifier the new host key verifier to set.
	 * @throws IOException in case of error during disconnection of the previous client, if it existed and was connected.
	 */
	public void setHostKeyVerifier(HostKeyVerifier hostKeyVerifier) throws IOException {
		this.hostKeyVerifier = hostKeyVerifier;
		if(sshClient!=null){
			if(sshClient.isConnected()){
				sshClient.disconnect();
			}
		}
	}
	
    /**
     * Runs a SSH command, reading the connection parameters from the specified
     * configuration file. If the exit status is 0 (= "success"), checks whether
     * the contents of the output and error streams match the patterns
     * specified.
     * 
     * @param command Command to execute
     * @param outputStreamPattern Pattern to match against the output stream. If
     *            <code>null</code> or empty, the output stream is not checked.
     * @param errorStreamPattern Pattern to match against the error stream. If
     *            <code>null</code> or empty, the error stream is not checked.
     * @throws IOException
     */
    public ShellResult runSSHCommand(String command,Integer timeOut)
                    throws IOException {
    	ShellResult executionResult;
        if(!sshClient.isConnected()){
        	connect();
        }
		try {
			authenticate();
		    final Session session = sshClient.startSession();
		    try {
		        final Command cmd = session.exec(command);
		        if(timeOut==null){
		        	LOGGER.debug("Applying default timeout.");
		        	cmd.join(5, TimeUnit.SECONDS);
		        }else{
		        	LOGGER.debug("Applying specified timeout: "+timeOut+" ms.");
		        	cmd.join(timeOut.intValue(), TimeUnit.MILLISECONDS);
		        }
		        Integer exitStatus = cmd.getExitStatus();
		        if (exitStatus == null) {
		            throw new SSHCommandException("Could not retrieve exit status for command {}", command);
		        }
		        String outputStream = IOUtils.readFully(cmd.getInputStream()).toString();
		        String errorStream = IOUtils.readFully(cmd.getErrorStream()).toString();
		        int exitValue=exitStatus.intValue();
		        executionResult=new ShellResult(exitValue, outputStream, errorStream, command);
		        LOGGER.debug("Exit code="+exitValue);
		        LOGGER.debug("Output stream = " + outputStream);
		        LOGGER.debug("Error stream = " + errorStream);
		    } finally {
		        session.close();
		    }
		} finally {
		    sshClient.disconnect();
		}
		return executionResult;
    }

    /**
     * Connect the ssh client to host.
     * @throws IOException thrown when SSH i/o fails.
     */
	public void connect() throws IOException {
        if(port==null){
            sshClient.connect(hostname);
        } else {
            sshClient.connect(hostname, port);
        }
    }

	 /**
     * Disconnect the ssh client.
     * @throws IOException thrown when SSH i/o fails.
     */
	public void dispose() throws IOException{
		if(sshClient.isConnected()){
			sshClient.disconnect();
		}
	}
	
    public void authenticate() throws UserAuthException,
			TransportException {
		if(!sshClient.isAuthenticated()){
			sshClient.authPassword(username, password);
		}
	}
    
}
