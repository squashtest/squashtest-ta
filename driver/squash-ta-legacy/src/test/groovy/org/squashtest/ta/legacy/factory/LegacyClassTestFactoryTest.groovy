/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.factory

import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.legacy.exception.MalformedCatsClassException;
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestBatchTestCase;
import org.squashtest.ta.legacy.test.factorydummies.TestFactoryClassTestCase;

import spock.lang.Specification

class LegacyClassTestFactoryTest extends Specification {
	def testee
	def noparm
	def setup(){
		testee=new LegacyClassTestFactory()
		noparm=new Class<?>[0]
	}
	
	def "test methods are void methods"(){
		given:
			Class<AbstractCatsTestCase> testClass=new AbstractCatsTestCase(){
				public String brokenTestMethod(){
					return "broken";
				}
			}.getClass()
		and:
			def method=testClass.getMethod("brokenTestMethod", noparm)
			AbstractCatsTestCase instance=testClass.newInstance()
		when:
			testee.buildTest(method,instance)
		then:
			thrown MalformedCatsClassException
	}
	
	def "test methods are noarg methods"(){
		given:
			Class<AbstractCatsTestCase> testClass=new AbstractCatsTestCase(){
				public void brokenTestMethod(String snare){
				}
			}.getClass()
		and:
			def unwelcomeParms=new Class<?>[1]
			unwelcomeParms[0]=String.class
			def method=testClass.getMethod("brokenTestMethod", unwelcomeParms)
			AbstractCatsTestCase instance=testClass.newInstance()
		when:
			testee.buildTest(method,instance)
		then:
			thrown MalformedCatsClassException
	}
	
	def "should create a test which name is that of the method"(){
		given:
			def testClass=LegacyTestBatchTestCase.class
		and:
			def method=testClass.getMethod("thatIsATestCase", noparm)
		and:
			def instance=testClass.newInstance()
		when:
			def result=testee.buildTest(method,instance)
		then:
			method.getName().equals(result.getName())
	}
	
	def "should add any @Before method to the test setup"(){
		given:
			def testClass=TestFactoryClassTestCase.class
			def instance=testClass.newInstance()
		and:
			def method=testClass.getMethod("testDummy", noparm)
		when:
			def result=testee.buildTest(method,instance)
		then:
			def iter=result.getSetup()
			iter.hasNext()==true
			def setup1=iter.next()
			setup1==TestFactoryClassTestCase.SETUP1_INSTR || setup1==TestFactoryClassTestCase.SETUP2_INSTR
			iter.hasNext()==true
			def setup2=iter.next()
			setup2!=setup1
			setup2==TestFactoryClassTestCase.SETUP1_INSTR || setup2==TestFactoryClassTestCase.SETUP2_INSTR
			iter.hasNext()==false
	}
	
	def "no @Before method should go into test phase test instructions"(){
		given:
			def testClass=TestFactoryClassTestCase.class
			def instance=testClass.newInstance()
		and:
			def method=testClass.getMethod("testDummy", noparm)
		when:
			def result=testee.buildTest(method,instance)
		then:
			def testIncludesSetup=false
			def iter=result.getTests()
			while(iter.hasNext()){
				def instru=iter.next()
				if(instru==TestFactoryClassTestCase.SETUP1_INSTR || instru==TestFactoryClassTestCase.SETUP2_INSTR){
					testIncludesSetup=true
				}
			}
			testIncludesSetup==false
	}
	
	def "no @Before method should go into teardown phase test instructions"(){
		given:
			def testClass=TestFactoryClassTestCase.class
			def instance=testClass.newInstance()
		and:
			def method=testClass.getMethod("testDummy", noparm)
		when:
			def result=testee.buildTest(method,instance)
		then:
			def teardownIncludesSetup=false
			def iter=result.getTeardown()
			while(iter.hasNext()){
				def instru=iter.next()
				if(instru==TestFactoryClassTestCase.SETUP1_INSTR || instru==TestFactoryClassTestCase.SETUP2_INSTR){
					teardownIncludesSetup=true
				}
			}
			teardownIncludesSetup==false
	}
	
	def "should add any @After method to the test teardown"(){
		given:
			def testClass=TestFactoryClassTestCase.class
			def instance=testClass.newInstance()
		and:
			def method=testClass.getMethod("testDummy", noparm)
		when:
			def result=testee.buildTest(method,instance)
		then:
			def iter=result.getTeardown()
			iter.hasNext()==true
			def teardown1=iter.next()
			teardown1==TestFactoryClassTestCase.TEARDOWN1_INSTR || teardown1==TestFactoryClassTestCase.TEARDOWN2_INSTR
			iter.hasNext()==true
			def teardown2=iter.next()
			teardown2!=teardown1
			teardown2==TestFactoryClassTestCase.TEARDOWN1_INSTR || teardown2==TestFactoryClassTestCase.TEARDOWN2_INSTR
			iter.hasNext()==false
	}
	
	def "no @After method should go into setup phase test instructions"(){
		given:
			def testClass=TestFactoryClassTestCase.class
			def instance=testClass.newInstance()
		and:
			def method=testClass.getMethod("testDummy", noparm)
		when:
			def result=testee.buildTest(method,instance)
		then:
			def testIncludesSetup=false
			def iter=result.getSetup()
			while(iter.hasNext()){
				def instru=iter.next()
				if(instru==TestFactoryClassTestCase.TEARDOWN1_INSTR || instru==TestFactoryClassTestCase.TEARDOWN2_INSTR){
					testIncludesSetup=true
				}
			}
			testIncludesSetup==false
	}
	
	def "no @After method should go into test phase test instructions"(){
		given:
			def testClass=TestFactoryClassTestCase.class
			def instance=testClass.newInstance()
		and:
			def method=testClass.getMethod("testDummy", noparm)
		when:
			def result=testee.buildTest(method,instance)
		then:
			def teardownIncludesSetup=false
			def iter=result.getTests()
			while(iter.hasNext()){
				def instru=iter.next()
				if(instru==TestFactoryClassTestCase.TEARDOWN1_INSTR || instru==TestFactoryClassTestCase.TEARDOWN2_INSTR){
					teardownIncludesSetup=true
				}
			}
			teardownIncludesSetup==false
	}
	
	def "no @BeforeClass method should go into any phase test instructions"(){
		given:
			def testClass=TestFactoryClassTestCase.class
			def instance=testClass.newInstance()
		and:
			def method=testClass.getMethod("testDummy", noparm)
		when:
			def result=testee.buildTest(method,instance)
		then:
			def teardownIncludesSetup=false
			def iter=result.getSetup()
			while(iter.hasNext()){
				def instru=iter.next()
				if(instru==TestFactoryClassTestCase.SETUP_SNARE_INSTR){
					teardownIncludesSetup=true
				}
			}
			def iter2=result.getTests()
			while(iter2.hasNext()){
				def instru=iter2.next()
				if(instru==TestFactoryClassTestCase.SETUP_SNARE_INSTR){
					teardownIncludesSetup=true
				}
			}
			def iter3=result.getTeardown()
			while(iter3.hasNext()){
				def instru=iter3.next()
				if(instru==TestFactoryClassTestCase.SETUP_SNARE_INSTR){
					teardownIncludesSetup=true
				}
			}
			teardownIncludesSetup==false
	}
	
	def "no @AfterClass method should go into any phase test instructions"(){
		given:
			def testClass=TestFactoryClassTestCase.class
			def instance=testClass.newInstance()
		and:
			def method=testClass.getMethod("testDummy", noparm)
		when:
			def result=testee.buildTest(method,instance)
		then:
			def teardownIncludesSetup=false
			def iter=result.getSetup()
			while(iter.hasNext()){
				def instru=iter.next()
				if(instru==TestFactoryClassTestCase.TEARDOWN_SNARE_INSTR){
					teardownIncludesSetup=true
				}
			}
			def iter2=result.getTests()
			while(iter2.hasNext()){
				def instru=iter2.next()
				if(instru==TestFactoryClassTestCase.TEARDOWN_SNARE_INSTR){
					teardownIncludesSetup=true
				}
			}
			def iter3=result.getTeardown()
			while(iter3.hasNext()){
				def instru=iter3.next()
				if(instru==TestFactoryClassTestCase.TEARDOWN_SNARE_INSTR){
					teardownIncludesSetup=true
				}
			}
			teardownIncludesSetup==false
	}
}
