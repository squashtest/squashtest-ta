/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.templates;

import static org.junit.Assert.*;

import org.squashtest.ta.core.templates.FileBasedCreator;

import spock.lang.Specification;

class FileBasedCreatorTest extends Specification {

	def creator = new FileBasedCreator(){};


	
	def "should get a file normally"(){
		given :
			def url = new URL("file:/da/resource")
			
		when :
			def file = creator.getFileOrNull(url)
		
		then :
			file != null
	}
	
	def "should fail to fetch a file because of protocol"(){
		given :
			def url = new URL("http:/da/resource")
		
		when :
			def file = creator.getFileOrNull(url)
		
		then :
			file == null
	}
	
	def "should get a file normally (2)"(){
		given :
		def url = new URL("file:/da/resource")
		
	when :
		def file = creator.getFileOrFail(url)
	
	then :
		file != null
	}
	
	def "should rant because of the protocol"(){
		given :
			def url = new URL("http:/da/resource")
		
		when :
			def file = creator.getFileOrFail(url)
		
		then :
			thrown URISyntaxException //that's what it is, right?
	}

	
}
