/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.squashtest.ta.commons.resources.PropertiesResource;
import org.squashtest.ta.core.tools.io.PropertiesLoader;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;

/**
 * File To Properties Resource
 * Converts a Properties File entry into a pseudo primary key dbunit filter
 * <p>Every line of the properties file must have the following specifications :<p>
 * <ul>
 * 	<li>The property name</li>
 * 	<li>spaces or tabs</li>
 *  <li>=</li>
 *  <li>spaces or tabs</li>
 *  <li>The property value</li>
 * </ul>
 * 
 * @author fgaillard
 *
 */
@EngineComponent("structured")
public class FileToProperties implements ResourceConverter<FileResource, PropertiesResource> {

	private static final Pattern VALID_START_MULTILINE_PROPERTY = Pattern.compile("^.*\\\\$");
	private static final Pattern VALID_CONTINUE_MULTILINE_PROPERTY = Pattern.compile("^.*[^,]$");
	private static final Pattern VALID_PROPERTY_LINE = Pattern.compile("^.*=.*[^,]$");
	private static final Pattern VALID_COMMENT_LINE = Pattern.compile("^#.*$");
	private static final PropertiesLoader PROPERTIES_LOADER=new PropertiesLoader();
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public FileToProperties(){}
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		//Noop		
	}

	@Override
	public PropertiesResource convert(FileResource resource) {
		Properties properties;
		//we want something like 
		//MY_TABLE  MY_FIRST_PRIMARY_KEY,MY_SECOND_PRIMARY_KEY,....
		
		try
		{
			SimpleLinesData data=new SimpleLinesData(resource.getFile().getPath());
			List<String> lines=data.getLines();
			boolean previousLineEndsWithBackSlash = false;
			for(String line:lines){
				previousLineEndsWithBackSlash = checkLineValidity(
						previousLineEndsWithBackSlash, line);
			}
			properties=PROPERTIES_LOADER.load(resource.getFile());
		} catch (FileNotFoundException fnfe) {
			throw new BadDataException("Properties file not found", fnfe);
		} catch (IOException ioe) {
			throw new BadDataException("Wrongly Configured properties file", ioe);
		}
		return new PropertiesResource(properties);
	}

	/**
	 * Check wether the current line is a valid properties file line, based on its content and wehter we are in a multiline or not.
	 * @param previousLineEndsWithBackSlash if this boolean indicator is <code>true</code>, then we are in a multiline property declaration.
	 * @param line current line content
	 * @return <code>true</code> if the current line is valid, <code>false</code> other wise.
	 */
	private boolean checkLineValidity(boolean previousLineEndsWithBackSlash,
			String line) {
		if (!(VALID_COMMENT_LINE.matcher(line).matches() || VALID_PROPERTY_LINE.matcher(line).matches())){
			//We check if previous line ended with a backslash
			if (previousLineEndsWithBackSlash){
				if (!(VALID_COMMENT_LINE.matcher(line).matches() || VALID_CONTINUE_MULTILINE_PROPERTY.matcher(line).matches())){
					throw new BadDataException("Wrongly Configured properties file");
				}
			} else {
				throw new BadDataException("Wrongly Configured properties file");
			}
		}
		if (VALID_START_MULTILINE_PROPERTY.matcher(line).matches()){
			previousLineEndsWithBackSlash = true;
		} else {
			previousLineEndsWithBackSlash = false;
		}
		return previousLineEndsWithBackSlash;
	}

	@Override
	public void cleanUp() {
		//noop
	}
	
}
