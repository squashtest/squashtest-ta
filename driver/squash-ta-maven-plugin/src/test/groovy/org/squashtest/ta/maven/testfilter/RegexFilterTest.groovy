/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter

import spock.lang.Specification;

class RegexFilterTest extends Specification{
	RegexFileFilter testee
	File baseDir
	String baseDirRegex
	String rootPathRegex
	String fileSepRegex
	
	def setup(){
		baseDir=new File("/home/testbase")
		baseDirRegex=baseDir.getAbsolutePath().replace("\\", "\\\\")
		fileSepRegex=File.separator.replace("\\", "\\\\")
		
		if(!baseDirRegex.endsWith(fileSepRegex)){
			baseDirRegex=baseDirRegex+fileSepRegex
		}
		
		rootPathRegex=new File("/").getAbsolutePath().replace("\\", "\\\\")
	}
	
	def "match OK relative path"(){
		given:
			String regex="found"+fileSepRegex+".*\\.txt"
			testee=new RegexFileFilter(baseDir, regex)
		and:
			File dir=new File(baseDir,"found")
			String name="test.txt"
		when:
			def accept=testee.accept(dir, name)
		then:
			accept
	}
	
	def "reject relative path out of base directory"(){
		given:
			String regex="found"+fileSepRegex+".*\\.txt"
			testee=new RegexFileFilter(baseDir, regex)
		and:
			File dir=new File("/home/other","found")
			String name="test.txt"
		when:
			def accept=testee.accept(dir, name)
		then:
			!accept
	}
	
	def "reject relative path in base directory not fulfilling regex"(){
		given:
			String regex="found"+fileSepRegex+".*\\.txt"
			testee=new RegexFileFilter(baseDir, regex)
		and:
			File dir=new File(baseDir,"found")
			String name="test_txt"
		when:
			def accept=testee.accept(dir, name)
		then:
			!accept
	}
	
	def "match path fulfilling absolute regex in basedir"(){
		given:
			String regex=baseDirRegex+"found"+fileSepRegex+".*\\.txt"
			testee=new RegexFileFilter(baseDir, regex)
		and:
			File dir=new File(baseDir,"found")
			String name="test.txt"
		when:
			def accept=testee.accept(dir, name)
		then:
			accept
	}
	
	def "match path fulfilling absolute regex out of basedir"(){
		given:
			String regex=rootPathRegex+"home"+fileSepRegex+"other"+fileSepRegex+"found"+fileSepRegex+".*\\.txt"
			testee=new RegexFileFilter(baseDir, regex)
		and:
			File dir=new File("/home/other/found")
			String name="test.txt"
		when:
			def accept=testee.accept(dir, name)
		then:
			accept
	}
	
	def "reject path in base directory not matching absolute regex conform to base"(){
		given:
			String regex=baseDirRegex+"found"+fileSepRegex+".*\\.txt"
			testee=new RegexFileFilter(baseDir, regex)
		and:
			File dir=new File(baseDir,"found")
			String name="test_txt"
		when:
			def accept=testee.accept(dir, name)
		then:
			!accept
	}
	
	def "reject path in base directory not matching absolute regex out of base"(){
		given:
			String regex=rootPathRegex+"home"+fileSepRegex+"other"+fileSepRegex+"found"+fileSepRegex+".*\\.txt"
			testee=new RegexFileFilter(baseDir, regex)
		and:
			File dir=new File(baseDir,"found")
			String name="test_txt"
		when:
			def accept=testee.accept(dir, name)
		then:
			!accept
	}
}
