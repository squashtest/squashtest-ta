/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.editors;

import org.eclipse.osgi.util.NLS;

public class ProposalMessages extends NLS {
	private static final String BUNDLE_NAME = "org.squashtest.ta.eclipse.plugin.editors.proposal_messages"; //$NON-NLS-1$
	
	public static String PhaseProposalSetupValue;
	public static String PhaseProposalSetupDesc;
	public static String PhaseProposalSetupDoc;
	
	public static String PhaseProposalTestValue;
	public static String PhaseProposalTestDesc;
	public static String PhaseProposalTestDoc;
	
	public static String PhaseProposalTeardownDesc;
	public static String PhaseProposalTeardownValue;
	public static String PhaseProposalTeardownDoc;
	
	public static String HeadProposalDefineValue;
	public static String HeadProposalDefineDesc;
	public static String HeadProposalDefineDoc;
	
	public static String HeadProposalLoadValue;
	public static String HeadProposalLoadDesc;
	public static String HeadProposalLoadDoc;

	public static String HeadProposalConverterValue;
	public static String HeadProposalConverterDesc;
	public static String HeadProposalConverterDoc;
	
	public static String HeadProposalExecuteValue;
	public static String HeadProposalExecuteDesc;
	public static String HeadProposalExecuteDoc;
	
	public static String HeadProposalAssertValue;
	public static String HeadProposalAssertDesc;
	public static String HeadProposalAssertDoc;
	
	public static String ClauseProposalAsValue;
	public static String ClauseProposalAsDesc;
	public static String ClauseProposalAsDoc;
	
	public static String ClauseProposalFromValue;
	public static String ClauseProposalFromDesc;
	public static String ClauseProposalFromDoc;
	
	public static String ClauseProposalToValue;
	public static String ClauseProposalToDesc;
	public static String ClauseProposalToDoc;
	
	public static String ClauseProposalUsingValue;
	public static String ClauseProposalUsingDesc;
	public static String ClauseProposalUsingDoc;
	
	public static String ClauseProposalWithValue;
	public static String ClauseProposalWithDesc;
	public static String ClauseProposalWithDoc;
	
	public static String ClauseProposalOnValue;
	public static String ClauseProposalOnDesc;
	public static String ClauseProposalOnDoc;
	
	public static String ClauseProposalIsValue;
	public static String ClauseProposalIsDesc;
	public static String ClauseProposalIsDoc;
	
	public static String ClauseProposalHasValue;
	public static String ClauseProposalHasDesc;
	public static String ClauseProposalHasDoc;
	
	public static String ClauseProposalDoesValue;
	public static String ClauseProposalDoesDesc;
	public static String ClauseProposalDoesDoc;
	
	public static String ClauseProposalThanValue;
	public static String ClauseProposalThanDesc;
	public static String ClauseProposalThanDoc;
	
	public static String ClauseProposalTheValue;
	public static String ClauseProposalTheDesc;
	public static String ClauseProposalTheDoc;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, ProposalMessages.class);
	}

	private ProposalMessages() {
	}
}
