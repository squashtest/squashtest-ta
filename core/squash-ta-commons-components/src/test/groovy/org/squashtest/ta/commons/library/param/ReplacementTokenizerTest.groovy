/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.param

import org.squashtest.ta.core.tools.io.SimpleLinesData;

import spock.lang.Specification

class ReplacementTokenizerTest extends Specification {
	def testee
	def spyParser
	def setup(){
		spyParser=Mock(ExpressionParser)
		spyParser.accept("target")>>true
		spyParser.parse("target")>>new LitteralExpression("parsed")
		testee=new ReplacementTokenizer([spyParser,new EntityExpressionParser()])
	}
	def "a fixed text with no litterals is unchanged"(){
		given:
		  def data=new SimpleLinesData(["this is me, I'm plain text"]);
		when:
		  def result=testee.tokenize(data)
		then:
			result.size()==1
			"this is me, I'm plain text\n".equals(createOutputString(result))
			0 * spyParser.accept(_)
	}
	
	def "a fixed text with one escaped placeholder (@\${something}@) is fragmented in 3"(){
		given:
		  def data=new SimpleLinesData(["He said @\${target}@..."]);
		when:
		  def result=testee.tokenize(data)
		then:
			result.size()==3
			"He said \${target}...\n".equals(createOutputString(result))
			0 * spyParser.accept("target")
	}
	
	def "find parseable between litterals"(){
		given:
		def data=new SimpleLinesData(["@\${litteral1}@ fixed @\${litteral2}@"]);
	  when:
		def result=testee.tokenize(data)
	  then:
		  result.size()==4
		  "\${litteral1} fixed \${litteral2}\n".equals(createOutputString(result))
		  0 * spyParser.accept("litteral1")
		  0 * spyParser.accept("litteral2")
	}
	
	def "find place holder in a simple case"(){
	  given:
		def data=new SimpleLinesData(["I found \${name} was here when I came back!"]);
	  when:
		def result=testee.tokenize(data)
	  then:
	  	1 * spyParser.accept("name")>>true
		1 * spyParser.parse("name")>>new LitteralExpression("Bill")
		result.size()==3
		"I found Bill was here when I came back!\n".equals(createOutputString(result))
		  
	}
	
	def "but DO interpret if it is before"(){
      given:
		def data=new SimpleLinesData(["I found \${name}, howling @\${my friend from Lamotte-Beuvron!}@ when I came back!"]);
	  when:
		def result=testee.tokenize(data)
	  then:
		1 * spyParser.accept("name")>>true
		1 * spyParser.parse("name")>>new LitteralExpression("Bill")
		result.size()==5
		"I found Bill, howling \${my friend from Lamotte-Beuvron!} when I came back!\n".equals(createOutputString(result))
	}
	
	def "but DO interpret if it is after"(){
		given:
		  def data=new SimpleLinesData(["I found @\${my friend from Lamotte-Beuvron!}@, \${name}, howling  when I came back!"]);
		when:
		  def result=testee.tokenize(data)
		then:
		  1 * spyParser.accept("name")>>true
		  1 * spyParser.parse("name")>>new LitteralExpression("Bill")
		  result.size()==5
		  "I found \${my friend from Lamotte-Beuvron!}, Bill, howling  when I came back!\n".equals(createOutputString(result))
	  }
	
	def "but DO interpret if it is in between"(){
		given:
		  def data=new SimpleLinesData(["He said @\${Go!}@ at \${time}. I answered @\${No way!}@"]);
		when:
		  def result=testee.tokenize(data)
		then:
		  1 * spyParser.accept("time")>>true
		  1 * spyParser.parse("time")>>new LitteralExpression("12:00")
		  result.size()==7
		  "He said \${Go!} at 12:00. I answered \${No way!}\n".equals(createOutputString(result))
	  }

		def "transmit as is if unrecognized expression"(){
		  given:
			def data=new SimpleLinesData(["Woo \${unrecognized}"]);
		  and:
		  	spyParser.accept("unrecognized")>>false
		  when:
			def result=testee.tokenize(data)
		  then:
		  	"Woo \${unrecognized}\n".equals(createOutputString(result))
		}
		
		def "computing a date between @@ is possible with \${at}"(){
			given:
			  def data=new SimpleLinesData(["Woo \${at}\${time}\${at}"]);
			when:
			  def result=testee.tokenize(data)
			then:
				1 * spyParser.accept("time")>>true
				1 * spyParser.parse("time")>>new LitteralExpression("12:00")
				
				"Woo @12:00@\n".equals(createOutputString(result))
		  }
		
		String createOutputString(List<Expression> result){
			StringBuilder resultBuilder=new StringBuilder()
			for(Expression e:result){
				e.evaluate(resultBuilder)
			}
			return resultBuilder.toString()
		}
}
