/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.resources.BundleResource;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;



/**
 * <p><strong>Description : </strong>That converter will "cast" a FileResource to a {@link BundleResource}. It makes little checks, as it 
 * simply ensures first that the FileResource is suitable enough, ie is actually pointing to a directory.
 *   </p>
 * 
 * 
 *  <p><strong>Configuration</strong> : (optional) a FileResource which entries are comma separated pairs of <key:value> (note that column ':' is the separator)
 *  
 *  Available options :
 *  
 *  <ul>
 *  	<li>mainpath : the path of the 'main file' of that bundle. The bundle doesn't care what it does mean : it's up to the 
 *  converter/command/etc that will use it in the future to interpret what the 'main file' is. The path MUST be relative to the base
 *  directory of that bundle, assuming that the base directory is the directory that the input Resource points to.</li>
 *  </ul>
 *  
 *  
 *  </p>
 *  <p><strong>DSL example :</strong> <pre>CONVERT myfile.file TO bundle(unchecked) USING $(mainpath : main/mainfile.txt) AS new.bundle</pre></p>
 * 
 * 
 * @author bsiri
 *
 */

@EngineComponent("unchecked")
public class FileToBundle implements ResourceConverter<FileResource, BundleResource> {

	private static final String MAINPATH_KEY = "mainpath";


	public static final Logger logger = LoggerFactory.getLogger(FileToBundle.class);
	
	
	private String mainPath = null;
	
	
	private Collection<Resource<?>> configuration = new ArrayList<Resource<?>>();
	
	/**
	 * A directory is always eligible as a Bundle, but it makes that converter not specific enough. Hence the low
	 * score here. 
	 * 
	 */
	@Override
	public float rateRelevance(FileResource input) {
		if (isValidInput(input)){
			return 0.2f;
		}else{
			return 0.0f;
		}
	}

	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	
	
	@Override
	public BundleResource convert(FileResource resource) {
		
		try{
			
			if (! isValidInput(resource)){
				throw logAndThrowBadData("FileToBundle : cannot convert to 'bundle' : file '"+resource.getFile().getPath()+"' is no directory", null);
			}
			
			configure();
			
			File base = FileTree.staticCreateTempDirectory(); 
			
			resource.dump(base, true);
			
			File main = null;
			
			if (mainPath != null){
				main = new File(base, mainPath);
				
			}
			
			return new BundleResource(base, main);
			
		
			
		}catch(IOException ex){
			throw logAndThrowRuntimeException("FileToBundle : failed to create bundle due to I/O error : ", ex);
		}
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

	private void configure(){
		try{
			for (Resource<?> resource : configuration ){
				if (FileResource.class.isAssignableFrom(resource.getClass())){
					
					Map<String, String> options = OptionsReader.BASIC_READER.getOptions(((FileResource)resource).getFile());
					
					_setMain(options);
					
				}
			}
		}catch (IOException ex){
			throw logAndThrowIllegalConf("FileToBundle : an error occured while reading configuration", ex);
		}
	}
	
	
	private void _setMain(Map<String, String> options){
		if (options.containsKey(MAINPATH_KEY)){
			mainPath = options.get(MAINPATH_KEY);
		}
	}
	
	private IllegalConfigurationException logAndThrowIllegalConf(String message, Exception ex){
		if (logger.isErrorEnabled()){
			logger.error(message, ex);
		}
		throw new IllegalConfigurationException(message, ex);
	}
	
	private BadDataException logAndThrowBadData(String message, Exception ex){
		if (logger.isErrorEnabled()){
			logger.error(message, ex);
		}
		throw new BadDataException(message, ex);
	}
	
	private InstructionRuntimeException logAndThrowRuntimeException(String message, Exception ex){
		if (logger.isErrorEnabled()){
			logger.error(message, ex);
		}
		throw new InstructionRuntimeException(message, ex);
	}
	
	
	private boolean isValidInput(FileResource resource){
		if (resource==null){
			return false;
		}
		
		File file = resource.getFile();
		
		return (
				(file!=null) &&
				(file.exists()) &&
				(file.isDirectory())
		) ;
		
	}
	
}
