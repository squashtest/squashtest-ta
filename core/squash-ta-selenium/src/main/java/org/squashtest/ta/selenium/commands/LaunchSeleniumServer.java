/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.commands;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.process.ProcessHandle;
import org.squashtest.ta.commons.resources.ProcessHandleResource;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.selenium.library.SeleniumServerManager;
import org.squashtest.ta.selenium.resources.SeleniumConfiguration;

/**
 * Command to launch a selenium server.
 * @author edegenetais
 *
 */
@EngineComponent("launch")
public class LaunchSeleniumServer extends SeleniumServerManager implements
		Command<SeleniumConfiguration, org.squashtest.ta.framework.components.VoidTarget> {

	private SeleniumConfiguration serverConfiguration;
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		if(configuration.size()>0){
			LoggerFactory.getLogger(LaunchSeleniumServer.class).warn(configuration.size()+ " irrelevant configuration elements ignored (this command takes none");
		}
	}

	@Override
	public void setTarget(VoidTarget target) {
		//only a place holder here.
	}

	@Override
	public void setResource(SeleniumConfiguration resource) {
		serverConfiguration=resource;
	}

	@Override
	public ProcessHandleResource apply() {
		ProcessHandle handle=null;
		try{

			setServerProperties(serverConfiguration.getData());
			
			List<String> command = buildCommand();
			
				handle = startProxyWith(command);
			
			return new ProcessHandleResource(handle);
		
		} catch (IOException e) {
			throw new InstructionRuntimeException("Process creation failed.", e);
		}catch(InstructionRuntimeException e){
			killIfAlive(handle);
			throw e;
		}catch(Exception e){
			killIfAlive(handle);
			throw new InstructionRuntimeException("Killed server process due to error.",e);
		}
	}

	protected void killIfAlive(ProcessHandle handle) {
		if(handle!=null && handle.isProcessAlive()){
			handle.killProcess();
		}
	}

	@Override
	public void cleanUp() {
		//noop: only memory resources held...
	}

	@Override
	protected void treatPollingException(Exception pollingException) {
		throw new InstructionRuntimeException("The selenium server status could not be checked.", pollingException);
	}

	@Override
	protected void treatPollerTimeout() {
		throw new InstructionRuntimeException("The selenium server had still not started after specified time.");
	}

	@Override
	protected void mournDeadProcess(String errorStreamContent) {
		throw new InstructionRuntimeException("Launching the selenium server process failed. Here is the error stream:\n"+errorStreamContent);
	}

	@Override
	protected File getWorkingDirectory() {
		return new File(".").getAbsoluteFile();
	}

	@Override
	protected int getStderrRecordLength() {
		return 300;
	}

}
