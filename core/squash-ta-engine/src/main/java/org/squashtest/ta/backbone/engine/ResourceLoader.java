/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine;

import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;

/**
 * A ResourceLoader keeps will search collections of repositories and most importantly, will ensure that any user of this
 * class <b>returns a copy of the resource rather than the resource itself.</b>
 * 
 * @author bsiri
 *
 */
public interface ResourceLoader {

	/**
	 * A Repository will be added to the pool only if no repository with the same name already exists.
	 * 
	 * @param repository
	 */
	public void addRepository(RepositoryWrapper repository);
	
	public void removeRepository(String repositoryName);
	
	/**
	 * Will look for a resource through each of its repositories if needed and return a FileResource if found. 
	 * When a throws exception when not found. Note that it must return a copy of the resource, not the resource itself.
	 * It helps ensuring that the precious user datasets aren't modified or erased later on.
	 * 
	 * @param settings
	 * @return
	 */
	public ResourceWrapper loadResource(ResourceLoadingSettings settings);
	
	/**
	 * Will init all its repositories.
	 * 
	 */
	public void initAll();

	/**
	 * Will reset all its repositories
	 * 
	 */
	public void resetAll();
	
	
	/**
	 * Will clean all its repositories
	 */
	public void cleanupAll();
}