/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class FtpTargetPropertiesWizardPage extends Page {

	private Text hostname;
	private Text username;
	private Text password;
	private Button enablePortField;
	private Text port;
	private Button enableFileTypeField;
	private Combo fileType;
	private Button enableSystemField;
	private Combo system;
	
	public FtpTargetPropertiesWizardPage() {
		super(FtpTargetPropertiesWizardPage.class.getSimpleName());
		setTitle("Enter ftp server properties");
		setDescription("This form lets you define the ftp server properties.");
	}

	@Override
	public void createControl(Composite parent) {
		Composite uiContainer=createPageUIContainer(parent, 3);
		
		addLabel(uiContainer, "Server hostname",2);
		hostname=createInput(uiContainer);
		addPageRule(new MandatoryRule(hostname, "Server hostname", this));
		setControl(uiContainer);
		
		addLabel(uiContainer, "User name",2);
		username=createInput(uiContainer);
		addPageRule(new MandatoryRule(username, "Server hostname", this));
		setControl(uiContainer);
		
		addLabel(uiContainer, "User password",2);
		password=createInput(uiContainer);
		setControl(uiContainer);
		
		
		// Field port definition
			// Check button
		enablePortField = createCheck(uiContainer);
			// Label
		addLabel(uiContainer, "Server port");
			// field
		port=createInput(uiContainer);
			// Mandatory rule
		final MandatoryRule portMandatoryRule = new MandatoryRule(port, "Server port", this); 
		addPageRule(portMandatoryRule);
		setControl(uiContainer);
			// Defined initial state for check button and field
		enablePortField.setSelection(false);
		port.setEnabled(false);
		
		/* Add the listeners for the check button*/
		enablePortField.addSelectionListener( new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				// enable / disable the field
				port.setEnabled(enablePortField.getSelection());
				// refresh the rule control
				if(enablePortField.getSelection())	{
					portMandatoryRule.activate();
				} else {
					portMandatoryRule.deactivate();
				}
			}
			
		});
		
		
		// File type definition
			// Check button
		enableFileTypeField = createCheck(uiContainer);
			// Label
		addLabel(uiContainer, "File type");
			// field
		fileType=createCombo(uiContainer, fileTypeEnum.getTypeListSorted());
		fileType.setText(fileTypeEnum.ASCII.getType());
			// Mandatory rule
		//final MandatoryRule fileTypeMandatoryRule = new MandatoryRule(fileType, "File type", this); 
		//addPageRule(fileTypeMandatoryRule);
		setControl(uiContainer);
			// Defined initial state for check button and combo
		enableFileTypeField.setSelection(false);
		fileType.setEnabled(false);
		
		/* Add the listeners for the check button*/
		enableFileTypeField.addSelectionListener( new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				// enable / disable the field
				fileType.setEnabled(enableFileTypeField.getSelection());
				// refresh the rule control
				/*if(enableFileTypeField.getSelection())	{
					fileTypeMandatoryRule.activate();
				} else {
					fileTypeMandatoryRule.deactivate();
				}*/
			}
			
		});
		
		
		// System definition
			// Check button
		enableSystemField = createCheck(uiContainer);
			// Label
		addLabel(uiContainer, "System type");
			// field
		system=createCombo(uiContainer, systemEnum.getSystemListSorted());
		system.setText(systemEnum.UNIX.getType());
			// Mandatory rule
		//final MandatoryRule systemMandatoryRule = new MandatoryRule(system, "System type", this); 
		//addPageRule(systemMandatoryRule);
		setControl(uiContainer);
			// Defined initial state for check button and field
		enableSystemField.setSelection(false);
		system.setEnabled(false);
		
		/* Add the listeners for the check button*/
		enableSystemField.addSelectionListener( new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				// enable / disable the field
				system.setEnabled(enableSystemField.getSelection());
				// refresh the rule control
				/*if(enableSystemField.getSelection())	{
					systemMandatoryRule.activate();
				} else {
					systemMandatoryRule.deactivate();
				}*/
			}
			
		});

	}
	
	private enum fileTypeEnum {

		ASCII("ascii"), BINARY("binary");

		private String type;

		private fileTypeEnum(String type) {
			this.type = type;
		}

		private String getType() {
			return type;
		}

		private static String[] getTypeListSorted() {
			List<String> list = new ArrayList<String>();
			for (fileTypeEnum type : values()) {
				list.add(type.getType());
			}
			Collections.sort(list);
			return list.toArray(new String[values().length]);
		}
	}

	private enum systemEnum {

		UNIX("unix"), VMS("vms"), WINDOWS("windows"), OS2("os/2"), OS400(
				"os/400"), AS400("as/400"), MVS("mvs"), L8("l8"), NETWARE(
				"netware"), MACOS("macos");

		private String type;

		private systemEnum(String type) {
			this.type = type;
		}

		private String getType() {
			return type;
		}

		private static String[] getSystemListSorted() {
			List<String> list = new ArrayList<String>();
			for (systemEnum type : values()) {
				list.add(type.getType());
			}
			Collections.sort(list);
			return list.toArray(new String[values().length]);
		}
	}
	
	public boolean isSystemFieldEnable() {
		return enableSystemField.getSelection();
	}
	
	public String getSystem() {
		return system.getText();
	}
	
	public boolean isFileTypeFieldEnable() {
		return enableFileTypeField.getSelection();
	}
	
	public String getFileType() {
		return fileType.getText();
	}
	
	public boolean isPortFieldEnable() {
		return enablePortField.getSelection();
	}
	
	public String getPort() {
		return port.getText();
	}

	public String getUsername() {
		return username.getText();
	}

	public String getPassword() {
		return password.getText();
	}

	public String getHostname() {
		return hostname.getText();
	}

}
