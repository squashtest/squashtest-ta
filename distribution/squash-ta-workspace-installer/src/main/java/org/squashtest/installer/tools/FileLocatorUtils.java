/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.tools;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.squashtest.installer.context.Context;

public class FileLocatorUtils {
	
	
	Context context;
	
	public FileLocatorUtils(Context context) throws URISyntaxException{
		this.context = context;
	}

	public FileLocatorUtils(){
		
	}
	
	public void setContext(Context context){
		this.context= context;
	}
	
	
	
	public File getFileFromJar(String resourceName) throws URISyntaxException, MalformedURLException{
		URL url = FileLocatorUtils.class.getClassLoader().getResource(resourceName);
		return new File(url.toURI());
	}
	
	public URL getURLFromJar(String resourceName){
		return FileLocatorUtils.class.getClassLoader().getResource(resourceName);
	}
	
	public InputStream getStreamFromJar(String resourceName){
		return FileLocatorUtils.class.getClassLoader().getResourceAsStream(resourceName);
	}
	
	
	
	/**
	 * Given the TA install path, will locate the correct eclipse directory.
	 * The Eclipse dir is the one that contains eclipse.ini and eclipse.exe. That should be enough to identify it. 
	 * @throws IOException 
	 */
	public File findEclipseDirectory() throws IOException{

		File[] content = context.getSquashDirectory().listFiles(new FileFilter() {			
			@Override
			public boolean accept(File candidate) {
				
				if (! candidate.isDirectory()){
					return false;
				}
				
				File[] content = candidate.listFiles();
				
				boolean iniFound=false;
				boolean exeFound=false;
				
				for (File f : content){
					if (f.getName().equals("eclipse.ini")) iniFound=true;
					if (f.getName().equals("eclipse.exe")) exeFound=true;
				}
				
				return (iniFound && exeFound);
			}
		});
		
		//Only one result is supposed to be found. If there are more -> exception
		if (content.length!=1){
			throw new RuntimeException("TA workspace installer : no eclipse found in "+context.getSquashDirectory().getCanonicalPath()+
										" or more than one is installer (in that case remove one of them)");
		}
		
		//if not, we're good
		return content[0];
		
	}
	
}
