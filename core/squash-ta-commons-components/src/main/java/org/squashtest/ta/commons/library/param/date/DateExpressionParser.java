/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.param.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.param.Expression;
import org.squashtest.ta.commons.library.param.ExpressionParser;
import org.squashtest.ta.commons.library.param.IllegalExpressionException;
import org.squashtest.ta.commons.library.param.LitteralExpression;

/**
 * {@link ExpressionParser} to parse date computation place holder.
 * 
 * @author edegenetais
 * 
 */
public class DateExpressionParser implements ExpressionParser {

	// pattern to recognize a valid date expression
	private static final Pattern VALIDATION_PATTERN = Pattern
			.compile("^now\\(\\)(?:\\.[a-zA-Z]+\\([^)]*\\))*\\.format\\([^)]+\\)$");

	// pattern to tokenize the date expression
	private static final Pattern TOKENIZE_PATTERN = Pattern
			.compile("([a-zA-Z][a-zA-Z0-9_]*)\\(([^)]*)\\)");

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DateExpressionParser.class);

	private Map<String, DateFunction> functions = new HashMap<String, DateFunction>();

	/**
	 * Constructor to use the parser with default date function set.
	 */
	public DateExpressionParser() {
	}

	/**
	 * Constructor with initial fnction {@link Map}, for testability or
	 * injection of additional functions.
	 * 
	 * @param functionMap
	 *            the function Map.
	 */
	public DateExpressionParser(Map<String, DateFunction> functionMap) {
		functions.putAll(functionMap);
	}

	@Override
	public boolean accept(String expression) {
		Matcher matcher = VALIDATION_PATTERN.matcher(expression);
		return matcher.matches();
	}

	@Override
	public Expression parse(String expression) {
		Expression result = null;
		if (accept(expression)) {
			Matcher expressionMatcher = TOKENIZE_PATTERN.matcher(expression);
			Calendar date = null;
			while (expressionMatcher.find()) {
				String functionName = expressionMatcher.group(1);
				String functionArg = expressionMatcher.group(2);
				if ("format".equals(functionName)) {
					/*
					 * the validation pattern ensures that the format function
					 * is the last, so we just have to create an expression
					 * based on the formatter output
					 */
					String dateString = new SimpleDateFormat(functionArg)
							.format(date.getTime());
					result = new LitteralExpression(dateString);
				} else {
					/*
					 * Otherwise, we are still computing the date before
					 * formatting it.
					 */
					DateFunction function = functionLookup(functionName);
					date = function.evaluate(date, functionArg);
				}
			}
		} else {
			throw new IllegalArgumentException(expression
					+ " is not a valid relative date computation expression.");
		}
		return result;
	}

	protected synchronized DateFunction functionLookup(String functionName) {
		DateFunction function = functions.get(functionName);
		if (function == null) {/*
								 * default function loading
								 * strategy: capitalize function
								 * name and look for a class of the
								 * same name in this package.
								 */
			String packageName = getClass().getPackage().getName();
			try {
				String functionClassName = packageName + "."
						+ (functionName.substring(0, 1)
								.toUpperCase() + functionName.substring(1));
				@SuppressWarnings("unchecked")//at times you need it...
				Class<? extends DateFunction> functionClass = (Class<? extends DateFunction>) Class
						.forName(functionClassName);
				function=functionClass.newInstance();
				functions.put(functionName, function);
				LOGGER.debug("Loaded function implementation "+functionClassName+" for function "+functionName);
			} catch (ClassNotFoundException e) {
				LOGGER.debug("Some class was not found during date replace treatment, probably because it is an undefined functor class", e);
				throw new IllegalExpressionException(
						functionName
								+ " is not a valid date computation function name.",e);
			} catch (InstantiationException e) {
				throw new IllegalExpressionException(
						functionName
								+ " is not a valid date computation function name.",e);
			} catch (IllegalAccessException e) {
				throw new IllegalExpressionException(
						functionName
								+ " is not a valid date computation function name.",e);
			}catch(ClassCastException cce){
				throw new IllegalExpressionException(
						functionName
								+ " is not a valid date computation function name.",cce);
			}
		}
		return function;
	}
}
