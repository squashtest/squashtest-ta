/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
*     This file is part of the Squashtest platform.
*     Copyright (C) 2011 - 2011 Squashtest TA, Squashtest.org
*
*     See the NOTICE file distributed with this work for additional
*     information regarding copyright ownership.
*
*     This is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Lesser General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     this software is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General Public License
*     along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.squashtest.ta.sahi.assertions

import org.squashtest.ta.sahi.assertions.SahiIsSuccess
import org.squashtest.ta.sahi.resources.SahiSuiteResultResource
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.GeneralStatus;

import spock.lang.Specification

class SahiIsSucessTest extends Specification {
	def testee
	
	def "no exception if SUCCESS"(){
		given:
			def result=new SahiSuiteResultResource(GeneralStatus.SUCCESS, new File("."))
		and:
			testee=new SahiIsSuccess();
			testee.setActualResult(result)
		when:
			testee.test()
		then:
			true
	}
	
	def "throw AssertionFailedException if not SUCCESS"(){
		given:
			def result=new SahiSuiteResultResource(GeneralStatus.FAIL, new File("."))
		and:
			testee=new SahiIsSuccess()
			testee.setActualResult(result)
		when:
			testee.test()
		then:
			thrown AssertionFailedException
	}
	
	def "report copy in exception context when failure"(){
		given:
			def file=File.createTempFile("test", ".report")
			file.deleteOnExit()
			BufferedWriter bw=new BufferedWriter(new FileWriter(file))
			bw.write("testString")
			bw.newLine()
			bw.close()
		and:
			def result=new SahiSuiteResultResource(GeneralStatus.FAIL, file)
		and:
			testee=new SahiIsSuccess()
			testee.setActualResult(result)
		when:
			testee.test()
		then:
			def e=thrown(AssertionFailedException)
			def context=e.getFailureContext()
			def report=context.get(0)
			def reportFile=report.resource.getFile()
			!reportFile.equals(file)
			byte[] refBuffer=new byte[1]
			byte[] reportBuffer=new byte[1]
			FileInputStream referenceIn=new FileInputStream(file)
			FileInputStream reportIn=new FileInputStream(reportFile)
			def refNb=referenceIn.read(refBuffer)
			def repNb=reportIn.read(reportBuffer)
			def ok=true
			while(ok && refNb>=0){
				if(refNb<0){
					ok=false
				}else{
					ok=(refBuffer[0]==reportBuffer[0])
					refNb=referenceIn.read(refBuffer)
					repNb=reportIn.read(reportBuffer)
				}
			}
			referenceIn.close()
			reportIn.close()
			ok
	}
}
