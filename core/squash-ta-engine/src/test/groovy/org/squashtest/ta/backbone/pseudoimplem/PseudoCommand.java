/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.pseudoimplem;

import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.exception.CannotApplyCommandException;

/* the declaration of that type is purposedly messed up and is meant to test the instrospection-based initializers */
@EngineComponent("pseudo")
public class PseudoCommand implements Iterable<String>, ResourceRepository, Command<PseudoResource, PseudoTarget> {

	public static boolean tested=false;
	
	PseudoTarget target;
	PseudoResource resource;
	
	public void addConfiguration(Collection<Resource<?>> configuration) {
	
	}

	public void setTarget(PseudoTarget target) {
		this.target=target;
	}

	public void setResource(PseudoResource resource) {
		this.resource=resource;
	}
	
	public PseudoTarget getTarget(){
		return target;
	}
	
	public PseudoResource getResource(){
		return resource;
	}

	public PseudoResource apply() {
		tested=true;
		return new PseudoResource();
	}

	public void cleanUp() {		
		
	}

	public Properties getConfiguration() {
		return null;
	}

	public FileResource findResources(String name) {
		return null;
	}

	public Iterator<String> iterator() {
		hiddenMethod();
		return null;
	}
	
	public void cleanup(){
		//nothing to do
	}
	
	public void init(){
		//nothing to do
	}
	
	/* **************** other testing code *************** */
	
	private void hiddenMethod(){
		
	}
	
	public void throwSecurityException(){
		throw new SecurityException();
	}
	
	public void throwInvocationTargetException(){
		throw new CannotApplyCommandException();
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}
