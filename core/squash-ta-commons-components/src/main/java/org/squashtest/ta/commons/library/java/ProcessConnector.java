/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is a base to build Process connectors.
 * @author edegenetais
 *
 */
public class ProcessConnector {
	private static final Logger LOGGER=LoggerFactory.getLogger(ProcessConnector.class);
	private static final Pattern JAR_PATH_EXTRACTOR = Pattern.compile("^jar:(file:[^!]+)!/META-INF/MANIFEST.MF$");

	/**
	 * This methods lists all jar files in a classpath. Use it to build a process classpath.
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	protected List<File> computeJarClassPath() throws IOException,
			URISyntaxException {
				ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
				return computeJarClassPath(contextClassLoader);
			}

	protected List<File> computeJarClassPath(ClassLoader contextClassLoader)
			throws IOException, URISyntaxException {
		Enumeration<URL> manifestURLs=contextClassLoader.getResources("META-INF/MANIFEST.MF");
		List<File> classPath=new ArrayList<File>();
		while(manifestURLs.hasMoreElements()){
			String manifestURL = manifestURLs.nextElement().toExternalForm();
			Matcher matcher=JAR_PATH_EXTRACTOR.matcher(manifestURL);
			if(matcher.matches()){
				String jarLoc=matcher.group(1);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Adding file '"+jarLoc+"' to the classpath.");
				}
				URI jarURI = new URI(jarLoc);
				classPath.add(new File(jarURI));
			}else{
				LOGGER.warn("Failed to extract classpath element: "+manifestURL+". Compilation may fail.");
			}
			
		}
		return classPath;
	}

}