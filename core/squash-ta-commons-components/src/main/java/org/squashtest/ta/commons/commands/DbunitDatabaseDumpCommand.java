/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.commands;

import java.sql.SQLException;
import java.util.Collection;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.resources.DbUnitConfiguration;
import org.squashtest.ta.commons.resources.DbUnitDatasetResource;
import org.squashtest.ta.commons.resources.DbUnitPPKFilter;
import org.squashtest.ta.commons.targets.DatabaseTarget;
import org.squashtest.ta.core.tools.ExceptionLogger;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;


/**
 * <p><strong>description</strong> Will read-out the entire content of a {@link DatabaseTarget} as a {@link DbUnitDatasetResource}. This command requires no input resource.<p>
 * 
 * <p><strong>Configuration (optional) : 
 * 	<ul>
 * 		<li>{@link DbUnitConfiguration} : additional configuration for the DbUnit connection. </li>
 * 		<li>{@link DbUnitPPKFilter} : A Pseudo Primary Key filter that will override the one supplied in the dbconfiguration if any. </li>
 *  </ul>
 * </p>
 * 
 * <p><strong>DSL example : </strong>EXECUTE get.all WITH $() ON my.db USING my.dbu.ppk, my.dbu.conf, AS my.result.dataset</p>
 * 
 * @author bsiri
 *
 */
@EngineComponent("get.all")
public class DbunitDatabaseDumpCommand extends AbstractDbUnitCommand implements Command<FileResource, DatabaseTarget> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DbunitDatabaseDumpCommand.class);
	private static final ExceptionLogger logger = new ExceptionLogger(DbunitDatabaseDumpCommand.class, InstructionRuntimeException.class);
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		configuration.addAll(configuration);
	}

	@Override
	public void setResource(FileResource resource) {
	}

	@Override
	public DbUnitDatasetResource apply() {
		
		try
		{
			configure();
			
			IDatabaseConnection connection = buildDbUnitConnection(); 
			
			IDataSet dataset = connection.createDataSet();
			DbUnitDatasetResource result = new DbUnitDatasetResource(dataset,true);
			
			return result;
		}
		catch(DatabaseUnitException ex)
		{
			String message = "db unit insert : an error originated from the DbUnit framework occured:";
			throw logger.errAndThrow(message, ex);
		}
		catch(SQLException ex)
		{
			String message = "db unit insert : an error originated from the database occured:";
			throw logger.errAndThrow(message, ex);	
		}
	}

	@Override
	public void cleanUp() {
		// nothing to do
	}
	
	@Override
	protected void applySpecificConfiguration(Resource<?> element) {
		LOGGER.warn("Unrecognized configuration element:"+element.toString()+" will be ignored!");
	}

}
