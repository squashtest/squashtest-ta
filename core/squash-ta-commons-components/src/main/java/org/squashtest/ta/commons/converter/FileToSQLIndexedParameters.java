/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.squashtest.ta.commons.resources.SQLIndexedParameters;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;

/**
 * File To SQLIndexedParameter Converter
 * Converts a File entry into an interpretable SQL Syntax
 * <p> The file must have the following specifications :<p>
 * <ul>
 * 	<li>Every line of the file contains 2 values separated by a comma ',',</li>
 * 	<li>The 2 values being : position and value</li>
 *  <li>none of the values can be empty except :</li>
 *  <ul>
 *  <li>If the name contains only spaces, the name will be defined by the spaces characters</li>
 *  </ul>
 * </ul>
 * 
 * @author fgaillard
 *
 */
@EngineComponent("from.text")
public class FileToSQLIndexedParameters implements ResourceConverter<FileResource, SQLIndexedParameters> {

	/**
	 * Default constructor for Spring enumeration only.
	 */
	public FileToSQLIndexedParameters(){}
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		
	}

	@Override
	public SQLIndexedParameters convert(FileResource resource) {
		SQLIndexedParameters indexedParameters = null;
		try
		{
			SimpleLinesData data = new SimpleLinesData(resource.getFile().getPath());
			List<String> dataLines=data.getLines();
			Map<String, String> tempParamters = new HashMap<String, String>();
			for(String line:dataLines) {
				if (line.contains(",")){
					String[] parameters = line.split(",");
					//we throw an exception if the position is not an integer
					try {
						Integer.parseInt(parameters[0]);
					} catch (NumberFormatException nfe) {
						throw new BadDataException("The fileResource is not correctly configured : The position is not an integer \n",nfe);
					}
					tempParamters.put(parameters[0], parameters[1]);
				} else {
					throw new BadDataException("The fileResource is not correctly configured : there is no \",\" to separate the values from their position");
				}
			}
			//We reorder the hashMap
			Set<String> keys = tempParamters.keySet();
			List<String> positions = new ArrayList<String>(keys);
			Collections.sort(positions);
			List<String> orderedParameters = new ArrayList<String>(positions.size());
			for (String position : positions) {
				orderedParameters.add(tempParamters.get(position));
			}
			indexedParameters = new SQLIndexedParameters(orderedParameters);
		} catch (FileNotFoundException fnfe) {
			System.err.println("file not found!!!!!");
		} catch (IOException ioe) {
			System.err.println("IOException!!!!!");
		}
		return indexedParameters;
	}

	@Override
	public void cleanUp() {
		
	}
}
