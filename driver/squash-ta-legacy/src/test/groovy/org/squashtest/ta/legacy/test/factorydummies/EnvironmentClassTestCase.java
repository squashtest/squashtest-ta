/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.test.factorydummies;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.squashtest.cats.runner.test.AbstractCatsTestCase;

public class EnvironmentClassTestCase extends AbstractCatsTestCase {
	@BeforeClass
	public void setup1(){
		
	}
	@BeforeClass
	public void setup2(){
		
	}
	@AfterClass
	public void teardown1(){
		
	}
	@AfterClass
	public void teardown2(){
		
	}

	//nothing in there, but a noop test.
	@Test
	public void testDummy(){
		
	}
	
	@Before
	public void setUpSnare(){
		//this method should be ignored by the ecosystem factory (it applies at test level)
	}
	
	@After
	public void tearDownSnare(){
		//this method should be ignored by the ecosystem factory (it applies at test level)
	}
}
