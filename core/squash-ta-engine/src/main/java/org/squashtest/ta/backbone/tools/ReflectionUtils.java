/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.tools;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.exception.EngineInitException;
import org.squashtest.ta.backbone.exception.EngineRuntimeException;
import org.squashtest.ta.backbone.exception.MissingAnnotationException;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.annotations.ResourceType;


/**
 * 
 * That class is just what the name says it is. It is tailormade for our modest needs. Also
 * note that it could have been a static library yet it was made instanciable : that way
 * the class is easier to mock when testing other classes that use it. 
 * 
 * @author bsiri
 *
 */
public class ReflectionUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReflectionUtils.class);


	/* 
	 * our framework uses parameterized interfaces. We need to fetch that
	 * interface and the concrete types that were used to parameterize it in the inspectedClass.
	 */
	public Class<?>[] getGenericTypes(Class<?> inspectedClass, Class<?> searchedInterface){
		
		Type[] types = inspectedClass.getGenericInterfaces();
		
		Type interfaceAsType=findTypeMatchingClass(types, searchedInterface);		

		if (interfaceAsType==null){
			String message = "Squash TA engine (init) : class '"+inspectedClass+"' does not implement '"+searchedInterface+"'.";
			message += "Please contact Squash TA dev team as something is obviously broken in the framework engine.";
			if (LOGGER.isErrorEnabled()){
				LOGGER.error(message);
			}
			throw new EngineInitException(message);
		}
		
		Type[] actualTypes = ((ParameterizedType)interfaceAsType).getActualTypeArguments();
		Class<?>[] result = new Class<?>[actualTypes.length];
		int counter=0;
		
		for (Type type : actualTypes){
			result[counter++]=getClassFromType(type);
		}
		
		return result;		
	}
	
	/**
	 * Returns the class of the result returned when applying Method named 'methodName' of Class 'inspectedClass'.
	 * The said method takes no argument.
	 * 
	 * @param inspectedClass
	 * @param methodName
	 * @return the Class of the result returned by that method
	 */
	public Class<?> getReturnType(Class<?> inspectedClass, String methodName){
		return getReturnType(inspectedClass, methodName, new Class<?>[0]);
	}
	
	/**
	 * Returns the class of the result returned when applying Method named 'methodName' of Class 'inspectedClass', which arguments are of classes supplied in 'argTypes' in that order. 
	 * 
	 * @param inspectedClass
	 * @param methodName
	 * @param argTypes
	 * @return the Class of the result returned by that method
	 */
	public Class<?> getReturnType(Class<?> inspectedClass, String methodName, Class<?>[] argTypes){
		try {
			Method method = inspectedClass.getMethod(methodName, argTypes);
			return method.getReturnType();
		} catch (SecurityException e) {			
			String message = makeSecurityExceptionMessage(inspectedClass, methodName, argTypes);		
			LOGGER.error(message, e);
			throw new EngineRuntimeException(message, e);
		} catch (NoSuchMethodException e) {
			String message = makeNoSuchMethodExceptionMessage(inspectedClass, methodName, argTypes);	
			LOGGER.error(message, e);
			throw new EngineRuntimeException(message, e);			
		} 
	}
	
	//excerpt from http://www.artima.com/weblogs/viewpost.jsp?thread=208860
	//returns null if the given type is not an instance of ParameterizedType (since that's what we are looking for).
	private Class<?> getClassFromType(Type type){
		Class<?> classForType=null;
		if (type instanceof Class){
			classForType=(Class<?>)type;
		}
		else if (type instanceof ParameterizedType){
			classForType=getClassFromType(((ParameterizedType)type).getRawType());
		}
		return classForType;
	}
	
	private Type findTypeMatchingClass(Type[] types, Class<?> matchingClass){
		for (Type type : types){
			Class<?> typeAsClass = getClassFromType(type);
			if ((typeAsClass!=null) && (typeAsClass.equals(matchingClass)) ){
				return type;
			}
		}
		return null;
				
	}

	public String getEngineComponentValue(Class<?> clazz){
		EngineComponent annot = clazz.getAnnotation(EngineComponent.class);
		if (annot==null){
			String message = "Squash TA engine error (init) : @EngineComponent not found on class '"+clazz.getName()+"'. ";
			message += "Please contact the culprit and tell him he MUST annotate the class.";
			
			if (LOGGER.isErrorEnabled()){
				LOGGER.error(message);
			}
			
			throw new MissingAnnotationException(message);
		}else{
			return annot.value();
		}		
	}
	
	
	public String getEngineComponentValue(Object object){
		return getEngineComponentValue(object.getClass());
	}
	
	public String getResourceTypeValue(Class<?> clazz){
		ResourceType annot= clazz.getAnnotation(ResourceType.class);
		if (annot==null){
			String message = "Squash TA engine error (init): @ResourceType not found on class '"+clazz.getName()+"'. ";
			message += "Please contact the culprit and tell him he MUST annotate the class.";
			
			if (LOGGER.isErrorEnabled()){
				LOGGER.error(message);
			}
			throw new MissingAnnotationException(message);
		}else{
			return annot.value();
		}
	}
	
	public String getResourceTypeValue(Object object){
		return getResourceTypeValue(object.getClass());
	}
	
	/**
	 * 
	 * @param owner
	 * @param methodName
	 * @param argument an object (not an array !). May be null
	 * @return
	 */
	public Object invoke(Object owner, String methodName, Object argument){
		try {
			
			Method method;
			if (argument!=null){
				method = owner.getClass().getMethod(methodName,argument.getClass());
				return method.invoke(owner, argument);
			}else{
				method = owner.getClass().getMethod(methodName, (Class<?>[])null);
				return method.invoke(owner, (Object[])null);
			}
		
		} catch (SecurityException e) {
			String message = makeSecurityExceptionMessage(owner, methodName, argument);
			LOGGER.error(message, e);
			throw new EngineRuntimeException(message, e);
		} catch (NoSuchMethodException e) {
			String message = makeNoSuchMethodExceptionMessage(owner, methodName, argument);
			LOGGER.error(message, e);
			throw new EngineRuntimeException(message, e);
		} catch (IllegalArgumentException e) {			
			String message = makeIllegalArgumentExceptionMessage(owner, methodName, argument);
			LOGGER.error(message, e);
			throw new EngineRuntimeException(message, e);
		} catch (IllegalAccessException e) {
			String message = makeIllegalAccessExceptionMessage(owner, methodName, argument);
			LOGGER.error(message, e);
			throw new EngineRuntimeException(message, e);
		} catch (InvocationTargetException e) {
			String message = makeInvocationTargetExceptionMessage(owner, methodName, argument);
			LOGGER.error(message, e.getCause());//true, here we throw out the InvocationTargetException because only its cause is meaningful
			throw new EngineRuntimeException(message, e.getCause());//NOSONAR
		}		
	}
	
	/* ************************* awful exception handling code here ************************** */	
	
	private String errorMessagePreamble(Object owner, String methodName, Object argument){
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("Test Automation Engine error (non SUT) : method '");
		builder.append(methodName);
		builder.append("' from engine component of class '");
		builder.append(owner.getClass().getName());
		builder.append("' ");
		if (argument!=null){
			builder.append("with argument '"+argument.getClass().getName()+"' ");
		}
		
		return builder.toString();
		
	}
	
	private String makeSecurityExceptionMessage(Object owner, String methodName, Object argument){
		
		StringBuilder builder = new StringBuilder();
		
		String message = errorMessagePreamble(owner, methodName, argument);
		builder.append(message);
		
		builder.append("is out of reach. Contact the developper who coded that class "+
						"and tell him to make it fully public.");
		
		return builder.toString();
		
	}
	
	private String makeNoSuchMethodExceptionMessage(Object owner, String methodName, Object argument){
		
		StringBuilder builder = new StringBuilder();
		
		String message = errorMessagePreamble(owner, methodName, argument);
		builder.append(message);
		
		builder.append(" was not found. Contact the developper who coded that class "+
					 	"and tell him to make fully public.");
		
		return builder.toString();
		
	}
	
	//seriously in the specific use of this class this situation is impossible.
	private String makeIllegalArgumentExceptionMessage(Object owner, String methodName, Object argument){
		
		StringBuilder builder = new StringBuilder();

		String message = errorMessagePreamble(owner, methodName, argument);
		builder.append(message);		
		
		builder.append(" should have worked but did not. Please contact the tech team and tell it to change the jvm.");
		
		return builder.toString();	
	}
	
	
	private String makeIllegalAccessExceptionMessage(Object owner, String methodName, Object argument){
		
		StringBuilder builder = new StringBuilder();
		
		String message = errorMessagePreamble(owner, methodName, argument);
		builder.append(message);
		
		builder.append(" is out of reach. Contact the developper who coded that class "+
					 	"and tell him to make fully public.");
		
		return builder.toString();	
		
	}
	
	private String makeInvocationTargetExceptionMessage(Object owner, String methodName, Object argument){
		
		StringBuilder builder = new StringBuilder();
		
		String message = errorMessagePreamble(owner, methodName, argument);
		builder.append(message);
		
		builder.append(" has thrown an exception.");
		
		return builder.toString();
		
	}
}
