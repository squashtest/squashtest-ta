/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtils {

	public static void copyDirFromStream(InputStream inStream, File output) throws IOException{
		ZipInputStream zipStream = new ZipInputStream(inStream);
		ZipEntry entry;
		
		while((entry=zipStream.getNextEntry())!=null){
			File next= new File(output, entry.getName());
			
			if (entry.isDirectory()){
				if (!next.exists()){
					next.mkdirs();
				}
			}else{
				next.createNewFile();
				
				byte[] buffer = new byte[2048];
				PrintStream outStream = new PrintStream(next);
				int nb;
				
				while((nb=zipStream.read(buffer, 0, 2048))!=-1){
					outStream.write(buffer, 0, nb);
				}
				
				outStream.flush();
				outStream.close();
				
			}
		}
		
		zipStream.close();
		
	}
}
