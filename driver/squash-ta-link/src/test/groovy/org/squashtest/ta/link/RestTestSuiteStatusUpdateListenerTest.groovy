/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.link

import java.util.Date;

import org.squashtest.ta.api.execution.client.StatusUpdate
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.TestSuiteExecutionStatus


import spock.lang.Specification;

class RestTestSuiteStatusUpdateListenerTest extends Specification{
	StatusUpdate mockService
	RestTestSuiteStatusUpdateListener testee
	TestSuiteStatusUpdate event
	SuiteResult mockResult
	Date startTime
	
	final SUITE_NAME="testSuite"
	
	def setup(){
	mockService=Mock()
		testee=new RestTestSuiteStatusUpdateListener(mockService,"testHost","testJob", "testId")
		event=Mock()
		
		startTime=new Date(2385)
		
		mockResult=Mock()
		mockResult.getStatus()>>GeneralStatus.SUCCESS
		mockResult.startTime()>>startTime
		mockResult.getName() >> SUITE_NAME 
		
		event.getPayload() >> mockResult
	}
	
	def "should transmit test host name"(){
		given:
			
			
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus("testHost", _, _, _)
	}
	
	def "should transmit job name"(){
		given:
			
			
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_, "testJob", _, _)
	}

	def "should transmit external ID"(){
		given:
			
			
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_,_,"testId", _)
	}
	
	def "should transmit SUCCESS event"(){
		given:
			Date endTime=new Date(9654)
		and:
			mockResult.endTime()>>endTime
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_,_,_,{
				it instanceof TestSuiteExecutionStatus
				TestSuiteExecutionStatus status=it
				startTime.equals(status.getStartTime())
				endTime.equals(status.getEndTime())
				status.getStatus()==ExecutionStatus.SUCCESS
				status.getSuiteName()==SUITE_NAME
			})
	}
	
	def "should transmit RUNNING event"(){
		given:
			mockResult.getStatus()>>GeneralStatus.RUNNING
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_,_,_,{
				it instanceof TestSuiteExecutionStatus
				TestSuiteExecutionStatus status=it
				startTime.equals(status.getStartTime())
				status.getEndTime()==null
				status.getStatus()==ExecutionStatus.RUNNING
				status.getSuiteName()==SUITE_NAME
			})
	}
}
