/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.targets;

import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * <p>Web application target. 
 * This manages the connection including HTTP authentication protocols.</p>
 * @author edegenetais
 *
 */
@ResourceType("http")
public class WebTarget implements Target{

	static final String NAME_SPACE="squashtest.ta.http";
	
	static final String ENDPOINT_URL_KEY = NAME_SPACE+".endpoint.url";
	static final String PROXY_HOST_KEY = NAME_SPACE+".proxy.host";
	static final String PROXY_PORT_KEY = NAME_SPACE+".proxy.port";

	private static final String HTTPS_PROTOCOL = NAME_SPACE+".https";

	private Properties configuration;
	
	private URL url;
	private Integer proxyPort;
	private String proxyHost;
	
	private boolean https;
	
	private List<WeakReference<HttpURLConnection>>connections=new ArrayList<WeakReference<HttpURLConnection>>();
	
	/** noarg constructor for Spring enumeration. */
	public WebTarget(){}
	
	/**
	 * Constructor for a valid object. 
	 * @param configuration
	 */
	public WebTarget(Properties configuration){
		this.configuration=configuration;
		try {
			url=new URL(configuration.getProperty(ENDPOINT_URL_KEY));
			https=(HTTPS_PROTOCOL.equals(url.getProtocol()));
			proxyHost = configuration.getProperty(PROXY_HOST_KEY);
			
			String pPort = configuration.getProperty(PROXY_PORT_KEY);
			if (pPort!=null){
				proxyPort = Integer.parseInt(pPort.trim());
			}
		} catch (MalformedURLException e) {
			throw new IllegalConfigurationException("WebTarget: endpoint.url not valid", e);
		}
	}
	
	@Override
	public void init() {
		//noop
	}

	@Override
	public void reset() {
		cleanImpl();
	}

	private void cleanImpl() {
		for(WeakReference<HttpURLConnection> connectionRef:connections){
			HttpURLConnection httpURLConnection = connectionRef.get();
			if(httpURLConnection!=null){
				httpURLConnection.disconnect();
			}
		}
		connections.clear();
	}

	@Override
	public void cleanup() {
		cleanImpl();
	}

	@Override
	public Properties getConfiguration() {
		return new Properties(configuration);
	}
	
	/**
	 * Opens a connection to the end-point represented by this target.
	 * as of version 1.0, this only manages plain HTTP connections.
	 * @return a new {@link HttpURLConnection}
	 */
	public HttpURLConnection getConnection(){
		if(https){
			throw new UnsupportedOperationException("Establishing an HTTPS connection not yet implemented inside WebTarget. (please use a 3rd party component until this is added)");
		}else{
			try {
				HttpURLConnection connection=null;
				
				if (usesProxy()){
					connection = (HttpURLConnection)url.openConnection(createProxy());
				}
				else{
					connection = (HttpURLConnection)url.openConnection();
				}
				connections.add(new WeakReference<HttpURLConnection>(connection));
				return connection;
			} catch (Exception e) {
				throw new InstructionRuntimeException(
						"Opening connection from "
								+ (url == null ? "<unkown URL>"
										: url.toExternalForm()) + " failed.", e);
			}
		}
	}

	/**
	 * Get the URL from this target.
	 * @return
	 */
	public URL getUrl() {
		return url;
	}
	
	public Integer getProxyPort(){
		return proxyPort;
	}
	
	public String getProxyHost(){
		return proxyHost;
	}
	
	
	public boolean usesProxy(){
		return ((proxyHost!=null) && (proxyPort!=null));
	}
	

	/**
	 * Check if this target stands for a plain HTTP or HTTPS endpoint.
	 * @return <code>true</code> if the target is a secured http (HTTPS) server.
	 */
	public boolean isHttps() {
		return https;
	}

	
	private Proxy createProxy(){
		return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost,proxyPort));
	}
	
}
