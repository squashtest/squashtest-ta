/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.assertions;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.shell.ExitCodeCheck;
import org.squashtest.ta.commons.library.shell.ShellResult;
import org.squashtest.ta.commons.resources.ShellResultResource;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

/**
 * This assertion checks that the shell execution returned the
 * <strong>success</strong> (0) exit code. No configuration needed.
 * 
 * @author edegenetais
 * 
 */
@EngineComponent("success")
public class ShellIsSuccess extends AbstractShellResultAssertion implements UnaryAssertion<ShellResultResource> {

	/** Logger for the class. */
	static final Logger LOGGER = LoggerFactory
			.getLogger(ShellIsSuccess.class);

	private static ExitCodeCheck SUCCESS_CHECK = new ExitCodeCheck();

	private ShellResultResource actual;

	@Override
	public void setActualResult(ShellResultResource actual) {
		this.actual = actual;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		if(LOGGER.isWarnEnabled() && configuration.size()>0){
		LOGGER.warn("Ignoring "
				+ configuration.size()
				+ " configuration elements, as none is needed by this assertion.");
		}
	}

	@Override
	public void test() throws AssertionFailedException {
		ShellResult shellResult = actual.getResult();
		if (SUCCESS_CHECK.check(shellResult)) {
			LOGGER.debug("Shell success assertion passed.");
		} else {

			Resource<?> actualCodeResource;
			try {
				File actualFile = dumpExitCodeToFile(shellResult);
				actualCodeResource = new FileResource(actualFile);
			} catch (IOException ioe) {
				LOGGER.warn("Unable to create exit code reporting file", ioe);
				actualCodeResource = actual;// fallback: actual if we can't
											// extract exit code
			}

			List<ResourceAndContext> context = buildShellFailureContext(shellResult);

			throw new AssertionFailedException(
					"Shell command should have been successful but failed with code "+shellResult.getExitValue(), actualCodeResource,
					context);
		}
	}

}
