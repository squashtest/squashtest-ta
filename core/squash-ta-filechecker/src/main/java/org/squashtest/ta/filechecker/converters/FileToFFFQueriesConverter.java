/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.filechecker.converters;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import org.squashtest.ta.filechecker.library.utils.xpath.InvalidXpathQueriesException;
import org.squashtest.ta.filechecker.library.utils.xpath.XpathAssertions;
import org.squashtest.ta.filechecker.resources.FFFQueriesResource;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.xml.sax.SAXException;

@EngineComponent("query")
public class FileToFFFQueriesConverter implements
		ResourceConverter<FileResource, FFFQueriesResource> {

	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		// No op
	}

	@Override
	public FFFQueriesResource convert(FileResource resource) {
		URL fileUrl;
		try {
			fileUrl = resource.getFile().toURI().toURL();
			XpathAssertions assertions = new XpathAssertions(fileUrl);
			return new FFFQueriesResource(assertions);
		} catch (InvalidXpathQueriesException e) {
			throw new BadDataException("The resource is not a valid Xpath query resource for FFF: "+e.getMessage(), e);
		} catch (IOException e) {
			throw new InstructionRuntimeException(e.getMessage(), e);
		} catch (SAXException e) {
			throw new BadDataException("The resource is not a valid Xpath query resource for FFF: "+e.getMessage(), e);
		}

	}

	@Override
	public void cleanUp() {
		// No op
	}

}
