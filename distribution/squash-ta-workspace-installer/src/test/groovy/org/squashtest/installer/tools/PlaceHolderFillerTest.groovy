/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.tools

import org.squashtest.installer.context.Context;
import org.squashtest.installer.tools.PlaceHolderFiller;

import spock.lang.Specification;

class PlaceHolderFillerTest extends Specification {
	
	Context context;
	String canonicalRoot=new File("/").getCanonicalPath()
	String canonicalRootDoubleBackslash=canonicalRoot.replace("\\","\\\\")
	String fileSep=File.separator
	String fileSepDoubleBackslash=File.separator.replace("\\", "\\\\")
	
	def "should map some placeholders to their expected counterpart"(){
		
		
		given :
			def context = new Context()
			context.squashDirectory = new File("/groovy/éàèç rep")
			context.outputDirectory = new File("/users/workspace ààà")
			context.workingDirectory = new File("/bob/ééé/temp")
		 
		and :
			
			def filler = new PlaceHolderFiller(context);
		
		when :
			filler.initReplacements()
			def map = filler.replacementMap
		
		then :
			def val1 = map.get('${SQUASH_TA_HOME_ESC}')
			val1 == canonicalRootDoubleBackslash+"groovy"+fileSepDoubleBackslash+"\\u00e9\\u00e0\\u00e8\\u00e7 rep"
			
			def val2 = map.get('${SQUASH_TA_PACKAGES_ESC}') 
			val2 == canonicalRoot.replace("\\", "/")+"groovy/\\u00e9\\u00e0\\u00e8\\u00e7 rep"
			
			def val3 = map.get('${USER_WORKSPACE_ESC}') 
			val3 == canonicalRootDoubleBackslash+"users"+fileSepDoubleBackslash+"workspace \\u00e0\\u00e0\\u00e0"
			
			def val4 = map.get('${SQUASH_TA_INSTALLER_WORKINGDIR_ESC}')
			val4 == canonicalRootDoubleBackslash+"bob"+fileSepDoubleBackslash+"\\u00e9\\u00e9\\u00e9"+fileSepDoubleBackslash+"temp"
			 
			def val5 = map.get('${SQUASH_TA_HOME_SIMPLEBACKSLASHES_ESC}') 
			val5 == canonicalRoot+"groovy"+fileSep+"\\u00e9\\u00e0\\u00e8\\u00e7 rep"
				
	}
	
	
	def "should replace placeholders in file"(){
		
		
		given :
			def context = new Context()
			context.squashDirectory = new File("/bsiri/apps/ééé/squash")
			context.outputDirectory = new File("/d/bsiri/ààà/eclipse")
			context.workingDirectory = new File("/local")
		
		and :
			URL input = this.getClass().getClassLoader().getResource("escaper-test-resources/input-file.epf");
			def inputFile = new File(input.toURI())
			
		and : "temporary directory"
			def outputFile = File.createTempFile("testoutput", "test");
			
		and :
			def filler = new PlaceHolderFiller(context )
			filler.inputFile = inputFile
			filler.outputFile = outputFile
					
		when :
			filler.process();		
		
		then :
			def readIt = new BufferedReader(new FileReader(outputFile));
			readIt.readLine() == ("eclipse.location : "
						+canonicalRootDoubleBackslash+"bsiri"+fileSepDoubleBackslash+"apps"
						+fileSepDoubleBackslash+"\\u00e9\\u00e9\\u00e9"+fileSepDoubleBackslash+"squash"
						+"\\\\eclipse") //the last backslash is in the input file --> it is not substituted
			readIt.readLine() == ("eclipse.packages : "
						+canonicalRoot.replace("\\", "/")+"bsiri/apps/\\u00e9\\u00e9\\u00e9/squash/packages")
			readIt.readLine() == ("user.workspace : "
						+canonicalRootDoubleBackslash+"d"+fileSepDoubleBackslash+"bsiri"
						+fileSepDoubleBackslash+"\\u00e0\\u00e0\\u00e0"+fileSepDoubleBackslash+"eclipse"
						+"\\\\workspace")//ditto
			readIt.readLine() == ("temp.directory : "
						+canonicalRootDoubleBackslash+"local"+"\\\\temp")//yet another one
			readIt.readLine() == ("eclipse.single : "
						+canonicalRoot+"bsiri"+fileSep+"apps"+fileSep+"\\u00e9\\u00e9\\u00e9"
						+fileSep+"squash"+"\\eclipse")
			readIt.close();
	}
	
}
