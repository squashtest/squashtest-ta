/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.commands;

import java.util.Collection;

import org.squashtest.ta.commons.resources.SQLResultSet;
import org.squashtest.ta.commons.resources.SQLScript;
import org.squashtest.ta.commons.targets.DatabaseTarget;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;



/**
 * <p>Will take a sql script and execute it against the given database</p>
 * 
 * @author bsiri
 *
 */
@EngineComponent("execute")
public class SimpleExecuteSQLScriptCommand implements Command<SQLScript, DatabaseTarget> {


	private DatabaseTarget database;
	private SQLScript query;
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		// no params
	}

	@Override
	public void setTarget(DatabaseTarget target) {
		this.database = target;
	}

	@Override
	public void setResource(SQLScript resource) {
		query=resource;
	}

	@Override
	public SQLResultSet apply() {
		
		int[] result = database.execute(query.getBatch());
		
		//TODO : check the result
		return null;
		
	}
	
		
	@Override
	public void cleanUp() {
		//nothing
	}


	
}
