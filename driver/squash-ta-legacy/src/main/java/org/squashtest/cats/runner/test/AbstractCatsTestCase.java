/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.cubictest.model.parameterization.ParameterList;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.operation.DatabaseOperation;
import org.squashtest.cats.data.db.internal.DatabaseAccessMode;
import org.squashtest.cats.io.ResourceNotFoundException;
import org.squashtest.cats.runner.verify.cubic.ParameterListToMapCoercer;
import org.squashtest.cats.runner.verify.cubic.ParameterPersistance;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.test.instructions.BinaryAssertionInstruction;
import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction;
import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.LoadResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.instructions.UnaryAssertionInstruction;
import org.squashtest.ta.legacy.components.SQLParametersMode;
import org.squashtest.ta.legacy.exception.MalformedCatsClassException;
import org.squashtest.ta.legacy.exception.TestTranslationException;
import org.squashtest.ta.legacy.factory.LegacyCatsCallListener;
import org.squashtest.ta.legacy.lib.DbUnitConnector;

/**
 * This class is a facade to integrate legacy CATS tests classes into the new
 * squashTA execution framework. NB: this is statefull, and therefore
 * <strong>NOT thread-safe</strong>. The general principle is as follows:
 * execution of the methods is re-implemented as {@link TestInstruction}
 * generation. These {@link TestInstruction}s are inserted by the Legacy factory
 * into a test suite that is afterwards forwarded to the engine for execution.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractCatsTestCase {

	private static final int AVERAGE_RESOURCE_NAME_ESTIMATE = 15;

	private static final String ASSERT_SQL_TRUE_METHOD_NAME = "assertSqlTrue()";

	private static final String ASSERT_DATASET_EQUALS_METHOD_NAME = "assertDatasetEquals()";

	private static final String RESOURCE_TYPE_NAMED_PARAMETER_SQL = "named.parameter.sql";

	private static final String RESOURCE_TYPE_INDEXED_PARAMETER_SQL = "indexed.parameter.sql";

	private static final String CONVERTER_CATEGORY_PARAMETER_SQL = "parameter.sql";

	private static final String RESOURCE_TYPE_SERIALIZED_SQL_PARMS = "serialized.sql.parms";

	private static final String CONVERTER_CATEGORY_SERIALIZED = "serialized";

	private static final String RESOURCE_TYPE_DIRECTORY = "directory";

	private static final String CONVERT_CATEGORY_FILESYSTEM = "filesystem";

	private static final String CONVERT_CATEGORY_DATASET = "dataset";

	private static final ParameterListToMapCoercer PARAMETER_LIST_TO_MAP_COERCER = new ParameterListToMapCoercer();

	/** Name of the "forget about me" temporary result name to throw ignored result into (nb: one by local context only!)) */
	protected static final ResourceName IGNORED_RESULT_NAME = new ResourceName(Scope.SCOPE_TEMPORARY,"dev-null");

	private static final String COMMAND_CATEGORY_DATASET_EQUALS = "contains";

	private static final String COMMAND_CATEGORY_GET_NEWEST = "get.newest";

	private static final String COMMAND_CATEGORY_GET = "get";

	private static final String EMPTY_MODE_RECURSIVE = "RECURSIVE";

	private static final String COMMAND_CATEGORY_WIPE = "wipe";

	private static final String LOCAL_URL_HOST = "localhost";

	private static final String FILE_URL_PROTOCOL = "file";

	protected static final String COMMAND_CATEGORY_EXECUTE = "execute";

	private static final String RESOURCE_TYPE_SCRIPT_SQL = "sql.query";

	protected static final String CONVERT_CATEGORY_SCRIPT = "script";

	private static final String COMMAND_CATEGORY_INSERT = "insert";

	private static final String RESOURCE_TYPE_DATASET_DBUNIT = "dataset.dbunit";

	private static final String RESOURCE_TYPE_DATASET = "dataset";

	private static final String COMMAND_CATEGORY_DELETE = "delete";

	/** This component listens to instruction generation events emitted by Cats API calls */
	private LegacyCatsCallListener callListener;
	
	private int localContextCounter=0;
	
	public LegacyCatsCallListener getCallListener() {
		return callListener;
	}

	public void setCallListener(LegacyCatsCallListener callListener) {
		this.callListener = callListener;
	}

	/**
	 * Check that a given dataset/target pair is not generated twice.
	 */
	private Set<String>datasetTargetRegistry=new HashSet<String>();
	
	/**
	 * This recovers all setup instructions generated by implicit setup calls
	 * like {@link #setDataset(String, String)}, as opposed to @Before methods
	 */
	private List<TestInstruction>implicitSetup=new ArrayList<TestInstruction>();
	
	/**
	 * Read-only view of the implicit setup.
	 * @return read-only list.
	 */
	public List<TestInstruction> getImplicitSetup() {
		return Collections.unmodifiableList(implicitSetup);
	}
	
	/**
	 * Read-only view of the implicit teardown.
	 * @return read-only list.
	 */
	public List<TestInstruction> getImplicitTearDown() {
		return Collections.unmodifiableList(implicitTearDown);
	}

	/**
	 * This recovers all teardown instructions generated by implicit setup calls
	 * like {@link #setDataset(String, String)}, as opposed to @After methods
	 */
	private List<TestInstruction>implicitTearDown=new ArrayList<TestInstruction>();

	private static final DbUnitConnector DB_UNIT_CONNECTOR = new DbUnitConnector();;
	
	/**
	 * <p>
	 * <b> This method is deprecated. Use method {@link #populateDatabase}
	 * instead.</b>
	 * </p>
	 * <p>
	 * Links a XML dataset file to a DB, so the dataset content will be inserted
	 * into the DB during setup (DbUnit operation CLEAN_INSERT), and deleted
	 * from the DB during teardown (DbUnit operation DELETE_ALL).
	 * </p>
	 * <p>
	 * The file must be located in directory data/db/datasets.
	 * </p>
	 * <p>
	 * This method must be invoked in the overriden constructor of the classes
	 * that inherit from {@link AbstractCubicTestCase}.
	 * </p>
	 * 
	 * @param pDatabaseName
	 *            DB name
	 * @param pDatasetFileName
	 *            XML dataset file name
	 */
	@Deprecated
	protected void setDataset(String pDatabaseName, String pDatasetFileName) {
		String pairName = pDatasetFileName + pDatabaseName;
		
		if (!datasetTargetRegistry.contains(pairName)) {
			
			ResourceName dsName = generateDatasetSetup(pDatabaseName,
					pDatasetFileName);
			
			generateDatasetTeardown(pDatabaseName, dsName);
			
			datasetTargetRegistry.add(pairName);
		}
	}

	@Deprecated
	public Map<String, String> getDatasets() {
		//XXX this is handled completely differently nowadays.
		return new HashMap<String, String>();
	}

	private void generateDatasetTeardown(String pDatabaseName,
			ResourceName dsName) {
		
		ResetLocalContextInstruction beginContext=createLocalContext("setDataset()[teardown]");
		
		ResourceName modeName = new ResourceName(Scope.SCOPE_TEMPORARY,"mode");
		DefineResourceInstruction define=createDefineInstruction(modeName, "DELETE_ALL");
		List<ResourceName>configuration=Arrays.asList(new ResourceName[]{modeName});
		
		ExecuteCommandInstruction cleanupCommand=createExecuteCommandInstruction(pDatabaseName, dsName,IGNORED_RESULT_NAME, "setDataset()[teardown]", configuration, COMMAND_CATEGORY_DELETE);
				
		implicitTearDown.add(beginContext);
		implicitTearDown.add(define);
		implicitTearDown.add(cleanupCommand);
	}

	private ResourceName generateDatasetSetup(String pDatabaseName,
			String pDatasetFileName) {
		
		List<TestInstruction> generatedInstructions=new ArrayList<TestInstruction>();
		
		ResourceName dataSetName = generateDatasetSetupInstructions(
				pDatabaseName, pDatasetFileName, generatedInstructions, "CLEAN_INSERT", false);
		
		for(TestInstruction instruction:generatedInstructions){
			implicitSetup.add(instruction);
		}
		
		return dataSetName;
	}

	private ResourceName generateDatasetSetupInstructions(String pDatabaseName,
			String pDatasetFileName, List<TestInstruction> generatedInstructions, String mode, boolean streaming) {
		ResetLocalContextInstruction beginContext = createLocalContext("setDataset()[setup]");
		generatedInstructions.add(beginContext);
		
		ResourceName localName = new ResourceName(Scope.SCOPE_TEMPORARY, pDatasetFileName);
		LoadResourceInstruction datasetLoad = createLoadInstruction(
				pDatasetFileName, localName, "setDataset()", "dbUnit dataset ");
		generatedInstructions.add(datasetLoad);
		
		ResourceName dataSetName = new ResourceName(pDatasetFileName);
		ConvertResourceInstruction datasetConvert = createConvertInstruction(
				localName, dataSetName, "setDataset()",
				 CONVERT_CATEGORY_DATASET, RESOURCE_TYPE_DATASET_DBUNIT);
		generatedInstructions.add(datasetConvert);
		
		ResourceName opModeName = new ResourceName(Scope.SCOPE_TEMPORARY,"mode");
		DefineResourceInstruction defineOpMode=createDefineInstruction(opModeName, mode);
		generatedInstructions.add(defineOpMode);
		
		ResourceName streamingModeName = new ResourceName(Scope.SCOPE_TEMPORARY,"streamingMode");
		DefineResourceInstruction defineStreaming=createDefineInstruction(streamingModeName, Boolean.toString(streaming));
		generatedInstructions.add(defineStreaming);
		
		List<ResourceName> configuration = Arrays.asList(new ResourceName[]{opModeName,streamingModeName});
		ExecuteCommandInstruction insertCommand = createExecuteCommandInstruction(
				pDatabaseName, dataSetName, IGNORED_RESULT_NAME,"setDataset()", configuration, COMMAND_CATEGORY_INSERT);
		generatedInstructions.add(insertCommand);
		
		return dataSetName;
	}

	/**
	 * <p>
	 * <b>This method is deprecated. Use {@link #assertSqlTrue} methods
	 * instead.</b>
	 * </p>
	 * <p>
	 * Runs a SQL query (may be parameterized).
	 * </p>
	 * <p>
	 * Every row returned by the query is converted into a <code>Map</code> so
	 * that :<br>
	 * --> key = column name (<code>String</code>)<br>
	 * --> value = column content (<code>Object</code>)
	 * </p>
	 * <p>
	 * The result is assigned to instance variable {@link #queryResults}
	 * </p>
	 * 
	 * @param pDbName
	 *            = DB name
	 * @param pQueryName
	 *            = SQL query name
	 * @param pParams
	 *            = Parameters (if the query is parameterized). The number and
	 *            order of the parameters must be the same than in the query.
	 */
	@Deprecated
	protected void getQueryResult(String pDbName, String pQueryName,
			Object... pParams) throws SQLException {
		throw new UnsupportedOperationException("This (deprecated) feature should be replaced by assrtSqlTrue calls. On hold until real life use is assessed.");
	}

	/**
	 * @param pDbName
	 *            DB name
	 * @param pQueryName
	 *            SQL query name
	 * @return
	 */
	protected int executeInsertOrUpdateQuery(String pDbName, String pQueryName)
			throws SQLException {
		
		ResetLocalContextInstruction context=createLocalContext("executeInsertOrUpdateQuery(): ");
		callListener.addInstruction(context);
		
		ResourceName localName = new ResourceName(Scope.SCOPE_TEMPORARY, pQueryName);
		LoadResourceInstruction load = createLoadInstruction(pQueryName,
				localName,
				"executeInsertOrUpdateQuery()", "SQL script");
		callListener.addInstruction(load);
		
		ResourceName scriptName = new ResourceName(pQueryName);
		ConvertResourceInstruction convert = createConvertInstruction(
				localName, scriptName,
				"executeInsertOrUpdateQuery()", CONVERT_CATEGORY_SCRIPT,
				RESOURCE_TYPE_SCRIPT_SQL);
		callListener.addInstruction(convert);
		
		List<ResourceName> configuration = Collections.emptyList();
		ExecuteCommandInstruction command = createExecuteCommandInstruction(
				pDbName, scriptName,IGNORED_RESULT_NAME,
				"executeInsertOrUpdateQuery()", configuration, COMMAND_CATEGORY_EXECUTE);
		callListener.addInstruction(command);
		
		return 0;//XXX don't know if this will be ok ==> let's hope we won't have to struggle with code treating this value!
	}

	/**
	 * @param pDbAccessMode will be ignored as access mode is now decided by target definitions
	 * @param pDbName name of the target database
	 * @param pQueryName name of the query file
	 * @return
	 * @throws SQLException
	 */
	protected int executeInsertOrUpdateQuery(DatabaseAccessMode pDbAccessMode,
			String pDbName, String pQueryName) throws SQLException {
		return executeInsertOrUpdateQuery(pDbName, pQueryName);
	}

	/**
	 * Deletes a file from a FTP server.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".<br>
	 * @param pRemotePath
	 *            Path of the file to delete (relative to the user's home
	 *            directory).
	 */
	protected void delete(String pFtpConfName, String pRemotePath)
			throws IOException {
		
		ResetLocalContextInstruction context=createLocalContext("delete(ftp)");
		callListener.addInstruction(context);
		
		ResourceName pathName=new ResourceName(Scope.SCOPE_TEMPORARY,"path");
		DefineResourceInstruction define=createDefineInstruction(pathName, pRemotePath);
		callListener.addInstruction(define);
		
		List<ResourceName> configuration=Collections.emptyList();
		ExecuteCommandInstruction command=createExecuteCommandInstruction(pFtpConfName, pathName, IGNORED_RESULT_NAME,"delete(ftp)", configuration, COMMAND_CATEGORY_DELETE);
		callListener.addInstruction(command);
		
	}

	/**
	 * Downloads a file from a FTP server and loads it as a {@link FileResource}, without changing its name.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP target.
	 * @param pRemotePath
	 *            Path of the file to download (relative to the user's home
	 *            directory).
	 */
	protected void download(String pFtpConfName, String pRemotePath)
			throws IOException, URISyntaxException {
		download(pFtpConfName,pRemotePath,pRemotePath);
	}

	/**
	 * Downloads a file from a FTP server and loads it as a {@link FileResource}, with a new name.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP target.
	 * @param pLocalName
	 *            Local file name.
	 */
	protected void download(String pFtpConfName, String pRemotePath,
			String pLocalName) throws IOException, URISyntaxException {
		generateDownload(pFtpConfName, pRemotePath, pLocalName, COMMAND_CATEGORY_GET);
	}

	private void generateDownload(String pFtpConfName, String pRemotePath,
			String pLocalName, String category) {
		ResourceName fileResourceName=new ResourceName(pLocalName);
		
		ResetLocalContextInstruction context=createLocalContext("download(ftp)");
		callListener.addInstruction(context);
		
		ResourceName remotePathName=new ResourceName(Scope.SCOPE_TEMPORARY,"remotePath");
		DefineResourceInstruction defineFilename=createDefineInstruction(remotePathName, pRemotePath);
		callListener.addInstruction(defineFilename);
		
		List<ResourceName> toto=Collections.emptyList();
		
		ExecuteCommandInstruction command=createExecuteCommandInstruction(pFtpConfName, remotePathName,IGNORED_RESULT_NAME,"download(ftp)",toto,category);
		command.setResultName(fileResourceName);
		callListener.addInstruction(command);
	}

	/**
	 * T�l�charge un fichier ou plusieurs fichiers �partir d'un serveur FTP et
	 * le load avec le nom xxx/nom. 
	 * 
	 * @param pFtpConfName
	 * @param pRemotePath
	 */
	protected URL downloadToLocalFolder(String pFtpConfName, String pRemotePath)
			throws IOException, URISyntaxException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
		String ftpLocalFolderName = sdf.format(new Date());
		String localName = ftpLocalFolderName+"/"+pRemotePath;
		
		download(pFtpConfName,pRemotePath,localName);
		return new URL(FILE_URL_PROTOCOL,LOCAL_URL_HOST,localName);
	}

	/**
	 * T�l�charge le fichier le plus r�cent parmi plusieurs fichiers � partir
	 * d'un serveur FTP et le load en tant que {@link FileResource} xxx/nom. 
	 * 
	 * @param pFtpConfName
	 *            Target FTP.
	 * @param pRemotePath
	 *            Nom du r�pertoire avec "/" � la fin.
	 * @param pLocalName
	 *            Le nom du fichier CSV r�sultat avec une extension ".csv"
	 */
	protected URL downloadRecentFileToLocalFolder(String pFtpConfName,
			String pRemotePath, String pLocalName) throws IOException,
			URISyntaxException {
		
		generateDownload(pFtpConfName, pRemotePath, pLocalName, COMMAND_CATEGORY_GET_NEWEST);
		
		return new URL(FILE_URL_PROTOCOL,LOCAL_URL_HOST,pLocalName);
	}

	/**
	 * Deletes all the files from the user's home directory.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 */
	protected void empty(String pFtpConfName) throws IOException {
		empty(pFtpConfName, "/");
	}

	/**
	 * Deletes all the files from a given directory.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pDirectoryPath
	 *            of the directory to empty (relative to the user's home
	 *            directory)
	 */
	protected void empty(String pFtpConfName, String pDirectory)
			throws IOException {
		generateEmpty(pFtpConfName, pDirectory, "FILE_ONLY");
	}

	/**
	 * Deletes all the files from the user's home directory and its children
	 * (recursively)
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 */
	protected void emptyAll(String pFtpConfName) throws IOException {
		generateEmpty(pFtpConfName, "/", EMPTY_MODE_RECURSIVE);
	}

	/**
	 * Deletes all the files from a given directory (recursively).
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pDirectoryPath
	 *            of the directory to empty (relative to the user's home
	 *            directory)
	 */
	protected void emptyAll(String pFtpConfName, String pDirectory)
			throws IOException {
		generateEmpty(pFtpConfName, pDirectory, EMPTY_MODE_RECURSIVE);
	}

	private void generateEmpty(String pFtpConfName, String pDirectory, String emptyingMode) {
		ResetLocalContextInstruction context=createLocalContext("empty(ftp)(!)");
		callListener.addInstruction(context);
		
		ResourceName path = new ResourceName(Scope.SCOPE_TEMPORARY,"path");
		DefineResourceInstruction definePath=createDefineInstruction(path, pDirectory);
		callListener.addInstruction(definePath);
		
		ResourceName modeName=new ResourceName(Scope.SCOPE_TEMPORARY,"mode");
		DefineResourceInstruction mode=createDefineInstruction(modeName, emptyingMode);
		callListener.addInstruction(mode);
		List<ResourceName>configuration=Arrays.asList(new ResourceName[]{modeName});
		
		ExecuteCommandInstruction command=createExecuteCommandInstruction(pFtpConfName, path, IGNORED_RESULT_NAME,"empty(ftp)(/!\\)", configuration, COMMAND_CATEGORY_WIPE);
		callListener.addInstruction(command);
	}

	/**
	 * Uploads a file to a FTP server, into the user's home directory, without
	 * changing its name.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pLocalName
	 *            Name of the file to upload (must be located in
	 *            "data/ftp/upload/")
	 * @throws IOException
	 */
	protected void upload(String pFtpConfName, String pLocalName)
			throws IOException {
		generateUpload(pFtpConfName, pLocalName,null);
	}

	/**
	 * Uploads a file to a FTP server, with a new name and/or into a chil
	 * directory of the user's home directory.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pLocalName
	 *            Name of the file to upload (must be located in
	 *            "data/ftp/upload/")
	 * @param pRemotePath
	 *            Path of the uploaded file on the FTP server (relative to the
	 *            user's home directory)
	 */
	protected void upload(String pFtpConfName, String pLocalName,
			String pRemotePath) throws IOException {
		generateUpload(pFtpConfName, pLocalName, pRemotePath);
	}

	private void generateUpload(String pFtpConfName, String pLocalName, String remoteName) {
		ResourceName resourceName = new ResourceName(Scope.SCOPE_TEMPORARY,pLocalName);
		LoadResourceInstruction load=createLoadInstruction(pLocalName, resourceName, "upload(ftp)", "fichier");
		callListener.addInstruction(load);
		
		List<ResourceName>configuration=new ArrayList<ResourceName>();
		if(remoteName!=null){
			ResourceName remoteNameResource=new ResourceName(Scope.SCOPE_TEMPORARY,"remoteName");
			DefineResourceInstruction defineRemoteName=createDefineInstruction(remoteNameResource, remoteName);
			callListener.addInstruction(defineRemoteName);
			
			configuration.add(remoteNameResource);
		}
		
		ExecuteCommandInstruction command=createExecuteCommandInstruction(pFtpConfName, resourceName, IGNORED_RESULT_NAME,"upload(ftp)", configuration, COMMAND_CATEGORY_INSERT);
		callListener.addInstruction(command);
	}

	/**
	 * Compares the content of a DbUnit flat XML dataset and a database and
	 * asserts those match. Only the primary keys, timestamps, tables and
	 * columns that are specified in the dataset are checked.
	 * 
	 * NB: translated as a contains command as it is closer to the real semantics.
	 * 
	 * @param pExpectedDataSetFile
	 *            Name of the flat XML dataset file
	 * @param pDataSource
	 *            Name of the datasource
	 */
	public void assertDatasetEquals(String pExpectedDataSetFile,
			String pDataSource) throws DataSetException,
			ResourceNotFoundException, IOException {
		
		ResetLocalContextInstruction context=createLocalContext(ASSERT_DATASET_EQUALS_METHOD_NAME);
		callListener.addInstruction(context);
		
		ResourceName rawExpectedName=new ResourceName(Scope.SCOPE_TEMPORARY,"raw");
		LoadResourceInstruction load=createLoadInstruction(pExpectedDataSetFile, rawExpectedName, ASSERT_DATASET_EQUALS_METHOD_NAME, pExpectedDataSetFile);
		callListener.addInstruction(load);
		
		ResourceName expectedName=new ResourceName(Scope.SCOPE_TEMPORARY,pExpectedDataSetFile);
		ConvertResourceInstruction convert=createConvertInstruction(rawExpectedName, expectedName, ASSERT_DATASET_EQUALS_METHOD_NAME, RESOURCE_TYPE_DATASET, RESOURCE_TYPE_DATASET_DBUNIT);
		callListener.addInstruction(convert);
		
		ResourceName actualName=new ResourceName(Scope.SCOPE_TEMPORARY,"actual");
		List<ResourceName> commandConfiguration=Collections.emptyList();
		
		ExecuteCommandInstruction query=createExecuteCommandInstruction(pDataSource, expectedName, actualName,ASSERT_DATASET_EQUALS_METHOD_NAME, commandConfiguration, "get");
		callListener.addInstruction(query);
		
		List<ResourceName> config = Collections.emptyList();
		BinaryAssertionInstruction assertion = createBinaryAssertion(
				expectedName, actualName, COMMAND_CATEGORY_DATASET_EQUALS, ASSERT_DATASET_EQUALS_METHOD_NAME, config);
		callListener.addInstruction(assertion);
	}

	/**
	 * <p>
	 * Executes a sql assertion request using the given connection and checks
	 * that expected and actual values from the request match.
	 * </p>
	 * <p>
	 * The assertion request should match the format :<br>
	 * SELECT COUNT (*) AS actual, ... AS expected, ... AS description FROM ...
	 * [ UNION ALL SELECT COUNT (*) AS actual, ... AS expected, ... AS
	 * description FROM ...] [ UNION ALL SELECT COUNT (*) AS actual, ... AS
	 * expected, ... AS description FROM ...] ...
	 * </p>
	 * 
	 * @param pDataSourceName
	 *            name of the datasource used to run the query
	 * @param pSqlQueryName
	 *            name of the sql assertion query
	 * @param pSqlQueryParams
	 *            parameters to the query.
	 */
	public void assertSqlTrue(String pDataSourceName, String pSqlQueryName,
			Object... pSqlQueryParams) {
		assertSqlTrueInternal(pDataSourceName, pSqlQueryName, null, SQLParametersMode.POSITION, pSqlQueryParams);
	}

	private void assertSqlTrueInternal(String pDataSourceName,
			String pSqlQueryName, String failureMessage, SQLParametersMode parametersMode, Object... pSqlQueryParams) {
		ResetLocalContextInstruction context=createLocalContext(ASSERT_SQL_TRUE_METHOD_NAME);
		callListener.addInstruction(context);
		
		ResourceName rawQueryName=new ResourceName(Scope.SCOPE_TEMPORARY,"rawQuery");
		LoadResourceInstruction load=createLoadInstruction(pSqlQueryName, rawQueryName, ASSERT_SQL_TRUE_METHOD_NAME, "SQL query");
		callListener.addInstruction(load);
		
		ResourceName queryName = new ResourceName(Scope.SCOPE_TEMPORARY,
				pSqlQueryName);
		ConvertResourceInstruction convert = createConvertInstruction(
				rawQueryName, queryName, ASSERT_SQL_TRUE_METHOD_NAME,
				CONVERT_CATEGORY_SCRIPT, RESOURCE_TYPE_SCRIPT_SQL);
		callListener.addInstruction(convert);
		
		try {
			File rscFile = serializeSQLPArms(parametersMode, pSqlQueryParams);
			
			ResourceName parmName=new ResourceName(Scope.SCOPE_TEMPORARY,rscFile.getName());
			LoadResourceInstruction lri=createLoadInstruction(rscFile.getPath(), parmName, ASSERT_SQL_TRUE_METHOD_NAME, "serialized SQL parameters");
			callListener.addInstruction(lri);

			ResourceName asSerialized=new ResourceName(Scope.SCOPE_TEMPORARY,"serialized.parms");
			ConvertResourceInstruction serializedConvert=createConvertInstruction(parmName, asSerialized, ASSERT_SQL_TRUE_METHOD_NAME, CONVERTER_CATEGORY_SERIALIZED, RESOURCE_TYPE_SERIALIZED_SQL_PARMS);
			callListener.addInstruction(serializedConvert);
			
			ResourceName sqlParams=new ResourceName(Scope.SCOPE_TEMPORARY,"parms");
			
			String parameterResourceType;
			switch(parametersMode){
			case NAME:
				parameterResourceType=RESOURCE_TYPE_NAMED_PARAMETER_SQL;
				break;
			case POSITION:
				parameterResourceType=RESOURCE_TYPE_INDEXED_PARAMETER_SQL;
				break;
			default:
				throw new MalformedCatsClassException("Unsupported parameter mode: '"+parametersMode.name()+"'");
			}
			ConvertResourceInstruction paramsConvert=createConvertInstruction(asSerialized, sqlParams, ASSERT_SQL_TRUE_METHOD_NAME, CONVERTER_CATEGORY_PARAMETER_SQL, parameterResourceType);
			callListener.addInstruction(paramsConvert);
			
			
			
			ResourceName resultSet=new ResourceName(Scope.SCOPE_TEMPORARY,"resultSet");
			List<ResourceName> configuration = new ArrayList<ResourceName>(); 
			configuration.add(sqlParams);
			
			if(failureMessage!=null){
				ResourceName failureMsg=new ResourceName(Scope.SCOPE_TEMPORARY,"failureMsg");
				DefineResourceInstruction failureMsgDefine=createDefineInstruction(failureMsg, failureMessage);
				callListener.addInstruction(failureMsgDefine);
				configuration.add(failureMsg);
			}
			
			ExecuteCommandInstruction sqlCommand = createExecuteCommandInstruction(
					pDataSourceName, queryName, resultSet,
					ASSERT_SQL_TRUE_METHOD_NAME, configuration, COMMAND_CATEGORY_EXECUTE);
			callListener.addInstruction(sqlCommand);
			
			UnaryAssertionInstruction unary666Instruction = createUnaryAssertion(resultSet, "legacy.666.equals");
			callListener.addInstruction(unary666Instruction);
			
		} catch (IOException e) {
			throw new TestTranslationException("parameter serialization error",e);
		}
	}

	private File serializeSQLPArms(SQLParametersMode mode, Object... pSqlQueryParams)
			throws IOException {
		File rscFile=File.createTempFile("squashTA", "parms.bin");
		ObjectOutputStream oStream=new ObjectOutputStream(new FileOutputStream(rscFile));
		rscFile.deleteOnExit();
		oStream.writeObject(mode.name());
		switch(mode){
		case POSITION:
			int index=0;
			for(Object parm:pSqlQueryParams){
				oStream.writeInt(index);
				oStream.writeObject(parm);
			}
			break;
		case NAME:
			if(pSqlQueryParams.length>1){
				throw new IllegalArgumentException("Multiple named parameter maps is an unsupported case.");
			}
			Map<String, String> namedParams=(Map<String, String>)pSqlQueryParams[0];
			for(Entry<String, String> parameterEntry:namedParams.entrySet()){
				oStream.writeObject(parameterEntry.getKey());
				oStream.writeObject(parameterEntry.getValue());
			}
			break;
		default:
			throw new IllegalArgumentException("Unsupported SQL parameter addressing mode: "+mode.name());
		}
		oStream.close();
		return rscFile;
	}

	/**
	 * Same as {@link #assertSqlTrue(String, String, Object...)} but lets the
	 * client customize the failure message.
	 * 
	 * @param pMessage
	 *            Error message
	 * @param pDataSourceName
	 *            name of the datasource used to run the query
	 * @param pSqlQueryName
	 *            name of the sql assertion query
	 * @param pSqlQueryParams
	 *            parameters to the query.
	 */
	public void assertSqlTrue(String pMessage, String pDataSourceName,
			String pSqlQueryName, Object... pSqlQueryParams) {
		assertSqlTrueInternal(pDataSourceName, pSqlQueryName, pMessage, SQLParametersMode.POSITION, pSqlQueryParams);
	}

	/**
	 * Executes a sql assertion request using the given connection and checks
	 * that expected and actual values from the request match. This method
	 * supports queries with named parameters : select * from TABLE where LABEL
	 * = ${label} This method supports parameters set read from a Cubic test
	 * file using the semantic:
	 * 
	 * <pre>
	 * QueryAssert.assertSqlTrue(&quot;myDataSet&quot;, &quot;myQuery&quot;, parametersFromCubicTest(
	 * 		&quot;path/to/test.aat&quot;, 0));
	 * </pre>
	 * 
	 * The assertion request should match the format :
	 * 
	 * <pre>
	 * SELECT COUNT (*) AS actual, ... AS expected, ... AS description
	 * FROM ... [ UNION ALL SELECT COUNT (*) AS actual, ... AS expected, ... AS description FROM ...] [ UNION ALL SELECT
	 * COUNT (*) AS actual, ... AS expected, ... AS description FROM ...] ...
	 * </pre>
	 * 
	 * @param pDataSource
	 *            name of the datasource used to run the query
	 * @param pSqlAssertionName
	 *            name of the sql assertion query.
	 * @param pNamedParameters
	 *            map of parameter name-value pairs
	 * @see {@link #parametersFromCubicTest(String, int)}
	 */
	public void assertSqlTrue(String pDataSourceName, String pSqlQueryName,
			Map<String, String> pSqlQueryNamedParams) {
		assertSqlTrueInternal(pDataSourceName, pSqlQueryName, null, SQLParametersMode.NAME, pSqlQueryNamedParams);
	}

	/**
	 * Same as {@link #assertSqlTrue(String, String, Map)} but lets the client
	 * customize the failure message.
	 * 
	 * @param pMessage
	 * @param pDataSourceName
	 * @param pSqlQueryName
	 * @param pSqlQueryNamedParams
	 */
	public void assertSqlTrue(String pMessage, String pDataSourceName,
			String pSqlQueryName, Map<String, String> pSqlQueryNamedParams) {
		assertSqlTrueInternal(pDataSourceName, pSqlQueryName, pMessage, SQLParametersMode.NAME, pSqlQueryNamedParams);
	}

	/**
	 * Loads named parameters from a Cubic test parameters file (*.params) with
	 * the given index.
	 * 
	 * @param pParamsFile
	 *            path of the cubic test parameters file (relative to the
	 *            project's root)
	 * @param pParametersIndex
	 *            index of the wanted set of value (starting from 0)
	 * @return
	 */
	public Map<String, String> parametersFromCubicTest(String pTestFile,
			int pParametersIndex) {
		File file=new File(pTestFile);
		if(file.exists()){
			ParameterList params=ParameterPersistance.loadFromFile(file);
			return PARAMETER_LIST_TO_MAP_COERCER.coerceToMap(params, pParametersIndex);
		}else{
			throw new MalformedCatsClassException("Missing test file "+pTestFile);
		}
	}

	/**
	 * Compares two CSV datasets (actual, expected) for equality (as seen by dbunit).
	 * @param actualValuesCsvFolder URL of the folder holding actual results.
	 * @param expectedValuesCsvFolder name of the expected values folder.
	 */
	public void assertRecentCsvEquals(URL actualValuesCsvFolder,
			String expectedValuesCsvFolder) throws URISyntaxException,
			FileNotFoundException, DatabaseUnitException{ 
			if("file".equals(actualValuesCsvFolder.getProtocol()) && LOCAL_URL_HOST.equals(actualValuesCsvFolder.getHost())){
				File directory=new File(actualValuesCsvFolder.getFile());
				if(directory.isDirectory()){
					File[] children=directory.listFiles();
					if(children.length==1){
						createTableOrderingFile(directory, children);
					}else{
						throw new MalformedCatsClassException(actualValuesCsvFolder.getPath()+" should contain one and only one csv file.");
					}
				}else{
					throw new MalformedCatsClassException(actualValuesCsvFolder.getPath()+" is no valid directory.");
				}
				
				ResetLocalContextInstruction context=createLocalContext("assertRecentCsvEquals()");
				callListener.addInstruction(context);
				
				ResourceName actual=loadCsvDataSet(actualValuesCsvFolder.getFile(), "actual");
				
				ResourceName expected=loadCsvDataSet(expectedValuesCsvFolder, "expected");
				
				List<ResourceName> config = Collections.emptyList();
				BinaryAssertionInstruction datasetEqualsAssertion=createBinaryAssertion(expected, actual, COMMAND_CATEGORY_DATASET_EQUALS, "assertRecentCsvEquals()", config);
				callListener.addInstruction(datasetEqualsAssertion);
				
			}else{
				throw new MalformedCatsClassException("Unsupported actual URL: '"+actualValuesCsvFolder.toExternalForm()+"'");
			}
	}

	private ResourceName loadCsvDataSet(String resourcePath, String datasetRole) {
		ResourceName fileName=new ResourceName(Scope.SCOPE_TEMPORARY,datasetRole+".file");
		LoadResourceInstruction loadActual=createLoadInstruction(resourcePath, fileName, "assertRecentCsvEquals(sic!)", "actual CSV folder");
		callListener.addInstruction(loadActual);
		
		ResourceName directoryName=new ResourceName(Scope.SCOPE_TEMPORARY,datasetRole+".directory");
		ConvertResourceInstruction convertToDirectory=createConvertInstruction(fileName, directoryName, "assertRecentCsvEquals()", CONVERT_CATEGORY_FILESYSTEM, RESOURCE_TYPE_DIRECTORY);
		callListener.addInstruction(convertToDirectory);
		
		ResourceName dataSetName=new ResourceName(Scope.SCOPE_TEMPORARY,datasetRole);
		ConvertResourceInstruction convertToDSet=createConvertInstruction(directoryName, dataSetName, "assertCsvEquals()", CONVERT_CATEGORY_DATASET, RESOURCE_TYPE_DATASET_DBUNIT);
		callListener.addInstruction(convertToDSet);
		
		return dataSetName;
	}

	private void createTableOrderingFile(File directory, File[] children) {
		String name=children[0].getName();
		int extensionSeparator = name.lastIndexOf('.');
		if(extensionSeparator>0){
			name=name.substring(0, extensionSeparator-1);
		}
		BufferedWriter writer=null;
		try {
			writer=new BufferedWriter(new FileWriter(new File(directory,"table-ordering.txt")));
			writer.write(name);
			writer.newLine();
		} catch (IOException e) {
			throw new TestTranslationException("Error while creating dbunit table ordering.",e);
		}finally{
			try {
				if(writer!=null){
					writer.close();
				}
			} catch (IOException e) {
				throw new TestTranslationException("Error while creating dbunit table ordering.",e);
			}
		}
	}

	/**
	 * Operation to populate a database on demand (possible operations are
	 * standards Dbunit operations : INSERT, DELETE, REFRESH, UPDATE,
	 * DELETE_ALL, TRUNCATE_TABLE
	 * 
	 * @param dbname
	 * @param datasetname
	 * @param operation
	 * @throws DataSetException
	 * @throws IOException
	 */
	public void populateDatabase(String dbname, String datasetname,
			DatabaseOperation operation) throws DataSetException, IOException {
		populateDatabase(dbname, datasetname, operation,false);
	}
	
	/**
	 * Operation to populate a database on demand (possible operations are
	 * standards Dbunit operations : INSERT, CLEAN_INSERT, DELETE, REFRESH,
	 * UPDATE, DELETE_ALL, TRUNCATE_TABLE
	 * 
	 * @param dbname
	 * @param datasetname
	 * @param operation
	 * @param streaming
	 * @throws DataSetException
	 * @throws IOException
	 */
	public void populateDatabase(String dbname, String datasetname,
			DatabaseOperation operation, boolean streaming)
			throws DataSetException, IOException {
		List<TestInstruction> populateInstructions=new ArrayList<TestInstruction>();
		String mode = DB_UNIT_CONNECTOR.translateToCode(operation);
		generateDatasetSetupInstructions(dbname, datasetname, populateInstructions, mode, streaming);
		for(TestInstruction instruction:populateInstructions){
			callListener.addInstruction(instruction);
		}
	}

	protected UnaryAssertionInstruction createUnaryAssertion(
			ResourceName resultSet, String category) {
		UnaryAssertionInstruction unary666Instruction=new UnaryAssertionInstruction();
		unary666Instruction.setActualResultName(resultSet);
		unary666Instruction.setAssertionCategory(category);
		unary666Instruction.setText(ASSERT_SQL_TRUE_METHOD_NAME+": assert["+category+"]("+resultSet.getName()+")");
		return unary666Instruction;
	}

	protected BinaryAssertionInstruction createBinaryAssertion(
			ResourceName expectedName, ResourceName actualName, String category, String translatedMethodName, Collection<ResourceName> configuration) {
		BinaryAssertionInstruction assertion=new BinaryAssertionInstruction();
		assertion.setActualResultName(actualName);
		assertion.setExpectedResultName(expectedName);
		assertion.setAssertionCategory(category);
		assertion.addAssertionConfiguration(configuration);
		assertion.setText(translatedMethodName+": ("+actualName+" "+category+" "+expectedName+") {}");
		return assertion;
	}

	protected ConvertResourceInstruction createConvertInstruction(
			ResourceName inputResourceName,
			ResourceName outputResourceName, String translatedMethodName, String category, String resultType) {
		ConvertResourceInstruction datasetConvert=new ConvertResourceInstruction();
		datasetConvert.setConverterCategory(category);
		datasetConvert.setResourceName(inputResourceName);
		datasetConvert.setResultName(outputResourceName);
		datasetConvert.setResultType(resultType);
		datasetConvert.setText(translatedMethodName+": convert "+inputResourceName.getName()+" to "+resultType+" resource.");
		return datasetConvert;
	}

	protected DefineResourceInstruction createDefineInstruction(ResourceName name, String value) {
		DefineResourceInstruction define=new DefineResourceInstruction();
		define.setResourceName(name);
		define.addLine(value);
		define.setText("define "+name.getName()+" as "+value);
		return define;
	}

	protected ExecuteCommandInstruction createExecuteCommandInstruction(
			String targetName,
			ResourceName inputResourceName, ResourceName resultName,String translatedMethodName, Collection<ResourceName> commandConfiguration, String category) {
		ExecuteCommandInstruction command=new ExecuteCommandInstruction();
		command.setCommandCategory(category);
		command.setResourceName(inputResourceName);
		command.setTargetName(targetName);
		command.addCommandConfiguration(commandConfiguration);
		command.setResultName(resultName);
		command.setText(translatedMethodName + ": " + category + "("
				+ inputResourceName.getName() + ") on target " + targetName + "{"
				+ configurationToString(commandConfiguration) + "}=>"+resultName.toString());
		return command;
	}

	/**
	 * Creates a load resource instruction.
	 * @param pFileName name of the source file
	 * @param resourceName name of the resource to create
	 * @param translatedMethodName name of the translated method (for instruction text)
	 * @param resourceDesignation designation of the loaded resource (for instruction text)
	 * @return
	 */
	protected LoadResourceInstruction createLoadInstruction(
			String pFileName, ResourceName resourceName, String translatedMethodName, String resourceDesignation) {
		LoadResourceInstruction datasetLoad=new LoadResourceInstruction();
		datasetLoad.setResourceName(resourceName);
		datasetLoad.setText(translatedMethodName+": load "+pFileName+" "+resourceDesignation+" as file resource"+resourceName.toString());
		return datasetLoad;
	}

	protected ResetLocalContextInstruction createLocalContext(String textStub) {
		ResetLocalContextInstruction beginContext=new ResetLocalContextInstruction();
		beginContext.setContextIdentifier(localContextCounter++);
		beginContext.setText(textStub+": localContext #"+localContextCounter);
		return beginContext;
	}

	private String configurationToString(Collection<ResourceName>configuration){
		StringBuilder builder=new StringBuilder(AVERAGE_RESOURCE_NAME_ESTIMATE*configuration.size());
		for(ResourceName element:configuration){
			builder.append(element.toString()).append(",");
		}
		if(builder.length()>0){
			builder.setLength(builder.length()-1);
		}
		return builder.toString();
	}

}
