/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.link;

import org.squashtest.ta.api.execution.client.StatusUpdate;
import org.squashtest.ta.framework.test.event.EcosystemStatusUpdate;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate;
import org.squashtest.tm.api.testautomation.execution.dto.TestSuiteExecutionStatus;

/**
 * Event listener to post {@link TestStatusUpdate} events back to a Squash TM API- compliant RESTful service. 
 * @author edegenetais
 *
 */
public class RestTestSuiteStatusUpdateListener extends RestSquashTMUpdateCallback implements StatusUpdateListener{

	public RestTestSuiteStatusUpdateListener(StatusUpdate updateClient,
			String executionHostname, String jobName, String externalId) {
		super(updateClient, executionHostname, jobName, externalId);
	}

	@Override
	public void handle(TestSuiteStatusUpdate event) {
		TestSuiteExecutionStatus status=new TestSuiteExecutionStatus();
		status.setSuiteName(event.getPayload().getName());
		
		TaTmStatusMapping statusMapping=TaTmStatusMapping.getMapping(event.getPayload().getStatus());
		status.setStatus(statusMapping.getTmStatus());
		
		status.setStartTime(event.getPayload().startTime());
		status.setEndTime(event.getPayload().endTime());
		
		getUpdateClient().updateTestSuiteExecutionStatus(getExecutionHostname(), getJobName(), getExternalId(), status);
	}

	@Override
	public void handle(EcosystemStatusUpdate event) {}//this listener only handles test suite events.

	@Override
	public void handle(TestStatusUpdate event) {}//this listener only handles test suite events.
	
}
