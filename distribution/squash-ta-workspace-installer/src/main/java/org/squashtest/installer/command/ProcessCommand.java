/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.command;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import org.squashtest.installer.context.Context;

public class ProcessCommand implements Command{

	private Context context;
	
	private boolean synchronous;
	
	private List<String> commandTokens=new LinkedList<String>();
	
	
	
	@Override
	public void setContext(Context context) {
		this.context=context;
	}

	@Override
	public Context getContext() {
		return context;
	}
	
	public boolean isSynchronous(){
		return synchronous;
	}
	
	public void setSynchronous(boolean synch){
		synchronous=synch;
	}

	
	@Override
	//todo : implement a better treatment of exceptions.
	public void execute() {
		try{
			ProcessBuilder builder = new ProcessBuilder(commandTokens);
			Process process = builder.start();
			
			ProcessListener listener = new ProcessListener(process);
			listener.setPriority(Thread.MIN_PRIORITY);
			
			Thread waiter = new ProcessWaiter(process, listener);
			waiter.setPriority(Thread.MIN_PRIORITY);
			
			listener.start();
			waiter.start();
						
			if (isSynchronous()){
				int ret=process.waitFor();
				listener.join();
				waiter.join();
				if (ret!=0){
					throw new RuntimeException("TA workspace installer : sub process failed.");
				}
			}
		}catch(IOException ioe){
			throw new RuntimeException(ioe);
		}catch(InterruptedException ie){
			throw new RuntimeException(ie);
		}
	}

	public void addCommandToken(String token){
		commandTokens.add(token);
	}
	
	public void addAllTokens(List<String> tokens){
		commandTokens.addAll(tokens);
	}

	//that runnable will run until the end of the other process triggers an IOException when trying to read the streams.
	private static class ProcessListener extends  Thread{


		private static final int RUNNING_STATE=1;
		private static final int STOPPING_STATE=2;
		
		private int state = RUNNING_STATE;
		
		BufferedReader procOut;
		BufferedReader procErr;		
		PrintStream outStream;
		
		private ProcessListener(Process process){
			
			procOut=new BufferedReader(new InputStreamReader(process.getInputStream()));
			procErr=new BufferedReader(new InputStreamReader(process.getErrorStream()));
			outStream=new PrintStream(System.out);
		}
		
		@Override
		public void run() {
			try{
				do{
					Thread.sleep(1000);
					String buffer;
					
					StringBuilder output = new StringBuilder();
					while((buffer=procOut.readLine())!=null) output.append(buffer+"\n");
					outStream.println(output.toString());
					
					StringBuilder error = new StringBuilder();
					while((buffer=procOut.readLine())!=null) error.append(buffer+"\n");
					outStream.println(error.toString());
	
				}while(state == RUNNING_STATE);	
			}catch(InterruptedException ie){
				//see finally
			}catch(IOException ioe){
				//see finally
			}
			finally{
				//exceptions mean end of the watched process : it means end of this thread too
				closeAll();
			}
		}
		
		public void warnMe(){
			state = STOPPING_STATE;
		}
		
		private void closeAll(){
			try{
				procOut.close();
				procErr.close();
				outStream.close();
			}catch(IOException ioe){
				throw new RuntimeException(ioe);
			}
		}
	}
	
	private static class ProcessWaiter extends Thread{
		
		Process process;
		ProcessListener listener;
		
		private ProcessWaiter(Process process, ProcessListener listener){
			this.process=process;
			this.listener=listener;
		}
		
		@Override
		public void run(){
			try{
				process.waitFor();
			}catch(InterruptedException ie){
				//see finally
			}finally{
				notifyListener();
			}
		}
		
		private void notifyListener(){
			listener.warnMe();
		}
	}
	
}
