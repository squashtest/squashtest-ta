/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.init;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.framework.components.RepositoryCreator;
import org.squashtest.ta.framework.components.ResourceRepository;

/**
 * This class regroups all instances of RepositoryCreator it finds. When the frameworks initializes, it will first look for
 * ResourceRepositories in the main configuration and try to create them. The success of this operation rely on the RepositoryCreator
 * instances that the RepositoryCreatorPool holds, and will query each of them for each properties files it will be given. 
 * 
 * @author bsiri
 *
 */
@Component
public class RepositoryCreatorPool {
	
	@Autowired(required=false)
	private List<RepositoryCreator<?>> creators = new LinkedList<RepositoryCreator<?>>();
	
	@Inject
	private EngineComponentDefinitionManager definitionManager;
	
	public void addRepositoryCreator(RepositoryCreator<?> creator){
		creators.add(creator);
	}
	
	public RepositoryWrapper wrapRepository(ResourceRepository repo){
		Nature nat = definitionManager.fetchOrAddUniqueType(repo.getClass()); 
		return new RepositoryWrapper("default", nat, repo);
	}
	
	/**
	 * @param propertiesFile 
	 * @return if one of the RepositoryCreators could handle the file, null if none could.
	 */
	public RepositoryWrapper createRepository(URL propertiesFile){
		for (RepositoryCreator<?> creator : creators){
			if (creator.canInstantiate(propertiesFile)){
				ResourceRepository repo =  creator.createRepository(propertiesFile);
				RepositoryWrapper wrapper = wrapRepository(repo);
				String shortName = getShortName(propertiesFile.getPath());
				wrapper.setName(shortName);
				return wrapper;
			}
		}
		return null;
	}
	
	private String getShortName(String filePath){
		String name = FilenameUtils.getName(filePath);
		int suffixIndex = name.lastIndexOf(".properties");
		if (suffixIndex==-1){
			return name;
		}else{
			return name.substring(0, suffixIndex);
		}
	}
	

	

}
