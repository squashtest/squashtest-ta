/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.squashtest.ta.backbone.engine.AssertionFindSettings;
import org.squashtest.ta.backbone.engine.impl.AssertionFindSettingsImpl;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler;
import org.squashtest.ta.backbone.exception.CannotApplyAssertionException;
import org.squashtest.ta.backbone.exception.IncompatibleNaturesException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.backbone.test.DefaultResourceMetadata;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.instructions.UnaryAssertionInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.ResourceMetadata.ResourceRole;

//TODO : fix the factory accordingly to the split between unary and binary assertions
public class DefaultUnaryAssertionRunner extends
		AbstractDefaultInstructionRunner {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUnaryAssertionRunner.class); 
	
	private UnaryAssertionInstruction instruction;
	
	private static final String MISSING_ACTUAL_RESULT_NAME_ERROR = "the name of the actual result is missing";
	private static final String MISSING_ASSERTION_NAME_ERROR = "the identifier of the assertion is missing";
	private static final String GENERIC_ERROR_MESSAGE = "Cannot apply assertion : ";
	
	public DefaultUnaryAssertionRunner(UnaryAssertionInstruction instruction) {
		this.instruction=instruction;
	}

	@Override
	protected void checkSettings() {
		if (instruction.getActualResultName()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_ACTUAL_RESULT_NAME_ERROR);
		}
		
		if (instruction.getAssertionCategory()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_ASSERTION_NAME_ERROR);
		}
	}

	@Override
	protected TestInstruction getInstruction() {
		return instruction;
	}

	@Override
	protected void doRun() {
	
		ResourceWrapper actualResult = getActualResult();
		
		UnaryAssertionHandler assertion = setupAssertion(actualResult);
		
		try{
			//apply the assertion
			assertion.test();
		}catch(AssertionFailedException afe){
			if(afe.getActual()==null){
				AssertionFailedException completeReport=new AssertionFailedException(afe.getMessage(), actualResult.unwrap(), afe.getFailureContext());
				completeReport.initCause(afe);//so sad Mr Checkstyle shuns initCause()
				throw completeReport;//NOSONAR
			}else{
				throw afe;
			}
		}
	}
	
	@Override
	protected void addInputToFailureReport(ExecutionDetails executionDetails) {
		ResourceWrapper actualResult = testRunner.getResourceFromCache(instruction.getActualResultName());
		if (actualResult != null) {
			ResourceAndContext actualRac = new ResourceAndContext();
			actualRac.resource = actualResult.unwrap().copy();

			DefaultResourceMetadata actualMetadata = new DefaultResourceMetadata(
					actualResult);
			actualMetadata.setRole(ResourceRole.ACTUAL_RESULT);
			actualRac.metadata = actualMetadata;
			executionDetails.getResourcesAndContext().add(actualRac);
		}
	}
	
	/* *********** private stuffs *************** */
	
	private ResourceWrapper getActualResult(){
		return fetchResourceOrFail(instruction.getActualResultName());
	}
	
	private AssertionFindSettings buildSettings(ResourceWrapper actualResource){
		
		AssertionFindSettings settings = new AssertionFindSettingsImpl();
		settings.setActualResultNatureName(actualResource.getNature().getName());
		settings.setAssertionCategory(instruction.getAssertionCategory());

		return settings;
	}
	
	//if an assertion is not found because the settings contains unknown natures or identifier,
	//if doesn't mean the operation fail. The diagnostic will be run later.
	private UnaryAssertionHandler safeFindAssertion(AssertionFindSettings settings){
		try{
			Collection<UnaryAssertionHandler> found = testRunner.getContextManager().getUnaryAssertion(settings);
			if (found.size()==1){
				return found.iterator().next();
			}else{
				return null;
			}
		}catch(ResourceNotFoundException ex){
			return null;
		}
	}
	
	//will attemps to get the assetion. In case it's impossible because of incompatible natures, if the test runner is set to autoconvert
	//it will retry with optimistic conversion.
	private UnaryAssertionHandler setupAssertion(ResourceWrapper actualResource){
		
		AssertionFindSettings settings = buildSettings(actualResource);
		UnaryAssertionHandler assertion;
		
		assertion = safeFindAssertion(settings);
		
		if (assertion!=null){
			assertion.setActualResult(actualResource);
			Collection<ResourceWrapper> conf = fetchConfiguration(instruction.getAssertionConfiguration());
			assertion.addConfiguration(conf);
			return assertion;
		}else{
			
				BrokenTestException error = diagnoseError(actualResource, instruction.getAssertionCategory());
				throw error;
		}		
	}
	
	//basically, the question is : was absolutely no assertion found, or was the user assertion definition ambiguous ? 
	private BrokenTestException diagnoseError(ResourceWrapper actualResult, String assertionName){
		AssertionFindSettings settings = buildSettings(actualResult);
		settings.setActualResultNatureName(null);
		
		Collection<UnaryAssertionHandler> assertions = null;
		
		assertions = testRunner.getContextManager().getUnaryAssertion(settings);
		
		if (assertions.size()!=0){
			return throwIncompatibleNatureException(actualResult, assertionName);
		}
		else{
			return throwCannotApplyAssertionException(assertionName);
		}
	}
	
	private BrokenTestException throwIncompatibleNatureException(ResourceWrapper actual, String assertionName){
		ResourceName actualName = actual.getName();
		String actNatureName = actual.getNature().getName();
		
		FormattingTuple tuple = MessageFormatter.arrayFormat("Cannot apply unary assertion : assertion '{}' found but cannot handle resource '{}'. " +
				"This is probably due to incompatible natures : '{}' is of nature '{}' , and no assertion in the context is able " +
				"to handle that nature and all conversion attempts failed. If needed, consider explicit conversion and deactivate" +
				" auto conversion.", new Object[]{assertionName, actualName, actualName, actNatureName});
		
		
		LOGGER.error(tuple.getMessage());
		
		return new IncompatibleNaturesException(tuple.getMessage());		
		
	}
	
	
	private BrokenTestException throwCannotApplyAssertionException(String assertionName){
		CannotApplyAssertionException ex = new CannotApplyAssertionException("Cannot apply assertion : assertion'"+assertionName+"' (unary) not found.");
		LOGGER.error(ex.getMessage());
		return ex;		
	}

	@Override
	protected String getInstructionGenericFailureMessage() {
		return GENERIC_ERROR_MESSAGE;
	}
	

}
