/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.filechecker.resources;

import org.squashtest.ta.filechecker.library.utils.xpath.XpathAssertions;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;

@ResourceType("fixed.field.file.queries")
public class FFFQueriesResource implements Resource<FFFQueriesResource> {

	private XpathAssertions assertions;
	
	/**
	 * No arg constructor for Spring
	 */
	public FFFQueriesResource() {
	
	}
	
	
	/**
	 * Constructor
	 * 
	 * @param assertions List of Xpath assertions 
	 */
	public FFFQueriesResource(XpathAssertions assertions) {
		this.assertions = assertions;
	}



	@Override
	public FFFQueriesResource copy() {
		return new FFFQueriesResource(assertions);
	}

	@Override
	public void cleanUp() {
		// No op
		
	}


	public XpathAssertions getAssertions() {
		return assertions;
	}

}
