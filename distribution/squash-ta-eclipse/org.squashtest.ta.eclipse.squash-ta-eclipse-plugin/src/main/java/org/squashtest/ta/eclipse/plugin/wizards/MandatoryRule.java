/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Text;

/**
 * This rule implements a rule to define a text field as mandatory.
 * @author edegenetais
 *
 */
public class MandatoryRule implements PageRule {
	private Text field;
	private String fieldName;
	private Page page;
	private String id;
	//private boolean isActivated;
	
	/**
	 * Create the rule instance to make a field mandatory.
	 * @param targetField the target text field.
	 * @param targetFieldName the name of the target field (for the violation message)
	 * @param targetPage the page the field belongs to.
	 */
	public MandatoryRule(Text targetField, String targetFieldName, Page targetPage) {
		id=toString();
		fieldName=targetFieldName;
		page=targetPage;
		field=targetField;
		field.addModifyListener(new MandatoryModifyListener());
	}
	
	/**
	 * This listener class is internal so that nobody tries to add the rule as listener - she adds its own listener.
	 * @author edegenetais
	 *
	 */
	private class MandatoryModifyListener implements ModifyListener{
		public void modifyText(ModifyEvent arg0) {
			if(arg0.getSource()==field){
				if(check()){//OK
					page.updateStatus(id, null);
				}else{//KO
					page.updateStatus(id, getMessage());
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.squashtest.ta.eclipse.plugin.wizards.PageRule#check()
	 */
	@Override
	public boolean check() {
		if(field.isEnabled())
		{
			if(field.getText()==null || field.getText().trim().length()==0){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.squashtest.ta.eclipse.plugin.wizards.PageRule#getMessage()
	 */
	@Override
	public String getMessage(){
		return "Field "+fieldName+" is mandatory.";
	}

	public String getId() {
		return id;
	}
	
	public void activate() {
		if(check()){//OK
			page.updateStatus(id, null);
		}else{//KO
			page.updateStatus(id, getMessage());
		}
	}
	
	public void deactivate() {
		page.updateStatus(id, null);
	}
}
