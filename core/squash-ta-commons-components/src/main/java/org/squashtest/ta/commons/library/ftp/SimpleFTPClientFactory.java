/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.ftp;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.net.ftp.FTPClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleFTPClientFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleFTPClientFactory.class);
	
	public static final String FTP_HOST_KEY = "squashtest.ta.ftp.host";					
	public static final String FTP_LOGIN_KEY = "squashtest.ta.ftp.username";
	public static final String FTP_PASSWORD_KEY = "squashtest.ta.ftp.password";
	public static final String FTP_PORT_KEY = "squashtest.ta.ftp.port";
	public static final String FTP_FILETYPE_KEY = "squashtest.ta.ftp.fileType";
	public static final String FTP_SYSTEM_KEY ="squashtest.ta.ftp.system";
	
	public static final String FTP_KEYS_REGEXP = "squashtest\\.ta\\.ftp\\..*";
	
	static private final Map<String, String> SUPPORTED_SYSTEMS = new HashMap<String, String>();
	
	static {
		SUPPORTED_SYSTEMS.put("unix", FTPClientConfig.SYST_UNIX);
		SUPPORTED_SYSTEMS.put("vms", FTPClientConfig.SYST_VMS);
		SUPPORTED_SYSTEMS.put("windows", FTPClientConfig.SYST_NT);
		SUPPORTED_SYSTEMS.put("os/2", FTPClientConfig.SYST_OS2);
		SUPPORTED_SYSTEMS.put("os/400", FTPClientConfig.SYST_OS400);
		SUPPORTED_SYSTEMS.put("as/400", FTPClientConfig.SYST_AS400);
		SUPPORTED_SYSTEMS.put("mvs", FTPClientConfig.SYST_MVS);
		SUPPORTED_SYSTEMS.put("l8", FTPClientConfig.SYST_L8);
		SUPPORTED_SYSTEMS.put("netware", FTPClientConfig.SYST_NETWARE);
		SUPPORTED_SYSTEMS.put("macos", FTPClientConfig.SYST_MACOS_PETER);
	}	
	
	
	public SimpleFTPClient createClient(Properties properties){
		
		SimpleFTPClient client = new SimpleFTPClient();
		
		String host = properties.getProperty(FTP_HOST_KEY);
		client.setHostName(host);
		
		String username = properties.getProperty(FTP_LOGIN_KEY);
		client.setUsername(username);
		
		String password = properties.getProperty(FTP_PASSWORD_KEY);
		client.setPassword(password);
		
		Integer port = readPort(properties);
		client.setPort(port);
		
		Integer filetype = readFiletype(properties);
		client.setFiletype(filetype);
		
		String system = readSystem(properties);
		client.setSystem(system);
		
		LOGGER.debug("Client configured to open "+username+"@"+host+":"+port+" with pw "+password);
		
		return client;
	}
	
	private Integer readPort(Properties properties){
		
		String strPort = properties.getProperty(FTP_PORT_KEY);
		if (strPort == null){
			return null;
		}
		
		Integer result=null;
		try{
			result = Integer.decode(strPort);
		}catch(NumberFormatException ex){
			if (LOGGER.isWarnEnabled()){
				LOGGER.warn("ftp creator : supplied port number is not an valid integer. Proceeding with default port : 21");
			}
		}
		
		return result;		
		
	}
	
	
	/**
	 * will decode from the properties 'filetype' either an int or a string.
	 * @param properties
	 * @return
	 */
	private Integer readFiletype(Properties properties){
		
		String fileType = properties.getProperty(FTP_FILETYPE_KEY);
		if (fileType==null){
			return null;
		}
		
		Integer result = SimpleFTPClient.decodeFileType(fileType);
		
		if (result == null){
			if (LOGGER.isWarnEnabled()){
				LOGGER.warn("ftp creator : the supplied file type is not supported. Supported file type : 'ascii', 'binary'");
			}
		}
		
		return result;
		
	}
	


	
	/*
	 * returns null if the supplied system
	 */
	private String readSystem(Properties properties){
		
		String raw = properties.getProperty(FTP_SYSTEM_KEY);
		if (raw==null){
			return null;
		}
		
		String lowRaw = raw.toLowerCase();
		String result = SUPPORTED_SYSTEMS.get(lowRaw);
		
		if (result == null){
			if (LOGGER.isWarnEnabled()){
				LOGGER.warn("ftp creator : supplied system parameter is not supported, proceeding with defaults");
			}
		}
		
		return result;
	}	
}
