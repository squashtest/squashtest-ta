/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.soapui.library;

import java.util.Map;

import org.squashtest.ta.commons.library.java.ProcessStreamListener;
import org.squashtest.ta.commons.library.shell.OutputStream;

import com.eviware.soapui.model.testsuite.TestRunner.Status;

class PipeListener extends ProcessStreamListener {
	private Element currentElement;
	private Map<String,Status>testStatusMap;
	private Map<String,String>testMessageMap;
	PipeListener(Process process, Map<String,Status> testStatusMap, Map<String,String>testMessageMap) {
		super(process,OutputStream.out);
		this.testStatusMap=testStatusMap;
		this.testMessageMap=testMessageMap;
		this.currentElement=new IntervalElement();
	}

	@Override
	protected void commitOutputLine(String osString) {
		currentElement=currentElement.add(osString.trim(),testStatusMap,testMessageMap);
	}
}