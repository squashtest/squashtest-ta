/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.init;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;
import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.engine.wrapper.SpringObjectFactory;
import org.squashtest.ta.backbone.exception.EngineInitException;
import org.squashtest.ta.backbone.exception.ResourceTypeConflictException;
import org.squashtest.ta.backbone.tools.ReflectionUtils;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.components.UnaryAssertion;

/**
 * <p>The goal of the EngineComponentClassManager is :
 * 	<ul>
 * 		<li>parse the bean factory definitions, find the framework classes and set their scope to Prototype, except for repositories and 
 * 		targetcreators.</li>
 * 		<li>create the component definitions
 * 	</ul> 
 * <p>
 * @author bsiri
 *
 */

// TODO : consider splitting this into several objects as it may have too many responsibilities
@Component
public class EngineComponentDefinitionManager implements BeanFactoryPostProcessor {

	private BeanFactory springFactory;

	private static final Logger LOGGER = LoggerFactory.getLogger(EngineComponentDefinitionManager.class); 
	
	private ReflectionUtils reflector = new ReflectionUtils();
	
	private List<ConverterDefinition> converterDefinitions = new LinkedList<ConverterDefinition>();
	private List<CommandDefinition> commandDefinitions = new LinkedList<CommandDefinition>();
	private List<UnaryAssertionDefinition> unaryAssertionDefinitions = new LinkedList<UnaryAssertionDefinition>();
	private List<BinaryAssertionDefinition> binaryAssertionDefinitions = new LinkedList<BinaryAssertionDefinition>();
	
	
	/* 
	 * We keep tracks of two sets of nature here : those corresponding to Resource's ResourceType, and those corresponding to EngineComponent's categories.
	 * 
	 * They obey to different rules : 
	 * 	for ResourceTypes : One ResourceType#value() <-> One Nature <-> One Resource class.
	 *  for EngineComponent : One EngineComponent#value() <-> One Nature <-> multiple EngineComponent classes.
	 *  
	 * Think about it when reading the code below.
	 */
	private Map<Class<?>, Nature> naturesMap = new HashMap<Class<?>, Nature>();
	private Map<String, Nature> categoryMap = new HashMap<String, Nature>();
	
	
	
	/* *********** the following methods provide the refined information that the initializer needs ************ */
	
	
	public ObjectFactory getFactory(){
		SpringObjectFactory factory = new SpringObjectFactory();
		factory.setBeanFactory(springFactory);
		return factory;
	}
	
	
	public List<ConverterDefinition> getConverterDefinitions(){
		return converterDefinitions;
	}
	
	public List<CommandDefinition> getCommandDefinitions(){
		return commandDefinitions;
	}
	
	
	public List<UnaryAssertionDefinition> getUnaryAssertionDefinitions() {
		return unaryAssertionDefinitions;
	}


	public List<BinaryAssertionDefinition> getBinaryAssertionDefinitions() {
		return binaryAssertionDefinitions;
	}
	
	
	
	/* ********************** the ugly work starts here *********************** */

	public void postProcessBeanFactory(
			ConfigurableListableBeanFactory beanFactory) throws BeansException {
		
		springFactory=beanFactory;
		
		BeanDefinition def=null;
		
		try {
			String[] allDefNames = beanFactory.getBeanDefinitionNames();

			for (String dName : allDefNames) {
				
				def = beanFactory.getBeanDefinition(dName);
				Class<?> defClass = Class.forName(def.getBeanClassName());
				
				if (ResourceConverter.class.isAssignableFrom(defClass)) {
					def.setScope(BeanDefinition.SCOPE_PROTOTYPE);
					processAsResourceConverter(defClass);
				}
				else if (Command.class.isAssignableFrom(defClass)){
					def.setScope(BeanDefinition.SCOPE_PROTOTYPE);
					processAsCommand(defClass);
				}
				else if (BinaryAssertion.class.isAssignableFrom(defClass)){
					def.setScope(BeanDefinition.SCOPE_PROTOTYPE);
					processAsBinaryAssertion(defClass);
				}
				else if (UnaryAssertion.class.isAssignableFrom(defClass)){
					def.setScope(BeanDefinition.SCOPE_PROTOTYPE);
					processAsUnaryAssertion(defClass);
				}
				//else we leave the definition untouched.
			}
		} catch (ClassNotFoundException e){
			
			StringBuilder builder = new StringBuilder();
			builder.append("Squash Test Automation Engine (init) : class '");
			builder.append(def.getBeanClassName());
			builder.append("' was found as a Spring Bean definition, yet remains unknown from the classloader. Please contact the Squash TA dev team.");
			String message = builder.toString();
			if (LOGGER.isErrorEnabled()){
				LOGGER.error(message, e);
			}
			throw new EngineInitException(message, e);
			
		}
	
	}
	

	@SuppressWarnings("unchecked")
	private void processAsResourceConverter(final Class<?> defClass){
		
		Class<?>[] parameters = reflector.getGenericTypes(defClass, ResourceConverter.class);
		
		//due to the ResourceConverter contract, the typed parameters are two, non null, and necessarily Resource.class and Resource.class, in that order.
		
		Nature inputNature = fetchOrAddUniqueType(parameters[0]);
		Nature outputNature = fetchOrAddUniqueType(parameters[1]);
		Nature category = fetchOrAddCategory(defClass);
		
		converterDefinitions.add(new ConverterDefinition(inputNature, outputNature, category, (Class<ResourceConverter<?,?>>)defClass));
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Registering converter class: "+(defClass==null?null:defClass.getName()));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processAsCommand(final Class<?> defClass){
		Class<?>[] parameters = reflector.getGenericTypes(defClass, Command.class);
		Class<?> applyMethodResult = reflector.getReturnType(defClass, "apply");
		if(Resource.class.equals(applyMethodResult)){
			throw new EngineInitException("Command class "+defClass+" is malformed: it should have a concrete Resource implementation class as result type");
		}
		
		//due to the Command contract, the typed parameters are two, non null, and necessarily Resource.Class and Target.class, in that order.
		//Additionally the result type requires a nature too.
		
		Nature resourceNature = fetchOrAddUniqueType(parameters[0]);
		Nature targetNature = fetchOrAddUniqueType(parameters[1]);
		Nature category = fetchOrAddCategory(defClass);
		Nature resultNature = fetchOrAddUniqueType(applyMethodResult);
		
		commandDefinitions.add(new CommandDefinition(resourceNature, targetNature, category, resultNature, (Class<Command<?,?>>)defClass));
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Registering command class: "+(defClass==null?null:defClass.getName()));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processAsBinaryAssertion(final Class<?> defClass){
		Class<?>[] parameters = reflector.getGenericTypes(defClass,BinaryAssertion.class);
		
		//due to the BinaryAssertion contract, the typed parameters are two, non null, and necessarily Resource.Class and Resource.class, in that order.
		
		Nature actualNature = fetchOrAddUniqueType(parameters[0]);
		Nature expectedNature = fetchOrAddUniqueType(parameters[1]);
		Nature category = fetchOrAddCategory(defClass);
		
		binaryAssertionDefinitions.add(new BinaryAssertionDefinition(actualNature, expectedNature, category, (Class<BinaryAssertion<?,?>>)defClass));
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Registering binary assertion class: "+(defClass==null?null:defClass.getName()));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processAsUnaryAssertion(final Class<?> defClass){
		Class<?>[] parameters = reflector.getGenericTypes(defClass, UnaryAssertion.class);
		
		//due to the UnaryAssertion contract, the typed parameters are one, non null, and necessarily Resource.Class.
		
		Nature resourceNature = fetchOrAddUniqueType(parameters[0]);
		Nature category = fetchOrAddCategory(defClass);
		
		unaryAssertionDefinitions.add(new UnaryAssertionDefinition(resourceNature,category, (Class<UnaryAssertion<?>>)defClass));
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Registering unary assertion class: "+(defClass==null?null:defClass.getName()));
		}
	}
	
	/**
	 * That method will :
	 * 
	 * <ol>
	 * <li>if the class is a known type class, return the associate nature</li>
	 * <li>if the class is unknown type class, try to create a nature and add it to the natureMap</li>
	 * </ol>
	 * Type classes are implementations of the {@link Resource}, {@link Target} or {@link ResourceRepository} interface.
	 * @throws EngineInitException: if the class does not implement {@link Resource}, {@link Target} or {@link ResourceRepository}
	 * @throws ResourceTypeConflictException: if a {@link Nature} with the same name already exists in the registry for another type class.
	 * 
	 */
	public Nature fetchOrAddUniqueType(final Class<?> resourceClass){
		//if this class is not a type class as defined by this method, initialization can't succeed
		throwIfNotTypeClass(resourceClass);
		
		Nature nat = naturesMap.get(resourceClass);
		
		if (nat == null){
			String resourceType = reflector.getResourceTypeValue(resourceClass);
			
			//check that no other nature have the same name
			throwIfNatureNameAlreadyUsed(resourceClass, resourceType);

			nat = new Nature(resourceType);
			
			naturesMap.put(resourceClass, nat);
		}
		
		return nat;
	}

	private void throwIfNatureNameAlreadyUsed(final Class<?> resourceClass,
			final String resourceType) {
		for (Entry<Class<?>, Nature> e : naturesMap.entrySet()) {
			if (e.getValue().getName().equals(resourceType)) {

				String message = "Squash TA engine (init) : attempted to register a Resource type named '"
						+ resourceType
						+ "' attached to a class '"
						+ resourceClass.getName()
						+ "'."
						+ " However class '"
						+ e.getKey().getName()
						+ "' is already bound to that name. Please contact the implementor of either of these to change the value"
						+ " of the @ResourceType.";

				if (LOGGER.isErrorEnabled()) {
					LOGGER.error(message);
				}

				throw new ResourceTypeConflictException(message);
			}
		}
	}

	private void throwIfNotTypeClass(final Class<?> resourceClass) {
		if (!isType(resourceClass)) {
			String message = "Squash TA engine (init) : attempted to process a Converter, Command, UnaryAssertion or BinaryAssertion that uses '"
					+ resourceClass.getName()
					+ "' "
					+ "as a Resource or Target in a place it should not. Such error is not supposed to happen. Please check that the Squash TA jar plugins you use are not corrupted.";
			if (LOGGER.isErrorEnabled()) {
				LOGGER.error(message);
			}
			throw new EngineInitException(message);
		}
	}

	/**
	 * Defines the condition for a class to define a type.
	 * @param resourceClass the class to assess.
	 * @return <code>true</code> if the class is a type class, <code>false</code> if not.
	 */
	private boolean isType(final Class<?> resourceClass) {
		return (Resource.class.isAssignableFrom(resourceClass) || (Target.class.isAssignableFrom(resourceClass)) || ResourceRepository.class.isAssignableFrom(resourceClass));
	}
	
	

	public Nature fetchOrAddCategory(final Class<?> componentClass){
		String category = reflector.getEngineComponentValue(componentClass);
		
		Nature nat = categoryMap.get(category);
		
		if (nat==null){
			nat = new Nature(category);
			categoryMap.put(category, nat);
		}
		
		return nat;
	}
	
	

}
