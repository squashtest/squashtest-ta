/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools

import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.core.tools.io.BinaryData;

import spock.lang.Specification
import spock.lang.Unroll;

class OptionsReaderTest extends Specification {
	
	def "should read options from a line"(){
		given :
			def input = " name : robert , profession: plumber, hobby :produce write:only C# code"
		when :
			def options = OptionsReader.BASIC_READER.getOptions(input)
			
		then :
			options == ["name":"robert", "profession":"plumber", "hobby":"produce write:only C# code"]
	}
	
	
	
	@Unroll("default reader should say that #line represents a valid options line is #result")
	def "default should test options line"(){
		
		expect : 
			result == OptionsReader.BASIC_READER.isOptions(line);
		
		where :
			
			result	|	line
			true	|	" name : robert , profession: plumber, hobby :produce write:only C# code"
			false	|	" name = robert , profession= plumber, hobby =produce write:only C# code"
			false	|	" random text, with some random words "
		
	}
	
	@Unroll("comma reader should say that #line represents a valid options line is #result")
	def "comma should test options line"(){
		
		expect :
			char optionSeparator=',' //because otherwise groovy screws it!
			result == new OptionReaderTestHelper(optionSeparator).isOptions(line);
		
		where :
			
			result	|	line
			true	|	" name : robert , profession: plumber, hobby :produce write:only C# code"
			false	|	" name = robert , profession= plumber, hobby =produce write:only C# code"
			false	|	" random text, with some random words "
		
	}
	
	@Unroll("semi-colon reader should say that #line represents a valid options line is #result")
	def "semi-colon should test options line"(){
		
		expect :
			char optionSeparator=';' //because otherwise groovy screws it!
			result == new OptionReaderTestHelper(optionSeparator).isOptions(line);
		
		where :
			
			result	|	line
			true	|	" name : robert, prosper ; profession: plumber; hobby :produce write:only C# code"
			false	|	" name = robert , profession= plumber, hobby =produce write:only C# code"
			false	|	" random text, with some random words "
		
	}
	
	def "should say that a file is no options because it's too long"(){
		
		given :
			def file = File.createTempFile("tester", "tester")
			file.deleteOnExit()
			file.withWriter { 
				it.writeLine("name : robert, profession : plumber\n hobby : produce write:only C# code")
			}
			
		when :
			def res = OptionsReader.BASIC_READER.isOptions(file)
			
		then :
			res == false
		
	} 
	
	def "should remove unsupported keys"(){
		given:
			File file=File.createTempFile("test", "options")
			file.deleteOnExit()
		and:
			new BinaryData("name:robert,toto:le heros".getBytes()).write(file)
		when:
			Map<String, String>result=new OptionReaderTestHelper().getFilteredOptions(file)
		then:
			!result.containsKey("name")
		cleanup:
			file.delete()
	}
	
	def "should keep supported keys"(){
		given:
			File file=File.createTempFile("test", "options")
			file.deleteOnExit()
		and:
			new BinaryData("name:robert,toto:le heros".getBytes()).write(file)
		when:
			Map<String, String>result=new OptionReaderTestHelper().getFilteredOptions(file)
		then:
			result.containsKey("toto")
		cleanup:
			file.delete()
	}
}
