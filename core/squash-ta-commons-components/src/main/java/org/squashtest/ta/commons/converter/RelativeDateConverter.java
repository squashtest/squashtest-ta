/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.param.date.DateReplacer;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * Converter to apply date computation formulae. For supported functions, see
 * the org.squashtest.ta.commons.library.param.date package documentation.
 * 
 * @author edegenetais
 * 
 */
@EngineComponent("param.relativedate")
public class RelativeDateConverter implements
		ResourceConverter<FileResource, FileResource> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RelativeDateConverter.class);

	private List<File> createdResources = new LinkedList<File>();

	private FileTree FILE_ENUMERATOR = new FileTree();

	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		if (configuration.size() > 0) {
			LOGGER.warn(configuration.size()
					+ " useless configuration resources ignored.");
		}
	}

	@Override
	public FileResource convert(FileResource resource) {
		try {
			File resourceFile = resource.getFile();
			DateReplacer replacer = new DateReplacer();

			File convertedFile = FILE_ENUMERATOR.createTempCopyDestination(resourceFile);
			createdResources.add(convertedFile);
			if (resourceFile.isDirectory()) {
				FILE_ENUMERATOR.toTempDirectory(convertedFile);
				List<File> content = FILE_ENUMERATOR.enumerate(resourceFile,
						EnumerationMode.FILES_ONLY);
				for (File contentFile : content) {
					File convertedContent = new File(convertedFile,
							FILE_ENUMERATOR.getRelativePath(resourceFile,
									contentFile));
					boolean dirCreated=convertedContent.getParentFile().mkdirs();
					if(!dirCreated){//clear demonstration that C-style error handling is SHIT
						throw new InstructionRuntimeException("Failed to create directory "+convertedContent.getParent()+" during relative dates resolution.");
					}
					BinaryData inputData = new BinaryData(contentFile);
					BinaryData outputData = replacer.apply(inputData);
					outputData.write(convertedContent);
				}
			} else {
				BinaryData inputData = new BinaryData(resourceFile);
				BinaryData outputData = replacer.apply(inputData);
				outputData.write(convertedFile);
			}
			return new FileResource(convertedFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException(
					"RelativeDate computation failed on I/O", e);
		}
	}

	@Override
	public void cleanUp() {
		for (File file : createdResources) {
			try {
				if (file.isDirectory()) {
					FILE_ENUMERATOR.clean(file);
				} else {
					//clear demonstration that C-style error handling is SHIT
					boolean deleted=file.delete();
					if(!deleted){
						LOGGER.warn("Failed to delete created file resource "
								+ file.getAbsolutePath());
					}
				}
			} catch (IOException e) {
				LOGGER.warn(
						"Failed to delete created file resource "
								+ file.getAbsolutePath(), e);
			}
		}
	}

}
