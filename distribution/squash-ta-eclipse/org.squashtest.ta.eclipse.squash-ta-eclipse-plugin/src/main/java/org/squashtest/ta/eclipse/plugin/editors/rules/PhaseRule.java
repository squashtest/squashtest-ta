/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.editors.rules;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;
import org.squashtest.ta.eclipse.plugin.model.DSLPhase;

/**
 * Rule to recognize a phase marker DSL line.
 * @author edegenetais
 *
 */
public class PhaseRule extends WordRule implements IPredicateRule {
	
	private IToken successToken;
	private boolean isFirstWhiteSpace;
	private StringBuffer strBuffer = new StringBuffer();
	
	/**
	 * Full initialization constructor.
	 * @param successToken the token to return when the rule matches the scanner content.
	 * @param phase all supported phase names.
	 */
	public PhaseRule(IToken successToken) {
		super(new PhaseKeyWordDetector(),Token.UNDEFINED,true);
		this.successToken=successToken;
		this.fColumn=0;
		for(DSLPhase phase: DSLPhase.values()){
			addWord(phase.getValue(), successToken);
		}
	}
	
	
	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		return evaluate(scanner, false);
	}

	@Override
	public IToken evaluate(ICharacterScanner scanner, boolean arg1) {
		isFirstWhiteSpace = true;
		int c= scanner.read();
		if (c != ICharacterScanner.EOF && fDetector.isWordStart((char) c)) {
			if (fColumn == UNDEFINED || (fColumn == scanner.getColumn() - 1)) {

				strBuffer.setLength(0);
				do {
					strBuffer.append((char) c);
					c= scanner.read();
				} while (c != ICharacterScanner.EOF && (fDetector.isWordPart((char) c) || isFirstWhiteSpace(c)));
				scanner.unread();

				String buffer= strBuffer.toString();
				
				// If case-insensitive, convert to lower case before accessing the map
				buffer= buffer.toLowerCase();
				
				IToken token= (IToken)fWords.get(buffer);

				if (token != null)
					return token;

				if (fDefaultToken.isUndefined())
					unreadBuffer(scanner);

				return fDefaultToken;
			}
		}

		scanner.unread();
		return Token.UNDEFINED;
	}

	private boolean isFirstWhiteSpace(int c) {
		boolean isOk;
		if(Character.isWhitespace(c)){
			if(isFirstWhiteSpace){
				isFirstWhiteSpace = false;
				isOk = true;
			}else{
				isOk = false;
			}
		}else {
			isOk = true;
		}
		return isOk;
	}


	@Override
	public IToken getSuccessToken() {
		return successToken;
	}

}
