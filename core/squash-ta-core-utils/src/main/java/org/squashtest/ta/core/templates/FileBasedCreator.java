/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.templates;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class FileBasedCreator {
	
	protected static final Logger logger = LoggerFactory.getLogger(FileBasedCreator.class);
	
	protected String[] protocols = new String[]{"file"};
	
	protected void setProtocols(String... protocols){
		this.protocols=protocols;
	}
	
	private boolean isURLProtocolHandled(URL url){
		String urlProtocol = url.getProtocol();
		for (String protocol : protocols){
			if (protocol.equals(urlProtocol)){
				return true;
			}
		}
		return false;
	}
	
	
	protected File getFileOrNull(URL url){
		try {
			if (! isURLProtocolHandled(url)){
				if (logger.isWarnEnabled()){
					logger.warn(wrongProtocolMessage());
				}
				return null;
			}
			URI uri = url.toURI();
			return new File(uri);			
		}catch(URISyntaxException ex){
			logger.warn(invalidURIMessage(), ex);
			return null;			
		}
	}
	
	protected File getFileOrFail(URL url) throws URISyntaxException{
			if (! isURLProtocolHandled(url)){
				String msg = wrongProtocolMessage();
				if (logger.isErrorEnabled()){
					logger.error(msg);
				}
				throw new URISyntaxException("File URL cannot be opened.",msg);
			}
			URI uri = url.toURI();
			return new File(uri);			
	}
	
	private String wrongProtocolMessage(){
		StringBuilder builder= new StringBuilder();
		
		String msg = "engine component creator : cannot create from URL because of wrong protocol. Supported protocols : ";
		builder.append(msg);
		
		for (String proto : protocols){
			builder.append(proto+", ");
		}
		return builder.toString();
	}
	
	private String invalidURIMessage(){
		return "engine component creator : supplied URL failed to convert into valid URI";
	}
	
}
