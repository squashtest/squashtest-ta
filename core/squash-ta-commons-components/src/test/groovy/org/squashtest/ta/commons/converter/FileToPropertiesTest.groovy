/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.File

import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository
import org.squashtest.ta.commons.converter.PropertiesToDbuPPK
import org.squashtest.ta.commons.resources.DbUnitPPKFilter
import org.squashtest.ta.commons.resources.PropertiesResource
import org.squashtest.ta.framework.exception.BadDataException;
import org.dbunit.dataset.Column

import static org.junit.Assert.*

import spock.lang.Specification

class FileToPropertiesTest extends Specification {

	def "the properties file is correctly configured"(){
		
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/commons/converter/goodPpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
		when :
			PropertiesResource myProperties = myPropertiesConverter.convert(myFileResource)
			
		then :
			notThrown BadDataException
	}
	
	def "the properties file with multiline properties is correctly configured"(){
		
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/commons/converter/goodMultiLinePpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
		when :
			PropertiesResource myProperties = myPropertiesConverter.convert(myFileResource)
			
		then :
			notThrown BadDataException
	}
	
	def "should accept comments in the file"(){
	given :
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/commons/converter/commentInProperties.properties")
		File file = new File(url.toURI())
		FileResource myFileResource = new FileResource(file)
		FileToProperties myPropertiesConverter = new FileToProperties()
	when :
		PropertiesResource myProperties = myPropertiesConverter.convert(myFileResource)
		
	then :
		notThrown BadDataException
	}
	
	def "the conversion should crash"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/commons/converter/badPpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
		when :
			PropertiesResource myResource = myPropertiesConverter.convert(myFileResource)
			
		then :
			thrown BadDataException
	}
	
	def "the conversion with multiline properties should crash"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/commons/converter/badMultiLinePpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
		when :
			PropertiesResource myResource = myPropertiesConverter.convert(myFileResource)
			
		then :
			thrown BadDataException
	}
}
