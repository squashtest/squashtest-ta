/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.soapui.commands;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.assertions.DbUnitDatasetContains;
import org.squashtest.ta.commons.resources.PropertiesResource;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.soapui.library.SquashSoapUIRunner;
import org.squashtest.ta.soapui.resources.SoapUIScript;
import org.squashtest.ta.soapui.resources.SoapUIResult;

import com.eviware.soapui.support.SoapUIException;

/**
 * This command executes a SoapUI xml script given as input resource ({@link SoapUIScript}) with a Void_target (the target is specified inside the XML File)
 * <p><strong>Returns : </strong> {@link #apply()} returns a {@link SoapUIResult}</p>
 * 
 * @author fgaillard
 *
 */
@EngineComponent("execute")
public class ExecuteSoapUIScriptCommand extends OptionsReader implements Command<SoapUIScript, VoidTarget> {
	
	private static final Logger LOGGER = LoggerFactory
	.getLogger(DbUnitDatasetContains.class);

	static class SoapUIRunnerFactory{
		public SquashSoapUIRunner getRunner(Properties configuration){
			return new SquashSoapUIRunner(configuration);
		}
	}
	
	private SoapUIRunnerFactory runnerFactory=new SoapUIRunnerFactory();
	
	private Properties confProperties = new Properties();
	
	private SoapUIScript soapUIResource;
	
	/** Noarg constructor for Spring enumeration. */
	public ExecuteSoapUIScriptCommand(){
		super(';');//we use ';' as options separator because here configuration keys are comma-separated lists
	}
	
	/**
	 * Two configuration formats here:
	 * 	- properties resource to get configuration from properties files
	 *  - file resource to get configuration from inline definition (as ; separated options because each option value is a comma separated name list).
	 */
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		for (Resource<?> confElement : configuration) {
			if (confElement instanceof PropertiesResource) {
				addPropertiesConfigurationSource(confElement);
			} else if(confElement instanceof FileResource){
				addFileConfigurationSource(confElement);
			}else{
				LOGGER.warn("Unrecognized configuration resource will be ignored (type: "
						+ confElement.getClass().getName() + ")");
			}
		}
	}

	private void addFileConfigurationSource(Resource<?> confElement) {
		try {
			FileResource confFile = (FileResource)confElement;
			if(isOptions(confFile.getFile())){
				Map<String, String> optionsMap=getOptions(confFile.getFile());
				for(Entry<String, String> entry:optionsMap.entrySet()){
					confProperties.setProperty(entry.getKey(), entry.getValue());
				}
			}else{
				throw new IllegalConfigurationException("Configuration resource "+confFile.getFile()+" is not a valid ; separated options resource.");
			}
		} catch (IOException e) {
			throw new InstructionRuntimeException("Failed to open configuration resource.", e);
		}
	}

	private void addPropertiesConfigurationSource(Resource<?> confElement) {
		Properties properties = ((PropertiesResource)confElement).getProperties();
		Set<Object> keys = properties.keySet();
		for (Object object : keys) {
			confProperties.setProperty((String)object, properties.getProperty((String)object));
		}
	}

	@Override
	public void setResource(SoapUIScript resource) {
		soapUIResource=resource;
	}

	@Override
	public SoapUIResult apply() {

		SquashSoapUIRunner runner=runnerFactory.getRunner(confProperties);
		try{
			File soapUIFile = soapUIResource.getSoapUIFile();
			return runner.run(soapUIFile);
		} catch(SoapUIException suie){
			throw new InstructionRuntimeException("SoapUI Test failed", suie);
		} catch (IOException ioe) {
			throw new InstructionRuntimeException("SoapUI test failed from IO problem.", ioe);
		} catch (XmlException xmle) {
			throw new InstructionRuntimeException("SoapUI test failed from XML exception.", xmle);
		}
	}
	
	@Override
	public void cleanUp() {
	}

	@Override
	public void setTarget(VoidTarget target) {
		//noop
	}

	@Override
	protected Set<String> supportedKeys(Map<String, String> source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	} 	
}
