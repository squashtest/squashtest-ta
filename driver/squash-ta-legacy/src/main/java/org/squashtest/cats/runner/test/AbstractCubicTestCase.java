/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.test;

import java.util.Collections;
import java.util.List;

import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

/**
 * This class is a facade to integrate legacy CATS cubictest test classes into the new
 * squashTA execution framework.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractCubicTestCase extends AbstractCatsTestCase {

	public AbstractCubicTestCase() {
		initDatasets();
	}
	
    /**
     * Hook method, in which the {@link #setDataset(String, String)} is invoked
     * in order to set the datasets to be injected during setup.
     */
    protected void initDatasets() {
    	//this base implementation does nothing: some tests may not need data injection...
    }

    /**
     * Runs a CubicTest test or testsuite, and checks whether it passed or
     * failed.
     * 
     * @param pTestPath Path of the test (.aat) or testsuite (.ats) to run. Must
     *            be relative to the project's root.
     */
    public void runAndCheckTestCubicTest(final String pTestPath) {
    	ResetLocalContextInstruction context=createLocalContext("runAndCheckTestCubicTest()");
    	getCallListener().addInstruction(context);
    	
    	ResourceName testPath = new ResourceName(Scope.SCOPE_TEMPORARY,"testPath");
		DefineResourceInstruction testPathDefine=createDefineInstruction(testPath, pTestPath);
		getCallListener().addInstruction(testPathDefine);

		List<ResourceName> configuration = Collections.emptyList();
		ExecuteCommandInstruction command=createExecuteCommandInstruction("", testPath, IGNORED_RESULT_NAME, "runAndCheckTestCubicTest()", configuration, "cubictest");
		getCallListener().addInstruction(command);
    }

}
