/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.AssertionFindSettings;
import org.squashtest.ta.backbone.engine.AssertionManager;
import org.squashtest.ta.backbone.engine.CommandFindSettings;
import org.squashtest.ta.backbone.engine.CommandManager;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.EventManager;
import org.squashtest.ta.backbone.engine.ResourceConversionSettings;
import org.squashtest.ta.backbone.engine.ResourceConverterManager;
import org.squashtest.ta.backbone.engine.ResourceLoader;
import org.squashtest.ta.backbone.engine.ResourceLoadingSettings;
import org.squashtest.ta.backbone.engine.event.ContextualStatusUpdateEvent;
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler;
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler;
import org.squashtest.ta.backbone.exception.AmbiguousConversionException;
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.backbone.tools.ElementUtils;
import org.squashtest.ta.framework.test.instructions.ResourceName;

public class ContextManagerImpl implements ContextManager {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ContextManager.class);
	
	private static final ElementUtils ELEMENT_UTILS=new ElementUtils();
	
	private Map<String, TargetWrapper> targets = new HashMap<String, TargetWrapper>();

	

	/* the following managers should be configured then passed to the context manager */ 

	private ResourceLoader resourceLoader;
	
	private ResourceConverterManager converterManager;
	
	private CommandManager commandManager;
	
	private AssertionManager assertionManager;
	
	private EventManager eventManager;
	
	private Map<ResourceName, ResourceWrapper> ecosystemResources=new HashMap<ResourceName, ResourceWrapper>();
	
	/* ***************** building ******************** */
	
	public ResourceLoader getResourceLoader(){
		return resourceLoader;
	}
	
	public void setResourceLoader(ResourceLoader loader){
		this.resourceLoader=loader;
	}
	
	public ResourceConverterManager getResourceConverterManager(){
		return converterManager;
	}
	
	public void setResourceConverterManager (ResourceConverterManager manager){
		this.converterManager=manager;
	}


	public CommandManager getCommandManager() {
		return commandManager;
	}

	public void setCommandManager(CommandManager manager) {
		this.commandManager=manager;
	}
	
	public AssertionManager getAssertionManager() {
		return assertionManager;
	}
	
	public void setAssertionManager(AssertionManager manager) {
		this.assertionManager=manager;	
	}

	public void setEventManager(EventManager manager){
		eventManager=manager;
	}
	
	/* ****************************** lifecycle ************************************************ */
	
	@Override
	public void initAll() {
		LOGGER.debug("Initializing all resources and targets.");
		resourceLoader.initAll();
		for (TargetWrapper w : targets.values()){
			try{
				w.init();
			}catch(Exception e){
				String targetName=(w==null?"null":w.getName());
				LOGGER.warn("Target "+targetName+" could not be initialized. Some tests may fail.",e);
			}
		}
	}
	
	@Override
	public void resetAll() {
		LOGGER.debug("Resetting all resources and targets.");
		resourceLoader.resetAll();
		for (TargetWrapper w : targets.values()){
			try{
				w.reset();
			}catch(Exception e){
				String targetName=(w==null?"null":w.getName());
				LOGGER.warn("Target "+targetName+" could not be reset. Some tests may fail.",e);
			}
		}
	}

	@Override
	public void cleanupAll() {
		LOGGER.debug("Cleaning all resources and targets.");
		resourceLoader.cleanupAll();
		for (TargetWrapper w : targets.values()){
			try{
				w.cleanup();
			}catch(Exception e){
				String targetName=(w==null?"null":w.getName());
				LOGGER.warn("Target "+targetName+" could not be cleaned.",e);
			}
		}
		//as we are closing everything, let's nudge event processing a bit:
		eventManager.shutdown();
	}
	
	
	/* ****************************** resgistrers ********************************************** */



	public void registerRepository(RepositoryWrapper repository) {
		resourceLoader.addRepository(repository);
	}

	public void registerTarget(TargetWrapper target) {
		ELEMENT_UTILS.checkUniqueness(targets.keySet(), target.getName());
		targets.put(target.getName(), target);
	}
	
	
	public void registerConverterDefinition(ConverterDefinition definition){
		this.converterManager.addConverterDefinition(definition);
	}
	

	public void registerCommandDefinition(CommandDefinition definition) {
		this.commandManager.addCommandDefinition(definition);
	}
	
	public void registerAssertionDefinition(BinaryAssertionDefinition definition) {
		this.assertionManager.addAssertionDefinition(definition);	
	}
	
	public void registerAssertionDefinition(UnaryAssertionDefinition definition) {
		this.assertionManager.addAssertionDefinition(definition);		
	}


	/* ****************************** unregistrers ************************************************ */

	public void unregisterRepository(RepositoryWrapper repository) {
		resourceLoader.removeRepository(repository.getName());
	}

	public void unregisterRepository(String repositoryName) {
		resourceLoader.removeRepository(repositoryName);
	}

	public void unregisterTarget(TargetWrapper target) {
		targets.remove(target.getName());
	}

	public void unregisterTarget(String targetName) {
		targets.remove(targetName);
	}
	
	public void unregisterConverterDefinition(ConverterDefinition definition){
		converterManager.removeConverterDefinition(definition);
	}
	
	public void unregisterConverterDefinition(Nature input, Nature output, Nature category){
		converterManager.removeConverterDefinition(input, output, category);
	}
	
	
	public void unregisterCommandDefinition(CommandDefinition definition) {
		commandManager.removeCommandDefinition(definition);
	}

	public void unregisterCommandDefinition(Nature resourceNature,
			Nature targetNature, Nature commandCategory) {
		commandManager.removeCommandDefinition(resourceNature, targetNature, commandCategory);
	}
	
	public void unregisterAssertionDefinition(BinaryAssertionDefinition definition) {
		assertionManager.removeAssertionDefinition(definition);
	}
	
	public void unregisterAssertionDefinition(Nature actualNature,
			Nature expectedNature, Nature assertionCategory) {
		assertionManager.removeAssertionDefinition(actualNature, expectedNature, assertionCategory);		
	}
	
	public void unregisterAssertionDefinition(UnaryAssertionDefinition definition) {
		assertionManager.removeAssertionDefinition(definition);
	}
	
	public void unregisterAssertionDefinition(Nature actualNature,Nature assertionCategory) {
		assertionManager.removeAssertionDefinition(actualNature, assertionCategory);		
	}

	/* ******************************** getters ***************************************************** */

	public ResourceWrapper getResource(ResourceName resourceName) {
		return simpleLoad(resourceName);
	}

	public ResourceWrapper getResource(ResourceName resourceName, String repositoryName) {
		
		ResourceLoadingSettings settings = new ResourceLoadingSettingsImpl();
		settings.setResourceName(resourceName);
		settings.setRepositoryName(repositoryName);
		return resourceLoader.loadResource(settings);
	}
	
	/* in this case the resource might be specifically converted into something */
	public ResourceWrapper getResource(ResourceLoadingSettings settings){
		
		//check the most important parameter : the resource name
		
		ResourceName name = settings.getResourceName();
		
		if (name==null){
			ResourceNotFoundException ex = new ResourceNotFoundException("cannot load resource : name was not specified.");
			LOGGER.error(ex.getMessage());
			throw ex;
		}
		
		//now load a copy of the data 
		return resourceLoader.loadResource(settings);
	}
	
	public TargetWrapper getTarget(String targetName) {
		return targets.get(targetName);
	}
	
	/* *********************************** conversion ************************************************ */

	public Collection<ResourceConverterHandler> getConverters(ResourceConversionSettings settings){
		Collection<ResourceConverterHandler> converters = 
				converterManager.getAllConvertersByName(settings.getOriginalNature(), 
														settings.getDesiredNature(), 
														settings.getConverterCategory());

		return converters;		
	}
	
	
	public ResourceWrapper convertResource(ResourceWrapper resource, ResourceConversionSettings settings){
		
		String desiredType = settings.getDesiredNature();
		String identifier = settings.getConverterCategory();

		Collection<ResourceConverterHandler> converters;
		ResourceConverterHandler converter=null; 
				
		String inputNature = resource.getNature().getName();
		converters = converterManager.getAllConvertersByName(inputNature, desiredType, identifier);
		
		if (converters.size()==1){
			
			converter = converters.iterator().next();
			ResourceWrapper result = converter.convert(resource);
			converter.cleanUp();
			return result;
			
			
		}else if (converters.size()==0){
			throw new ImpossibleConversionException("Impossible to convert resource "+resource.getName()+
					" : no converter found from nature "+resource.getNature().getName()+" to nature "+desiredType);
		}else{
			throw new AmbiguousConversionException(buildAmbiguousConversionMessage(resource,
					desiredType, converters));
		}
	}

	public Collection<CommandHandler> getCommand(CommandFindSettings settings) {
		Collection<CommandHandler> commands = commandManager.getAllCommandsByName(settings.getResourceNatureName(), 
															settings.getTargetNatureName(), 
															settings.getCommandIdentifier());
		
		return commands;
		
		
	}
	

	/* ******************** assertions **************************** */

	public Collection<BinaryAssertionHandler> getBinaryAssertion(AssertionFindSettings settings) {
		
		Collection<BinaryAssertionHandler> assertions = assertionManager.getAllBinaryAssertionsByName(settings.getActualResultNatureName(), 
																settings.getExpectedResultNatureName(), 
																settings.getAssertionCategory());

		
		return assertions;
		
	}
	
	public Collection<UnaryAssertionHandler> getUnaryAssertion(AssertionFindSettings settings){
		Collection<UnaryAssertionHandler> assertions = assertionManager.getAllUnaryAssertionsByName(settings.getActualResultNatureName(),  
																									settings.getAssertionCategory());

		
		return assertions;	
	}
	
	@Override
	public Map<ResourceName, ResourceWrapper> getEcosystemResources() {
		return ecosystemResources;
	}
	
	/* *************************events************************** */
	@Override
	public void postEvent(ContextualStatusUpdateEvent<?> event) {
		eventManager.postEvent(event);
	}
	/* ********************** non public *********************** */
	
	
	private String buildAmbiguousConversionMessage(ResourceWrapper resource,
			String desiredType, Collection<ResourceConverterHandler> converters) {
		StringBuilder message = new StringBuilder(
				"Impossible to convert resource ")
				.append(resource.getName())
				.append(" : multiple converters found from nature ")
				.append(resource.getNature().getName())
				.append(" to nature ").append(desiredType).append(" : ");
		for (ResourceConverterHandler c : converters){
			message.append(c.getConverterCategory().getName()).append(" , ");
		}
		if(converters.size()>0){
			message.setLength(message.length()-" , ".length());
		}
		return message.toString();
	}

	private ResourceWrapper simpleLoad(ResourceName name){
		ResourceLoadingSettings settings = new ResourceLoadingSettingsImpl();
		settings.setResourceName(name);
		return resourceLoader.loadResource(settings);
	}

}
