/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl

import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.engine.CommandManager
import org.squashtest.ta.backbone.engine.impl.CommandManagerImpl
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.exception.DuplicateFactoryException
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.facade.Engine;

import spock.lang.Specification

class CommandManagerImplTest extends Specification {

	Nature nat1=new Nature("nat1")
	Nature nat2=new Nature("nat2")
	Nature nat3=new Nature("nat3")
	Nature nat4=new Nature("nat4")
	Nature nat5=new Nature("nat5")
	Nature nat6=new Nature("nat6")
	Nature nat7=new Nature("nat7")
	
	CommandManager manager
	ObjectFactory factory
	
	def setup(){
		manager = new CommandManagerImpl();
		factory = Mock(ObjectFactory)
		manager.factory=factory
	}
	
	def "should add a command definition"(){
		
		given :
			def definition = new CommandDefinition(nat1, nat3, nat5, nat7, String.class)
			
			def ass = Mock(Command)
			factory.newInstance(String.class) >> ass
			
		when :
			manager.addCommandDefinition(definition)
			
			def res = manager.getAllCommands(nat1, nat3, nat5)
			
		then :
			res.size() == 1
			def handler = res.get(0)
			handler.resourceNature==nat1
			handler.targetNature==nat3
			handler.commandCategory==nat5
		
	}
	
	def "should rant when trying to add twice the same definition"(){
		given :
			def definition = new CommandDefinition(nat1, nat2, nat3, nat7, String.class)

		when :
			manager.addCommandDefinition(definition)
			manager.addCommandDefinition(definition)

		then :
			thrown DuplicateFactoryException
	
	}
	
	def "should remove a command definition (1)"(){
		given :
			def definition= new CommandDefinition(nat1, nat2, nat3, nat7, String.class)
			
		when :
			manager.addCommandDefinition(definition)
			manager.removeCommandDefinition(definition)
			def res = manager.getAllCommands(nat1, nat2, nat3)

		then :
			res.size()==0		
	}
	
	def "should remove a command factory (2)"(){
		given :
			def definition = new CommandDefinition(nat1, nat2, nat3, nat7, String.class)
	
		when :
			manager.addCommandDefinition(definition)
			manager.removeCommandDefinition(nat1,nat2,nat3)
			def res = manager.getAllCommands(nat1, nat2, nat3)
		then :
			res.size()==0
		
	}
	
	
	def "should get a subset of commands (1)"(){
		
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and : 
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(null, nat3, null)
			
		then :
			res.size() == 4
			res.collect{it.wrappedCommand}.containsAll([
				commands[0],
				commands[1],
				commands[4],
				commands[5]	
			])
	}
	
	def "should get a subset of commands (2)"(){
		
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(null, nat4, nat5)
			
		then :
			res.size()==2
			res.collect{it.wrappedCommand}.containsAll([
				commands[2],
				commands[6]	
			])
		
	}
	
	def "should get one command"(){
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(nat1, nat4, nat6)
			
		then :
			res.size()==1
			res.collect{it.wrappedCommand}.containsAll([
				commands[3]
			])
	
	}

	def "should get all commands"(){
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(null, null, null)
			
		then :
			res.size()==8
			res.collect{it.wrappedCommand}.containsAll(commands)
	}
		
	def "should not find a command"(){
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(nat1, nat1, nat3)
			
		then :
			res.size()==0
		
	}
	
	def "should get all commands by name arguments"(){
		
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
		
		when :
			def res = manager.getAllCommandsByName(null, null, null)
		
		then :
			res.size()==8
			res.collect{it.wrappedCommand}.containsAll(commands)		
	}
	
	
	def "should rant because the resource nature is unknown"(){
		when :
			manager.getAllCommandsByName("test", null, null)
		then :
			thrown ResourceNotFoundException
		
	}
		
	def "should rant because the target nature is unknown"(){
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			manager.getAllCommandsByName("nat1", "test", null)
			
		then :
			thrown ResourceNotFoundException
		
	}
	
	
	/* ************************ scaffolding code ********************* */
	
	
	def buildDefinition = { a, b, c, t ->
		return new CommandDefinition(a,b,c, nat7, t)
	}
	
	def buildCommand = { a, b , c ->
		def command = Mock(Command)
		return command
	}
	
	def buildDefinitionSet = {
		def definitions = [] 	
		
		
		def counter=0		
		//the following items just need to be different types of Class.
		def testClasses = [ Integer.class, String.class, ArrayList.class, Map.class, GString.class, Double.class, ResourceConverter.class, Engine.class]
		
		[ nat1, nat2].each{r ->
				[nat3, nat4].each{t ->
					[nat5, nat6].each{ n -> definitions << buildDefinition(r, t, n, testClasses[counter++]) }
			}
		}
		return definitions
	}
	
	def buildCommandSet = {
		def commands = []	
	
		[ nat1, nat2].each{r ->
				[nat3, nat4].each{t ->
					[nat5, nat6].each{ n -> commands << buildCommand(r, t, n) }
			}
		}
		return commands
	}
	
	def associate = { defs, convs -> 
		defs.size().times {factory.newInstance(defs[it].componentClass) >> convs[it] }	
	}
	
}
