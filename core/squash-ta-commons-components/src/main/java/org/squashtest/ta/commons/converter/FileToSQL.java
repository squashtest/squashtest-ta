/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;

import org.squashtest.ta.commons.library.sql.SQLFormatUtils;
import org.squashtest.ta.commons.resources.SQLQuery;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * File To SQL Converter
 * Converts a File entry into an interpretable SQL Syntax
 * <p> The file must have the following specifications :<p>
 * <ul>
 * 	<li>The file contains only one SQL Query,</li>
 * 	<li>The Query can be on one or more lines</li>
 *  <li>The Query is terminated by a semi-column</li>
 *  <li>The Query can be encapsulated with comments and contain comments within</li>
 * </ul>
 * 
 * @author fgaillard
 *
 */
@EngineComponent("query")
public class FileToSQL implements ResourceConverter<FileResource, SQLQuery> {

	/**
	 * Default constructor for Spring enumeration only.
	 */
	public FileToSQL(){}
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		
	}

	@Override
	public SQLQuery convert(FileResource resource) {
		SQLQuery resultQuery = null;
		BufferedReader br= null;
		try
		{
			br = new BufferedReader(new FileReader(resource.getFile()));
			String query = "";
			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains("--")){
					//we take away the comments behind --
					line = line.substring(0,line.indexOf("--"));
				}
				query = query.concat(" ").concat(line);
			}
			//We take only what is before the first semicolumn
			if (query.split(";").length>=2){
				query = query.split(";")[0].concat(";");
			}
			query = SQLFormatUtils.stripComments(query);
			query = SQLFormatUtils.stripWhiteLines(query);
			
			query = query.trim();
			resultQuery = new SQLQuery(query);
		} catch (FileNotFoundException fnfe) {
			throw new BadDataException("file not found!!!!!\n",fnfe);
		} catch (IOException ioe) {
			throw new InstructionRuntimeException("Resource convert I/O error",ioe);
		}
		finally{
			try{
				if (br!=null){
					br.close();
				}
			}
			catch(IOException ex){
				throw new InstructionRuntimeException("file to sql : failed to close stream on input file", ex);
			}
		}
		return resultQuery;
	}

	@Override
	public void cleanUp() {
		
	}

	

	
}
