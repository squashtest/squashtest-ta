/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.factory

import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.result.ResultPart;
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestBatchTestCase;
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestCubictestTestCase;
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestSahiTestCase;
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestSoapuiTestCase;

import spock.lang.Specification;

class LegacyClassTestSuiteFactoryTest extends Specification{
	def testee
	def ecosystemFactoryMock
	def projectName="project"
	
	def setup(){
		testee=new LegacyClassTestSuiteFactory(projectName)
		ecosystemFactoryMock=Mock(LegacyClassEcosystemFactory)
		testee.factory=ecosystemFactoryMock
	}
	
	def "should create a test suite with one ecosystem per class"(){
		given:
			Set<Class<?>> classes=[LegacyTestBatchTestCase.class,LegacyTestCubictestTestCase.class,LegacyTestSahiTestCase.class,LegacyTestSoapuiTestCase.class]
		and:
			def batchEco=new Ecosystem()
			def ctEco=new Ecosystem()
			def sahiEco=new Ecosystem()
			def suiEco=new Ecosystem()
		when:
			def result=testee.buildTestSuite(classes)
		then:
			1 * ecosystemFactoryMock.buildEcosystem(LegacyTestBatchTestCase.class) >> batchEco
			1 * ecosystemFactoryMock.buildEcosystem(LegacyTestCubictestTestCase.class) >> ctEco
			1 * ecosystemFactoryMock.buildEcosystem(LegacyTestSahiTestCase.class) >> sahiEco
			1 * ecosystemFactoryMock.buildEcosystem(LegacyTestSoapuiTestCase.class) >> suiEco
			result!=null
			
			def nb=0
			for(Ecosystem e:result){
				nb++
			}
			nb==classes.size()
	}
	
	def "suite name is the project name"(){
		given:
			Set<Class<?>> classes=[LegacyTestBatchTestCase.class,LegacyTestCubictestTestCase.class,LegacyTestSahiTestCase.class,LegacyTestSoapuiTestCase.class]
		and:
			def batchEco=new Ecosystem()
			def ctEco=new Ecosystem()
			def sahiEco=new Ecosystem()
			def suiEco=new Ecosystem()
		and:
			ecosystemFactoryMock.buildEcosystem(LegacyTestBatchTestCase.class) >> batchEco
			ecosystemFactoryMock.buildEcosystem(LegacyTestCubictestTestCase.class) >> ctEco
			ecosystemFactoryMock.buildEcosystem(LegacyTestSahiTestCase.class) >> sahiEco
			ecosystemFactoryMock.buildEcosystem(LegacyTestSoapuiTestCase.class) >> suiEco
		when:
			def result=testee.buildTestSuite(classes)
		then:
			result!=null
			projectName.equals(result.getName())
	}
}
