/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.shell.OutputStream;

public abstract class ProcessStreamListener implements Runnable{

	private static final Logger LOGGER=LoggerFactory.getLogger(ProcessStreamListener.class);
	
	private InputStream stream;
	
	private WeakReference<Process> processRef;

	public ProcessStreamListener(Process process, OutputStream targetStream) {
		processRef=new WeakReference<Process>(process);
		switch(targetStream){
		case out:
			stream=process.getInputStream();
			break;
		case err:
			stream=process.getErrorStream();
			break;
		default:
				throw new IllegalArgumentException(targetStream.name()+" is no known stream!");
		}
	}

	public void start() {
		Process process=processRef.get();
		if(process==null){
			LOGGER.warn("No more process to monitor, nothing will start.");
		}else{
			Thread pollingThread = new Thread(this, "stdoutPoller-"
					+ process.toString());
			pollingThread.setDaemon(true);
			pollingThread.start();
		}
	}

	@Override
	public void run() {
		byte[] buffer = new byte[1024];
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			int nb = stream.read(buffer);
			while (processAlive()) {
				getStdoutContentIfAvailable(buffer, os, nb);
				if (processAlive()) {
					nb = stream.read(buffer);
				}
			}
			LOGGER.debug("Process is dead");
		} catch (IOException e) {
			LOGGER.error("stdout reading failed.", e);
		}
		try {
			if (stream != null) {
				stream.close();
			}
		} catch (IOException e) {
			LOGGER.warn("stdout closing failed", e);
		}
	}

	private void getStdoutContentIfAvailable(byte[] buffer, ByteArrayOutputStream os,
			int nb) throws IOException {
				try{
				while (nb >= 0) {
					os.write(buffer, 0, nb);
					String osString = os.toString();
					int indexNewline = osString.indexOf('\n');
					if (indexNewline > 0) {
						while (indexNewline >= 0) {
							commitOutputLine(osString.substring(0, indexNewline));
							osString = osString.substring(indexNewline + 1);
							indexNewline = osString.indexOf('\n');
						}
						os.reset();
						os.write(osString.getBytes());
					}
					if (processAlive()) {
						nb = stream.read(buffer);
					}
				}
				}catch(IOException ioe){
					LOGGER.warn("stdout reading failed.", ioe);
				}
			}

	protected abstract void commitOutputLine(String osString);

	private boolean processAlive() {
		Process process = processRef.get();
		boolean alive = false;
		if (process != null) {
			try {
				process.exitValue();
				alive = false;
			} catch (IllegalThreadStateException e) {
				alive = true;
			}
		}
		return alive;
	}

}