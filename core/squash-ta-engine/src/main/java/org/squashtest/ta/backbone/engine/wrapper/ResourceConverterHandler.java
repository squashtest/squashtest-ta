/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.wrapper;


import java.util.Collection;

import org.squashtest.ta.backbone.tools.ReflectionUtils;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

public class ResourceConverterHandler extends ConfiguredComponentHandler {
	
	private static final String RELEVANCE_METHOD = "rateRelevance";
	private static final String CONVERT_METHOD = "convert";
	

	
	private ReflectionUtils reflector = new ReflectionUtils();
	
	private Nature inputNature;
	private Nature outputNature;
	private Nature converterCategory;
	private ResourceConverter<?,?> wrappedConverter;
	
	
	/* ************************* ctors ******************************* */
	
	public ResourceConverterHandler(){
		super();
	}
	
	public ResourceConverterHandler(Nature inputNature, Nature outputNature,
			Nature converterCategory, ResourceConverter<?, ?> wrappedConverter) {
		super();
		this.inputNature = inputNature;
		this.outputNature = outputNature;
		this.converterCategory = converterCategory;
		this.wrappedConverter = wrappedConverter;
	}
	
	
	/* ************************* metadata **************************** */
	
	public Nature getInputNature() {
		return inputNature;
	}

	public void setInputNature(Nature inputNature) {
		this.inputNature = inputNature;
	}
	
	public Nature getOutputNature() {
		return outputNature;
	}
	
	public void setOutputNature(Nature outputNature) {
		this.outputNature = outputNature;
	}
	
	public Nature getConverterCategory() {
		return converterCategory;
	}
	
	public void setConverterCategory(Nature converterCategory) {
		this.converterCategory = converterCategory;
	}
	
	
	public void wrap(ResourceConverter<?, ?> wrappedConverter) {
		this.wrappedConverter = wrappedConverter;
	}
	
	/* **************************** wrapped methods ************************* */
	
	public float rateRelevance(ResourceWrapper input){
		Resource<?> resource = input.unwrap();
		return (Float)reflector.invoke(wrappedConverter, RELEVANCE_METHOD, resource);
	}
	
	public ResourceWrapper convert(ResourceWrapper input){
		Resource<?> resource = input.unwrap();
		Resource<?> output = (Resource<?>)reflector.invoke(wrappedConverter, CONVERT_METHOD, resource);
		return new ResourceWrapper(outputNature, new ResourceName(Scope.SCOPE_TEST,"will be set later"), output, input.getGenerator());
	}
	
	public void cleanUp(){
		wrappedConverter.cleanUp();
	}

	@Override
	protected void addConfigurationToWrapped(
			Collection<Resource<?>> unwrappedConfig) {
		wrappedConverter.addConfiguration(unwrappedConfig);
	}
	
	
	
}
