/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.command.ta

import org.squashtest.installer.main.InstallCommandFactory;

import spock.lang.Specification;

class ImportPreferencesCommandTest extends Specification {


	

	def "should get the correct import preferences plugin jar name"(){
		given :
			URL eclipseURL = this.getClass().getClassLoader().getResource("pseudo-TA-directory/eclipse-java-indigo-win32")
			File eclipseDir = new File(eclipseURL.toURI())
			
		and :
			ImportPreferencesCommand prefs = new ImportPreferencesCommand(eclipseDirectory:eclipseDir)
			
		expect : 
			prefs.findEclipseLauncher().getName()== "org.eclipse.equinox.launcher_1.2.0.v20110502.jar"
		
	}
	
}
