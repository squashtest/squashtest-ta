/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.SuiteRunner;
import org.squashtest.ta.backbone.test.SuiteRunnerSettingsImpl;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;
import org.squashtest.ta.framework.test.result.SuiteResult;

/**
 * This is the main engine entry point, as instanciated by the engine loader.
 * @author edegenetais
 *
 */
public class EngineImpl implements Engine {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EngineImpl.class);
	
	@Autowired
	private ContextInitializer contextFactory;
	
	@Override
	public SuiteResult execute(TestSuite suite,
			TestWorkspaceBrowser resourceBrowser) {
		
		LOGGER.debug("Engine starts.");
		
		contextFactory.setTestWorkspaceBrowser(resourceBrowser);
		ContextManager contextManager=contextFactory.newContextManager();
		
		SuiteRunner suiteRunner=new SuiteRunnerImpl();
		SuiteRunnerSettingsImpl settings=new SuiteRunnerSettingsImpl();
		settings.setContextManager(contextManager);
		suiteRunner.configure(settings);
		suiteRunner.setSuite(suite);
		
		LOGGER.debug("Engine begins test suite execution.");
		
		SuiteResult result = suiteRunner.execute();
		
		LOGGER.debug("Engine has completed test suite execution.");
		
		return result;
	}

	@Override
	public void addEventListener(StatusUpdateListener listener) {
		contextFactory.addEventListener(listener);
	}

	@Override
	public boolean removeEventListener(StatusUpdateListener listener) {
		return contextFactory.removeEventListener(listener);
	}
}
