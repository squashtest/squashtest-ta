/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl

import org.squashtest.ta.framework.test.result.GeneralStatus;

import spock.lang.Specification;

class DefaultTeardownExecutionStrategyTest extends Specification{
	def testee
	def setup(){
		testee=new DefaultTeardownExecutionStrategy()
	}
	
	def "should halt on error"(){
		when:
			def resultat=testee.continueExecution(GeneralStatus.ERROR)
		then:
			resultat==false;
	}
	
	def "should continue on failure"(){
		when:
			def resultat=testee.continueExecution(GeneralStatus.FAIL)
		then:
			resultat==true;
	}
	
	def "should continue on sucess"(){
		when:
			def resultat=testee.continueExecution(GeneralStatus.SUCCESS)
		then:
			resultat==true;
	}
	
	def "success + success = success"(){
		when:
			def resultat=testee.aggregate(GeneralStatus.SUCCESS,GeneralStatus.SUCCESS)
		then:
			resultat==GeneralStatus.SUCCESS;
	}
	
	def "success + error = warning"(){
		when:
			def resultat=testee.aggregate(GeneralStatus.SUCCESS,GeneralStatus.ERROR)
		then:
			resultat==GeneralStatus.WARNING;
	}
	
	def "success + failure = warning"(){
		when:
			def resultat=testee.aggregate(GeneralStatus.SUCCESS,GeneralStatus.FAIL)
		then:
			resultat==GeneralStatus.WARNING;
	}

	def "should engage if previous successful"(){
		when:
			def resultat=testee.beginGroupExecution(GeneralStatus.SUCCESS)
		then:
			resultat==true;
	}
	
	def "should still engage if previous failed"(){
		when:
			def resultat=testee.beginGroupExecution(GeneralStatus.FAIL)
		then:
			resultat==true;
	}
	
	def "should still engage if previous hung"(){
		when:
			def resultat=testee.beginGroupExecution(GeneralStatus.ERROR)
		then:
			resultat==true;
	}
	
	def "should still engage if pre test steps failed"(){
		when:
			def resultat=testee.beginGroupExecution(GeneralStatus.NOT_RUN)
		then:
			resultat==true;
	}
}
