/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.resources;

import org.squashtest.ta.commons.library.shell.ShellResult;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;

/**
 * Resource holding the result of a shell command execution.
 * @author edegenetais
 *
 */
@ResourceType("result.shell")
public class ShellResultResource implements Resource<ShellResultResource>{

	private ShellResult result;
	
	/** Noarg constructor for Spring enumeration. */
	public ShellResultResource() {}
	
	/**
	 * @param result
	 */
	public ShellResultResource(ShellResult result) {
		this.result = result;
	}

	@Override
	public ShellResultResource copy() {
		return new ShellResultResource(result);
	}

	@Override
	public void cleanUp() {
		//noop: nothing in memory.
	}

	public ShellResult getResult(){
		return result;
	}
}
