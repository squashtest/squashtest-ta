/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools.io

import org.squashtest.ta.core.tools.io.FileTree;

import spock.lang.Specification;

class FileTreeTest extends Specification{
	def baseDir
	
	def setup(){
		baseDir=new TempUnzipper().unzipInTemp(getClass().getResourceAsStream("/defaultSquashWorkspace.zip"))
	}
	
	def "should not include base directory in enumerations with directories"(){
		given:
			def testee=new FileTree()
		when:
			def result=testee.enumerate(baseDir,true);
		then:
			def hasRootIn=false;
			for(File element:result){
				if(element.getCanonicalFile().equals(baseDir.getCanonicalFile())){
					hasRootIn=true
				}
			}
			!hasRootIn
	}
	
	def "should not include a non existing base directory in enumerations with directories"(){
		given:
			def testee=new FileTree()
		and:
			new TempUnzipper().clean(baseDir)
		when:
			def result=testee.enumerate(baseDir,true);
		then:
			def hasRootIn=false;
			for(File element:result){
				if(element.getCanonicalFile().equals(baseDir.getCanonicalFile())){
					hasRootIn=true
				}
			}
			!hasRootIn
	}
	
	def "should not include a non existing base directory in enumerations without directories"(){
		given:
			def testee=new FileTree()
		and:
			new TempUnzipper().clean(baseDir)
		when:
			def result=testee.enumerate(baseDir,false);
		then:
			def hasRootIn=false;
			for(File element:result){
				if(element.getCanonicalFile().equals(baseDir.getCanonicalFile())){
					hasRootIn=true
				}
			}
			!hasRootIn
	}
	
	def "should not include directories in enumerations without directories"(){
		given:
			def testee=new FileTree()
		when:
			def result=testee.enumerate(baseDir,false);
		then:
			def hasDirectory=false;
			for(File element:result){
				if(element.isDirectory()){
					hasDirectory=true
				}
			}
			!hasDirectory
	}
	
	
	def "should say that a file is contained in a directory"(){
		
		given :
			def relPath = "defaultSquashWorkspace/src/squashTA/tests/www/ww3.txt"
			def temp = new File(baseDir, relPath)
			def absolutePath = temp.getCanonicalPath();
			
		and :
			def toTest = new File(absolutePath)
			
		
		when :
			def res = FileTree.staticCheapCheckIfInsideBaseDir(baseDir, toTest);
		
		then :
			res == true
		
	}
	
	def "should say  that a file is not contained in a directory"(){
		
		
		given :
			def anotherFile = File.createTempFile("uhuhuh", "uh")
		
		when :
			def res = FileTree.staticCheapCheckIfInsideBaseDir(baseDir, anotherFile);
		
		then :
			res == false
		
	}
	
	def "should return the relative path of a file in a folder that contains it"(){
		
		
		given :
			def relPath = "defaultSquashWorkspace/src/squashTA/tests/www/ww3.txt"
			def temp = new File(baseDir, relPath)
			def absolutePath = temp.getCanonicalPath();
			
		and :
			def toTest = new File(absolutePath)
			
		
		when :
			def res = FileTree.staticGetRelativePath(baseDir, toTest);
		
		then :
			def replace = res.replaceAll("\\\\","/")
			replace == relPath
		
	}
	
	def cleanup(){
		new TempUnzipper().clean(baseDir)
	}
}
