/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.commands;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.squashtest.ta.commons.targets.FTPTarget;
import org.squashtest.ta.core.tools.ExceptionLogger;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;

/**
 * <p><strong>Description : </strong>That command will fetch a resource from a given ftp. It doesn't need any Resource as input so the FileResource type argument exists
 * because it has to. The result of the file transfer will be returned as a {@link FileResource}, once {@link #apply()} has terminated successfully.</p>
 * 
 * <p>NOTE : since the input resource is irrelevant, you may pass any resource, the command won't care.</p>
 * 
 * 
 *  <p><strong>Configuration</strong> : a FileResource which entries are comma separated pairs of <key:value> (note that column ':' is the separator)
 *  
 *  Available options :
 *  
 *  <ul>
 *  	<li>remotepath : file path name, relative to the user home directory. Mandatory.</li>
 *  	<li>filetype : 'ascii' or 'binary', case insensitive. Optional.</li>
 *  </ul>
 *  
 *  
 *  </p>
 *  <p><strong>DSL example :</strong> <pre>EXECUTE get WITH $() ON myftp USING $(remotepath : existingfolder/newfile.txt, filetype : ascii) AS my.downloaded.resource</pre></p>
 * 
 * 
 * 
 * @author bsiri
 *
 */

@EngineComponent("get")
public class FTPGetFileCommand implements Command<FileResource, FTPTarget> {

	private static final ExceptionLogger logger = new ExceptionLogger(FTPGetFileCommand.class, IllegalConfigurationException.class);
	
	private static String REMOTE_NAME_OPTION = "remotepath";
	private static String FILE_TYPE_OPTION = "filetype";
	
	private FTPTarget ftp;
	
	private Collection<Resource<?>> configuration = new LinkedList<Resource<?>>(); 
	
	
	public FTPGetFileCommand(){
		super();
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	@Override
	public void setTarget(FTPTarget target) {
		this.ftp=target;
	}

	@Override
	public void setResource(FileResource resource) {
		
	}

	@Override
	public FileResource apply() {
		Map<String, String> options = getOptions();
		
		String remotePath = options.get(REMOTE_NAME_OPTION);
		String filetype = options.get(FILE_TYPE_OPTION);
		
		File downloaded;
		if (filetype==null){
			downloaded = ftp.getFile(remotePath);
		}else{
			downloaded = ftp.getFile(remotePath, filetype);
		}
		
		return new FileResource(downloaded);
	}

	@Override
	public void cleanUp() {
		//nothing special
	}
	
	
	private Map<String, String> getOptions(){
		
		Map<String, String> options=null;
		
		for (Resource<?> resource : configuration){
			if (FileResource.class.isAssignableFrom(resource.getClass())){
				options = readConf(((FileResource)resource).getFile());
			}
		}
		
		if ((options==null) || (! options.containsKey(REMOTE_NAME_OPTION))){
			String message = "ftp file download : missing configuration. You must specify at least what is the remote file name (option '"+REMOTE_NAME_OPTION+"')";
			throw logger.errAndThrow(message, null);
		}
		
		return options;
	}
	
	private Map<String, String> readConf(File file){
		try{
			return OptionsReader.BASIC_READER.getOptions(file);
		}
		catch(IOException ex){
			String message = "ftp file download : an error occured while reading the configuration : ";
			throw logger.errAndThrow(message, ex);
		}
		catch(IllegalArgumentException ex){
			String message = "ftp file download : an error occured while reading the configuration : ";
			throw logger.errAndThrow(message, ex);
		}
	}

}
