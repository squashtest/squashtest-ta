/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.commands;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.BundleComponent;
import org.squashtest.ta.commons.targets.WebTarget;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.selenium.library.SeleneseResult;
import org.squashtest.ta.selenium.library.SeleniumHtmlTestSuiteLauncher;
import org.squashtest.ta.selenium.resources.SeleniumHtmlTestSuite;
import org.squashtest.ta.selenium.resources.SeleniumResult;

/**
 * This command executes HTML format Selenium suites.
 * @author edegenetais
 *
 */
@EngineComponent("execute")
public class ExecuteHtmlSeleniumCommand extends BundleComponent implements
		Command<SeleniumHtmlTestSuite, WebTarget> {

	private Collection<Resource<?>> configuration=new ArrayList<Resource<?>>();
	private WebTarget target;
	private SeleniumHtmlTestSuite suite;
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	@Override
	public void setTarget(WebTarget target) {
		this.target=target;
	}

	@Override
	public void setResource(SeleniumHtmlTestSuite resource) {
		this.suite=resource;
	}

	@Override
	public SeleniumResult apply() {
		try {
			File suiteFile=extractMainFileFromConfiguration(configuration, suite.getBase());
			if(suiteFile==null){
				suiteFile=suite.getMain();
			}
			if(suiteFile==null){
				throw new IllegalConfigurationException("The main suite file was not configured for Selenium HTML bundle "+suite.getBase().getAbsolutePath());
			}
			
			String browserString=getOptions(configuration).get("browser");
			if(browserString==null){
				browserString=suite.getBrowserString();
			}
			if(browserString==null){
				throw new IllegalConfigurationException("The browser string was not configured for Selenium HTML bundle "+suite.getBase().getAbsolutePath());
			}
			
			URL urlSUT=target.getUrl();
			
			
			
			SeleniumHtmlTestSuiteLauncher launcher=new SeleniumHtmlTestSuiteLauncher(suiteFile);
			SeleneseResult seleneseResult=launcher.launchProcess(urlSUT, browserString);
			
			return seleneseResult.toSeleniumResult();
		} catch (IOException e) {
			throw new InstructionRuntimeException("Execution of Selenium HTML suite failed on I/O.", e);
		}
	}

	@Override
	public void cleanUp() {
		//noop.
	}

	@Override
	protected Logger getLogger() {
		return LoggerFactory.getLogger(ExecuteHtmlSeleniumCommand.class);
	}

}
