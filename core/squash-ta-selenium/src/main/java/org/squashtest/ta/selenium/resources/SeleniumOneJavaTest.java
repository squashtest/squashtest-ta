/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.selenium.library.JavaExecutableUnit;

@ResourceType("script.java.selenium1")
public class SeleniumOneJavaTest implements Resource<SeleniumOneJavaTest>, JavaExecutableUnit {

	private static final Logger logger = LoggerFactory.getLogger(SeleniumOneJavaTest.class);
	
	private ClassLoader dedicatedClassLoader;
	
	private Class<?> mainClass;
	
	/** Noarg constructor for Spring enumeration. */
	public SeleniumOneJavaTest() {
		super();
	}
	
	public SeleniumOneJavaTest(ClassLoader loader){
		super();
		setClassLoader(loader);
	}
	
	public SeleniumOneJavaTest(ClassLoader loader, String mainClassName){
		this(loader);
		setMainClass(mainClassName);
	}


	public void setClassLoader(ClassLoader dedicatedClassLoader) {
		this.dedicatedClassLoader = dedicatedClassLoader;
	}

	/**
	 * will look for the class using the fully qualified name, and the dedicated classloader. So be sure to set the 
	 * dedicated class loader first.
	 * 
	 * @param fullClassName
	 */
	public void setMainClass(String fullClassName){
		try {
			this.mainClass=dedicatedClassLoader.loadClass(fullClassName);
		} catch (ClassNotFoundException e) {
			throw logAndThrow("selenium test : main test class '"+ fullClassName +"'not found.", e);
		}
	}
	
	@Override
	public SeleniumOneJavaTest copy() {
		if (mainClass != null){
			return new SeleniumOneJavaTest(dedicatedClassLoader, mainClass.getName()); 
		}
		else{
			return new SeleniumOneJavaTest(dedicatedClassLoader);
		}
	}
	
	@Override
	public void cleanUp() {
		// noop
	}
	
	private InstructionRuntimeException logAndThrow(String message, Exception exception){
		if (logger.isErrorEnabled()){
			logger.error(message);
		}
		return new InstructionRuntimeException(message, exception);
		
	}
	
	// ********** from JavaExecutableUnit ********* 
	
	@Override
	public Class<?> getMainClass(){
		return mainClass;
	}
	
	@Override
	public ClassLoader getDedicatedClassloader() {
		return dedicatedClassLoader;
	}
}
