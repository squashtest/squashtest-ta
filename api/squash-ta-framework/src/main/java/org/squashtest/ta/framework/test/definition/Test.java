/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.framework.test.definition;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.squashtest.ta.framework.test.instructions.TestInstruction;


/**
 * A Test is made of three phases : setup, test and teardown. Those three phases exist for test semantic only 
 * and you may put any instruction you like in either of them. 
 * The only special case is the teardown phase, that will always be executed even when an exception occurs.
 * 
 * @author bsiri
 *
 */

public class Test  {
	
	private String name;
	
	private List<TestInstruction> setupPhase = new LinkedList<TestInstruction>();	
	private List<TestInstruction> testPhase = new LinkedList<TestInstruction>();	
	private List<TestInstruction> teardownPhase = new LinkedList<TestInstruction>();
	
	
	/* ******************************** getters *************************** */
	
	public String getName() {
		return name;
	}
	
	public Iterator<TestInstruction> getSetup(){
		return setupPhase.iterator();		
	}
	
	public Iterator<TestInstruction> getTests(){
		return testPhase.iterator();
	}
	
	
	public Iterator<TestInstruction> getTeardown(){
		return teardownPhase.iterator();
	}
	
	/* ******************************** adders **************************** */
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void addToSetup(TestInstruction instr){
		setupPhase.add(instr);
	}
	
	public void addToTests(TestInstruction instr){
		testPhase.add(instr);
	}
	
	public void addToTeardown(TestInstruction instr){
		teardownPhase.add(instr);
	}
	
	public void addToSetup(Iterator<TestInstruction> instructionSource){
		while(instructionSource.hasNext()){
			setupPhase.add(instructionSource.next());
		}
	}
	
	public void addToTests(Iterator<TestInstruction> instructionSource){
		while(instructionSource.hasNext()){
			testPhase.add(instructionSource.next());
		}
	}
	
	public void addToTeardown(Iterator<TestInstruction> instructionSource){
		while(instructionSource.hasNext()){
			teardownPhase.add(instructionSource.next());
		}
	}
	
	public void addToSetup(List<TestInstruction> instrs){
		setupPhase.addAll(instrs);
	}
	
	public void addToTests(List<TestInstruction> instrs){
		testPhase.addAll(instrs);
	}
	
	public void addToTeardown(List<TestInstruction> instrs){
		teardownPhase.addAll(instrs);
	}
	 
}
