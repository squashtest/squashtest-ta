/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.sahi.converters


import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.core.tools.io.TempUnzipper;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.sahi.converters.FileToSahiConverter;
import org.squashtest.ta.sahi.resources.SahiSuiteResource

import spock.lang.Specification

class FileToSahiConverterTest extends Specification {
	FileToSahiConverter converter
	def setup(){
		converter=new FileToSahiConverter()
	}
	
	def "should add API include to sah file"(){
		given:
			BinaryData targetTestSuite=new BinaryData(getClass().getResource("testTargetSuite.sah"))
			File tempDeploy=File.createTempFile("testTarget", ".sah")
			tempDeploy.deleteOnExit()
			targetTestSuite.write(tempDeploy)
			FileResource testTarget=new FileResource(tempDeploy)
		when:
			SahiSuiteResource suite=converter.convert(testTarget)
		then:
			String destination=new File(suite.getBase(),".squashTAExtension/api.js").getAbsolutePath()
			String expected=("_include(\""+destination+"\");")
			File script=suite.getMain()
			SimpleLinesData scriptContent=new SimpleLinesData(script.getAbsolutePath())
			List<String>scriptLines=scriptContent.getLines()
			scriptLines.size()>0
			String firstline=scriptLines.get(0)
			expected.equals(firstline)
		cleanup:
			tempDeploy.delete()
	}
	
	def "should auto-define main script in single-file suites"(){
		given:
			BinaryData targetTestSuite=new BinaryData(getClass().getResource("testTargetSuite.sah"))
			File tempDeploy=File.createTempFile("testTarget", ".sah")
			tempDeploy.deleteOnExit()
			targetTestSuite.write(tempDeploy)
			FileResource testTarget=new FileResource(tempDeploy)
		when:
			SahiSuiteResource suite=converter.convert(testTarget)
		then:
			new File(suite.getBase(),tempDeploy.getName()).equals(suite.getMain())
		cleanup:
			tempDeploy.delete()
	}
	
	def "shoudl take main file definition from option resource if injected"(){
		given:
			File tempDeploy=new TempUnzipper().unzipInTemp(getClass().getResourceAsStream("testSahiBundle.zip"))
			FileResource testTarget=new FileResource(tempDeploy)
		and:
			File tempConfig=File.createTempFile("test", "config")
			tempConfig.deleteOnExit()
			new BinaryData("mainpath:testSahiBundle/sahi1.sah".getBytes()).write(tempConfig)
		when:
			converter.addConfiguration([new FileResource(tempConfig)])
			SahiSuiteResource suite=converter.convert(testTarget)
		then:
			new File(suite.getBase(),"testSahiBundle/sahi1.sah").equals(suite.getMain())
		cleanup:
			tempDeploy.delete()
			tempConfig.delete()
	}
	
	def "should add API include to every sah file in sahi bundle"(){
		given:
			File tempDeploy=new TempUnzipper().unzipInTemp(getClass().getResourceAsStream("testSahiBundle.zip"))
			FileResource testTarget=new FileResource(tempDeploy)
		when:
			SahiSuiteResource suite=converter.convert(testTarget)
		then:
			String destination=new File(suite.getBase(),".squashTAExtension/api.js").getAbsolutePath()
			String expected=("_include(\""+destination+"\");")
			List<File> files=new FileTree().enumerate(suite.getBase(), EnumerationMode.FILES_ONLY)
			StringBuilder difference=new StringBuilder()
			for(File file:files){
				if(file.getName().endsWith(".sah")){
					SimpleLinesData script=new SimpleLinesData(file.getAbsolutePath())
					if(script.getLines().size()==0
						|| !script.getLines().get(0).equals(expected)){
						difference.append("Missing include in script "+file.getAbsolutePath()+"\n")
					}
				}
			}
			difference.toString().equals("")
		cleanup:
			new TempUnzipper().clean(tempDeploy)
	}
	
	def "should leave non-sah files alone in sahi bundle"(){
		given:
			File tempDeploy=new TempUnzipper().unzipInTemp(getClass().getResourceAsStream("testSahiBundle.zip"))
			FileResource testTarget=new FileResource(tempDeploy)
		when:
			SahiSuiteResource suite=converter.convert(testTarget)
		then:
			List<File> files=new FileTree().enumerate(tempDeploy, EnumerationMode.FILES_ONLY)
			StringBuilder difference=new StringBuilder()
			for(File file:files){
				if(!file.getName().endsWith(".sah")){
					BinaryData origData=new BinaryData(file)
					String relativeName=new FileTree().getRelativePath(tempDeploy, file)
					File convertedLocation=new File(suite.getBase(),relativeName)
					BinaryData convertedData=new BinaryData(convertedLocation)
					byte[] origDataBuffer=origData.toByteArray()
					byte[] convertedDataBuffer=convertedData.toByteArray()
					boolean identical=true
					int index=0
					while(identical && index<origDataBuffer.length && index<convertedDataBuffer.length){
						if(origDataBuffer[index]!= convertedDataBuffer[index]){
							identical=false
						}
						index++
					}
					if(!identical || index<origDataBuffer.length || index<convertedDataBuffer.length){
						difference.append(origDataBuffer).append("\n")
						difference.append(convertedDataBuffer)
					}
				}
			}
			difference.toString().equals("")
		cleanup:
			new TempUnzipper().clean(tempDeploy)
	}
	
	def "api js script should exist in bundle created from sah file"(){
		given:
			BinaryData targetTestSuite=new BinaryData(getClass().getResource("testTargetSuite.sah"))
			File tempDeploy=File.createTempFile("testTarget", ".sah")
			tempDeploy.deleteOnExit()
			targetTestSuite.write(tempDeploy)
			FileResource testTarget=new FileResource(tempDeploy)
		when:
			SahiSuiteResource suite=converter.convert(testTarget)
		then:
			new File(suite.getBase(),".squashTAExtension/api.js").exists()
		cleanup:
			tempDeploy.delete()
	}
	
	def "api js should define the getBundleBase function in bundle created from sah file"(){
		given:
			BinaryData targetTestSuite=new BinaryData(getClass().getResource("testTargetSuite.sah"))
			File tempDeploy=File.createTempFile("testTarget", ".sah")
			tempDeploy.deleteOnExit()
			targetTestSuite.write(tempDeploy)
			FileResource testTarget=new FileResource(tempDeploy)
		when:
			SahiSuiteResource suite=converter.convert(testTarget)
		then:
			SimpleLinesData scriptContent=new SimpleLinesData(new File(suite.getBase(),".squashTAExtension/api.js").getPath())
			scriptContent.getLines().contains("function getBundleBase(){")
		cleanup:
			tempDeploy.delete()
	}
}
