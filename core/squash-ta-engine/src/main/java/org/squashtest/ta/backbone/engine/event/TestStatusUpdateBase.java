/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.event;

import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.result.TestResult;

/**
 * Base for {@link TestStatusUpdate} events: in our execution model it needs to
 * be enriched because the {@link org.squashtest.ta.backbone.engine.TestRunner} that posts the event doesn't know
 * which ecosystem it is running in.
 * 
 * @author edegenetais
 * 
 */
public abstract class TestStatusUpdateBase extends TestStatusUpdate implements ContextualStatusUpdateEvent<TestResult> {
	private String ecosystemName;

	@Override
	public void addContext(ContextSource manager) {
		manager.enrich(this);
	}
	
	public String getEcosystemName() {
		return ecosystemName;
	}

	public void setEcosystemName(String ecosystemName) {
		this.ecosystemName = ecosystemName;
	}
}
