/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.soapui.library;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.java.JavaSystemProcessConnector;
import org.squashtest.ta.commons.library.process.StreamMuncher;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.soapui.resources.SoapUIResult;

import com.eviware.soapui.model.testsuite.TestRunner.Status;
import com.eviware.soapui.support.SoapUIException;

/**
 * Port from CATS of the Sahi test suite runner. Used by the sahi components to run sahi test suites.
 * @author Agnes
 * @author edegenetais
 */
public class SquashSoapUIRunner extends JavaSystemProcessConnector implements SoapUiProcessCommunication{
	private final static Logger LOGGER = LoggerFactory.getLogger(SquashSoapUIRunner.class);
	
	private Properties config;
	
    public SquashSoapUIRunner(Properties config) {
    	this.config=config;
	}
    
    /**
     * Runs a SoapUI test file
     * The test file contains 
     * the definition of the endpoints
     * the tests suite to execute
     * the test cases to execute
     * 
     * @param soapUIFile
     * @throws IOException
     * @throws InterruptedException
     */
    public SoapUIResult run(File soapUIFile) throws XmlException, IOException, SoapUIException {
    	
    	logSoapUIConf(soapUIFile);
    	
    	//NB: the class name is transmitted as a String to avoid loading it and triggering the SoapUi related logging snafu...
    	ArrayList<String> arguments = buildSoapuiProcessArguements(soapUIFile);
    	
		List<String> command=createCommand(arguments, "org.squashtest.ta.soapui.library.SoapUiProcessExecutor");
    	ProcessBuilder processBuilder=new ProcessBuilder(command);
    	processBuilder.directory(new File(".").getAbsoluteFile());
    	Process p=processBuilder.start();
    	
    	Map<String,Status> testStatusesByName=new HashMap<String, Status>();
    	Map<String,String> testMessagesByName=new HashMap<String, String>();
    	
    	PipeListener statusPrintListener=new PipeListener(p,testStatusesByName,testMessagesByName);
    	statusPrintListener.start();
    	StreamMuncher errTail=new StreamMuncher("SoapUI stderr", p.getErrorStream(), 25600);
    	errTail.start();
    	
    	int waitFor;
		try {
			waitFor = p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			waitFor = 5;
		}
		errTail.requestStop();
		
		switch(waitFor){
    	case INPUT_FILE_IO_ERROR:
    		throw new IOException("I/O error in the soapUI process:\n"+errTail.getStreamContent());
    	case WORKSPACE_XML_EXCEPTION:
    		throw new XmlException("Trouble loading the SoapUI workspace:\n"+errTail.getStreamContent());
    	case WORKSPACE_SOAPUI_EXCEPTION:
    		throw new SoapUIException("Trouble loading the SoapUI workspace:\n"+errTail.getStreamContent());
    	case 0:
    		//this is the OK case, Noop.
    		break;
    	default:
    		throw new InstructionRuntimeException("SoapUI process failed with code "+waitFor+"\n"+errTail.getStreamContent());
    	}

        return new SoapUIResult(testStatusesByName, testMessagesByName);
    }

	private ArrayList<String> buildSoapuiProcessArguements(File soapUIFile)
			throws IOException {
		ArrayList<String> arguments = new ArrayList<String>();
    	arguments.add(soapUIFile.getAbsolutePath());
    	File configTemp=File.createTempFile("sopaui_temp", ".properties");
    	configTemp.deleteOnExit();
    	FileWriter writer = new FileWriter(configTemp);
		config.store(writer,"Temporary");
		writer.close();
    	arguments.add(configTemp.getAbsolutePath());
		return arguments;
	}
    
    /**
     * @param pSoapUIFile
     * @param pSoapUITestSuite
     * @param pSoapUITestCase
     */
    private void logSoapUIConf(File soapUIFile) {
        LOGGER.debug("SoapUI Test File = {}", soapUIFile.getName());
        LOGGER.debug("SoapUI Test Suite to test = {}", config.get(SoapUiProcessExecutor.SOAPUI_TESTSUITE));
        LOGGER.debug("SoapUI Test Case to test = {}", config.get(SoapUiProcessExecutor.SOAPUI_TESTCASE));
    }
}
