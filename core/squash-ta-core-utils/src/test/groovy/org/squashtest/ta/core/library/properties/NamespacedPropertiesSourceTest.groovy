/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.library.properties

import spock.lang.Specification

class NamespacedPropertiesSourceTest extends Specification {

	def "should find value for a key using a namespace"(){
		
		given :
			def ppts = initProperties()
			def source = new NamespacedPropertiesSource(ppts, "bob");
		
		when :
			def res = source.getValue("ta.key1");
		
		
		then :
			res == "value1"
			
		
	}
	
	def "should make a namespaced pattern (1)"(){
		
		given :
			def pattern = "my\\.property.*"
			
		when :
			def res = new NamespacedPropertiesSource(null, "bob").makeNamespacedPattern(pattern)
		
		then :
			res == "bob\\.my\\.property.*"
	}
	
	def "should make a namespaced pattern (2)"(){
		
		given :
			def pattern = "^my\\.property.*"
		
		when :
			def res = new NamespacedPropertiesSource(null, "bob").makeNamespacedPattern(pattern)
		
		then :
			res == "^bob\\.my\\.property.*"
	}
	
	
	def "should strip the namespace"(){
		
		given :
			def key = "bob.my.property"
		
		when :
			def res = new NamespacedPropertiesSource(null, "bob").stripNamespace(key)
		
		then :
			res == "my.property"
		
	}
	
	def "should find keys matching a pattern with respect to the namespace(1)"(){
		
		given :
			def ppts = initProperties()
			def source = new NamespacedPropertiesSource(ppts, "bob")
		
		when :
			def res = source.findKeysMatchingPattern("ta.key\\d") as Set
		
		
		then :
			res == ["ta.key1", "ta.key3"] as Set
	}
	
	def "should find keys matching a pattern with respect to the namespace(2)"(){
		
		given :
			def ppts = initProperties()
			def source = new NamespacedPropertiesSource(ppts, "bob")
		
		when :
			def res = source.findKeysMatchingPattern("ta.key[12]") as Set
		
		
		then :
			res == ["ta.key1"] as Set
	}
	
	def initProperties={
		
		def ppts = new Properties();
		
		ppts.setProperty("bob.ta.key1", "value1")
		ppts.setProperty("ta.key2", "value2")
		ppts.setProperty("bob.ta.key3", "value3")
		
		return ppts
		
	}
}
