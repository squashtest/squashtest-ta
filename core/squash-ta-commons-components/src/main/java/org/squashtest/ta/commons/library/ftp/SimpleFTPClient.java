/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

public class SimpleFTPClient {


	private static final Logger logger = LoggerFactory.getLogger(SimpleFTPClient.class);
	
	private Properties configuration;	
	
	private FTPClient client;
	
	/* ****** config ********** */

	private String hostname = null;
	private String username = null;
	private String password = null;
	private Integer port = null;
	
	private Integer filetype = null;
	
	private String system = null;
	
	
	/* ****** init status ****** */
	
	private boolean initialized;
	private InstructionRuntimeException why;
	private String homeDir;

	public void setHostName(String hostName) {
		this.hostname = hostName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
	
	public void setDefaultPort(){
		this.port=FTP.DEFAULT_PORT;
	}


	public void setFiletype(Integer filetype) {
		this.filetype = filetype;
	}

	public void setDefaultFiletype(){
		this.filetype= FTP.ASCII_FILE_TYPE;
	}

	public 	void setSystem(String system) {
		this.system = system;
	}


	public SimpleFTPClient(){
		super();
		initialized=false;
	}
	
	
	/**
	 * May fail init without crashing the app : a failing ftp is only a problem when you require it.  
	 * 
	 * 
	 */
	public void init() {		
		client = new FTPClient();		
		start();
	}


	public void reset() {
		stop();
		start();
	}

	
	private void start(){
		try {
			
			preInitialization();
			
			connect();
			login();
			
			postInitialization();
			
			initialized=true;
			
		} catch (SocketException e) {
			failWith("ftp client : could not connect to socket (net interface down ?) : ", e);
		} catch (IOException e) {
			failWith("ftp client : error during communication with the server", e);
		}		
	}


	
	private void stop(){
		try {
			client.logout();
			client.disconnect();
		} catch (IOException e) {
			if (logger.isWarnEnabled()){
				logger.warn("ftp client : error occured when logging out / disconnect", e);
			}
		}		
	}

	public void cleanup() {
		stop();
	}
	
	public void setConfiguration(Properties configuration){
		this.configuration = configuration;
	}

	public Properties getConfiguration() {
		return (Properties)configuration.clone();		
	}
	
	
	
	public FTPClient getClient(){
		return client;
	}
	

	
	//* ****************** public interface for common operations ****************** 
	
	public void putFile(String remotePath, File file){
		__putFile(remotePath, file, null);
	}
	
	public void putFile(String remotePath, File file, String fileType){
		
		Integer iFiletype =  decodeFileType(fileType);
		
		if (iFiletype == null){
			throw logAndThrowIllegalConfiguration("ftp : unsupported file type "+fileType, null);
		}
		
		__putFile(remotePath, file, iFiletype);
	}
	


	public File getFile(String remotePath){
		return __getFile(remotePath, null);
	}
	
	
	public File getFile(String remotePath, String fileType){
		
		Integer iFiletype =  decodeFileType(fileType);
		
		if (iFiletype == null){
			throw logAndThrowIllegalConfiguration("ftp : unsupported file type "+fileType, null);
		}
		
		return __getFile(remotePath, iFiletype);
	}
	
	/**
	 * Deletes a remote file. In this default version, the attempt to delete a non-existing file or directory triggers an exception.
	 * @param remotePath
	 *            the path of the file to delete on the remote system.
	 * @param isDirectory
	 *            set to <code>true</code> if the remote target is a directory.
	 */
	public synchronized void deleteFile(String remotePath, boolean isDirectory){
		deleteFile(remotePath, isDirectory, true);//by default, we fail
	}

	/**
	 * Deletes a remote file or directory.
	 * 
	 * @param remotePath
	 *            the path of the file to delete on the remote system.
	 * @param isDirectory
	 *            set to <code>true</code> if the remote target is a directory.
	 * @param failIfDoesNotExist
	 *            if <code>true</code> the all failures trigger exceptions, if
	 *            false, then attempts to delete non-existing files are silently
	 *            failed.
	 */
	public synchronized void deleteFile(String remotePath, boolean isDirectory, boolean failIfDoesNotExist){
		
		connectionAliveOrDie();
		
		boolean completed=false;
		
		try{
			if (isDirectory==true){
				completed = client.removeDirectory(remotePath.substring(0, remotePath.length()-1));
			}
			else{
				completed = client.deleteFile(remotePath);
			}
					
			if (throwForFailure(failIfDoesNotExist, completed)){
				throw logAndThrow("ftp client : an error occured while removing file '"+remotePath+"', server returned error code "+client.getReplyCode(), null);
			}
			
		}catch(IOException ex){
			throw logAndThrow("ftp client : an error occured while removing file '"+remotePath+"', server returned error code "+client.getReplyCode(), ex);
		}
			
	}

	/**
	 * Decide if we must throw an exception to report delete failure
	 * @param failIfDoesNotExist
	 * @param completed
	 * @return
	 */
	private boolean throwForFailure(boolean failIfDoesNotExist,
			boolean completed) {
		boolean throwForFailure;
		if(completed){
			throwForFailure=false;
		}else{
			if(failIfDoesNotExist || client.getReplyCode()!=550){
				/*
				 * we throw if the file existed or if failIfDoesNotExist=true
				 * (meaning we had to report non-existing files)
				 */
				throwForFailure=true;
			}else{
				throwForFailure=false;
			}
		}
		return throwForFailure;
	}

	
	// ********************** private code corresponding to actual operations *****************
	
	
	private synchronized void __putFile(String remotePath, File file, Integer fileType){
		
		connectionAliveOrDie();
		
		InputStream input=null;
		
		try {
			input = new FileInputStream(file);
			
			__uploadFromStream(remotePath, fileType, input);			
			
		} 
		catch (FileNotFoundException e) {
			throw logAndThrow("ftp client : could not open file '"+file.getPath()+"'", e);
		} 
		catch (IOException e) {
			throw logAndThrow("ftp client : an error occured while uploading file '"+remotePath+"', server returned error code "+client.getReplyCode(), e);
		}
		finally{
			closeStream(input);
		}
		
	}

	private void __uploadFromStream(String remotePath, Integer fileType,
			InputStream input) throws IOException{
		
		try{
			if (fileType!=null) { 
				client.setFileType(fileType); //set the specified file type	 
			}	
				
			navigateHome();
			// Add the parents directory creation if they not already exist. [Feat 864]
			// This replacement allow the use of '\' and '/' in the path given in the property file  
			String path = remotePath.replace('\\', '/');
			__createDir(path);
			navigateHome();
			
			boolean success = client.storeFile(path, input);

			if (! success){
				throw logAndThrow("ftp client : an error occured while uploading file '"+remotePath+"', server returned error code "+client.getReplyCode(), null);
			}
		}
		finally{			
			if (fileType!=null) { 
				client.setFileType(this.filetype); //reset the configuration file type 
			}						
		}
		
	}
	
	
	private void __createDir( String remotePath) throws IOException
	{
		String[] directories = remotePath.split("/");
		for (int i = 0; i < directories.length-1; i++) {
			String directory = directories[i];
			if(directory!="" && !client.changeWorkingDirectory(directory)){
				boolean success = client.makeDirectory(directory);
				if (! success){
					throw logAndThrow("ftp client : an error occured while creating one of the the parent directory of the file to upload '"+directory+
							"', server returned error code "+client.getReplyCode(), null);
				}
				client.changeWorkingDirectory(directory);
			}
		}
	}
	
	
	private synchronized File __getFile(String remotePath, Integer fileType){
		
		connectionAliveOrDie();		
		
		File local;
		FileOutputStream out=null;
		
		try {

			local = File.createTempFile("ftp.client", "temp");
			out = new FileOutputStream(local);
			
			__downloadToStream(remotePath, fileType, out);
			
		} 
		catch (IOException e) {
			throw logAndThrow("ftp client : an error occured while downloading the file :", e);
		}
		finally {
			closeStream(out);
		}
		
		return local;
		
	}


	private void __downloadToStream(String remotePath, Integer fileType,
			FileOutputStream out) throws IOException{
		try {
			
			if (fileType!=null) { 
				client.setFileType(fileType); //set the overidden file type	
			}	
			
			navigateHome();
			boolean success = client.retrieveFile(remotePath, out);
			
			if (! success){
				throw logAndThrow("ftp client : an error occured while downloading file '"+remotePath+"', server returned error "+client.getReplyCode(), null);				
			}
			
		} 
		finally{
			if (fileType!=null) { 
				client.setFileType(this.filetype);    //restore the default file type	 
			}	
		}
	}


	// ****************************** argument parsing ************** 
	
	public static Integer decodeFileType(String rawFiletype){
		
		if (rawFiletype==null){
			return null;
		}
		
		Integer result = tryFiletypeAsInt(rawFiletype);
		if (result == null){
			result = tryFiletypeAsString(rawFiletype);
		}
		
		return result;		
	}
	
	/*
	 * returns null if designated an unsupported file type
	 * 
	 */
	private static Integer tryFiletypeAsString(String raw){
		if (raw.trim().equalsIgnoreCase("ascii")){
			return FTP.ASCII_FILE_TYPE;
		}else if (raw.trim().equalsIgnoreCase("binary")){
			return FTP.BINARY_FILE_TYPE;
		}else{		
			return null;
		}
	}

	
	/*
	 * Returns null if was not a valid int, or indicated an unsupported file type.
	 * 
	 * @param raw
	 * @return
	 */
	private static  Integer tryFiletypeAsInt(String raw){
		Integer result=null;
		try{
			result = Integer.decode(raw.trim());
		}catch(NumberFormatException ex){
			// well, we'll try as String later
		}
		
		if ((result!=null) && (checkFileTypeAsInt(result))){
			return result;
		}else{
			return null;
		}
	}
	
	
	private static boolean checkFileTypeAsInt(Integer fileType){
		return ((fileType == FTP.ASCII_FILE_TYPE) || (fileType == FTP.BINARY_FILE_TYPE));
	}
	
	
	//*************************** micro methods because SONAR complained too loud ******
	
	private void preInitialization(){
		
		if (filetype==null){
			setDefaultFiletype();		
		}
		if (port==null){
			setDefaultPort();		
		}
		
		if (system!=null){
			FTPClientConfig config = new FTPClientConfig(system);
			client.configure(config);
		}		
	}
		
	private void connect() throws SocketException, IOException{
		if (port!=null){
			client.connect(hostname, port);
		}else{
			client.connect(hostname);
		}		
		client.enterLocalPassiveMode();
	}
	
	private void login() throws IOException{
		boolean successfulLogin;
		
		String pass = (password!=null) ? password : ""; 
		String user = (username!=null) ? username : "";
		successfulLogin = client.login(user, pass);
	
		if (! successfulLogin){
			int reply = client.getReplyCode();
			failWith("ftp client : could not login to the server, perhaps the credentials were rejected. Failed with reply code : "+reply, null);
		}		
	}

	
	
	private void postInitialization() throws IOException{
		client.setFileType(filetype);
		homeDir = client.printWorkingDirectory();
	}

	
	private void closeStream(OutputStream out){
		try{
			if (out!=null){
				out.close();				
			}
		} catch (IOException e) {
			if (logger.isWarnEnabled()){
				logger.warn("ftp client : download complete, however an error" +
							" occured after operation completion. Proceeding anyway.",e);
			}
		}	
	}
	
	private void closeStream(InputStream in){
		try{
			if (in!=null){
				in.close();				
			}
		} catch (IOException e) {
			if (logger.isWarnEnabled()){
				logger.warn("ftp client : upload complete, however an error occured after " +
							"operation completion. Proceeding anyway.",e);
			}
		}	
	}
	
	/* ************************************** other useful things ***********************/
	
	private void navigateHome() throws IOException{
		client.changeWorkingDirectory(homeDir);
	}
	
	private void connectionAliveOrDie(){
		boolean shouldRestart=false;
		
		if (initialized){
			try{
				boolean completed = client.sendNoOp();
				if (! completed) {
					shouldRestart=true;
				}
			}catch(IOException ex){
				shouldRestart=true;
			}
		}else{
			shouldRestart=true;
		}
		
		if (shouldRestart){
			start();
		}

		if (! initialized){
			throw why;
		}
	}

	
	private void failWith(String message, Exception why){
		if (logger.isErrorEnabled()){
			logger.error(message,why);
		}
		initialized=false;
		this.why = new InstructionRuntimeException(message, why);
	}
	
	
	private InstructionRuntimeException logAndThrow(String message, Exception orig){
		if (logger.isErrorEnabled()){
			logger.error(message, orig);
		}
		return new InstructionRuntimeException(message, orig);
	}

	
	private IllegalConfigurationException logAndThrowIllegalConfiguration(String message, Exception orig){
		if (logger.isErrorEnabled()){
			logger.error(message, orig);
		}
		return new IllegalConfigurationException(message, orig);
	}

}
