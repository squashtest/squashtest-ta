/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.resources;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.java.JavaCodeBundleClassLoader;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;

/**
 * This resource encapsulates a java code bundle, including resources and java classes that can be used through a 
 * @author edegenetais
 *
 */
@ResourceType("script.java")
public class JavaCodeBundle implements Resource<JavaCodeBundle>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JavaCodeBundle.class);
	private File bundleBase;
	private Set<String>classNames;
	
	/**
	 * noarg constructor for Spring enumeration.
	 */
	public JavaCodeBundle() {}
	
	public JavaCodeBundle(File directoryLocation, Set<String>classNames) {
		directoryLocation.canRead();//null check
		bundleBase=directoryLocation;
		this.classNames=Collections.unmodifiableSet(new HashSet<String>(classNames));
	}
	public ClassLoader getDedicatedClassloader(){
		return new JavaCodeBundleClassLoader(bundleBase);
	}
	
	public Set<String> getBundleClassNames(){
		return classNames;
	}
	
	@Override
	public JavaCodeBundle copy() {
		return new JavaCodeBundle(bundleBase,classNames);
	}

	@Override
	public void cleanUp() {
		try {
			if(bundleBase.exists()){
				new FileTree().clean(bundleBase);
			}
		} catch (IOException e) {
			LOGGER.warn("Failed to clean up java code bundle base "+bundleBase.getAbsolutePath(),e);
		}
	}
}
