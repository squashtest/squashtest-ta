-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Sam 28 Avril 2012 à 14:26
-- Version du serveur: 5.5.20
-- Version de PHP: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `ta-test`
--

-- --------------------------------------------------------

--
-- Structure de la table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `OWNER` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `company`
--

INSERT INTO `company` (`ID`, `NAME`, `OWNER`) VALUES
(1, 'ink inc.', 'Martin');

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `OWNER` varchar(100) NOT NULL,
  `DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `test`
--

-- --------------------------------------------------------

--
-- Structure de la table `worker`
--

CREATE TABLE IF NOT EXISTS `worker` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `POSITION` varchar(100) NOT NULL,
  `COMPANY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `COMPANY` (`COMPANY`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `worker`
--

INSERT INTO `worker` (`ID`, `NAME`, `POSITION`, `COMPANY`) VALUES
(1, 'Bob', 'employee', 1),
(5, 'Martin', 'boss', 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `worker`
--
ALTER TABLE `worker`
  ADD CONSTRAINT `worker_ibfk_1` FOREIGN KEY (`COMPANY`) REFERENCES `company` (`ID`) ON DELETE SET NULL;

-- table without primary key to test the pseudo-primary-key configuration in dbunit
CREATE TABLE IF NOT EXISTS `no_pk_table` (
  `NUMBER` bigint(10) NOT NULL,
  `NAME` varchar(100) NOT NULL
)ENGINE=InnoDB;
-- table WITH primary key to test mixed PPK/DB PK mode in dbunit --
CREATE TABLE IF NOT EXISTS `db_pk_table_testppk` (
  `NB` bigint(10) NOT NULL,
  `NOM` varchar(100) NOT NULL,
  `OWNER` varchar(100) NOT NULL,
  PRIMARY KEY (`NB`)
)ENGINE=InnoDB; 

-- table WITH mutli-column PK to test multi-column pk information extraction from DB --
CREATE TABLE IF NOT EXISTS `db_multipk_table` (
  `NB` bigint(10) NOT NULL,
  `NOM` varchar(100) NOT NULL,
  `OWNER` varchar(100) NOT NULL,
  PRIMARY KEY (`NB`,`NOM`)
)

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
