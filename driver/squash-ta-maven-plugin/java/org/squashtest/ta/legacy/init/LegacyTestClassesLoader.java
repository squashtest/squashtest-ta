/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.init;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.backbone.init.ScannerComponentLoader;
import org.squashtest.ta.legacy.exception.LegacyTestRunnerInitializationException;

/**
 * This component finds and loads legacy CATS test classes.
 * <p>
 * <em>NB:This object is
 * not in itself a {@link ClassLoader} implementation, it makes use of the class<em>
 * </p>
 * loader it was loaded by.
 * 
 * @author edegenetais
 * 
 */
public class LegacyTestClassesLoader extends ScannerComponentLoader{
	private static final Logger TEST_DETECTION_LOGGER=LoggerFactory.getLogger("legacy-detection-logger") ;
	
	private static final String TEMPLATE_RESOURCE_NAME = "legacyTestLoaderConfigTemplate.xml";
	private static final String TEMPLATE_CLOSING_ERROR_MSG = "Error while closing the legacy class loader configuration template.";

	/**
	 * List of package names to enumerate.
	 * @param packageNames
	 * @return
	 */
	public Set<Class<AbstractCatsTestCase>>enumerateTestClasses(Set<String> packageNames){
		String template=loadConfigurationTemplate(TEMPLATE_RESOURCE_NAME);
		Set<String> excludeSet = Collections.emptySet();
		String configuration=generateConfigurationFromTemplate(template, packageNames, excludeSet);
		if(TEST_DETECTION_LOGGER.isDebugEnabled()){
			TEST_DETECTION_LOGGER.debug("Voici la configuration de scan g�n�r�e:"+configuration);
		}
		try {
			ApplicationContext context=loadGeneratedConfiguration(configuration, "Legacy CATS test loader configuration.");
			LegacyTestClassManager classManager=context.getBean(LegacyTestClassManager.class);
			return classManager.getTestSet();
		} catch (UnsupportedEncodingException e) {
			StringBuilder msgBuilder=new StringBuilder("Error while loading cats test classes in package(s):");
			setToList(msgBuilder, packageNames);
			throw new LegacyTestRunnerInitializationException(msgBuilder.toString(),e);
		}
	}

	@Override
	protected String getTemplateCloseErrorMsg() {
		return TEMPLATE_CLOSING_ERROR_MSG;
	}
}
