/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.init;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.exception.ManifestParseException;
import org.squashtest.ta.core.tools.io.SimpleLinesData;

/**
 * This object parses the component manifest file used to declare packages that
 * contain squashTA engine components. This file must contain the following
 * lines:
 * <ul>
 * <li><code>pluginName: &lt;name-of-the-plugin&gt;</code> (one occurrence of
 * this line only)</li>
 * <li><code>basePackage: &lt;base-package-name&gt;</code> (as many occurences
 * as necessary)</li>
 * <li><code>elementExclude: &lt;element-to-exclude&gt;</code> (as many
 * occurences as necessary)</li>
 * <li>&lt;unsupportedEntryName&gt;: &lt;content&gt; unsupported entries: will
 * be ignored.
 * </ul>
 * <p>
 * The plugin name line must be the first line of the manifest and is mandatory.
 * Please note that plugin names should be unique (identical plugin names will
 * lead to silent fusion of the configuration data, however).
 * </p>
 * <p>
 * Base package and excluded element lines may follow the plugin name
 * declaration in any order. Each declared base package (syntax of the
 * <code>component-scan</code> spring configuration element) will be scanned for
 * squashTA engine component classes. Elements excluded in elementExclude lines
 * (syntax of the &quot;regex&quot; type component-scan exclude filter) will not
 * be scanned or loaded even if they are included in one of the declared base
 * packages.
 * </p>
 * <p>
 * Entries (lines on the format &lt;name&gt;: &lt;content&gt;) with an
 * unsupported name will be accepted but ignored. Non entries (any line of the
 * without the <code>&quot;: &quot;</code> separator will cause a parsing error.
 * </p>
 * 
 * @author edegenetais
 * 
 */
public class ComponentManifestParser {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ComponentManifestParser.class);

	public static final String ELEMENT_EXCLUDE = "elementExclude";
	public static final String PLUGIN_NAME = "pluginName";
	public static final String BASE_PACKAGE = "basePackage";
	public static final String ENTRY_ELEMENT_SEPARATOR = ": ";

	/**
	 * This method reads a squashTA component manifest and lists all referenced
	 * packages.
	 * 
	 * @param configuration
	 *            engine configuration being built.
	 * @param manifestURL
	 *            URL of the manifest to parse.
	 * @throws IOException
	 *             in case the manifest data fetching fails.
	 */
	public void loadManifest(ComponentConfiguration configuration,
			URL manifestURL) throws IOException {
		SimpleLinesData manifestData=new SimpleLinesData(manifestURL);
		Iterator<String> manifest=manifestData.getLines().iterator();
		// The first line has to be the plugin name: extract it.
		if (!manifest.hasNext()) {
			throw new ManifestParseException("Found empty manifest in "
				+ manifestURL.toExternalForm());
		}
		String pluginName = extractPluginName(manifest.next());
			
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Loading plugin configuration for: "+pluginName);
		}
			
		// we go to next line and iterate on entries until the end of the
		// manifest.
		while (manifest.hasNext()) {
			parseElementEntry(configuration, manifest.next(), pluginName);
		}
	}

	/**
	 * Parse an element (see {@link #BASE_PACKAGE} and {@link #ELEMENT_EXCLUDE})
	 * manifest entry. Any entry that is neither a base package declaration nor
	 * an excluded element declaration will be silently ignored.
	 * 
	 * @param configuration
	 *            the configuration being built.
	 * @param manifestEntry
	 *            the manifest entry to parse.
	 * @param pluginName
	 *            the plugin name declared by the package.
	 */
	private void parseElementEntry(ComponentConfiguration configuration,
			String manifestEntry, String pluginName) {
		String[] entryElements = parseEntry(manifestEntry);

		if (ComponentManifestParser.PLUGIN_NAME.equals(entryElements[0])) {
			throw new ManifestParseException(
					"Too many (more than one) plugin name declaration in "
							+ pluginName);
		}

		if (BASE_PACKAGE.equals(entryElements[0])) {

			configuration.addBasePackageToPlugin(pluginName, entryElements[1]);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("....base package: "
						+ entryElements[1]);
			}
		} else if (ComponentManifestParser.ELEMENT_EXCLUDE
				.equals(entryElements[0])) {
			configuration.addExcludedElementToPlugin(pluginName,
					entryElements[1]);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("....excluded element: " + manifestEntry);
			}
		}
	}

	/**
	 * Try to get a plugin name from a manifest line.
	 * 
	 * @param manifestEntry
	 *            the manifest entry.
	 * @return the plugin name declared in the entry, if it is a correct plugin
	 *         name entry.
	 * @throws ManifestParseException
	 *             if the file line is not a correct plugin name entry (@see
	 *             {@link ComponentManifestParser#extractPluginName(String)})
	 */
	private String extractPluginName(String manifestEntry) {
		String[] entryElements = parseEntry(manifestEntry);
		String pluginName;
		if (ComponentManifestParser.PLUGIN_NAME.equals(entryElements[0])) {
			pluginName = entryElements[1];
		} else {
			throw new ManifestParseException(
					"pluginName is not the first manifest entry");
		}
		return pluginName;
	}

	/**
	 * Breaks an entry down in name and content.
	 * @param manifestEntry the file line to parse.
	 * @return a String array laid out as follows: {entryName,entryContent}
	 */
	private String[] parseEntry(String manifestEntry) {
		String[] entryElements = manifestEntry
				.split(ComponentManifestParser.ENTRY_ELEMENT_SEPARATOR);
		if (entryElements.length < 2) {
			throw new ManifestParseException("Not a valid manifest entry: "
					+ manifestEntry);
		}
		return entryElements;
	}
}