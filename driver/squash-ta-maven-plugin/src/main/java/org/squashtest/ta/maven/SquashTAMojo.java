/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.commons.factories.TestSuiteDescription;
import org.squashtest.ta.commons.factories.dsl.DSLTestSuiteFactory;
import org.squashtest.ta.commons.factories.macros.MacroDSLProcessor;
import org.squashtest.ta.commons.init.DefaultTestProjectWorkspaceBrowser;
import org.squashtest.ta.commons.init.FileSystemTestSuiteDefinition;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.maven.testfilter.FileFilterFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * <p>That mojo is a maven way to start Squash TA. It will perform the following action in that order : 
 * 
 * <ol>
 * 	<li>Build a {@link TestSuite} using the Squash TA project this build is bound to,</li>
 * 	<li>Initialize the test context,</li>
 * 	<li>Initialize the engine,</li>
 * 	<li>Perform the actual tests,</li>
 * 	<li>Execute the exporters.</li> 
 * </ol>
 * </p>

 * @author bsiri
 * 
 * @goal run
 * @requiresDependencyResolution test
 * @phase integration-test
 * 
 */
public class SquashTAMojo extends AbstractSquashTaMojo {
	
	private static final String BUILT_IN_MACROS = "builtin/macros/";
	
	private File macroTempDirectory;
	
	/* *************************** configuration section *********************************** */
	
	/**
	 * 
	 * Tells the mojo where to find the project sources.
	 * 
	 * @parameter expression="${ta.src}" default-value="src/squashTA"
	 * 
	 */	
	private File mainDirectory;
		
	
	/**
	 * The base directory where lie the definition for the additional repositories used by Squash TA. Defaults to {@link #mainDirectory}/repositories.
	 * 
	 * @parameter expression="${ta.src.repositories}" 
	 * 
	 */
	private File repositoriesDirectory;
	

	/**
	 * The base directory where lie the definition for the target that Squash TA will hit. Defaults to {@link #mainDirectory}/targets.
	 * 
	 * @parameter expression="${ta.src.targets}"
	 * 
	 */
	private File targetsDirectory;
	
	
	/**
	 * The base directory where lie the different test resources that the engine may have to load. Defaults to {@link #mainDirectory}/resources.
	 * 
	 * @parameter expression="${ta.src.resources}"
	 * 
	 */
	private File resourcesDirectory;
	
	
	
	/**
	 * The base directory where lie the different test scripts. Defaults to {@link #mainDirectory}/tests.
	 * 
	 * @parameter expression="${ta.src.tests}"
	 */
	private File testsDirectory;
	
	
	/**
	 * The base directory where lie the user-defined shortcuts definitions. Defaults to {@link #mainDirectory}/shortcuts.
	 * 
	 * @parameter expression="${ta.src.shortcuts}"
	 * 
	 */
	private File shortcutsDirectory;
	
	
	/**
	 * @parameter expression="${project.build.directory}"
	 */
	private File buildDirectory;
	
	
	/**
	 * If set, defines the list of specific tests that should be executed. The format is &lt;testname1<gt;, [ &lt;testname1<gt; etc ], Where 
	 * a test name may be absolute, or relative to the test directory. If not set, all the tests in the test directory will be included.
	 * 
	 * @parameter expression="${ta.test.suite}"
	 * 
	 */
	private String testSuiteComposition;
	
	private void init(){
		
		if (repositoriesDirectory==null){
			repositoriesDirectory = new File(mainDirectory, "repositories");
		}
		
		if (targetsDirectory==null){
			targetsDirectory=new File(mainDirectory, "targets");
		}
		
		if (resourcesDirectory==null){
			resourcesDirectory=new File(mainDirectory, "resources");
		}
		
		if (testsDirectory==null){
			testsDirectory=new File(mainDirectory, "tests");
		}
		
		if (shortcutsDirectory==null){
			shortcutsDirectory=new File(mainDirectory, "shortcuts");
		}
		
		if (getOutputDirectory()==null){
			setOutputDirectory(new File(buildDirectory, "squashTA"));
		}
		
		if (getLogger().isDebugEnabled()){
			getLogger().debug("Using effective configuration : \n" +
				"\tmainDirectory : "+mainDirectory.getPath()+"\n"+
				"\trepositoriesDirectory : "+repositoriesDirectory.getPath()+"\n"+
				"\ttargetsDirectory : "+targetsDirectory.getPath()+"\n"+
				"\tresourcesDirectory : "+resourcesDirectory.getPath()+"\n"+
				"\ttestsDirectory : "+testsDirectory.getPath()+"\n"+
				"\tshortcutsDirectory : "+shortcutsDirectory.getPath()+"\n"+
				"\toutputDirectory : "+getOutputDirectory().getPath()+"\n"
			);
		}
	}

	public SuiteResult executeImpl() throws MojoExecutionException, MojoFailureException {
		
		init();

		resetOutputDirectory();        
 
		
        //***************** test suite generation and outer context preparation*
		
        logPhase("compiling tests...");        
        
        TestSuiteDescription description = createSuiteDescription();
      
        TestSuite suite = buildTestSuite(description);
        
        TestWorkspaceBrowser browser = makeWorkspaceBrowser();
      
        
        
        //***************** engine init

        logPhase("initializing context...");
      
        EngineLoader loader = new EngineLoader();
        
        Engine engine = loader.load();
     
        applyConfigurers(engine);
        
        //***************** execution
        
        logPhase("testing...");
 
        SuiteResult results = engine.execute(suite, browser);
        
        cleanMacroTempDirectory();
        
        return results;
	}
	
	private void cleanMacroTempDirectory() {
		FileTree fileTree = new FileTree();
        try {
			fileTree.clean(macroTempDirectory);
		} catch (IOException e) {
			if(getLogger().isDebugEnabled())
			{
				getLogger().warn("Could not clean macro temporary directory : "+macroTempDirectory.getPath());
			}
		}
	}

	private TestSuiteDescription createSuiteDescription()
	{
		FilenameFilter filter=new FileFilterFactory().createFilter(testsDirectory, testSuiteComposition);
		return new FileSystemTestSuiteDefinition(testsDirectory, filter);
	}
	
	
	private TestWorkspaceBrowser makeWorkspaceBrowser(){
        
        DefaultTestProjectWorkspaceBrowser browser = new DefaultTestProjectWorkspaceBrowser(mainDirectory);
        
        browser.setRepositoriesDirectory(repositoriesDirectory);
        
        browser.setResourcesDirectory(resourcesDirectory);
        
        browser.setTargetsDirectory(targetsDirectory);
        
        return browser;
	}
	
	
	private TestSuite buildTestSuite( TestSuiteDescription description){
		  
        // **** first, a roundtrip through the macro processor
        
        MacroDSLProcessor macroProcessor = new MacroDSLProcessor();
        macroProcessor.addMacrosFromClasspath(BUILT_IN_MACROS);
        
    	try {
    		if ( (shortcutsDirectory!=null) &&
    			 (shortcutsDirectory.exists())){
    			macroProcessor.addMacrosFromURL(shortcutsDirectory.toURI().toURL());
    		}else{
    			logPhase("could not find the shortcuts directory in the test project, skipping");
    		}
		} 
    	catch (MalformedURLException e) {
			if (getLogger().isErrorEnabled()){
				getLogger().error("Squash TA : shortcuts directory '"+shortcutsDirectory.getPath()+"' could not be translated in a valid url, skipping", e);
			}
		}
       
        TestSuiteDescription processed = macroProcessor.process(description);
        macroTempDirectory = macroProcessor.getMacroTempDirectory();
        
        // **** second, generate the actual TestSuite
        
        DSLTestSuiteFactory testFactory = new DSLTestSuiteFactory();
        return testFactory.buildTestSuite(processed);
	}
	
	public void setMainDirectory(File mainDirectory) {
		this.mainDirectory = mainDirectory;
	}

	public void setRepositoriesDirectory(File repositoriesDirectory) {
		this.repositoriesDirectory = repositoriesDirectory;
	}

	public void setTargetsDirectory(File targetsDirectory) {
		this.targetsDirectory = targetsDirectory;
	}

	public void setResourcesDirectory(File resourcesDirectory) {
		this.resourcesDirectory = resourcesDirectory;
	}

	public void setTestsDirectory(File testsDirectory) {
		this.testsDirectory = testsDirectory;
	}

	public void setBuildDirectory(File buildDirectory) {
		this.buildDirectory = buildDirectory;
	}
	
}
