@echo off

:: Positionnement des variables d'environnement et du path
call env.cmd

::Démarrage d'eclipse
%SQUASH_TA_DRIVE%
cd %ECLIPSE_HOME%
start eclipse.exe -vm "%JAVA_HOME%\bin"