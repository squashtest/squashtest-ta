/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.sut.web.servlets.ws.impl;

import java.util.ArrayList;
import java.util.List;

import org.squashtest.ta.sut.web.servlets.ws.IssueService;
import org.squashtest.ta.sut.web.servlets.ws.NoSuchIssueException;
import org.squashtest.ta.sut.web.servlets.ws.dto.Issue;

public class IssueServiceImpl implements IssueService {
	private static int counter;
	private static final List<Issue> ISSUES = new ArrayList<Issue>();
	static {
		ISSUES.add(createIssue(
				"The engine does not start when I turn the ingition key.",
				"Doesn't start"));
		ISSUES.add(createIssue("The engine dies when stopped at a light.",
				"Engine dies."));
		ISSUES.add(createIssue(
				"The dashboard creaks for every small bump in the road.",
				"Board creaks."));
		ISSUES.add(createIssue("The front lights barely show 5 meters ahead!",
				"Front lights weak."));
		ISSUES.add(createIssue(
				"Every signel day a front light, brake light or blinker bulb burns out.",
				"Burn a bulb a day."));
		ISSUES.add(createIssue("Exhaust fumes just stink!!", "Stinks."));
	}

	protected static Issue createIssue(String description, String summary) {
		Issue issue = new Issue();
		issue.setDescription(description);
		issue.setSummary(summary);
		issue.setId(counter++);
		return issue;
	}

	@Override
	public boolean issueExists(Integer issueId) {
		if (issueId == null) {
			throw new IllegalArgumentException("Id is mandatory");
		}
		int id = issueId.intValue();
		return id < ISSUES.size() && id > -1;
	}

	@Override
	public Issue retrieveIssue(Integer issueId) throws NoSuchIssueException {
		if(!issueExists(issueId)){
			throw new NoSuchIssueException(issueId);
		}
		int id=issueId.intValue();
		return ISSUES.get(id);
	}

	@Override
	public Integer getLatestProjectIssue(Integer projectId) {
		if(projectId==null){
				throw new IllegalArgumentException("project Id is mandatory");
		}
		if(projectId.intValue()==0){
			return 4;
		}else{
			return null;
		}
	}

}
