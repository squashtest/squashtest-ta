/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.dbunit.DatabaseUnitException;
import org.dbunit.assertion.FailureHandler;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.SortedTable;
import org.squashtest.ta.commons.helpers.DiffReportBuilder;
import org.squashtest.ta.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.commons.library.dbunit.PPKFilter;
import org.squashtest.ta.commons.library.dbunit.assertion.DifferenceExtension;
import org.squashtest.ta.commons.library.dbunit.assertion.FailureHandlerExtension;
import org.squashtest.ta.commons.library.dbunit.helper.LowerCasedTable;
import org.squashtest.ta.commons.resources.DbUnitDatasetResource;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

/**
 * Binary assertion that checks that effective dataset contains expected
 * dataset.
 * 
 * @author edegenetais
 * 
 */
@EngineComponent("contain")
public class DbUnitDatasetContains extends AbstractDbUnitDatasetCompare implements
		BinaryAssertion<DbUnitDatasetResource, DbUnitDatasetResource> {

	private static final String DIFF_RESOURCE_NAME = "diff";

	private static final Column[] NO_PK_VALUE = new Column[0];

	/**
	 * Accessible mainly for testability purpose. May be used to change
	 * reporting behavior.
	 */
	public class DiffReportBuilderFactory {
		public DiffReportBuilder newInstance() {
			return new DiffReportBuilder();
		}
	}

	@Override
	protected void compare(IDataSet pExpected, IDataSet pActual) {
		try {
			String[] tableNames = pExpected.getTableNames();

			// Handler which will eventually contain the differences found
			// between
			// Expected dataset and Actual dataset
			FailureHandlerExtension myHandler = new FailureHandlerExtension();
			
			// Comparison is made between sorted tables
			for (int i = 0; i < tableNames.length; i++) {
				performTableCompare(tableNames[i], pExpected, pActual, myHandler);
			}

			if (myHandler.getSize() > 0) {
				throwAssertionFailure(myHandler.getMap());
			}
		} catch (DataSetException bde) {
			throw new BadDataException("Dataset comparison threw dbunit error",
					bde);
		} catch (DatabaseUnitException e) {
			throw new BadDataException("Dataset comparison threw dbunit error",
					e);
		}
	}

	private void performTableCompare(String currentTableName, IDataSet pExpected, IDataSet pActual, FailureHandler myHandler ) throws DatabaseUnitException{
		ITable expTable = new LowerCasedTable(pExpected.getTable(currentTableName));
		ITable actTable = new LowerCasedTable(pActual.getTable(currentTableName));

		Column[] primaryKeys = extractPrimaryKeys(pExpected.getTableMetaData(currentTableName), pActual.getTableMetaData(currentTableName));
		List<String> primaryKeysName = getColumnName(primaryKeys);
		if(!primaryKeys.equals(NO_PK_VALUE))
		{
			SortedTable expSortedTable = new SortedTable(expTable, primaryKeys);
			SortedTable actSortedTable = new SortedTable(actTable, primaryKeys);
			assertConnector.assertContains(expSortedTable, actSortedTable,myHandler,primaryKeysName);
		}else{
			assertConnector.assertContains(expTable, actTable,myHandler,primaryKeysName);
		}
		
	}

	private List<String> getColumnName(Column[] columnArray) {
		List<String> listColumnName =  new ArrayList<String>();
		for (Column column : columnArray) {
			listColumnName.add(column.getColumnName());
		}
		return listColumnName;
	}

	private void throwAssertionFailure(Map<String, List<DifferenceExtension>> map) {
		List<ResourceAndContext> context = new ArrayList<ResourceAndContext>();

		try {
			
			// The use of the DiffReportBuilder is temporary bypass. 
			// Restore it's use would be a good idea
			// DiffReportBuilder builder = diffReportBuilderFactory.newInstance();
			
			StringBuilder builder = new StringBuilder();
			for (String tableName : map.keySet()) {
				List<DifferenceExtension> diffList = map.get(tableName);
				ITable table = diffList.get(0).getExpectedTable();
				ITableMetaData metadata = table.getTableMetaData();
				Column[] colList = metadata.getColumns();
				int fullSize = table.getRowCount();
				
				builder.append("For Table: ");
				builder.append(tableName);
				builder.append(", ");
				builder.append(diffList.size());
				builder.append(" line(s) (on ");
				builder.append(fullSize);
				builder.append(") was(were) not found.\n");
				builder.append("Not found lines are :\n");
				String tab;
				for (DifferenceExtension diff : diffList) {
					tab ="\t";
					rowBuilder(builder, tab, table, colList, diff.getRowIndex());
					tab = "\t\t";
					DifferenceExtension.PotentialMatchStatus status = diff.getPotentialMatchstatus();
					switch (status) {
					case NO_MATCH:
						builder.append("\t\t -> No match found in the actual dataset by reducing the search to primary key column(s) \n");
						builder.append("\t\t ( Used primary key(s) is(are): ");
						builder.append(diff.getPrimaryKeys().toString());
						builder.append(" )\n");
						break;
						
					case ONE_MATCH:
						builder.append("\t\t -> One match was found in the actual dataset by reducing the search to the primary key column(s) :\n");
						builder.append("\t\t ( Used primary key(s) is(are): ");
						builder.append(diff.getPrimaryKeys().toString());
						builder.append(" )\n");
						rowBuilder(builder, tab, diff.getActualTable(), colList, diff.getPossibleRowIndex());
						break;
						
					case MANY_MATCH:
						builder.append("\t\t -> Many match were found in the actual dataset by reducing the search to primary key column(s). The first match is :\n");
						builder.append("\t\t ( Used primary key(s) is(are): ");
						builder.append(diff.getPrimaryKeys().toString());
						builder.append(" )\n");
						rowBuilder(builder, tab, diff.getActualTable(), colList, diff.getPossibleRowIndex());
						break;

					default:
						// Disabled status
						builder.append("\t\t No primary key used\n");
						break;
					}
					
				}
				builder.append("\n");
				builder.append("\n");
			}

			File tempFile = File.createTempFile("binaryDataSet",
					".diff");
			tempFile.deleteOnExit();
			FileResource diff=new FileResource(tempFile);
			BinaryData diffData=new BinaryData(builder.toString().getBytes("UTF-8"));
			diffData.write(tempFile);
			ResourceAndContext diffContext=new ResourceAndContext();
			diffContext.resource=diff;
			diffContext.metadata=new ExecutionReportResourceMetadata(getClass(), new Properties(), FileResource.class, DIFF_RESOURCE_NAME);
			context.add(diffContext);
		} catch (IOException e) {
			logFailureReportingError(e);
		} catch (DataSetException e) {
			logFailureReportingError(e);
		}

		throw new BinaryAssertionFailedException(
				"Actual resultset did not contain expected dataset",
				expected, actual, context);
	}

	private void rowBuilder(StringBuilder builder, String tab , ITable table,
			Column[] colList, int rowIndex) throws DataSetException {
		builder.append(tab);
		builder.append(" *");
		for (Column column : colList) {
			String columnName = column.getColumnName(); 
			builder.append(" {");
			builder.append(columnName);
			builder.append("='");
			builder.append(table.getValue( rowIndex, columnName));
			builder.append("'}");
		}
		builder.append("\n");
	}
	
	private Column[] extractPrimaryKeys(ITableMetaData pExpected, ITableMetaData pActual) throws DataSetException {
		
		ITableMetaData tableMetaData = pExpected;
		String tableName = tableMetaData.getTableName();
		Column[] pkColumn = NO_PK_VALUE;
		if(pseudoPrimaryKeys!= null && pseudoPrimaryKeys.getFilter().hasPpk(tableName)){
			PPKFilter.ValidationResult validationResult = pseudoPrimaryKeys.getFilter().validPpkDefinition(tableMetaData);
			if(validationResult.isValid()){
				pkColumn = validationResult.getMatchingColumn();
			}else{
				throw new IllegalConfigurationException(validationResult.getNotFoundColumnAsString());
			}
		}
		else{
			pkColumn = tableMetaData.getPrimaryKeys();
			if ( pkColumn == null || pkColumn.length == 0 ) {
				List<String> expectedColumn = getColumnName(tableMetaData.getColumns()); 
				tableMetaData = pActual;
				List<Column> actualPrimaryKeys = new ArrayList<Column>();
				for (Column actualColumn : tableMetaData.getPrimaryKeys()) {
					if(expectedColumn.contains(actualColumn.getColumnName()))
					{
						actualPrimaryKeys.add(actualColumn);
					}
				}
				pkColumn = actualPrimaryKeys.toArray(new Column[actualPrimaryKeys.size()]);
			}
		}
		if(pkColumn == null)
		{
			pkColumn = NO_PK_VALUE;
		}
		return pkColumn;
	}

}
