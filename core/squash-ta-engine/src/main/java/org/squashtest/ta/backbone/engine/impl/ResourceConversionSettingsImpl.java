/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;


import org.squashtest.ta.backbone.engine.ResourceConversionSettings;

public class ResourceConversionSettingsImpl implements
		ResourceConversionSettings {
	
	private String originalNatureName;
	private String desiredNatureName;
	private String converterCategory;
	
	
	public String getOriginalNature(){
		return originalNatureName;
	}
	
	public void setOriginalNature(String name){
		this.originalNatureName=name;
	}
	
	public String getDesiredNature() {
		return desiredNatureName;
	}

	public void setDesiredNature(String resourceCreatorName) {
		this.desiredNatureName = resourceCreatorName;
	}
	
	public void setConverterCategory(String identifier){
		this.converterCategory=identifier;
	}
	
	public String getConverterCategory(){
		return converterCategory;
	}


}
