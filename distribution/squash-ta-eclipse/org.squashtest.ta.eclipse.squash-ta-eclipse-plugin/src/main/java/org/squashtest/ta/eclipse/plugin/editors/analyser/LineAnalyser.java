/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.editors.analyser;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.rules.ICharacterScanner;

public class LineAnalyser {

	private LineIdentifier line;
	private List<LineElement> lineElementList = new ArrayList<LineElement>();
	
	public LineAnalyseResult parse(ITextViewer sourceDoc, int initialOffset) throws BadLocationException {
		ICharacterScanner scanner = new AdapterCharacterScanner(initialOffset, sourceDoc);
		line = createLineIdentifier(sourceDoc.getDocument(), initialOffset);
		int lineBeginColumn = scanner.getColumn()- (initialOffset-line.getBeginOffset());
		
		int lineLength = line.getLength();
		// We shift to the beginning of the line
		shiftScannerToLineBeginning(scanner, lineBeginColumn);
		if(lineLength > 0){
			int index = 0;
			StringBuilder wordBuilder = new StringBuilder();
			char nextChar;
			while(index < lineLength){
				nextChar = (char)scanner.read();
				index++;
				if( !Util.isWhitespaceChar(nextChar) ){
					wordBuilder.append(nextChar);
				}else{
					if(wordBuilder.length()>0){
						addToElementList(scanner.getColumn()-1, wordBuilder);
						wordBuilder = new StringBuilder();
					}
				}
			}
			if(wordBuilder.length()>0){
				addToElementList(scanner.getColumn(), wordBuilder);
			}
		}	

		return new LineAnalyseResult(line, initialOffset, lineElementList);
		
	}
	
	private void addToElementList(int column, StringBuilder wordBuilder) {
		String tmp = wordBuilder.toString().toUpperCase();
		lineElementList.add(new LineElement(line.getBeginOffset()+column-tmp.length(), tmp));
	}

	private LineIdentifier createLineIdentifier(IDocument document, int initialOffset ) throws BadLocationException {
		int lineNumber = document.getLineOfOffset(initialOffset);
		IRegion region = document.getLineInformationOfOffset(initialOffset);
		int lineBeginOffset = region.getOffset();
		int lineLength = region.getLength();
		return new LineIdentifier(lineNumber, lineBeginOffset, lineLength);
	}

	private void shiftScannerToLineBeginning(ICharacterScanner scanner, int lineBeginColumn){
		while(scanner.getColumn() > lineBeginColumn){
			scanner.unread();
		}
	}
	
	
	
}
