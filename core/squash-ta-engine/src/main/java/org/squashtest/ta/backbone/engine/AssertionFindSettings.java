/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine;


/**
 * An AssertionFindSettings contains parameters that will help to find an assertion. At least the assertion identifier and the 
 * actual result name must be supplied. If no expectedResult is supplied, then it is assumed that we are looking for an
 * unary assertion.
 * 
 * @author bsiri
 */
public interface AssertionFindSettings {
	
	String getActualResultNatureName();
	String getExpectedResultNatureName();
	String getAssertionCategory();
	
	void setActualResultNatureName(String name);
	void setExpectedResultNatureName(String name);
	void setAssertionCategory(String name);
	
}
