/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.sahi.converters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.BundleComponent;
import org.squashtest.ta.commons.library.param.Expression;
import org.squashtest.ta.commons.library.param.ReplacementTokenizer;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.sahi.converters.FileToSahiConverter;
import org.squashtest.ta.sahi.library.APIExpressionParser;
import org.squashtest.ta.sahi.resources.SahiSuiteResource;

/**
 * Converter to mark a file as a sahi test suite, <strong>without validation</strong>.
 * @author edegenetais
 *
 */
@EngineComponent("script")
public class FileToSahiConverter extends BundleComponent implements
		ResourceConverter<FileResource, SahiSuiteResource> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileToSahiConverter.class);
	private static final String SQUASH_TA_EXTENSION_API_JS_FILENAME = ".squashTAExtension/api.js";
	private static final FileTree FILE_TREE = new FileTree();
	
	private List<Resource<?>> configuration=new ArrayList<Resource<?>>();
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	@Override
	public SahiSuiteResource convert(FileResource resource) {
		File file=resource.getFile();		
		try {
			File newBundleBase=FILE_TREE.createTempDirectory("sahi", "bundle");
			
			File mainFile = extractMainFileFromConfiguration(configuration, newBundleBase);
			
			mainFile = convertSahiSuiteFilesToSquashTaSahiSuiteFiles(resource,
					file, newBundleBase, mainFile);
			
			installSquashTASahiAPIinBundle(newBundleBase);
			return new SahiSuiteResource(newBundleBase,mainFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException("file to sahi script converte failed for an I/O error.", e);
		}
	}

	private void installSquashTASahiAPIinBundle(File newBundleBase)
			throws IOException {
		File apiScriptDir=new File(newBundleBase,SQUASH_TA_EXTENSION_API_JS_FILENAME).getParentFile();
		if(!apiScriptDir.exists()){
			boolean success=apiScriptDir.mkdirs();
			if(!success){
				throw new InstructionRuntimeException("Failed to create SAHI squashTA api js directory: "+apiScriptDir.getAbsolutePath());
			}
		}
		ReplacementTokenizer tokenizer=new ReplacementTokenizer(new APIExpressionParser(newBundleBase));
		List<Expression> apiScriptInstanceLines=tokenizer.tokenize(new SimpleLinesData(getClass().getResource("sahi_api_template.js")));
		StringBuilder apiScriptInstance=new StringBuilder();
		for(Expression expression:apiScriptInstanceLines){
			expression.evaluate(apiScriptInstance);
		}
		String apiScriptInstanceContent = apiScriptInstance.toString();
		BinaryData apiScript=new BinaryData(apiScriptInstanceContent.getBytes());
		LOGGER.debug("API script: "+apiScriptInstanceContent);
		apiScript.write(new File(newBundleBase,SQUASH_TA_EXTENSION_API_JS_FILENAME));
	}

	private File convertSahiSuiteFilesToSquashTaSahiSuiteFiles(
			FileResource resource, File file, File newBundleBase, File mainFile)
			throws IOException {
		if(file.isDirectory()){
			resource.dump(newBundleBase,true);
			List<File> fileList=FILE_TREE.enumerate(newBundleBase, EnumerationMode.FILES_ONLY);
			for(File candidate:fileList){
				if(candidate.getName().endsWith(".sah")){
					enrichScriptFile(candidate, newBundleBase,candidate);
				}
			}
		}else{
			File scriptFile = writeAndEnrichScript(file, newBundleBase);
			if(mainFile!=null && !mainFile.equals(scriptFile)){
				throw new IllegalConfigurationException("Useless mainpath parameter '"+mainFile.getAbsolutePath()+"' conflicts with single Sahi suite location '"+scriptFile+"'.");
			}else{//when there is only one file, no Nobel prize needed to know which is the main...
				mainFile = scriptFile;
			}
		}
		return mainFile;
	}

	private File writeAndEnrichScript(File file, File bundleBase)
			throws IOException {
		//compute main file path
		File mainFile=new File(bundleBase,file.getName());
		enrichScriptFile(file, bundleBase, mainFile);
		return mainFile;
	}

	private void enrichScriptFile(File originalFile, File bundleBase, File enrichedFile)
			throws IOException {
		//add the _include statement to inject the squashTA js API.
		SimpleLinesData script=new SimpleLinesData(originalFile.getPath());
		String element = "_include(\""+new File(bundleBase,SQUASH_TA_EXTENSION_API_JS_FILENAME).getAbsolutePath()+"\");\n";
		script.getLines().add(0, element);
		script.write(enrichedFile);
	}

	@Override
	public void cleanUp() {
		//noop, GC should be enough.
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

}
