/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.commands

import org.squashtest.ta.commons.library.shell.ShellResult;
import org.squashtest.ta.commons.library.ssh.SquashSSHClient;
import org.squashtest.ta.commons.resources.CommandLineResource;
import org.squashtest.ta.commons.resources.ShellResultResource;
import org.squashtest.ta.commons.targets.SSHTarget;
import org.squashtest.ta.core.tools.io.SimpleLinesData;

import spock.lang.Specification

class ExecuteSSHCommandTest extends Specification {
	ExecuteSSHCommand testee
	
	def setup(){
		testee=new ExecuteSSHCommand()
	}
	
	def "should execute said command"(){
		given:
			SimpleLinesData commandLine=new SimpleLinesData(["ls -la /home/tester"])
			File cf=File.createTempFile("cmd", ".txt")
			commandLine.write(cf)
			cf.deleteOnExit()
		and:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			SSHTarget target=Mock()
			testee.setTarget(target)
		and:
			SquashSSHClient client=Mock()
		when:
			testee.apply()
		then:
			1 * target.getClient()>>client
			1 * client.runSSHCommand("ls -la /home/tester",_)
		cleanup:
			cf.delete()
	}
	
	def "should transmit 0 exitValue"(){
		given:
			SimpleLinesData commandLine=new SimpleLinesData(["ls -la /home/tester"])
			File cf=File.createTempFile("cmd", ".txt")
			commandLine.write(cf)
			cf.deleteOnExit()
		and:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			SquashSSHClient client=Mock()
			client.runSSHCommand(_,_)>> new ShellResult(0, "", "","ls -la /home/tester")
		and:
			SSHTarget target=Mock()
			testee.setTarget(target)
			target.getClient()>>client
		when:
			ShellResultResource result=testee.apply()
		then:
			result.getResult().getExitValue()==0
		cleanup:
			cf.delete()
	}
	
	def "should transmit non-zero exitValue"(){
		given:
			SimpleLinesData commandLine=new SimpleLinesData(["ls -la /home/tester"])
			File cf=File.createTempFile("cmd", ".txt")
			commandLine.write(cf)
			cf.deleteOnExit()
		and:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			SquashSSHClient client=Mock()
			client.runSSHCommand(_,_)>> new ShellResult(-5, "", "","ls -la /home/tester")
		and:
			SSHTarget target=Mock()
			testee.setTarget(target)
			target.getClient()>>client
		when:
			ShellResultResource result=testee.apply()
		then:
			result.getResult().getExitValue()==-5
		cleanup:
			cf.delete()
	}
	
	def "should transmit stdout"(){
		given:
			SimpleLinesData commandLine=new SimpleLinesData(["ls -la /home/tester"])
			File cf=File.createTempFile("cmd", ".txt")
			commandLine.write(cf)
			cf.deleteOnExit()
		and:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			SquashSSHClient client=Mock()
			client.runSSHCommand(_,_)>> new ShellResult(0, "gotcha!", "","ls -la /home/tester")
		and:
			SSHTarget target=Mock()
			testee.setTarget(target)
			target.getClient()>>client
		when:
			ShellResultResource result=testee.apply()
		then:
			"gotcha!".equals(result.getResult().getStdout())
		cleanup:
			cf.delete()
	}
	
	def "should transmit stderr"(){
		given:
			SimpleLinesData commandLine=new SimpleLinesData(["ls -la /home/tester"])
			File cf=File.createTempFile("cmd", ".txt")
			commandLine.write(cf)
			cf.deleteOnExit()
		and:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			SquashSSHClient client=Mock()
			client.runSSHCommand(_,_)>> new ShellResult(0, "", "Come get some!","ls -la /home/tester")
		and:
			SSHTarget target=Mock()
			testee.setTarget(target)
			target.getClient()>>client
		when:
			ShellResultResource result=testee.apply()
		then:
			"Come get some!".equals(result.getResult().getStderr())
		cleanup:
			cf.delete()
	}
	
	def "should transmit timeout spec"(){
		given:
			SimpleLinesData commandLine=new SimpleLinesData(["ls -la /home/tester"])
			File cf=File.createTempFile("cmd", ".txt")
			commandLine.write(cf)
			cf.deleteOnExit()
		and:
			CommandLineResource cl=new CommandLineResource(cf,1500)
			testee.setResource(cl)
		and:
			SquashSSHClient client=Mock()
		and:
			SSHTarget target=Mock()
			testee.setTarget(target)
			target.getClient()>>client
		when:
			ShellResultResource result=testee.apply()
		then:
			1 * client.runSSHCommand(_,1500)>> new ShellResult(0, "", "Come get some!","ls -la /home/tester")
		cleanup:
			cf.delete()
	}
}
