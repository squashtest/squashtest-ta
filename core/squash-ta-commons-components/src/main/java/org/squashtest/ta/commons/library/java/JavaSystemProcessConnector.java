/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Connector class to execute separated java processes.
 * @author edegenetais
 *
 */
public class JavaSystemProcessConnector extends ProcessConnector {
	private Class<?>contextClass;
	/**
	 * This default constructor creates a process connector and sets it up to use the thread context class loader.
	 */
	public JavaSystemProcessConnector() {
		this.contextClass=null;
	}

	/**
	 * This constructor creates a process connector and sets it up to look for
	 * jars from the class loader of the designated context class (may be
	 * useful if the class is in a particular class-loading context).
	 * 
	 * @param contextClass the class which context class loading space must be reproduced.
	 */
	public JavaSystemProcessConnector(Class<?> contextClass){
		this.contextClass=contextClass;
	}
	
	/**
	 * Create command for a java main class launch process.
	 * @param arguments program argument.
	 * @param mainClassName main class to launch.
	 * @return The list of commands for process building.
	 */
	public List<String> createCommand(List<String>arguments, String mainClassName){
		List<String> commandLine=new ArrayList<String>(4+arguments.size());
		commandLine.add("java");
		List<File>jarClassPath;
		try {
			if (contextClass == null) {
				jarClassPath = computeJarClassPath();
			} else {
				jarClassPath = computeJarClassPath(contextClass
						.getClassLoader());
			}
		} catch (IOException e) {
			throw new JavaClassPathException(
					"Java subprocess classpath propagation failed", e);
		} catch (URISyntaxException e) {
			throw new JavaClassPathException(
					"Java subprocess classpath propagation failed", e);
		}
		StringBuilder cpBuilder=new StringBuilder();
		for(File jar:jarClassPath){
			cpBuilder.append(jar.getAbsolutePath()).append(File.pathSeparatorChar);
		}
		if(cpBuilder.length()>0){
			cpBuilder.insert(0, "\"");
			cpBuilder.setCharAt(cpBuilder.length()-1, '"');
			commandLine.add("-cp");
			commandLine.add(cpBuilder.toString());
		}
		commandLine.add(mainClassName);
		commandLine.addAll(arguments);
		return commandLine;
	}
}
