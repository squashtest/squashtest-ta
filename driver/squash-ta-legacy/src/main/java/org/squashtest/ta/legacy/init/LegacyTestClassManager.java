/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.init;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;
import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.legacy.exception.LegacyTestRunnerInitializationException;

/**
 * This classes loads all cats test classes and transmits the set to its clients.
 * @author edegenetais
 *
 */
@Component
public class LegacyTestClassManager implements BeanFactoryPostProcessor{
	private Set<Class<AbstractCatsTestCase>> testSet=new HashSet<Class<AbstractCatsTestCase>>();
	@Override
	@SuppressWarnings("unchecked")
	public void postProcessBeanFactory(
			ConfigurableListableBeanFactory beanFactory) throws BeansException {
		String[] beanDefNameTable=beanFactory.getBeanDefinitionNames();
		for(String defName:beanDefNameTable){
			BeanDefinition definition=beanFactory.getBeanDefinition(defName);
			String beanClassName = definition.getBeanClassName();
			try {
				Class<?> beanClass=beanFactory.getBeanClassLoader().loadClass(beanClassName);
				if(AbstractCatsTestCase.class.isAssignableFrom(beanClass)){
					//we only want to register test classes. Others are left alone
					definition.setScope(BeanDefinition.SCOPE_PROTOTYPE);
					testSet.add((Class<AbstractCatsTestCase>)beanClass);
				}
			} catch (ClassNotFoundException e) {
				throw new LegacyTestRunnerInitializationException("Error while loading detected CATS test class "+beanClassName, e);
			}
		}
	}
	
	public Set<Class<AbstractCatsTestCase>> getTestSet(){
		return testSet;
	}
}
