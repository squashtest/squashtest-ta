/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine;

/**
 * This interface describes how a given resource should be converted.
 * 
 * @author bsiri
 *
 */
public interface ResourceConversionSettings {
	
	public void setOriginalNature(String name);
	
	public String getOriginalNature();
	
	/**
	 * 
	 * @param name
	 */
	public void setDesiredNature(String name); 
	
	/**
	 * may return null;
	 * 
	 * @return
	 */
	public String getDesiredNature();
	
	
	/**
	 * The name of the converter we are looking for. Optional, and may return null. 
	 * 
	 * @return
	 */
	public String getConverterCategory();
	
	public void setConverterCategory (String category);

	
}
