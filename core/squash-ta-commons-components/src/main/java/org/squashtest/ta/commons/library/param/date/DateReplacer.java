/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.param.date;

import java.util.ArrayList;
import java.util.List;

import org.squashtest.ta.commons.library.param.EntityExpressionParser;
import org.squashtest.ta.commons.library.param.Expression;
import org.squashtest.ta.commons.library.param.ExpressionParser;
import org.squashtest.ta.commons.library.param.ReplacementTokenizer;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.SimpleLinesData;

/**
 * This class implements the top level date computation and replacement
 * algorithm.
 * 
 * @author edegenetais
 * 
 */
public class DateReplacer {
	private ReplacementTokenizer tokenizer;

	public DateReplacer() {
		
		List<ExpressionParser> parsers=new ArrayList<ExpressionParser>();
		
		parsers.add(new DateExpressionParser());
		parsers.add(new EntityExpressionParser());
		
		tokenizer = new ReplacementTokenizer(new DateExpressionParser());
	}
	
	public BinaryData apply(BinaryData originalData){
		byte[] content = originalData.toByteArray();
		byte[] output = applyInternal(content);
		return new BinaryData(output);
	}
	
	private byte[] applyInternal(byte[] content) {
		SimpleLinesData lineData=new SimpleLinesData(content);
		List<Expression> substituedStructure=tokenizer.tokenize(lineData);
		StringBuilder builder=new StringBuilder();
		for(Expression ex:substituedStructure){
			ex.evaluate(builder);
		}
		return builder.toString().getBytes();
	}
}
