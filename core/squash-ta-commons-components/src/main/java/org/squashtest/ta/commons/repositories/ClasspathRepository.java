/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.repositories;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;

@ResourceType("classpath")
public class ClasspathRepository implements ResourceRepository {

	private Logger logger = LoggerFactory.getLogger(ClasspathRepository.class);
	
	private Properties effectiveConfiguration;
	
	public ClasspathRepository() {}
	
	public ClasspathRepository(Properties properties){
		super();
		effectiveConfiguration = properties;
	}

	@Override
	public Properties getConfiguration() {
		return effectiveConfiguration;
	}

	@Override
	public void init() {
		// nothing to do
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void cleanup() {
		// nothing to do
	}

	@Override
	public FileResource findResources(String resourcePath) {
		URL url = this.getClass().getClassLoader()
					  .getResource(resourcePath);
		
		if (url!=null){
			try{
				File file = new File(url.toURI());
				
				FileResource resource = new FileResource(file);
				
				logger.info("ClasspathRepository : loaded resource '"+resourcePath+"'.");
				return resource;
				
			}catch(URISyntaxException ex){
				logger.warn("ClasspathRepository : resource path '"+resourcePath+"' was found at URL '"+url+"' that could not be translated to a well formed URI. Loading failure ensued.");
				MissingResourceException reported=new MissingResourceException("URL cannot be interpreted as URI","ClasspathRepository:repository",resourcePath);
				reported.initCause(ex);
				throw reported;
			}
		}else{
			return null;	//legal by contract.
		}
	}



}
