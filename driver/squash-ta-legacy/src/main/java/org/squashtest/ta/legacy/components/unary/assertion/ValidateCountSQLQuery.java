/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.components.unary.assertion;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.squashtest.ta.commons.resources.SQLQuery;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

import java.util.regex.Pattern;

/**
 * Definition for an "SQL assertion statement" i.e. a statement looking like : SELECT COUNT(*) AS actual, ... AS
 * expected, ... AS description FROM ...
 * 
 * @author fgaillard copied from gf
 * 
 */
@EngineComponent("sql.query")
public class ValidateCountSQLQuery implements UnaryAssertion<SQLQuery>{
	
	private static final Pattern VALID_SELECT_PATTERN = Pattern.compile(
			"^select count ?\\(\\*\\) as actual ?\\, ?\\d+ as expected ?\\, ?'.*' as description.*",
			Pattern.CASE_INSENSITIVE);
	private SQLQuery query;
	
	@Override
	public void setActualResult(SQLQuery actual) {
		this.query = actual;	
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		// TODO Auto-generated method stub
	}

	@Override
	public void test() throws AssertionFailedException {
		String[] subqueries = query.getQuery().split(" (?:union|UNION) (?:all|ALL) ");

		for (String subquery : subqueries) {
			if (!VALID_SELECT_PATTERN.matcher(subquery).matches()) {
				List<ResourceAndContext> context=Collections.emptyList();
				throw new AssertionFailedException(subquery,null,context);
			}
		}
	}

}
