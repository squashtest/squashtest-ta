/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.library;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.ExceptionLogger;
import org.squashtest.ta.core.tools.PropertiesUtils;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

public class SeleniumProxyPoller {
	
	private static final PropertiesUtils PROPERTIES_UTILS = new PropertiesUtils();

	private static final String KEY_PROBE_INTERVAL = "squashtest.ta.selenium.probe.interval";

	private static final String KEY_PROBE_ATTEMPTS = "squashtest.ta.selenium.probe.attempts";

	private static final Logger LOGGER = LoggerFactory.getLogger(SeleniumProxyPoller.class);

	private static final ExceptionLogger IRE_LOGGER = new ExceptionLogger(LOGGER, InstructionRuntimeException.class);

	private Integer serverPort;
	
	
	private int attempts = 0;
	private int retry;
	private int sleeptime = 1000;
	
	

	private boolean isServerBooted;
	private Exception exception;

	
	public SeleniumProxyPoller(Properties serverProperties) {
		
		String readPort = serverProperties.getProperty(SeleniumServerManager.SELENIUM_PORT_KEY);
		if (readPort == null) {
			serverPort = 4444;
		} else {
			serverPort = Integer.parseInt(readPort.trim());
		}

		retry=extractNumericValue(serverProperties, KEY_PROBE_ATTEMPTS, 3);
		
		sleeptime=extractNumericValue(serverProperties, KEY_PROBE_INTERVAL, 1000);
	}

	private int extractNumericValue(Properties serverProperties,
			String keyProbeAttempts, int defaultValue) {
		int value;
			try{
				Integer valueInt=PROPERTIES_UTILS.getIntegerValue(serverProperties, keyProbeAttempts);
				if(valueInt==null){
					value=defaultValue;
				}else{
					value=valueInt.intValue();
				}
			}catch(NumberFormatException e){
				IRE_LOGGER.simpleWarn("Bad value for "+keyProbeAttempts, e);
				value = defaultValue;
			}
		return value;
	}
	
	public void setSleepTime(int millis){
		sleeptime=millis;
	}
	
	
	/**
	 * kudos to http://vishnuagrawal.blogspot.fr/2012/04/selenium-check-if-selenium-server-is.html
	 * 
	 * @return true if the server is ready, false otherwise
	 * @throws IOException
	 */
	private boolean isSeleniumServerRunning() throws IOException{
		URL url = new URL("http://localhost:"+serverPort+"/selenium-server/driver/?cmd=testComplete");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		int response = connection.getResponseCode() ;
		return (response == HttpURLConnection.HTTP_OK);
	}
	
	
	private void reset(){
		attempts=0;
		isServerBooted=false;
		exception=null;
	}
	
	
	public void testProxy() {

		reset();
		
			while (!isServerBooted && attempts < retry) {
				try {		
					attempts++;
					LOGGER.debug("Selenium server : trying to reach selenium proxy, attemp #"+attempts+": "+new SimpleDateFormat("HH:mm:ss").format(new Date()));
					Thread.sleep(sleeptime);
					
					if (isSeleniumServerRunning()){
						isServerBooted=true;
						exception=null;//even if we had some exception before, we have now succeeded
					}
					} catch (InterruptedException e) {
						isServerBooted=false;
						retry++;//this is merely thread interaction, not probing error, so let's give peace a chance :)
					} catch (IOException e) {
						isServerBooted=false;
						exception=e;
					}
			}
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Server is "+(isServerBooted?"OK":"KO")+" after "+attempts+" probing attempts");
			}
	}
	
	public boolean isServerReady(){
		return isServerBooted;
	}
	
	public boolean timedOut(){
		return (attempts >= retry);
	}
	
	public boolean gotException(){
		return (exception!=null);
	}
	
	public Exception getException(){
		return exception;
	}

}

