/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.sahi.library;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import net.sf.sahi.ant.Report;
import net.sf.sahi.test.TestRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.sahi.resources.SahiSuiteResultResource;

/**
 * Port from CATS of the Sahi test suite runner. Used by the sahi components to run sahi test suites.
 * @author Agnes
 * @author edegenetais
 */
public class SquashSahiRunner {

    private static final String DEFAULT_SAHI_PROXY_PORT = "9999";

	private static final String DEFAULT_SAHI_PROXY_HOST = "localhost";

	private static final String DEFAULT_TARGET_URL_PROTOCOL = "default";

	private static final String BROWSER_TYPE = "browserType";

	private static final String HTML_REPORT = "html";

	private static final String ENDPOINT_URL = "endpoint.url";

	private static final String REPORT_FORMAT = "report.format";

	private static final String SAHI_THREAD_NB = "sahi.thread.nb";

	private static final String SAHI_PROXY_PORT = "sahi.proxy.port";

	private static final String SAHI_PROXY_HOST = "sahi.proxy.host";

	private final static Logger LOGGER = LoggerFactory.getLogger(SquashSahiRunner.class);

    private String sahiHost;

    private String sahiPort;

    private String threadNumber;

    private String reportFormat;

    private String baseUrl;

    private String browserType;

    /** Package accessible for testability purpose. */
    class TestRunnerFactory{
    	public TestRunner getRunner(String suitePath, String browserType, String targetUrl, String proxyHost, String proxyPort, String threadNumber){
    		return new TestRunner(suitePath, browserType, targetUrl, proxyHost, proxyPort, threadNumber);
    	}
    }
    private TestRunnerFactory runnerFactory=new TestRunnerFactory();
    
    public SquashSahiRunner(Properties config) {
    	sahiHost = config.getProperty(SAHI_PROXY_HOST,DEFAULT_SAHI_PROXY_HOST);
        sahiPort = config.getProperty(SAHI_PROXY_PORT,DEFAULT_SAHI_PROXY_PORT);
        threadNumber = config.getProperty(SAHI_THREAD_NB,"1");
        reportFormat = config.getProperty(REPORT_FORMAT, HTML_REPORT);
        browserType = config.getProperty(BROWSER_TYPE,"");
        baseUrl=config.getProperty(ENDPOINT_URL);
	}
    
    /**
     * Runs a Sahi suite against the specified browser, starting at the base
     * URL.
     * 
     * @param suiteName Name of the Sahi testsuite to run
     * @param browserType Browser type. Must refer to the &lt;name&gt; tag in
     *            Sahi browser configuration file
     *            (&lt;sahi_root&gt;/userdata/config/browsertypes.xml). If
     *            <code>null</code>, the value in sahi-config.properties will be
     *            used.
     * @throws URISyntaxException
     * @throws IOException
     * @throws InterruptedException
     */
    public SahiSuiteResultResource run(File suiteFile,URL targetUrl) throws IOException,
                    InterruptedException {

        if ("".equals(browserType)) {
                throw new IllegalConfigurationException("Browser type is missing.");
        }

        String targetUrlString;
        if (DEFAULT_TARGET_URL_PROTOCOL.equals(targetUrl.getProtocol())) {
			/*
			 * Manage the legacy case where a default URL is provided. Please
			 * note that this behavior is DEPRECATED, and that new tests should
			 * provide baseURL through the targetURL parameter (comes from a
			 * WebTarget instance at the squashTA test level)
			 */
            targetUrlString=baseUrl;
        } else {
        	targetUrlString=targetUrl.toExternalForm();
        	baseUrl=targetUrlString;
        }

        logSahiConf();

		TestRunner testRunner = runnerFactory.getRunner(
				suiteFile.getAbsolutePath(), browserType, targetUrlString,
				sahiHost, sahiPort, threadNumber);

        File reportFile=new FileTree().createTempDirectory("squash_sahi", "."+reportFormat);
        testRunner.addReport(new Report(reportFormat, reportFile.getCanonicalPath()));
        
        String statusString=testRunner.execute();
        GeneralStatus status="SUCCESS".equals(statusString)?GeneralStatus.SUCCESS:GeneralStatus.FAIL;
        return new SahiSuiteResultResource(status, reportFile);
    }

    /**
     * @param pSahiHost
     * @param pSahiPort
     * @param pThreadNumber
     * @param pReportDir
     * @param pScriptDir
     * @param pReportFormats
     * @param pBrowserType
     * @param pBaseUrl
     * @param pSuiteUrl
     */
    private void logSahiConf() {
        LOGGER.debug("Sahi proxy host = {}", sahiHost);
        LOGGER.debug("Sahi proxy port = {}", sahiPort);
        LOGGER.debug("Sahi thread number = {}", threadNumber);
        LOGGER.debug("Sahi report format(s) = {}", reportFormat);
        LOGGER.debug("Base URL of the webapp under testing = {}", baseUrl);
    }
}
