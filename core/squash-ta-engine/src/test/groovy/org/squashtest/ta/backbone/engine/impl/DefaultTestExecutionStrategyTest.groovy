/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl

import org.squashtest.ta.framework.test.result.GeneralStatus;

import spock.lang.Specification;

class DefaultTestExecutionStrategyTest extends Specification{
	def testee
	def setup(){
		testee=new DefaultTestExecutionStrategy()
	}
	
	def "should halt on error"(){
		when:
			def resultat=testee.continueExecution(GeneralStatus.ERROR)
		then:
			resultat==false;
	}
	
	def "should halt on failure"(){
		when:
			def resultat=testee.continueExecution(GeneralStatus.FAIL)
		then:
			resultat==false;
	}
	
	def "should continue on sucess"(){
		when:
			def resultat=testee.continueExecution(GeneralStatus.SUCCESS)
		then:
			resultat==true;
	}
	
	def "success + success = success"(){
		when:
			def resultat=testee.aggregate(GeneralStatus.SUCCESS,GeneralStatus.SUCCESS)
		then:
			resultat==GeneralStatus.SUCCESS;
	}
	
	def "success + error = error"(){
		when:
			def resultat=testee.aggregate(GeneralStatus.SUCCESS,GeneralStatus.ERROR)
		then:
			resultat==GeneralStatus.ERROR;
	}
	
	def "success + failure = failure"(){
		when:
			def resultat=testee.aggregate(GeneralStatus.SUCCESS,GeneralStatus.FAIL)
		then:
			resultat==GeneralStatus.FAIL;
	}

	def "should engage if previous successful"(){
		when:
			def resultat=testee.beginGroupExecution(GeneralStatus.SUCCESS)
		then:
			resultat==true;
	}
	
	def "should skip if previous failed"(){
		when:
			def resultat=testee.beginGroupExecution(GeneralStatus.NOT_RUN)
		then:
			resultat==false;
	}
}
