/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools;

import java.io.IOException;

/**
 * That class will eat characters and string, but will only remember the nth last character when asked to restitute the said string.
 * It is purely intended for saving memory, although it will not prevent some garbage collection.  
 * 
 * @author bsiri
 *
 */
public class AmnesicStringBuffer implements Appendable {

	private StringBuffer _buffer = new StringBuffer();
	
	private int maxSize=100;
	
	public AmnesicStringBuffer(){
		super();
	}
	
	public AmnesicStringBuffer(int maxSize){
		super();
		this.maxSize=maxSize;
	}
	
	public void setMaxSize(int maxSize){
		if (maxSize<0)
		{
			throw new IllegalArgumentException("the max size must be positive or zero");
		}
		this.maxSize = maxSize;
		trimToMaxSize();
	}
	
	
	@Override
	public Appendable append(CharSequence arg0) throws IOException {
		_buffer.append(arg0);		
		trimToMaxSize();
		return this;
	}

	@Override
	public Appendable append(char arg0) throws IOException {
		_buffer.append(arg0);
		trimToMaxSize();
		return this;
	}

	@Override
	public Appendable append(CharSequence arg0, int arg1, int arg2){
		_buffer.append(arg0, arg1, arg2);
		trimToMaxSize();
		return this;
	}
	
	
	private void trimToMaxSize(){
		int length = _buffer.length();
		if (length > maxSize){
			_buffer.delete(0, length-maxSize);
		}
	}
	
	public String toString(){
		return _buffer.toString();
	}

}
