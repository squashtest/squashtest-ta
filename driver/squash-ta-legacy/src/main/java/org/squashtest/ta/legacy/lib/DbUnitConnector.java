/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.lib;

import org.dbunit.operation.DatabaseOperation;
import org.squashtest.ta.legacy.exception.MalformedCatsClassException;

/**
 * Class to normalize DbUnit management.
 * @author edegenetais
 *
 */
public class DbUnitConnector{
	public String translateToCode(DatabaseOperation operation) {
		String mode;
		if(operation==DatabaseOperation.CLEAN_INSERT){
			mode="CLEAN_INSERT";
		}else if(operation==DatabaseOperation.DELETE){
			mode="DELETE";
		}else if(operation==DatabaseOperation.DELETE_ALL){
			mode="DELETE_ALL";
		}else if(operation==DatabaseOperation.INSERT){
			mode="INSERT";
		}else if(operation==DatabaseOperation.REFRESH){
			mode="REFRESH";
		}else if(operation==DatabaseOperation.TRUNCATE_TABLE){
			mode="TRUNCATE_TABLE";
		}else if(operation==DatabaseOperation.UPDATE){
			mode="UPDATE";
		}else{
			throw new MalformedCatsClassException("Unsupporter dbunit operation "+operation);
		}
		return mode;
	}
}