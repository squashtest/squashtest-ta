/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.resources;

import java.util.Properties;

import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;

/**
 * This class holds the configuration data for a SeleniumServer instance.
 * @author edegenetais
 *
 */
@ResourceType("configuration.selenium")
public class SeleniumConfiguration implements Resource<SeleniumConfiguration> {
	/**
	 * Selenium proxy configuration.
	 */
	private Properties configuration;
	
	public SeleniumConfiguration() {}
	
	/**
	 * Full initialization constructor.
	 * @param configuration
	 */
	public SeleniumConfiguration(Properties configuration) {
		this.configuration=configuration;
	}
	
	@Override
	public SeleniumConfiguration copy() {
		return new SeleniumConfiguration(configuration);
	}

	@Override
	public void cleanUp() {
		//noop: pure memory resources.
	}

	/**
	 * Get a copy of the configuration data.
	 * @return
	 */
	public Properties getData(){
		return new Properties(configuration);
	}
	
}
