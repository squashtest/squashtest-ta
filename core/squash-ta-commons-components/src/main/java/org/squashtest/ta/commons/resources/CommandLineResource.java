/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.resources;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * This resource holds a single shell command to be executed.
 * @author edegenetais
 *
 */
@ResourceType("query.shell")
public class CommandLineResource implements Resource<CommandLineResource> {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineResource.class);
	private File commandFile;
	private Integer timeout;
	
	/** Noarg constructor for Spring enumeration. */
	public CommandLineResource() {}
	
	/**
	 * Full initialization constructor.
	 * @param commandFile
	 */
	public CommandLineResource(File commandFile,Integer timeout) {
		this.commandFile = commandFile;
		this.timeout=timeout;
	}

	@Override
	public CommandLineResource copy() {
		return new CommandLineResource(commandFile,timeout);
	}

	/**
	 * Get the command line content.
	 * @return the content of the command line.
	 */
	public String getCommand(){
		try {
			SimpleLinesData content=new SimpleLinesData(commandFile.getAbsolutePath());
			String command;
			if(content.getLines().isEmpty()){
				throw new BadDataException("Empty command file at "+commandFile.getAbsolutePath());
			}else{
				command=null;
				for(String line:content.getLines()){
					LOGGER.debug("Command file line: "+line);
					if(!line.startsWith("#")){
						if(command==null){
							command=line;
						}else if(LOGGER.isWarnEnabled()){
							LOGGER.warn("Ignored excess line '"+line+"' in command file "+commandFile.getAbsolutePath());
						}
					}
				}
			}
			LOGGER.debug("Command: "+command);
			return command;
		} catch (IOException e) {
			throw new InstructionRuntimeException("Command content fetching failed.", e);
		}
	}
	
	@Override
	public void cleanUp() {
		//noop: this file is managed by the initial resource from which it was taken.
	}

	/**
	 * @return the configured timeout for this commandline.
	 */
	public Integer getTimeout() {
		return timeout;
	}
}
