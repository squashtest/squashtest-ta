/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.init;

import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 * Exception thrown by the legacy driver package when something goes amiss during initialization.
 * @author edegenetais
 *
 */
@SuppressWarnings("serial")
public class LegacyTestRunnerInitializationException extends BrokenTestException {

	/**
	 * Constructor with message and cause initialization.
	 * @param message
	 * @param cause
	 */
	public LegacyTestRunnerInitializationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor with message initialization only.
	 * @param message
	 */
	public LegacyTestRunnerInitializationException(String message) {
		super(message);
	}

	/**
	 * Constructor with cause initialization only.
	 * @param cause
	 */
	public LegacyTestRunnerInitializationException(Throwable cause) {
		super(cause);
	}

}
