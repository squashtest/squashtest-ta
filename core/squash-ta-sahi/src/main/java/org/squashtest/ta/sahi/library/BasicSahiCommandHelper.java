/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.sahi.library;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.resources.PropertiesResource;
import org.squashtest.ta.commons.targets.WebTarget;
import org.squashtest.ta.core.library.properties.PropertiesKeySet;
import org.squashtest.ta.core.tools.LoggerPrintWriter;
import org.squashtest.ta.core.tools.ReportBuilderUtils;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.sahi.resources.SahiSuiteResultResource;

public class BasicSahiCommandHelper {

	private static final Logger LOGGER=LoggerFactory.getLogger(BasicSahiCommandHelper.class);
	
	private WebTarget endPoint;
	
	private File sahiSuite;
	
	
	private Properties configuration=new Properties();
	
	private ReportBuilderUtils reportBuilderUtils=new ReportBuilderUtils();
	
	class SahiRunnerFactory{
		public SquashSahiRunner getRunner(Properties configuration){
			return new SquashSahiRunner(configuration);
		}
	}
	
	/** Package accessible for testability purpose. */
	private SahiRunnerFactory runnerFactory=new SahiRunnerFactory();
	
	/** Noarg constructor for Spring enumeration. */
	public BasicSahiCommandHelper(){}
	
	
	
	/**
	 * This command expects sahi configuration from one or more resources
	 * injected as configuration. Each configuration elements can be
	 * either a {@link FileResource} pointing on properties files, or a
	 * {@link PropertiesResource}. Default values are taken for any parameter
	 * except browserType, which MUST be injected.
	 */
	
	
	public void readPropertiesFrom(Resource<?> configResource) {
		if(configResource instanceof FileResource){
			FileResource configFile=(FileResource)configResource;
			Properties config=new Properties();
			try {
				config.load(configFile.openStream());
				extractConfiguration(config);
			} catch (IOException e) {
				throw new IllegalConfigurationException("Sahi execute command configuration loading failed.", e);
			}
		}else if(configResource instanceof PropertiesResource){
			Properties config = ((PropertiesResource) configResource)
					.getProperties();
			extractConfiguration(config);
		}
	}
	
	

	private void extractConfiguration(Properties config) {
		PropertiesKeySet newKeySet=new PropertiesKeySet(config);
		Set<String>redundantKeys=newKeySet.getCommonKeys(configuration);
		if(!redundantKeys.isEmpty()){
			StringBuilder messageBuilder=new StringBuilder("Conflict: the following properties are defined twice in your configuration:");
			reportBuilderUtils.appendCollectionContentString(messageBuilder, redundantKeys);
			throw new IllegalConfigurationException(messageBuilder.toString());
		}else{
			if(LOGGER.isDebugEnabled()){
				StringBuilder messageBuilder=new StringBuilder("Adding the following configuration keys:");
				for(String key:newKeySet){
					messageBuilder.append(key);
					messageBuilder.append("=");
					messageBuilder.append(config.getProperty(key));
					messageBuilder.append("\n");
				}
				LOGGER.debug(messageBuilder.toString());
			}
			configuration.putAll(config);
		}
	}

	public void setTarget(WebTarget target) {
		this.endPoint=target;
	}

	public void setSuite(File file) {
		this.sahiSuite = file;
	}

	public SahiSuiteResultResource apply() {
		SquashSahiRunner runner=runnerFactory.getRunner(configuration);
		
		PrintStream sysout=System.out;//Sahi's a tough guy ==> we need to a bit of coercion here!
		try {
			System.setOut(new LoggerPrintWriter(runner.getClass().getName()));
		} catch (FileNotFoundException e1) {
			LOGGER.warn("Could not redirect system.out",e1);
			System.setOut(sysout);
		}
		
		try{
			File sahiSuiteFile = sahiSuite.getCanonicalFile();
			return runner.run(sahiSuiteFile, endPoint.getUrl());
		}catch(InterruptedException ie){
			throw new InstructionRuntimeException("Sahi test was interrupted",ie);
		} catch (IOException e) {
			throw new InstructionRuntimeException("Sahi test failed from IO problem.",e);
		}finally{
			System.setOut(sysout);
		}
	}

	public void cleanUp() {
		//noop for now.
	}
	
	
}
