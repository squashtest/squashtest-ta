/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.targets;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.ShebangCheck;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 * Target creator for ssh targets.
 * 
 * @author edegenetais
 * 
 */
@EngineComponent("target.creator.ssh")
public class SSHTargetCreator implements TargetCreator<SSHTarget> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSHTargetCreator.class);
	private static final ShebangCheck SHEBANG_CHECK = new ShebangCheck("ssh");

	/** This object allows overriding the target properties with system properties (@see {@link System#getProperties()}) */
	private PropertiesBasedCreatorHelper propertiesHelper=new PropertiesBasedCreatorHelper();
	
	public SSHTargetCreator() {
		propertiesHelper.setKeysRegExp(SSHTarget.NAMESPACE.replaceAll("\\.", "\\.")+"\\..*");
	}
	
	@Override
	public boolean canInstantiate(URL propertiesFile) {
		LOGGER.debug("Testing eligibility of "+propertiesFile+" as SSH configuration URL.");
		boolean isSshConfiguration = false;
		try {
			if (isSSHConfigurationType(propertiesFile) && isCorrect(propertiesFile)) {
				isSshConfiguration = true;
			}
		} catch (IOException e) {
			if (LOGGER.isWarnEnabled()) {
				LOGGER.warn("I/O error while evaluating eligibility of "
						+ propertiesFile.toExternalForm()
						+ " as ssh configuration.", e);
			}
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(propertiesFile+" is "+(isSshConfiguration?" ":"not ")+"eligible.");
		}
		return isSshConfiguration;
	}

	private boolean isSSHConfigurationType(URL propertiesFile)
			throws IOException {
		return "file".equals(propertiesFile.getProtocol())
				&& SHEBANG_CHECK.hasShebang(propertiesFile);
	}
	
	private boolean isCorrect(URL propertiesFile){
		boolean correct;
		try {
			Properties configuration=load(propertiesFile);
			if (configuration.containsKey(SSHTarget.HOSTNAME_KEY)
					&& configuration.containsKey(SSHTarget.USERNAME_KEY)
					&& configuration.containsKey(SSHTarget.PASSWORD_KEY)) {
				correct=true;
			}else{
				LOGGER.debug("Detected missing mandatory keys is "+propertiesFile);
				correct=false;
			}
		} catch (URISyntaxException e) {
			correct=false;
			LOGGER.warn("Detected malformed URL while evaluating correctness of "+propertiesFile,e);
		} catch (IOException e) {
			correct=false;
			LOGGER.warn("Could not read target configuration file",e);
		}
		return correct;
	}

	@Override
	public SSHTarget createTarget(URL propertiesFile) {
		LOGGER.debug("trying to instanciate SSH target from "+propertiesFile);
		try{
			if(isCorrect(propertiesFile)){
				Properties configuration = load(propertiesFile);
				return new SSHTarget(configuration);
			}else{
				throw new BrokenTestException(propertiesFile+" is not a valid SSH target configuration file");
			}
		} catch (URISyntaxException e) {
			throw new BrokenTestException("SSHTarget configuration loading failed for "+propertiesFile.toExternalForm(),e);
		} catch (IOException e) {
			throw new BrokenTestException("SSHTarget configuration loading failed for "+propertiesFile.toExternalForm(),e);
		}
	}

	private Properties load(URL propertiesFile) throws URISyntaxException, IOException {
		File file=new File(propertiesFile.toURI());
		return propertiesHelper.getEffectiveProperties(file);
	}

}
