/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.resources;

import java.io.File;

import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;

/**
 * DirectoryResource implementation.
 * @author fgaillard
 *
 */
@ResourceType("directory")
public class DirectoryResource implements Resource<DirectoryResource> {

	private File directory;
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public DirectoryResource(){}
	
	public DirectoryResource(File file){
		directory = new File(file.getPath());
	}
	
	@Override
	public DirectoryResource copy() {
		return new DirectoryResource(new File(directory.getPath()));
	}

	@Override
	public void cleanUp() {
		//noop
	}

	public File getDirectory() {
		return directory;
	}
}
