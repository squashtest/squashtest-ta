/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.init;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;

/**
 * Workspace browser to expose legacy resources and targets to the engine.
 * 
 * @author edegenetais
 * 
 */
public class LegacyWorkspaceBrowser implements TestWorkspaceBrowser {
	private File root;

	private static final Logger LOGGER=LoggerFactory.getLogger(LegacyWorkspaceBrowser.class);
	
	public LegacyWorkspaceBrowser(File workspaceRoot) {
		this.root = workspaceRoot;
	}

	@Override
	public List<URL> getTargetsDefinitions() {
		List<URL> definitionList = new ArrayList<URL>();
		// database targets
		addDefinitionSubdirectory(definitionList, "data/db/config");
		// ftp targets
		addDefinitionSubdirectory(definitionList, "data/ftp/config");

		// ssh targets
		addDefinitionSubdirectory(definitionList, "ssh");

		return definitionList;
	}

	private void addDefinitionSubdirectory(List<URL> definitionList,
			String subdirectory) {
		File dbTargetDirectory = new File(root, subdirectory);
		if (dbTargetDirectory.isDirectory()) {//if it is not an existing directory, no target definitions to gather here!
			for (File dbConfig : dbTargetDirectory.listFiles()) {
				addURLFromFile(definitionList, dbConfig);
			}
		}
	}

	private void addURLFromFile(List<URL> definitionList, File dbConfig){
		try{
			definitionList.add(dbConfig.toURI().toURL());
		}catch(MalformedURLException mue){
			LOGGER.warn("Malfunction during target enumeration.", mue);
		}
	}

	@Override
	public List<URL> getRepositoriesDefinitions() {
		return Collections.emptyList();
	}

	@Override
	public ResourceRepository getDefaultResourceRepository() {
		return new DefaultLegacyResourceRepository();
	}
}
