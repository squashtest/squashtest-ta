/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.utils.authentication

import spock.lang.Specification

class AuthenticatorChainTest extends Specification {

	def "should search the chain for credentials"(){
		
		given :
			def auth1 = Mock(EngineAuthenticator)
			def auth2 = Mock(EngineAuthenticator)
			def auth3 = Mock(EngineAuthenticator)
			
		and :
			def chain = AuthenticatorChain.getInstance();
			chain.add(auth1)
			chain.add(auth2)
			chain.add(auth3)
			
		and :
			def creds = new PasswordAuthentication("bob", "bob".toCharArray())
		
		when :
			def res = chain.getPasswordAuthentication();
			
		then :
			1 * auth1.knowsCredentials() >> false
			1 * auth2.knowsCredentials() >> true
			0 * auth3.knowsCredentials()
			1 * auth2.getAuthentication() >> creds
			res == creds
		
	}
	
	
}
