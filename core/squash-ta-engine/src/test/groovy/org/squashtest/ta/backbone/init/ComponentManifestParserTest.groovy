/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.init

import org.squashtest.ta.backbone.exception.ManifestParseException;

import spock.lang.Specification

class ComponentManifestParserTest extends Specification {
	def testee
	
	def setup(){
		testee=new ComponentManifestParser();
	}
	
	def "should swear about missing plugin name entry"(){
		given:
			def config = Mock(ComponentConfiguration)
		and:
			def manifestURL=getClass().getClassLoader().getResource("malformedNotBeginName.mf")
		when:
			testee.loadManifest(config,manifestURL);
		then:
			thrown ManifestParseException
	}
	
	def "should read entries and add them to configuration"(){
		given:
			def config=Mock(ComponentConfiguration)
		and:
			def manifestURL=getClass().getClassLoader().getResource("componentManifest.mf");
		when:
			testee.loadManifest(config,manifestURL);
		then:
			1 * config.addBasePackageToPlugin("Groovy test Plugin Name", "org.test");
			1 * config.addBasePackageToPlugin("Groovy test Plugin Name", "fr.gouv");
			1 * config.addBasePackageToPlugin("Groovy test Plugin Name", "org.squashtest");
			
			1 * config.addExcludedElementToPlugin("Groovy test Plugin Name", "org.test.dummy");
			1 * config.addExcludedElementToPlugin("Groovy test Plugin Name", "no way this gets in");
			1 * config.addExcludedElementToPlugin("Groovy test Plugin Name", "fr.cockadoodledoo");
			
	}
	
	def "will swear about non-entry file line"(){
		given:
		def config=Mock(ComponentConfiguration)
	and:
		def manifestURL=getClass().getClassLoader().getResource("malformedNotEntry.mf");
	when:
		testee.loadManifest(config,manifestURL);
	then:
		thrown ManifestParseException
	}
}
