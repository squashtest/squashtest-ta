/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.sql;

public class SQLFormatUtils {
	
	
	/**
	 * The comments between comment symboles are deleted
	 * @param entryString
	 * @return the string without comments
	 */
	public static String stripComments(String entryString){
		if (entryString.contains("/*")){
			String[] tabedString = entryString.split("\\/\\*");
			String takeAwayComment="";
			for (int i=0; i<tabedString.length; i++) {
				if (i==0){
					takeAwayComment = takeAwayComment.concat(tabedString[0]);
				}
				else {
					if (tabedString[i].contains("*/")){
						//We strip the text contained before the */ characters and replace them with an empty string
						takeAwayComment = takeAwayComment.concat(tabedString[i].substring(tabedString[i].indexOf("*/")).replace("*/", ""));
					}
				}
			}
			return takeAwayComment;
		} else {
			//no comments in the string we return it as it is
			return entryString;
		}
	}
	

	/**
	 * the white lines contained in the file have all been concatenated to the file
	 * this method is recursive to replace all the double spaces "  " by single spaces until there are no more double spaces
	 * @param entryString
	 * @return
	 */
	public static  String stripWhiteLines(String entryString){
		String resultString = entryString.replace("  ", " ");
		if (entryString.contains("  ")){
			resultString = stripWhiteLines(resultString);
		}
		return resultString;
	}
	
	
}
