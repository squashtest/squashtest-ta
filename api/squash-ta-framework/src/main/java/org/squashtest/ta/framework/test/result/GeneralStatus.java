/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.framework.test.result;

import org.squashtest.ta.framework.test.definition.TestSuite;

/**
 * 
 * <p>The GeneralStatus gives a global overview of the status of a TestSuite, a Test, or a TestInstruction.</p>
 * 
 * <p>The four statuses are :
 * 	<ul>
 * 		<li>SUCCESS : the given {@link TestSuite} or {@link TestSuite} element went all fine all along.</li>
 * 		<li>WARNING : the given {@link TestSuite} or {@link TestSuite} element succeded, but some non-blocking operation 
 * 				(ex: a tear down step) went wrong.</li>
 * 		<li>NOT_RUN : the given {@link TestSuite} or {@link TestSuite} element was never ran or ended before actual expectation testing.</li>
 * 		<li>FAIL : the given {@link TestSuite} or {@link TestSuite} element encountered a business error (the SUT does not fulfill the expectations).</li>
 * 		<li>ERROR : the given {@link TestSuite} or {@link TestSuite} element encountered a software exception (from any origin).</li>
 * 	</ul>  
 * 
 * Those statuse are sorted by increasing importance. For instance if a test suite made of 3 tests of statuses 2x'SUCCESS' and 1x'NOT_RUN', 
 * the suite status is 'NOT_RUN'.
 * 
 *  </p>
 * 
 * @author bsiri
 *
 */

public enum GeneralStatus {

	/**
	 * This status denotes complete success for a {@link TestSuite} element. 
	 */
	SUCCESS(){
		@Override
		public boolean isPassed() {
			return true;
		}
	},
	
	/**
	 * This status denotes partial success for a {@link TestSuite} element: the test is considered as passed, but some non-blocking operation failed (ex: a teardown step).
	 * 
	 */
	WARNING(){

		@Override
		public boolean isPassed() {
			return true;
		}

	},

	/**
	 * This status denotes an interruption of a {@link TestSuite} element before actual expectation testing.
	 */
	NOT_RUN(){
		@Override
		public boolean isPassed() {
			return false;
		}
	},
	/**
	 * This status denotes an ongoing test execution.
	 */
	RUNNING(){
		@Override
		public boolean isPassed() {
			return false;
		}
	}
	,
	/**
	 * This status denotes a failure of a {@link TestSuite} element to meet the expectations.
	 */
	FAIL(){
		@Override
		public boolean isPassed() {
			return false;
		}
	}, 
	
	/**
	 * This status denotes a technical failure during the {@link TestSuite} execution.
	 */
	ERROR(){
		@Override
		public boolean isPassed() {
			return false;
		}
	};
	
	/**
	 * Returns the most severe status between this one and the other.
	 * @param otherStatus a status to compare.
	 * @return the most severe status of the two.
	 */
	public GeneralStatus mostSevereStatus(GeneralStatus otherStatus){
		return (moreSevere(otherStatus)) ? this : otherStatus;
	}
	
	/**
	 * Compares this status to an other to know which is the more severe.
	 * @param otherStatus the status we want to compare to this one.
	 * @return <code>true</code> if this {@link GeneralStatus} is more severe than the other status
	 */
	public boolean moreSevere(GeneralStatus otherStatus){
		return (this.ordinal()>otherStatus.ordinal());
	}
	
	public abstract boolean isPassed();
}
