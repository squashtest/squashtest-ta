/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Map;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.operation.DatabaseOperation;
import org.squashtest.cats.data.db.internal.DatabaseAccessMode;
import org.squashtest.cats.io.ResourceNotFoundException;

/**
 * This class is a facade to integrate legacy CATS tests classes into the new
 * squashTA execution framework.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractCatsTestCase {

	/**
	 * <p>
	 * <b> This method is deprecated. Use method {@link #populateDatabase}
	 * instead.</b>
	 * </p>
	 * <p>
	 * Links a XML dataset file to a DB, so the dataset content will be inserted
	 * into the DB during setup (DbUnit operation CLEAN_INSERT), and deleted
	 * from the DB during teardown (DbUnit operation DELETE_ALL).
	 * </p>
	 * <p>
	 * The file must be located in directory data/db/datasets.
	 * </p>
	 * <p>
	 * This method must be invoked in the overriden constructor of the classes
	 * that inherit from {@link AbstractCubicTestCase}.
	 * </p>
	 * 
	 * @param pDatabaseName
	 *            DB name
	 * @param pDatasetFileName
	 *            XML dataset file name
	 */
	@Deprecated
	protected void setDataset(String pDatabaseName, String pDatasetFileName) {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * <p>
	 * <b>This method is deprecated. Use {@link #assertSqlTrue} methods
	 * instead.</b>
	 * </p>
	 * <p>
	 * Runs a SQL query (may be parameterized).
	 * </p>
	 * <p>
	 * Every row returned by the query is converted into a <code>Map</code> so
	 * that :<br>
	 * --> key = column name (<code>String</code>)<br>
	 * --> value = column content (<code>Object</code>)
	 * </p>
	 * <p>
	 * The result is assigned to instance variable {@link #queryResults}
	 * </p>
	 * 
	 * @param pDbName
	 *            = DB name
	 * @param pQueryName
	 *            = SQL query name
	 * @param pParams
	 *            = Parameters (if the query is parameterized). The number and
	 *            order of the parameters must be the same than in the query.
	 * @throws SQLException
	 */
	@Deprecated
	protected void getQueryResult(String pDbName, String pQueryName,
			Object... pParams) throws SQLException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * @param pDbName
	 *            DB name
	 * @param pQueryName
	 *            SQL query name
	 * @return
	 * @throws SQLException
	 */
	protected int executeInsertOrUpdateQuery(String pDbName, String pQueryName)
			throws SQLException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * @param pDbAccessMode
	 * @param pDbName
	 * @param pQueryName
	 * @return
	 * @throws SQLException
	 */
	protected int executeInsertOrUpdateQuery(DatabaseAccessMode pDbAccessMode,
			String pDbName, String pQueryName) throws SQLException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Deletes a file from a FTP server.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".<br>
	 * @param pRemotePath
	 *            Path of the file to delete (relative to the user's home
	 *            directory).
	 * @throws IOException
	 */
	protected void delete(String pFtpConfName, String pRemotePath)
			throws IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Downloads a file from a FTP server and saves it into
	 * "data/ftp/download/", without changing its name.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pRemotePath
	 *            Path of the file to download (relative to the user's home
	 *            directory).
	 * @throws IOException
	 *             si une erreur survient lors de l'accès au fichier local, ou
	 *             lors de l'accès au serveur FTP.
	 * @throws URISyntaxException
	 */
	protected void download(String pFtpConfName, String pRemotePath)
			throws IOException, URISyntaxException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Downloads a file from a FTP server and saves it into
	 * "data/ftp/download/", with a new name.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties". * @param pRemotePath Path of
	 *            the file to download (relative to the user's home directory).
	 * @param pLocalName
	 *            Local file name.
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	protected void download(String pFtpConfName, String pRemotePath,
			String pLocalName) throws IOException, URISyntaxException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Télécharge un fichier ou plusieurs fichiers à partir d'un serveur FTP et
	 * le copie dans le répertoire "data/ftp/download/xxx" du projet. author :
	 * LG xxx = un répertoire unique
	 * 
	 * @param pFtpConfName
	 * @param pRemotePath
	 *            = si c'est un répertoire, il faut mettre "/" à la fin
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	protected URL downloadToLocalFolder(String pFtpConfName, String pRemotePath)
			throws IOException, URISyntaxException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
			}

	/**
	 * Télécharge le fichier le plus récent parmi plusieurs fichiers à partir
	 * d'un serveur FTP et le copie dans le répertoire "data/ftp/download/xxx"
	 * du projet. Le fichier distant (expected) est renomé avec le même nom du
	 * fichier résultat pour permettre la comparaison des dataset des CSV author
	 * : LG xxx = un répertoire unique
	 * 
	 * @param pFtpConfName
	 *            Nom de la configuration utilisée pour la connexion au serveur
	 *            FTP.
	 * @param pRemotePath
	 *            C'est obligatoirement le nom du répertoire avec "/" à la fin.
	 * @param pLocalName
	 *            Le nom du fichier CSV résultat avec une extension ".csv"
	 * @throws IOException
	 * @throws URISyntaxException
	 */

	protected URL downloadRecentFileToLocalFolder(String pFtpConfName,
			String pRemotePath, String pLocalName) throws IOException,
			URISyntaxException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
			}

	/**
	 * Deletes all the files from the user's home directory.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @throws IOException
	 */
	protected void empty(String pFtpConfName) throws IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Deletes all the files from a given directory.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pDirectoryPath
	 *            of the directory to empty (relative to the user's home
	 *            directory)
	 * @throws IOException
	 */
	protected void empty(String pFtpConfName, String pDirectory)
			throws IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Deletes all the files from the user's home directory and its children
	 * (recursively)
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @throws IOException
	 */
	protected void emptyAll(String pFtpConfName) throws IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Deletes all the files from a given directory (recursively).
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pDirectoryPath
	 *            of the directory to empty (relative to the user's home
	 *            directory)
	 * @throws IOException
	 */
	protected void emptyAll(String pFtpConfName, String pDirectory)
			throws IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Uploads a file to a FTP server, into the user's home directory, without
	 * changing its name.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pLocalName
	 *            Name of the file to upload (must be located in
	 *            "data/ftp/upload/")
	 * @throws IOException
	 */
	protected void upload(String pFtpConfName, String pLocalName)
			throws IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Uploads a file to a FTP server, with a new name and/or into a chil
	 * directory of the user's home directory.
	 * 
	 * @param pFtpConfName
	 *            Name of the FTP configuration file. It is a properties file,
	 *            located in "data/ftp/config". Ex : "foo" for file
	 *            "data/ftp/config/foo.properties".
	 * @param pLocalName
	 *            Name of the file to upload (must be located in
	 *            "data/ftp/upload/")
	 * @param pRemotePath
	 *            Path of the uploaded file on the FTP server (relative to the
	 *            user's home directory)
	 * @throws IOException
	 */
	protected void upload(String pFtpConfName, String pLocalName,
			String pRemotePath) throws IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Compares the content of a DbUnit flat XML dataset and a database and
	 * asserts those match. Only the primary keys, timestamps, tables and
	 * columns that are specified in the dataset are checked.
	 * 
	 * @param pExpectedDataSetFile
	 *            Name of the flat XML dataset file
	 * @param pDataSource
	 *            Name of the datasource
	 * @throws ResourceNotFoundException
	 * @throws IOException
	 */
	public void assertDatasetEquals(String pExpectedDataSetFile,
			String pDataSource) throws DataSetException,
			ResourceNotFoundException, IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
		}

	/**
	 * <p>
	 * Executes a sql assertion request using the given connection and checks
	 * that expected and actual values from the request match.
	 * </p>
	 * <p>
	 * The assertion request should match the format :<br>
	 * SELECT COUNT (*) AS actual, ... AS expected, ... AS description FROM ...
	 * [ UNION ALL SELECT COUNT (*) AS actual, ... AS expected, ... AS
	 * description FROM ...] [ UNION ALL SELECT COUNT (*) AS actual, ... AS
	 * expected, ... AS description FROM ...] ...
	 * </p>
	 * 
	 * @param pDataSourceName
	 *            name of the datasource used to run the query
	 * @param pSqlQueryName
	 *            name of the sql assertion query
	 * @param pSqlQueryParams
	 *            parameters to the query.
	 */
	public void assertSqlTrue(String pDataSourceName, String pSqlQueryName,
			Object... pSqlQueryParams) {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");	
		}

	/**
	 * Same as {@link #assertSqlTrue(String, String, Object...)} but lets the
	 * client customize the failure message.
	 * 
	 * @param pMessage
	 *            Error message
	 * @param pDataSourceName
	 *            name of the datasource used to run the query
	 * @param pSqlQueryName
	 *            name of the sql assertion query
	 * @param pSqlQueryParams
	 *            parameters to the query.
	 */
	public void assertSqlTrue(String pMessage, String pDataSourceName,
			String pSqlQueryName, Object... pSqlQueryParams) {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Executes a sql assertion request using the given connection and checks
	 * that expected and actual values from the request match. This method
	 * supports queries with named parameters : select * from TABLE where LABEL
	 * = ${label} This method supports parameters set read from a Cubic test
	 * file using the semantic:
	 * 
	 * <pre>
	 * QueryAssert.assertSqlTrue(&quot;myDataSet&quot;, &quot;myQuery&quot;, parametersFromCubicTest(
	 * 		&quot;path/to/test.aat&quot;, 0));
	 * </pre>
	 * 
	 * The assertion request should match the format :
	 * 
	 * <pre>
	 * SELECT COUNT (*) AS actual, ... AS expected, ... AS description
	 * FROM ... [ UNION ALL SELECT COUNT (*) AS actual, ... AS expected, ... AS description FROM ...] [ UNION ALL SELECT
	 * COUNT (*) AS actual, ... AS expected, ... AS description FROM ...] ...
	 * </pre>
	 * 
	 * @param pDataSource
	 *            name of the datasource used to run the query
	 * @param pSqlAssertionName
	 *            name of the sql assertion query.
	 * @param pNamedParameters
	 *            map of parameter name-value pairs
	 * @see {@link #parametersFromCubicTest(String, int)}
	 */
	public void assertSqlTrue(String pDataSourceName, String pSqlQueryName,
			Map<String, String> pSqlQueryNamedParams) {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Same as {@link #assertSqlTrue(String, String, Map)} but lets the client
	 * customize the failure message.
	 * 
	 * @param pMessage
	 * @param pDataSourceName
	 * @param pSqlQueryName
	 * @param pSqlQueryNamedParams
	 */
	public void assertSqlTrue(String pMessage, String pDataSourceName,
			String pSqlQueryName, Map<String, String> pSqlQueryNamedParams) {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Loads named parameters from a Cubic test parameters file (*.params) with
	 * the given index.
	 * 
	 * @param pParamsFile
	 *            path of the cubic test parameters file (relative to the
	 *            project's root)
	 * @param pParametersIndex
	 *            index of the wanted set of value (starting from 0)
	 * @return
	 */
	public Map<String, String> parametersFromCubicTest(String pTestFile,
			int pParametersIndex) {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Compare le contenu d'une base avec un dataset CSV et vérifie que ce
	 * contenu correspond au dataset.
	 * 
	 * @param downloadLocalFolder
	 * @see AbstractCubicTestCase#downloadRecentFileToLocalFolder(String
	 *      pFtpConfName, String pRemotePath, String pLocalName) : ce dossier ne
	 *      contient qu'un seul fichier
	 * @param resultCSVFolderName
	 *            le répertoire qui contient le CSV : Résultat attendu =>
	 *            data/db/datasets/resultscsv/
	 * @throws URISyntaxException
	 * @throws FileNotFoundException
	 * @throws DatabaseUnitException
	 */
	public void assertRecentCsvEquals(URL downloadLocalFolder,
			String resultCSVFolderName) throws URISyntaxException,
			FileNotFoundException, DatabaseUnitException {

		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	@Deprecated
	public Map<String, String> getDatasets() {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Operation to populate a database on demand (possible operations are
	 * standards Dbunit operations : INSERT, DELETE, REFRESH, UPDATE,
	 * DELETE_ALL, TRUNCATE_TABLE
	 * 
	 * @param dbname
	 * @param datasetname
	 * @param operation
	 * @throws DataSetException
	 * @throws IOException
	 */
	public void populateDatabase(String dbname, String datasetname,
			DatabaseOperation operation) throws DataSetException, IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

	/**
	 * Operation to populate a database on demand (possible operations are
	 * standards Dbunit operations : INSERT, CLEAN_INSERT, DELETE, REFRESH,
	 * UPDATE, DELETE_ALL, TRUNCATE_TABLE
	 * 
	 * @param dbname
	 * @param datasetname
	 * @param operation
	 * @param streaming
	 * @throws DataSetException
	 * @throws IOException
	 */
	public void populateDatabase(String dbname, String datasetname,
			DatabaseOperation operation, boolean streaming)
			throws DataSetException, IOException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}

}
