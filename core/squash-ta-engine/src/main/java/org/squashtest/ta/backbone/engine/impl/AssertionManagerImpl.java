/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.AssertionManager;
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.UnaryAssertion;

public class AssertionManagerImpl implements AssertionManager {

	private ObjectFactory factory;
	
	private Map<String, Nature> natureMap = new HashMap<String, Nature>();
	private Map<String, Nature> categoryMap = new HashMap<String, Nature>();
	
	private UnaryAssertionMap unaryMap = new UnaryAssertionMap();
	private BinaryAssertionMap binaryMap = new BinaryAssertionMap(); 

	
	public void setObjectFactory(ObjectFactory factory){
		this.factory=factory;
	}

	/* *********************** interface part **************************** */
	
	public void addAssertionDefinition(BinaryAssertionDefinition definition) {		
		mapIfUnknown(definition.getFirstNature());
		mapIfUnknown(definition.getSecondNature());
		addIfUnknown(definition.getCategory());
		binaryMap.addDefinition(definition);	
	}
	
	public void removeAssertionDefinition(BinaryAssertionDefinition definition){
		binaryMap.removeDefinition(definition);
	}
	
	public void removeAssertionDefinition(Nature actualNature, Nature expectedNature, Nature category){
		binaryMap.removeDefinition(actualNature, expectedNature, category);
	}
	
	public void addAssertionDefinition(UnaryAssertionDefinition definition) {		
		mapIfUnknown(definition.getFirstNature());
		addIfUnknown(definition.getCategory());
		unaryMap.addDefinition(definition);	
	}
	
	public void removeAssertionDefinition(UnaryAssertionDefinition definition){
		unaryMap.removeDefinition(definition);
	}
	
	public void removeAssertionDefinition(Nature actualNature, Nature category){
		unaryMap.removeDefinition(actualNature, UnaryAssertionDefinition.UNARY_ASSERTION_NATURE, category);
	}	


	public Collection<BinaryAssertionHandler> getAllBinaryAssertions(Nature actualNature,
			Nature expectedNature, Nature category ) {
		Collection<BinaryAssertionHandler> result = new ArrayList<BinaryAssertionHandler>();
		Collection<BinaryAssertionDefinition> defs = binaryMap.getAllDefinitions(actualNature, expectedNature, category );
		for (BinaryAssertionDefinition def : defs){
			result.add(createHandler(def));
		}
		return result;
	}

	public Collection<BinaryAssertionHandler> getAllBinaryAssertionsByName(String actualNatureName, String expectedNatureName,
			String assertionIdentifier) {
		
		checkIfNatureKnownOrFail(actualNatureName);
		Nature actualNature = natureMap.get(actualNatureName);
		
		checkIfNatureKnownOrFail(expectedNatureName);
		Nature expectedNature = natureMap.get(expectedNatureName);
		
		checkIfCategoryKnownOrFail(assertionIdentifier);
		Nature category = categoryMap.get(assertionIdentifier);
		
		return getAllBinaryAssertions(actualNature, expectedNature, category);
	}
	
	public Collection<UnaryAssertionHandler> getAllUnaryAssertions(Nature actualNature,
			Nature category ) {
		Collection<UnaryAssertionHandler> result = new ArrayList<UnaryAssertionHandler>();
		Collection<UnaryAssertionDefinition> defs = unaryMap.getAllDefinitions(actualNature, null, category );
		for (UnaryAssertionDefinition def : defs){
			result.add(createHandler(def));
		}
		return result;
	}

	public Collection<UnaryAssertionHandler> getAllUnaryAssertionsByName(String actualNatureName, String assertionIdentifier) {
		
		checkIfNatureKnownOrFail(actualNatureName);
		Nature actualNature = natureMap.get(actualNatureName);
				
		checkIfCategoryKnownOrFail(assertionIdentifier);
		Nature category = categoryMap.get(assertionIdentifier);
		
		return getAllUnaryAssertions(actualNature, category);
	}

	/* ***************************** private code *********************** */
	
	private void mapIfUnknown(Nature nature){
		if (! natureMap.containsKey(nature.getName())){
			natureMap.put(nature.getName(), nature);
		}
	}
	
	private void addIfUnknown(Nature category){
		if (! categoryMap.containsKey(category.getName())){
			categoryMap.put(category.getName(), category);
		}
	}
	
	//null is always valid, as a wildcard
	private void checkIfNatureKnownOrFail(String natureName){ 
		if ((natureName!=null) &&(! natureMap.containsKey(natureName))){
			throw new ResourceNotFoundException("Cannot find assertion : resource type "+natureName+" is unknown");
		}
	}
	
	private void checkIfCategoryKnownOrFail(String categoryName){
		if ((categoryName!=null) && (! categoryMap.containsKey(categoryName))){
			throw new ResourceNotFoundException("Cannot find assertion : category"+categoryName+" is unknown");
		}
	}
	
	
	private BinaryAssertionHandler createHandler(BinaryAssertionDefinition def){
		BinaryAssertion<?,?> instance = factory.newInstance(def.getComponentClass());
		return new BinaryAssertionHandler(def.getFirstNature(), def.getSecondNature(), def.getCategory(), instance);
	}
	
	private UnaryAssertionHandler createHandler(UnaryAssertionDefinition def){
		UnaryAssertion<?> instance = factory.newInstance(def.getComponentClass());
		return new UnaryAssertionHandler(def.getFirstNature(),def.getCategory(), instance);
	}
	

	/* ********** inner class hiding the generic types mess ***************** */
	
	private static class BinaryAssertionMap extends SimpleManager<BinaryAssertionDefinition>{}
	
	private static class UnaryAssertionMap extends SimpleManager<UnaryAssertionDefinition>{}
	
	

}
