/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.squashtest.ta.commons.converter.FileToJavaCodeBundle;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;

/**
 * Object to enumerate a transmitted source tree. The supported source code is
 * as follows:
 * <ul>
 * <li>java source files: will be parsed as source code</li>
 * <li>other files: will be copied as is and served as resource files</li>
 * </ul>
 * A major restriction is that class files or jar archives in this tree won't be
 * recognized as dependency implementations during the compiling process.
 * @see FileToJavaCodeBundle converter class for details about dependency resolution during compilation.
 * 
 * @author edegenetais
 * 
 */
public class SourceEnumerator {
	private static final String JAVA_FILE_EXTENSION = ".java";
	private static final FileTree FILE_ENUMERATOR = new FileTree();
	private static final String DEFAULT_RESOURCE_DIRNAME = "resources";
	private static final String DEFAULT_JAVA_DIRNAME = "java";
	
	private File codeTree;
	private File resourceTree;
	
	public SourceEnumerator(File sourceLocation){
		if(sourceLocation.isDirectory()){
			codeTree=new File(sourceLocation,DEFAULT_JAVA_DIRNAME);
			resourceTree=new File(sourceLocation,DEFAULT_RESOURCE_DIRNAME);
		}else{//case of an isolated source file
			codeTree=sourceLocation;
			resourceTree=codeTree;
		}
	}
	
	public Set<File> getJavaSourceFiles(){
		Set<File> sourceFiles=new HashSet<File>();
		if(codeTree.isDirectory()){
		List<File> allCodeFiles=FILE_ENUMERATOR.enumerate(codeTree, EnumerationMode.FILES_ONLY);
		for(File candidate:allCodeFiles){
			if(isJavaSource(candidate)){
				sourceFiles.add(candidate);
			}
		}
		}else{
			if(codeTree.isFile() && codeTree.getName().endsWith(JAVA_FILE_EXTENSION)){
				sourceFiles.add(codeTree);
			}
		}
		return sourceFiles;
	}

	public boolean isJavaSource(File candidate) {
		return candidate.getName().endsWith(JAVA_FILE_EXTENSION);
	}
	
	public Set<File> getResourceFiles(){
		Set<File> resourceFiles = listResourceFromDefaultDirectory();
		
		/*strictly speaking, we should always separate resources and code source, but let he that is without sin, cast the first stone...*/
		listResourcesFromExecutableSourceDirectory(resourceFiles);
		
		return resourceFiles;
	}

	private void listResourcesFromExecutableSourceDirectory(
			Set<File> resourceFiles) {
		if(codeTree.isDirectory()){
		List<File> allCodeFiles=FILE_ENUMERATOR.enumerate(codeTree, EnumerationMode.FILES_ONLY);
		for(File candidate:allCodeFiles){
			if(isResource(candidate)){
				resourceFiles.add(candidate);
			}
		}
		}else if(codeTree.isFile() && !codeTree.getName().endsWith(JAVA_FILE_EXTENSION)){
			resourceFiles.add(codeTree);
		}
	}

	private Set<File> listResourceFromDefaultDirectory() {
		Set<File> resourceFiles;
		if(resourceTree.isDirectory()){
			resourceFiles=new HashSet<File>(FILE_ENUMERATOR.enumerate(resourceTree, EnumerationMode.FILES_ONLY));
		}else{
			resourceFiles=new HashSet<File>(1);
			if(resourceTree.isFile() && isResource(resourceTree)){
				resourceFiles.add(resourceTree);
			}
		}
		return resourceFiles;
	}

	public String getResourceRelativePath(File resourcePath) throws IOException{
		String path=FILE_ENUMERATOR.getRelativePath(resourceTree, resourcePath);
		if(path==null){/*strictly speaking, we should always separate resources and code source, but let he that is without sin, cast the first stone...*/
			path=FILE_ENUMERATOR.getRelativePath(codeTree, resourcePath);			
		}
		return path;
	}
	
	public boolean isResource(File candidate) {
		return !isJavaSource(candidate);
	}
}
