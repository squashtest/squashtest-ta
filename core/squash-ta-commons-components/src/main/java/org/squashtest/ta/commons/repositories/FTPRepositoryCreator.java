/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.repositories;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.ftp.SimpleFTPClient;
import org.squashtest.ta.commons.library.ftp.SimpleFTPClientFactory;
import org.squashtest.ta.core.library.properties.PropertiesKeySet;
import org.squashtest.ta.core.templates.FileBasedCreator;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.RepositoryCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;


/**
 * <p>Will parse a properties file that will configure and create accordingly a FTP client. The static 
 * attributes in the upper section of that file describe the available keys in the configuration file.
 * If you wonder what they mean, then either you don't need them, either you need to refer to
 * {@link FTPClient}, {@link FTP} or {@link FTPClientConfig} for more details.</p>
 * 
 * <p>The current FTPTargets won't support connection through proxy.</p>
 * 
 * <p>Supported configuration is :
 * <ul>
 * 	<li> 'squashtest.ta.ftp.host' 		: supply the host name (mandatory)</li>
 * 	<li> 'squashtest.ta.ftp.username' 	: the username to log to (mandatory)</li>
 * 	<li> 'squashtest.ta.ftp.password' 	: the corresponding password (mandatory)</li>
 *  <li> 'squashtest.ta.ftp.port' 		: a different port than default</li>
 *  <li> 'squashtest.ta.ftp.filetype'	: the default file type used for each transfert when not specified otherwise. Currently supported : ascii or binary</li>
 *  <li> 'squashtest.ta.ftp.system' 	: the host system type. Currently supported : unix, vms, windows, os/2, os/400, as/400, mvs, l8, netware, macos</li>
 * 	<li> 'squashtest.ta.ftp.useCache'	: tells if the repository must cache its resource to increase performances. Default is false.
 * </ul>
 * </p>
 * 
 * @author bsiri
 *
 */
@EngineComponent("repo.creator.ftp")
public class FTPRepositoryCreator extends FileBasedCreator implements RepositoryCreator<FTPRepository> {


	private static final Logger LOGGER = LoggerFactory.getLogger(FTPRepositoryCreator.class);

	private PropertiesBasedCreatorHelper helper = new PropertiesBasedCreatorHelper();
	
	private SimpleFTPClientFactory factory = new SimpleFTPClientFactory();
	
	public FTPRepositoryCreator(){
		super();
		helper.setKeysRegExp(SimpleFTPClientFactory.FTP_KEYS_REGEXP);
	}
	
	@Override
	public boolean canInstantiate(URL propertiesURL) {
		File file = getFileOrNull(propertiesURL);
		
		if (file==null){
			return false;
		}
		
		Properties properties;
		try {
			properties = helper.getEffectiveProperties(file);
		} catch (IOException e) {
			LOGGER.warn("Could not read from URL "+propertiesURL);
			return false;
		}
		PropertiesKeySet keySet = new PropertiesKeySet(properties);
		
		return (keySet.contains(SimpleFTPClientFactory.FTP_HOST_KEY));
	}

	@Override
	public FTPRepository createRepository(URL propertiesFile) {
		
		try {
		File file = getFileOrFail(propertiesFile);
		
		Properties properties = helper.getEffectiveProperties(file);
		
		SimpleFTPClient client = factory.createClient(properties);
		
		boolean useCache = isUseCache(properties);		
		
		Properties anonymised = helper.anonymize(properties, 1, 
												SimpleFTPClientFactory.FTP_LOGIN_KEY, 
												SimpleFTPClientFactory.FTP_PASSWORD_KEY);
		
		
		client.setConfiguration(anonymised);
		
		return new FTPRepository(client, useCache);
		} catch (IOException e) {
			throw new BrokenTestException("Could not read repository specification.", e);
		} catch (URISyntaxException e) {
			throw new BrokenTestException("Definition URL was no valid URI", e);
		}
	}
	
	protected boolean isUseCache(Properties properties){
		String pptCache = properties.getProperty(FTPRepository.FTP_USE_CACHE);
		return ((pptCache!=null) && (pptCache.equalsIgnoreCase("yes")));
	}

}
