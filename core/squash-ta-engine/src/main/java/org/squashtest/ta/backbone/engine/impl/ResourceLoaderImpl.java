/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.squashtest.ta.backbone.engine.ResourceLoader;
import org.squashtest.ta.backbone.engine.ResourceLoadingSettings;
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.ElementNotFoundException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.backbone.tools.ElementUtils;
import org.squashtest.ta.framework.test.instructions.ResourceName;

/**
 * <p>Another very important class : that class will manage the resource repositories, and load resources according to
 * certain specifications. Those specifications are defined using a {@link ResourceLoadingSettings} object. 
 * Using it, it allows to :<p>
 * 
 * <ul>
 * 	<li>look for a resource given it's name,</li>
 * 	<li>look for it in a specific repository,</li>
 * </ul>
 * 
 * @author bsiri
 *
 */

public class ResourceLoaderImpl implements ResourceLoader {

	private static final ElementUtils ELEMENT_UTILS=new ElementUtils();
	
	private Map<String,RepositoryWrapper> repositories = new HashMap<String, RepositoryWrapper>();


	/* ******************* implementation ***************** */
	


	public void addRepository(RepositoryWrapper repository){
		ELEMENT_UTILS.checkUniqueness(repositories.keySet(), repository.getName());
		repositories.put(repository.getName(), repository);
	}

	public void removeRepository(String repositoryName){
		repositories.remove(repositoryName);
	}
	
	
	/*
	 * Will return a file given its name.
	 * if a dedicated repository was set in the settings, it'll be used. Otherwise they'll be all searched.
	 * 
	 *  Exceptions are thrown when the file name was not set, a specified repository doesn't exist, or if 
	 *  the file was not found eventually.
	 *  
	 *  When no repository was specified, to prevent mistakes in ResourceRepository implementations that may throw exceptions when the resource was not found,
	 *  we will catch them all and restitute them only if the resource was not found (because some might be legit).
	 */
	public ResourceWrapper loadResource(ResourceLoadingSettings settings){
		ResourceName resourceName = settings.getResourceName();
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if (resourceName==null){
			throw new ElementNotFoundException("cannot load resource : name wasn't provided");
		}	
		
		String repoName= settings.getRepositoryName();
		ResourceWrapper resource = null;
		
		if (repoName!=null){
			if (! repositories.containsKey(repoName)){
				throw new ElementNotFoundException("cannot load resource "+resourceName+" : specified repository "+repoName+" doesn't exists");
			}
			RepositoryWrapper repository=repositories.get(repoName);
			resource = repository.findResources(resourceName);
		}else{
			for (RepositoryWrapper r : repositories.values()){
				try{
					resource = r.findResources(resourceName);
					if (resource!=null){
						break;
					}
				}catch(Exception ex){
					exceptions.put(r.getName(), ex);
				}
			}
		}
		
		if (resource == null){
			String message = buildErrorMessage(exceptions, resourceName);
			throw new ResourceNotFoundException(message);
		}
		return resource.copy();	//remember : we want a copy here, not the resource itself.
	}
	
	private String buildErrorMessage(Map<String, Exception> errors, ResourceName resourceName){
		
		StringBuilder builder = new StringBuilder();
		
		String errorMessage = "cannot load resource "+resourceName+" : not found. ";
		
		builder.append(errorMessage);
		
		if (errors.size()!=0){
			
			builder.append("Furthermore, the following exceptions where thrown during the process :\n");
			
			for (Entry<String, Exception> e : errors.entrySet()){
				Exception ex = e.getValue();
				builder.append("repository "+e.getKey()+" : ");
				builder.append(ex.getClass().getSimpleName()+" : "+ex.getMessage()+" \n");
			}
		}		
		return builder.toString();
	}

	@Override
	public void initAll() {
		for (RepositoryWrapper w : repositories.values()){
			w.init();
		}
	}

	@Override
	public void cleanupAll() {
		for (RepositoryWrapper w : repositories.values()){
			w.cleanup();
		}		
	}

	@Override
	public void resetAll() {
		for (RepositoryWrapper w : repositories.values()){
			w.reset();
		}		
	}

	
}
