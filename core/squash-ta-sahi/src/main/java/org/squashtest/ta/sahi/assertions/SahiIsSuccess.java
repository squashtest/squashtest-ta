/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.sahi.assertions;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.sahi.resources.SahiSuiteResultResource;

/**
 * This assertion checks that a sahi test suite succeeded. If the assertion fails, its context contains the sahi report.
 * @author edegenetais
 *
 */
@EngineComponent("success")
public class SahiIsSuccess implements UnaryAssertion<SahiSuiteResultResource>{

	private static final Logger LOGGER=LoggerFactory.getLogger(SahiIsSuccess.class);
	
	private SahiSuiteResultResource sahiResult;
	
	@Override
	public void setActualResult(SahiSuiteResultResource actual) {
		this.sahiResult=actual;
	}

	/**
	 * For this assertion, no configuration is necessary. Any injected resource will be ignored.
	 */
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		if(LOGGER.isWarnEnabled() && configuration.size()>0){
			LOGGER.warn("Ignoring "+configuration.size()+" useless configuration resources (none is expected)");
		}
	}

	@Override
	public void test() throws AssertionFailedException {
		if(!GeneralStatus.SUCCESS.equals(sahiResult.getSuiteStatus())){
			ArrayList<ResourceAndContext> context = new ArrayList<ResourceAndContext>();
			File report=sahiResult.getExecutionReport();
			//we make a copy to keep the report available.
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Begin report content");
				List<File> contents=new FileTree().enumerate(report, EnumerationMode.FILES_AND_DIRECTORIES);
				for(File contentElement:contents){
					LOGGER.debug(contentElement.getAbsolutePath()+"\n");
				}
				LOGGER.debug("End report content");
			}
			FileResource reportResource=new FileResource(report).copy();
			ResourceAndContext reportContext=new ResourceAndContext();
			reportContext.resource=reportResource;
			reportContext.metadata=new ExecutionReportResourceMetadata(SahiIsSuccess.class,new Properties(),FileResource.class,"sahiReport");
			context.add(reportContext);
			throw new AssertionFailedException("Sahi script failed. See attached context report for more details.", sahiResult, context);
		}
	}
}
