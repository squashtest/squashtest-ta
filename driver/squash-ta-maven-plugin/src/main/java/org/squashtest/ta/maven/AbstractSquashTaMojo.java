/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.apache.log4j.PropertyConfigurator;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.commons.exporter.surefire.SurefireSuiteResultExporter;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.facade.Configurer;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;

public abstract class AbstractSquashTaMojo extends AbstractMojo {

	
	private static class Log4jHandler extends Handler {
		private enum Util2Log4jLevel{
			ALL(Level.ALL,org.apache.log4j.Level.ALL),
			SEVERE(Level.SEVERE,org.apache.log4j.Level.ERROR),
			WARNING(Level.WARNING,org.apache.log4j.Level.WARN),
			INFO(Level.INFO,org.apache.log4j.Level.INFO),
			CONFIG(Level.CONFIG,org.apache.log4j.Level.INFO),
			FINE(Level.FINE,org.apache.log4j.Level.DEBUG),
			FINER(Level.FINER,org.apache.log4j.Level.DEBUG),
			FINEST(Level.FINEST,org.apache.log4j.Level.TRACE),
			OFF(Level.OFF,org.apache.log4j.Level.OFF)
			;
			private Util2Log4jLevel(Level utilLevel,org.apache.log4j.Level log4jLevel){
				this.utilName=utilLevel.getName();
				this.log4jLevel=log4jLevel;
			}
			private String utilName;
			private org.apache.log4j.Level log4jLevel;
			
			public static org.apache.log4j.Level transcription(Level utilLevel){
				String name = utilLevel.getName();
				org.apache.log4j.Level log4jLevel=org.apache.log4j.Level.ALL;
				boolean searching=true;
				for(Util2Log4jLevel element:values()){
					if(element.utilName.equals(name)){
						log4jLevel=element.log4jLevel;
						searching=false;
					}else if(searching && log4jLevel.isGreaterOrEqual(element.log4jLevel)){
						log4jLevel=element.log4jLevel;
					}
				}
				return log4jLevel;
			}
		}

		@Override
		public void publish(LogRecord record) {
			// TODO Auto-generated method stub
			Level level=record.getLevel();
			org.apache.log4j.Logger effectiveLogger=org.apache.log4j.Logger.getLogger(record.getLoggerName());
			org.apache.log4j.Level log4jLevel=Util2Log4jLevel.transcription(level);
			if(effectiveLogger.isEnabledFor(log4jLevel)){
				String mString=record.getMessage();
				Object[] parameters=record.getParameters();
				String message=MessageFormat.format(mString, parameters);
				Throwable t=record.getThrown();
				if(t==null){
					effectiveLogger.log(log4jLevel, message);
				}else{
					effectiveLogger.log(log4jLevel, message, t);
				}				
			}
		}

		@Override
		public void flush() {
			//noop
		}

		@Override
		public void close() throws SecurityException {
			// noop
		}
	}

	// refers to
	// http://maven.apache.org/plugin-developers/common-bugs.html#Retrieving_the_Mojo_Logger
	private Log logger;
	
	protected Log getLogger(){
		return logger;
	}
	
	/**
	 * List of all used result exporters.
	 * @parameter
	 */
	private ResultExporter[] exporters = new ResultExporter[]{ new SurefireSuiteResultExporter() };

	/**
	 * List of external configurer definitions. The Mojo will apply them to the engine.
	 * @parameter
	 */
	private Configurer[] configurers;
	
	/**
	 * External configuration file path to override the default (internal) configuration.
	 * @parameter
	 */
	private File logConfiguration;
	
	/**
	 * Where to dump exporter output. Defaults to
	 * ${project.build.directory}/squashTA.
	 * 
	 * @parameter expression="${project.build.directory}/squashTA"
	 */
	private File outputDirectory;
	
	/**
	 * That variable will tell whether the mojo should exit with status failure when a test failed, or if 
	 * it should exit with success no matter what. That later option could be useful if the TA build 
	 * is part of a multimodule project and you don't want it to break. Default is false.
	 * 
	 * @parameter expression="${ta.always.success}" default-value="false"
	 */
	private boolean alwaysSuccess = false;
	
	/** To avoid reconfigured loggers to be garbage collected */
	private List<Logger> javaUtilTaRootLogger=new ArrayList<Logger>();
	
	public AbstractSquashTaMojo() {
	}

	/* Mojo core methods */
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		
		initLogging();
		
		String log4jSystemProperty=System.getProperty("log4j.configuration");
		if(logConfiguration!=null){
			String absolutePath = logConfiguration.getAbsolutePath();
			if(logConfiguration.canRead()){
				if(logger.isInfoEnabled()){
					logger.info("Loading external logging configuration: "+absolutePath);
				}
				setLog4jConfigurationSystemProperty(logConfiguration);
				PropertyConfigurator.configure(absolutePath);
				redirectJavaUtilLogging();
			}else{
				logger.error("External log configuration file "+absolutePath+" is not readable, falling back to internal.");
				configurationLog("mojoLogConfiguration.properties");
			}
		}else{
			configurationLog("mojoLogConfiguration.properties");
		}
		
		SuiteResult result=executeImpl();
		
		logger.info("Exporting results");
		exportResults(result);
		
		logger.info("Cleaning resources");
		result.cleanUp();
		
		//***************** done
        
        logPhase("build complete.");
        
        //freeing al java.util.logging loggers
        javaUtilTaRootLogger.clear();
        
        ifTestsFailedThenFail(result);
        
        restoreLog4jConfigurationProperty(log4jSystemProperty);
	}

	protected abstract SuiteResult executeImpl() throws MojoExecutionException, MojoFailureException;
	
	protected void applyConfigurers(Engine engine){
		if(configurers!=null){
			for(Configurer configurer:configurers){
				try{
					configurer.apply(engine);
				}catch(Exception e){
					throw new ConfigurationException(e,configurer);
				}
			}
		}
	}
	
	public void setConfigurers(Configurer[] configurerTable){
		configurers=Arrays.copyOf(configurerTable, configurerTable.length);
	}
	
	public Configurer[] getConfigurers(){
		return configurers;
	}
	
	/* end Mojo core methods */
	
	/* Result export-related methods */
	public ResultExporter[] getExporters() {
		return exporters;
	}

	public void setExporters(ResultExporter[] exporters) {
		this.exporters = Arrays.copyOf(exporters,exporters.length);
	}
	
	protected void exportResults(SuiteResult results) {
		initLogging();
		
		Map<String,String> usedOutputDirectories=new HashMap<String, String>();
	    for (ResultExporter exporter : exporters){
	    	try{
	    		
				String outputSubdirectory = exporter.getOutputDirectoryName();
				if (outputSubdirectory == null) {
					outputSubdirectory = ".";
				}
				if (usedOutputDirectories.containsKey(outputSubdirectory)) {
					logger.warn("Subdirectory '" + outputSubdirectory
							+ "' used by both " + exporter.getClass().getName()
							+ " and "
							+ usedOutputDirectories.get(outputSubdirectory));
				}
				usedOutputDirectories.put(outputSubdirectory, exporter
						.getClass().getName());
	
				File reportTarget=new File(outputDirectory,outputSubdirectory);
				reportTarget.mkdirs();
				exporter.write(reportTarget, results);
				
	    	}catch(IOException ex){
	    		logger.error("Squash TA : a result exporter failed, some result data may be missing.",ex);
	    	}
	    }
	}

	/* logging related methods. */
	protected void initLogging() {
		logger=getLog();
	}

	public void setLogConfiguration(File logConfiguration) {
		this.logConfiguration = logConfiguration;
	}

	protected void logPhase(String message){
		if (logger.isInfoEnabled()){
			logger.info("Squash TA : "+message);
		}
	}

	private void configurationLog(String confname) {
		URL configurationUrl=getClass().getResource(confname);
		System.setProperty("log4j.configuration", configurationUrl.toExternalForm());
		PropertyConfigurator.configure(configurationUrl);
		redirectJavaUtilLogging();
	}

	private void redirectJavaUtilLogging() {
		Enumeration<String> loggerNames=LogManager.getLogManager().getLoggerNames();
		while(loggerNames.hasMoreElements()){
			String currentName=loggerNames.nextElement();
			if(currentName.startsWith("org.squashtest.ta")){
				Logger logger=Logger.getLogger(currentName);
				logger.setUseParentHandlers(true);
				for(Handler h:logger.getHandlers()){
					logger.removeHandler(h);
				}
				javaUtilTaRootLogger.add(logger);
			}
		}
		Logger taRootLogger=Logger.getLogger("org.squashtest.ta");
		for(Handler h:taRootLogger.getHandlers()){
			taRootLogger.removeHandler(h);
		}
		taRootLogger.addHandler(new Log4jHandler());
		taRootLogger.setUseParentHandlers(false);
		javaUtilTaRootLogger.add(taRootLogger);
	}

	protected void setLog4jConfigurationSystemProperty(File confFile) {
		try {
			System.setProperty("log4j.configuration", confFile.toURI().toURL().toExternalForm());
		} catch (MalformedURLException e) {
			logger.warn(
					"Cannot set log4j.configuration system property ==> I can't guarantee that no rogue tool like soapui runner will screw our log setup!",
					e);
		}
	}

	protected void restoreLog4jConfigurationProperty(String log4jSystemProperty) {
		if(log4jSystemProperty!=null){
        	System.setProperty("log4j.configuration", log4jSystemProperty);
        }else if(System.getProperty("log4j.configuration")!=null){
        	System.clearProperty("log4j.configuration");	
        }
	}

	/* Output directory-related methods */
	protected File getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	protected void resetOutputDirectory() throws MojoExecutionException{
        if (getOutputDirectory().exists()){
        	try {
				new FileTree().clean(getOutputDirectory());
			} catch (IOException e) {
				throw new MojoExecutionException("Could not clean output directory", e);
			}
        }
        
        boolean success=getOutputDirectory().mkdirs();
        if(!success && !getOutputDirectory().exists()){
        	throw new MojoExecutionException("Failed to create outpu directory "+getOutputDirectory().getAbsolutePath());
        }
	}

	/* Test status accouting methods: should the execution fail? */
	protected void ifTestsFailedThenFail(SuiteResult results) throws MojoFailureException{
		
		GeneralStatus status = results.getStatus();
		
		if (! (status.isPassed() || alwaysSuccess) ){
			
			StringBuilder builder = new StringBuilder();
			
			builder.append("Build failure : there are tests failures\n");
			builder.append("Test statistics : "+results.getTotalTests()+" test runs, "+results.getTotalPassed()+" passed, "+results.getTotalNotPassed()+" tests didn't pass\n");
			builder.append("Tests failed / crashed : ");
			builder.append(getFailedTestNames(results));
			
			throw new MojoFailureException(builder.toString());
		}
	}
	
	protected String getFailedTestNames(SuiteResult results){
		
		StringBuilder builder = new StringBuilder();
		
		Iterator<? extends EcosystemResult> ecoIterator = results.getSubpartResults().iterator();
		
		while (ecoIterator.hasNext()){
			
			EcosystemResult ecoResult = ecoIterator.next();
			
			String testNames = getFailedTestNames (ecoResult);
			
			builder.append(testNames);
			
		}
		
		return builder.toString();
	}

	private String getFailedTestNames(EcosystemResult results){
		
		if (results.getStatus().isPassed()){
			
			return "";
			
		}
		else if (results.getSetupResult()!=null && !results.getSetupResult().getStatus().isPassed()){
			
			return "all tests in '"+results.getName()+"' (setup failed)\n";
		
		}
		else{
			
			Iterator<? extends TestResult> testIterator = results.getSubpartResults().iterator();
		
			return "-----------\n"+results.getName()+ ": \n" + collectFailedTestNames(testIterator);
		}	
	}
	
	private String collectFailedTestNames(Iterator<? extends TestResult> iterator){
		
		StringBuilder builder = new StringBuilder();
		
		while ( iterator.hasNext() ){
			
			TestResult result = iterator.next();
			
			if (! result.getStatus().isPassed()){
				builder.append("\t"+result.getName()+"\n");
			}
			
		}
		
		return builder.toString();
		
	}
}