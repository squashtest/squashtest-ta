/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.event;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;

public class TestSuiteLaunchEvent extends
		TestSuiteStatusUpdateBase {
	
	private TestSuite suite;
	
	public TestSuiteLaunchEvent(TestSuite suite) {
		this.suite=suite;
	}
	
	@Override
	public SuiteResult getPayload() {
		return new LaunchResult();
	}

	@Override
	public void addContext(ContextSource manager) {}
	
	private final class LaunchResult implements SuiteResult {
		private Date startTime=new Date();

		@Override
		public Date startTime() {
			return startTime;
		}

		@Override
		public GeneralStatus getStatus() {
			return GeneralStatus.RUNNING;
		}

		@Override
		public String getName() {
			return suite.getName();
		}

		@Override
		public Date endTime() {
			return null;
		}

		@Override
		public void cleanUp() {/* clean up what? */}

		@Override
		public int getTotalWarning() {
			return 0;
		}

		@Override
		public int getTotalTests() {
			int nbTests=0;
			Iterator<Ecosystem> ecosystemIter = suite.iterator();
			while(ecosystemIter.hasNext()){
				Ecosystem ecosystem=ecosystemIter.next();
				nbTests+=ecosystem.getTestPopulation().size();
			}
			return nbTests;
		}

		@Override
		public int getTotalSuccess() {
			return 0;
		}

		@Override
		public int getTotalPassed() {
			return 0;
		}

		@Override
		public int getTotalNotRun() {
			return 0;
		}

		@Override
		public int getTotalNotPassed() {
			return 0;
		}

		@Override
		public int getTotalFailures() {
			return 0;
		}

		@Override
		public int getTotalErrors() {
			return 0;
		}

		@Override
		public List<? extends EcosystemResult> getSubpartResults() {
			return Collections.emptyList();
		}
	}

}