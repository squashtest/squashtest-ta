/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.commands;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.squashtest.ta.commons.targets.FTPTarget;
import org.squashtest.ta.core.tools.ExceptionLogger;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;


/**
 * <p><strong>Description : </strong>That command will delete a remote resource. It doesn't need any Resource as input so the FileResource type argument exists
 * because it has to. It doesn't return anything so you can ignore the result of {@link #apply()}.</p>
 * 
 * <p>NOTE : since the input resource is irrelevant, you may pass any resource, the command won't care.</p>
 * <p>NOTE (again) : as for now the deletion of a directory is NOT recursive. If the file to remove is a directory and the said directory is not empty 
 * an exception will be raised.</p>
 * 
 * 
 *  <p><strong>Configuration</strong> : a FileResource which entries are comma separated pairs of <key:value> (note that column ':' is the separator)
 *  
 *  Available options :
 *  
 *  <ul>
 *  	<li>remotepath : file path name to remove, relative to the user home directory. If it ends with a '/' it will be assumed to be a directory, 
 *  		otherwise it will be treated as a file. Mandatory.</li>
 *  </ul>
 *  
 *  
 *  </p>
 *  <p><strong>DSL example :</strong> <pre>EXECUTE delete WITH $() ON myftp USING $(remotepath : existingfolder/) AS null</pre></p>
 *  <p><strong>DSL example :</strong> <pre>EXECUTE delete WITH $() ON myftp USING $(remotepath : existingfile) AS null</pre></p>
 * 
 * 
 * @author bsiri
 *
 */

@EngineComponent("delete")
public class FTPDeleteCommand implements Command<FileResource, FTPTarget> {

	private static final ExceptionLogger logger = new ExceptionLogger(FTPDeleteCommand.class, IllegalConfigurationException.class);
	
	private static String REMOTE_NAME_OPTION = "remotepath";
	
	private FTPTarget ftp;
	private Collection<Resource<?>> configuration = new LinkedList<Resource<?>>(); 
	
	
	public FTPDeleteCommand(){
		super();
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	@Override
	public void setTarget(FTPTarget target) {
		this.ftp=target;
	}

	@Override
	public void setResource(FileResource resource) {
		
	}

	@Override
	public FileResource apply() {
		Map<String, String> options = getOptions();
		
		String remotePath = options.get(REMOTE_NAME_OPTION);
		
		String failIfNotExistString = options.get("failIfDoesNotExist");
		boolean failIfDoesNotExist=!"false".equalsIgnoreCase(failIfNotExistString);
		ftp.deleteFile(remotePath,isDirectory(remotePath), failIfDoesNotExist);
		
		return null;
	}

	@Override
	public void cleanUp() {
		//nothing special
	}
	
	
	private Map<String, String> getOptions(){
		
		Map<String, String> options=null;
		
		for (Resource<?> resource : configuration){
			if (FileResource.class.isAssignableFrom(resource.getClass())){
				options = readConf(((FileResource)resource).getFile());
			}
		}
		
		if ((options==null) || (! options.containsKey(REMOTE_NAME_OPTION))){
			String message = "ftp file delete : missing configuration. You must specify at least what is the remote file name (option '"+REMOTE_NAME_OPTION+"')";
			throw logger.errAndThrow(message, null);
		}
		
		return options;
	}
	
	private Map<String, String> readConf(File file){
		try{
			return OptionsReader.BASIC_READER.getOptions(file);
		}
		catch(IOException ex){
			String message = "ftp file delete : an error occured while reading the configuration : ";
			throw logger.errAndThrow(message, ex);
		}
		catch(IllegalArgumentException ex){
			String message = "ftp file delete : an error occured while reading the configuration : ";
			throw logger.errAndThrow(message, ex);
		}
	}
	
	private boolean isDirectory(String fileName){
		return fileName.endsWith("/");
	}
	
}
