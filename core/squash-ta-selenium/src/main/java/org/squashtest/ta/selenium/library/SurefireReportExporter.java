/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.library;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.junit.runner.notification.Failure;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.selenium.resources.SeleniumResult;

public class SurefireReportExporter {
	
	/**
	 * empty constructor for Spring instanciation purposes
	 */
	public SurefireReportExporter(){}
	
	private static final SurefireReportExporter INSTANCE = new SurefireReportExporter();
	
	public static SurefireReportExporter getInstance(){
		return INSTANCE;
	}
	
	public File generateReport(SeleniumResult seleniumResult) throws IOException{
		File tempXMLFile = File.createTempFile("TEST-jUnitReport", "Test.xml");
		tempXMLFile.deleteOnExit();
		
		writeXMLFile(tempXMLFile, seleniumResult);
		//we first check if the class implements the TestCase interface
		return tempXMLFile;
	}
	
	private void writeXMLFile(File tempXMLFile, SeleniumResult seleniumResult) throws IOException{
		StringBuilder xmlFile = new StringBuilder();
		int nbTests = seleniumResult.getResult().getRunCount();
		int nbFailedTests = seleniumResult.getFailureCount();
		int nbSkipedTests = seleniumResult.getResult().getIgnoreCount();
		long runTime = seleniumResult.getResult().getRunTime();
		xmlFile.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
		xmlFile.append("<testsuite failures=\"");
		xmlFile.append(nbFailedTests+"");
		xmlFile.append("\" time=\"");
		xmlFile.append(""+runTime/1000f);
		xmlFile.append("\" errors=\"");
		xmlFile.append(nbFailedTests+"");
		xmlFile.append("\" skipped=\"");
		xmlFile.append(nbSkipedTests+"");
		xmlFile.append("\" tests=\"");
		xmlFile.append(nbTests+"");
		xmlFile.append("\" name=\"selenium-test\">\n");
		//we iterate on the test results that have failed
		List<Failure> failures = seleniumResult.getFailures();
		for (Failure failure : failures) {
			xmlFile.append("\t<testcase time=\"0.003\" classname=\"");
			xmlFile.append(failure.getDescription().getClassName());
			xmlFile.append("\" name=\"");
			xmlFile.append(failure.getDescription().getMethodName());
			xmlFile.append("\">\n");
			xmlFile.append("\t\t<error message=\"");
			xmlFile.append(StringEscapeUtils.escapeXml(failure.getMessage()));
			xmlFile.append("\" type=\"");
			xmlFile.append(getType(failure.getTrace()));
			xmlFile.append("\">\n");
			xmlFile.append(StringEscapeUtils.escapeXml(failure.getTrace()) + "\n");
			xmlFile.append("\t\t</error>\n");
			xmlFile.append("\t</testcase>\n");
		}
		xmlFile.append("</testsuite>\n");
		new BinaryData(xmlFile.toString().getBytes("utf-8")).write(tempXMLFile);
	}
	
	private String getType(String trace){
		String[] tab = trace.split(":");
		return tab[0];
	}
}
