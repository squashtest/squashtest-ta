/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.impl.InstructionRunner;
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.MalformedInstructionException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.backbone.test.DefaultExecutionDetails;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;

public abstract class AbstractDefaultInstructionRunner implements InstructionRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstructionRunner.class); 
	
	protected InternalTestRunner testRunner;
	
	public void setTestRunner(InternalTestRunner runner) {
		this.testRunner = runner;
	}
	
	public InternalTestRunner getTestRunner(){
		return testRunner;
	}
	
	public ExecutionDetails  run(){
		
		if(LOGGER.isDebugEnabled()){
			String instructionText;
			if(getInstruction()==null){
				instructionText = "<none>";
			}else{
				instructionText=getInstruction().toText();
			}
			LOGGER.debug("Begin execution of instruction:\n> "+instructionText);
		}
		
		DefaultExecutionDetails executionDetails=new DefaultExecutionDetails();
		try{
			checkSettings();
			doRun();
			executionDetails.setStatus(GeneralStatus.SUCCESS);
		}catch(AssertionFailedException fe){
			executionDetails.setCaughtException(fe);
			executionDetails.setStatus(GeneralStatus.FAIL);
			executionDetails.setInstructionAsText(getInstruction().toText());
			executionDetails.getResourcesAndContext().addAll(fe.getFailureContext());
		}catch(RuntimeException e){
			executionDetails.setCaughtException(e);
			executionDetails.setStatus(GeneralStatus.ERROR);
			executionDetails.setInstructionAsText(getInstruction().toText());
			addInputToFailureReport(executionDetails);
		}
		
		LOGGER.debug("Instruction execution complete.");
		
		return executionDetails;
	}
	
	protected abstract void addInputToFailureReport(ExecutionDetails executionDetails);
	
	protected ResourceWrapper fetchResourceOrFail(ResourceName name){
		ResourceWrapper resource = testRunner.getResourceFromCache(name);
		if (resource == null){
			throwResourceNotFound(name);
		}	
		return resource;
	}

	
	protected ResourceWrapper toFileResourceOrFail(ResourceWrapper resource){
		if (resource.getNature().equals(Nature.FILE_RESOURCE_NATURE)){
			return (ResourceWrapper)resource;
		}else{
			throwResourceNotFound(resource.getName()+" (exists as "+resource.getNature().getName()+", but not as a file)");
			return null;	//this line will never be reached but makes the compiler happy
		}
	}
	
	protected Collection<ResourceWrapper> fetchConfiguration(Collection<ResourceName> resourceNames){
		
		Collection<ResourceWrapper> resources  = new ArrayList<ResourceWrapper>();
		
		for (ResourceName name : resourceNames){
			ResourceWrapper r = fetchResourceOrFail(name);
			resources.add(r);
		}
		
		return resources;
			
	}
	
	protected void logAndThrow(String message){
		MalformedInstructionException ex = new MalformedInstructionException(message);
		LOGGER.warn(message);
		throw ex;
	}
	
	private void throwResourceNotFound(ResourceName name){
		String msg = getInstruction().toText()+": "+getInstructionGenericFailureMessage()+". "+name+
				" does not exist in this test context : you must load it first.";
		throwResourceNotFound(msg);
	}

	private void throwResourceNotFound(String msg) {
		ResourceNotFoundException ex = new ResourceNotFoundException(msg);
		LOGGER.error(ex.getMessage());
		throw ex;
	}
		
	protected abstract String getInstructionGenericFailureMessage();

	/**
	 * should throw an TestSyntaxException if something is incorrect
	 */
	protected abstract void checkSettings();
	
	protected abstract TestInstruction getInstruction();
	
	protected abstract void doRun();

	
	
}
