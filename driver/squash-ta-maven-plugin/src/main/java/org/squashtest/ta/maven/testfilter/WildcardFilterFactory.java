/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.squashtest.ta.maven.testfilter.wildcard.WildcardTestFilter;

/**
 * {@link TokenInterpreter} to build a wildcard filter from a specification
 * token.
 * 
 * @author edegenetais
 * 
 */
public class WildcardFilterFactory implements TokenInterpreter {
	/** Pattern to recognize widlcard tokens.*/
	private static final Pattern ELIGIBILITY_PATTERN = Pattern
			.compile("[/\\\\]?(?:[a-zA-Z*]:\\\\)?(?:[a-zA-Z0-9_\\-.*][a-z A-Z0-9_\\-.*]*[\\\\/]?)+");

	@Override
	public boolean isEligible(File baseDirectory, List<String> availableTokens) {
		boolean eligible;
		if (availableTokens.isEmpty()) {
			eligible = false;
		} else {
			String filterCandidate = availableTokens.get(0);
			@SuppressWarnings("unchecked")
			List<String> pathElms=new LinkedList<String>(Arrays.asList(filterCandidate.split("[/\\\\]")));
			if("".equals(pathElms.get(0))){
				pathElms.remove(0);//the wildcard may begin by /
			}else if(Pattern.compile("[a-zA-Z*]:").matcher(pathElms.get(0)).matches()){
				pathElms.remove(0);//or by some windows drive spec (or *: to specify any drive spec)
			}
			eligible=true;
			for(String pathElt:pathElms){
				if(!Pattern.compile("[a-zA-Z0-9_\\-.*][a-z A-Z0-9_\\-.*]*").matcher(pathElt).matches()){
					eligible=false;
				}
			}
			eligible = eligible && filterCandidate.indexOf('*')>=0;
		}
		return eligible;
	}

	@Override
	public FilenameFilter interprete(File baseDirectory,
			List<String> availableTokens) {
		if(isEligible(baseDirectory, availableTokens)){
			String wildcard=availableTokens.remove(0);
			return new WildcardTestFilter(baseDirectory,wildcard);
		}else if(availableTokens.isEmpty()){
			throw new IllegalArgumentException("No token left");
		}else{
			throw new IllegalArgumentException("Token "+availableTokens.get(0)+" is not a valid wilcard token");
		}
	}

}
