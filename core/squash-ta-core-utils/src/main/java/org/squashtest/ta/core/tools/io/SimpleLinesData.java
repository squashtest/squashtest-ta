/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class wraps the complete loading of a text file into memory as a list of
 * strings, one per line. Of course, this is suitable for small files only.
 * 
 * @author edegenetais
 * 
 */
public class SimpleLinesData {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(SimpleLinesData.class);
	
	/** Data lines. */
	private List<String> dataLines=new LinkedList<String>();
	
	/**
	 * Create from raw data.
	 * @param data
	 */
	public SimpleLinesData(byte[] data){
		this(toLineTable(data));
	}
	
	private static List<String> toLineTable(byte[] content){
		String contentString=new String(content);
		String[] lines=contentString.split("\n");
		return Arrays.asList(lines);
	}
	
	/**
	 * Load text data from an URL.
	 * @param url the URL.
	 * @throws IOException in case the URL can't be opened for reading, or an error occurs while reading from it.
	 */
	public SimpleLinesData(URL url) throws IOException{
		Reader reader=new InputStreamReader(url.openStream());
		load(url.toExternalForm(), reader);
	}
	
	/**
	 * Load a text file.
	 * @param path file path.
	 * @throws IOException in case the file can't be opened or an error occurs while reading it.
	 */
	public SimpleLinesData(final String path) throws IOException{
		FileReader fileReader=new FileReader(path);
		load(path, fileReader);
	}

	/**
	 * Create from in memory data.
	 * @param data the text lines data.
	 */
	public SimpleLinesData(Iterable<String> data){
		for(String line:data){
			if(line.endsWith("\n")){
				//filtering
				line=line.substring(0, line.length()-"\n".length());
			}
			dataLines.add(line);
		}
	}
	
	private void load(final String location, Reader reader)
			throws IOException {
		try{
			BufferedReader lineReader=new BufferedReader(reader);
			String line=lineReader.readLine();
			while(line!=null){
				dataLines.add(line);
				line=lineReader.readLine();
			}
		}finally{
			try{
				reader.close();
			}catch(IOException e){
				LOGGER.warn("Error while closing file "+location+" after loading it.", e);
			}
		}
	}
	
	/**
	 * Get the excluded element list.
	 * @return
	 */
	public List<String> getLines(){
		return dataLines;
	}
	
	/**
	 * Write the data to file.
	 * @param path path to the file.
	 * @throws IOException if an I/O error occurs during writing.
	 */
	public void write(String path) throws IOException{
		writeInternal(path, new FileWriter(path));
	}
	
	/**
	 * Write the data to file.
	 * @param destination file reference.
	 * @throws IOException if an I/O error occurs during writing.
	 */
	public void write(File destination) throws IOException{
		writeInternal(destination.getAbsolutePath(), new FileWriter(destination));
	}

	private void writeInternal(String path, FileWriter writer)
			throws IOException {
		BufferedWriter lineWriter=null;
		try{
			
			lineWriter=new BufferedWriter(writer);
			for(String line:dataLines){
				lineWriter.write(line);
				lineWriter.newLine();
			}
		}finally{
			try{
				if(lineWriter!=null){
					lineWriter.close();
				}
			}catch(IOException e){
				IOException exception=new IOException("Error while closing file "+path+" after writing it.", e);
				throw exception;
			}
		}
	}
}
