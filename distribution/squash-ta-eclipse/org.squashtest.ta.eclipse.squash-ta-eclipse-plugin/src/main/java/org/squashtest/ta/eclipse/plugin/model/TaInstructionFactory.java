/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.model;

import java.util.ArrayList;
import java.util.List;

public final class TaInstructionFactory {

	private static DSLInstruction define;
	
	private static DSLInstruction load;
	
	private static DSLInstruction convert;
	
	private static DSLInstruction execute;
	
	private static DSLInstruction assertInst;
	
	public static DSLInstruction getInstruction(DSLHeadClause headClause){
		DSLInstruction instruction;
		switch (headClause) {
		case DEFINE:
			if(define==null){
				createDefine();
			}
			instruction=define;
			break;
			
		case LOAD:
			if(load==null){
				createLoad();
			}
			instruction = load;
			break;
			
		case CONVERT:
			if(convert==null){
				createConvert();
			}
			instruction=convert;
			break;
	
		case EXECUTE:
			if (execute==null) {
				createExecute();
			}
			instruction = execute;
			break;

		case ASSERT:
			if(assertInst == null){
				createAssert();
			}
			instruction=assertInst;
			break;
			
		default:
			// else noop
			instruction = null;
			break;
		}
		return instruction;
	}

	private static void createAssert() {
		List<DSLClause> possibleClause = new ArrayList<DSLClause>();
		possibleClause.add(DSLClause.IS);
		possibleClause.add(DSLClause.HAS);
		possibleClause.add(DSLClause.DOES);
		possibleClause.add(DSLClause.WITH);
		possibleClause.add(DSLClause.THAN);
		possibleClause.add(DSLClause.THE);
		possibleClause.add(DSLClause.USING);
		assertInst = new DSLInstruction(DSLHeadClause.ASSERT, possibleClause);
	}

	private static void createExecute() {
		List<DSLClause> possibleClause = new ArrayList<DSLClause>();
		possibleClause.add(DSLClause.WITH);
		possibleClause.add(DSLClause.ON);
		possibleClause.add(DSLClause.USING);
		possibleClause.add(DSLClause.AS);
		execute = new DSLInstruction(DSLHeadClause.EXECUTE, possibleClause);
	}

	private static void createConvert() {
		List<DSLClause> possibleClause = new ArrayList<DSLClause>();
		possibleClause.add(DSLClause.TO);
		possibleClause.add(DSLClause.AS);
		possibleClause.add(DSLClause.USING);
		convert = new DSLInstruction(DSLHeadClause.CONVERT, possibleClause);
	}

	private static void createLoad() {
		List<DSLClause> possibleClause = new ArrayList<DSLClause>();
		possibleClause.add(DSLClause.FROM);
		possibleClause.add(DSLClause.AS);
		load = new DSLInstruction(DSLHeadClause.LOAD, possibleClause);
	}

	private static void createDefine() {
		List<DSLClause> possibleClause = new ArrayList<DSLClause>();
		possibleClause.add(DSLClause.AS);
		define = new DSLInstruction(DSLHeadClause.DEFINE, possibleClause);
	}
	
	
}
