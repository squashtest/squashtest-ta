/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.command

import org.squashtest.installer.context.Context;

import spock.lang.Specification;

class InstallFileCommandTest extends Specification {

	def "should init by attaching relative pathes to different directory"(){
		
		given :
			Context context = new Context();
			context.setWorkingDirectory new File("/blablabla") 
			context.setOutputDirectory new File("/blehblehbleh/ohoho")
		and:
			String expectedTemplatePath=new File(new File("/blablabla"),"bob_template").getCanonicalPath()
			String expectedResultFile=new File(new File("/blablabla"),"bob").getCanonicalPath()
			String expectedInstallDirectory=new File(new File("/blehblehbleh/ohoho"),".settings/stuffs").getCanonicalPath()		
		when :
			def command = new InstallFileCommand(context, "bob_template", "bob", ".settings/stuffs")
		
		then :
			command.templateFile.canonicalPath == expectedTemplatePath
			command.resultFile.canonicalPath == expectedResultFile
			command.installDirectory.canonicalPath == expectedInstallDirectory
		
	}
	
}
