/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of OptionReader for testing purpose.
 * @author edegenetais
 *
 */
public class OptionReaderTestHelper extends OptionsReader{

	//default version
	OptionReaderTestHelper() {}
	//version with chosen option separator
	OptionReaderTestHelper(char optionSeparator) {
		super(optionSeparator);
	}
	
	@Override
	protected Set<String> supportedKeys(Map<String, String> source) {
		if(source.containsKey("toto")){
			return new HashSet<String>(Arrays.asList(new String[]{"toto"}));
		}else{
			return Collections.emptySet();
		}
	}

	@Override
	protected Logger getLogger() {
		return LoggerFactory.getLogger(OptionReaderTestHelper.class);
	}

}
