/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.resources;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;

@ResourceType("result.selenium")
public class SeleniumResult implements Resource<SeleniumResult>{
	
	private Result result;
	/** used to hold the verbatim selenese report */
	private File additionalReport;
	
	public SeleniumResult(){
		super();
	}
	
	public SeleniumResult(Result result){
		this.result=result;
	}

	public SeleniumResult(Result result, File additionalReport){
		this.result=result;
		this.additionalReport=additionalReport;
	}
	
	public void setResult(Result result){
		this.result=result;
	}
	
	public Result getResult(){
		return result;
	}

	@Override
	public SeleniumResult copy() {
		return new SeleniumResult(result);
	}

	@Override
	public void cleanUp() {
		
	}
	
	public boolean isSuccess(){
		return (result.getFailureCount() == 0);
	}
	
	public int getFailureCount(){
		return result.getFailureCount();
	}
	
	public List<Failure> getFailures(){
		return result.getFailures();
	}

	public String getMessage(int failureIndex){
		return result.getFailures().get(failureIndex).getMessage();
	}
	
	public List<String> getMessages(){
		List<String> messages = new LinkedList<String>();
	
		for (Failure fail : result.getFailures()){
			messages.add(fail.getMessage());
		}
		
		return messages;
	}
	
	public Throwable getException(int failureIndex) {
		return result.getFailures().get(0).getException();
	}
	
	public List<Throwable> getExceptions(){
		List<Throwable> exceptions = new LinkedList<Throwable>();
		
		for (Failure fail : result.getFailures()){
			exceptions.add(fail.getException());
		}
		
		return exceptions;
	}

	public File getAdditionalReport() {
		return additionalReport;
	}
	
	
	
	
	
	
	
}
