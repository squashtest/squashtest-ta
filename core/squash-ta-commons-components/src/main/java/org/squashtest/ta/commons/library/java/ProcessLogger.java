/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.shell.OutputStream;

/**
 * This class is intended to log stdout from external processes on demand.
 * 
 * @author edegenetais
 * 
 */
public class ProcessLogger extends ProcessStreamListener implements Runnable {
	private final Logger LOGGER;
	private static int counter;
	/**
	 * Creates the stdout logger thread.
	 * 
	 * @param processClass
	 *            an arbitrary string to describe what kind of process is logged
	 *            (will be part of the logger name)
	 * @param process
	 *            the process which stdout we want to log
	 */
	public ProcessLogger(String processClass, Process process) {
		super(process,OutputStream.out);
		LOGGER = LoggerFactory.getLogger("stdout." + processClass + "."
				+ (counter++));
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.info("stdout logger daemon started");
		} else if(LOGGER.isInfoEnabled()){
			String name = LOGGER.getName();
			LOGGER.info("stdout logger daemon started (switch the '"
					+ name.substring(0, name.lastIndexOf('.'))
					+ "' logger category to level DEBUG to see more)");
		}
	}
	
	/**
	 * Creates the stdout logger thread.
	 * 
	 * @param processClass
	 *            an arbitrary string to describe what kind of process is logged
	 *            (will be part of the logger name)
	 * @param process
	 *            the process which stdout we want to log
	 */
	public ProcessLogger(String processClass, Process process, OutputStream targetStream) {
		super(process,targetStream);
		LOGGER = LoggerFactory.getLogger("std"+targetStream.name()+"." + processClass + "."
				+ (counter++));
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.info("std"+targetStream.name()+" logger daemon started");
		} else if(LOGGER.isInfoEnabled()){
			String name = LOGGER.getName();
			LOGGER.info("stdout logger daemon started (switch the '"
					+ name.substring(0, name.lastIndexOf('.'))
					+ "' logger category to level DEBUG to see more)");
		}
	}
	
	@Override
	protected void commitOutputLine(String osString) {
		LOGGER.debug(osString);
	}
}
