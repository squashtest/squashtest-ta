/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter.wildcard;

import java.util.Collections;
import java.util.List;

/**
 * Represents the current match state (may be an intermediate result).
 * 
 * @author edegenetais
 * 
 */
class MatchState {
	/**
	 * Tristate. <code>null</code> means the match computation is not
	 * finished, <code>true</code> means it is finished and the path
	 * matches, and <code>false</code> it is finished and the path does not
	 * match.
	 */
	private Boolean matches;
	/**
	 * Parts of the path still untreated (may contain something even after
	 * match computation if it is a mismatch).
	 */
	private List<String> leftOver;

	/**
	 * Full initialization constructor.
	 * 
	 * @param matches
	 *            global match state (tristate). See {@link #getMatches()}
	 * @param leftOver
	 *            what's left untreated of the path elements. See {@link #getLeftOver()}
	 */
	public MatchState(Boolean matches, List<String> leftOver) {
		this.matches = matches;
		this.leftOver = Collections.unmodifiableList(leftOver);
	}

	/**
	 * Factory method to create the initial {@link MatchState}.
	 * @param initialState the initial path.
	 * @return the initial {@link MatchState}
	 */
	public static MatchState initialState(List<String> initialState){
		return new MatchState(null, initialState);
	}
	
	/**
	 * Tristate: <code>null</code> means the match computation is not
	 * finished, <code>true</code> means it is finished and the path
	 * matches, and <code>false</code> it is finished and the path does not
	 * match.
	 * 
	 * @return the current match state.
	 */
	public Boolean getMatches() {
		return matches;
	}

	/**
	 * @return read-only view of the parts of the path that are still
	 *         untreated (may contain something even after match computation
	 *         if it is a mismatch).
	 */
	public List<String> getLeftOver() {
		return leftOver;
	}

}