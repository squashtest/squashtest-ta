/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools

import spock.lang.Specification

class AmnesicStringBufferTest extends Specification {

	def "should remember only the 20 last characters"(){
		
		given :
			def buffer = new AmnesicStringBuffer(20)
		
		and : 
			def teststring = "that is a very long string that should make more than twenty characters of length, "+ 
							 "hence be of some use for the current test";
							 
			def expected = teststring.substring(teststring.length()-20)			
			
		when :
			buffer.append(teststring)
			def res = buffer.toString();

		then :
			res == expected
		
	}
	
}
