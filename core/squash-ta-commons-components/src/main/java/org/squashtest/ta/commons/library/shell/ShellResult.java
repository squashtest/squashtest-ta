/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.shell;


/**
 * Immutable data object to hold the result of a Shell command execution.
 * @author edegenetais
 *
 */
public class ShellResult {

	private String commandFile;
	private int exitValue;
	private String stdout;
	private String stderr;
	
	/**
	 * Constructor with complete initialization.
	 * @param exitValue exit value of the process.
	 * @param stdout contents emitted by the child process on stdout
	 * @param stderr contents emitted by the child process on stderr
	 * @param command the executed command
	 */
	public ShellResult(int exitValue, String stdout, String stderr, String command) {
		this.exitValue = exitValue;
		this.stdout = stdout;
		this.stderr = stderr;
		this.commandFile = command;
	}

	/**
	 * @return the exit value of the child process.
	 */
	public int getExitValue() {
		return exitValue;
	}
	
	/**
	 * @return the standard output of the child process.
	 */
	public String getStdout() {
		return stdout;
	}

	/**
	 * @return the standard error stream of the child process.
	 */
	public String getStderr() {
		return stderr;
	}
	
	public String getCommand(){
		return commandFile;
	}
}
