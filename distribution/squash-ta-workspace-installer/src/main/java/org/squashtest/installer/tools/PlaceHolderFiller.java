/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.squashtest.installer.context.Context;
import org.squashtest.installer.escaper.Escaper;

/**
 * that class is plain stupid : it takes an input file, replace strings with others then write out the result.
 * 
 * Note that both files must exist.
 * 
 * @author bsiri
 * 
 * TODO : refactor it to make it more flexible and less dependant of the purpose of the app. It may induce slight
 * modifications of the file processed if the names of the placeholders change.
 *
 */
public class PlaceHolderFiller {

	private static final String TA_HOME_PATH_PLACEHOLDER="${SQUASH_TA_HOME_ESC}";
	private static final String USER_WORKSPACE_PLACEHOLDER="${USER_WORKSPACE_ESC}";
	private static final String TA_HOME_PKG_PLACEHOLDER="${SQUASH_TA_PACKAGES_ESC}";
	private static final String TA_INSTALLER_WORKINGDIR_PLACEHOLDER="${SQUASH_TA_INSTALLER_WORKINGDIR_ESC}";
	private static final String TA_HOME_PATH_SIMPLEBACKSLASHES_PLACEHOLDER="${SQUASH_TA_HOME_SIMPLEBACKSLASHES_ESC}";
	
	
	File inputFile;
	File outputFile;
	Context context;


	private Map<String, String> replacementMap = new HashMap<String, String>();
	
	public void setInputFile(File inputFile) {
		this.inputFile = inputFile;
	}
	
	public void setOutputFile(File outputFile) {
		this.outputFile = outputFile;
	}
	
	public void setContext(Context context) {
		this.context=context;		
	}


	public Map<String, String> getReplacementMap(){
		return replacementMap;
	}


	
	public PlaceHolderFiller(){
		
	}
	
	public PlaceHolderFiller(Context context){
		this.context = context;
	}
	

	
	
	public void process() throws IOException{		
		initReplacements();
		replaceAndWrite();		
	}
	
	private String replace(String original){
		String result = original;
		for (String key : replacementMap.keySet() ){
			if (result.contains(key)){
				result = result.replace(key, replacementMap.get(key));
			}
		}
		return result;
	}
	
	

	public void initReplacements() throws IOException{
		
		replacementMap.clear();
		
		String squashTaHomePath=context.getSquashDirectory().getCanonicalPath();
		String userWorkspacePath=context.getOutputDirectory().getCanonicalPath();
		String workingDirPath= context.getWorkingDirectory().getCanonicalPath();
		
		String unicodeEscapedTaPath = Escaper.escapeUnicode(squashTaHomePath);
		String unicodeEscapedWorkspacePath = Escaper.escapeUnicode(userWorkspacePath);
		String unicodeEscapedworkingDirPath = Escaper.escapeUnicode(workingDirPath);
		
		//get the pckg version of ta path
		String unicodeEscapedTaPckg = Escaper.escapeUnicode(squashTaHomePath.replace("\\", "/"));
		
		//For windows, we must double the '\' for the paths - it's how eclipse prefs like them
		//Note that this operation wont affect unix paths so we can safely call them.
		String doubleEscapedTaPath = Escaper.doubleBackslashes(unicodeEscapedTaPath);
		String doubleEscapedWrkspcPath = Escaper.doubleBackslashes(unicodeEscapedWorkspacePath);
		String doubleEscapedWorkingDirPath = Escaper.doubleBackslashes(unicodeEscapedworkingDirPath);

		
		//init the map
		replacementMap.put(TA_HOME_PATH_PLACEHOLDER, doubleEscapedTaPath);
		replacementMap.put(USER_WORKSPACE_PLACEHOLDER, doubleEscapedWrkspcPath);
		replacementMap.put(TA_HOME_PKG_PLACEHOLDER, unicodeEscapedTaPckg);
		replacementMap.put(TA_INSTALLER_WORKINGDIR_PLACEHOLDER, doubleEscapedWorkingDirPath);
		replacementMap.put(TA_HOME_PATH_SIMPLEBACKSLASHES_PLACEHOLDER, unicodeEscapedTaPath);

		
	}	

	
	private void replaceAndWrite() throws FileNotFoundException, IOException {
	
		PrintStream outStream = new PrintStream(outputFile);
		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		
		try{
			
			String buffer;
			
			while((buffer=reader.readLine())!=null){
				String replaced = replace(buffer);
				outStream.println(replaced);
			}
			

			outStream.close();
			reader.close();
			
		}finally{
			outStream.close();
			reader.close();			
		}
	}

}
