/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools.io;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BinaryData {
	private static final String DEFAULT_CLOSE_ERROR_MSG = "Error while closing file";
	private static final Logger LOGGER=LoggerFactory.getLogger(BinaryData.class);
	private static final int DEFAULT_BUFFER_SIZE = 4096;
	private byte[] data;
	
	/**
	 * Reload a binary instance from a file.
	 * @param file the file to read from.
	 * @throws IOException 
	 */
	public BinaryData(File file) throws IOException{
		try {
			URI fileUri = file.toURI();
			URL fileURL = fileUri.toURL();
			load(fileURL, DEFAULT_BUFFER_SIZE, DEFAULT_CLOSE_ERROR_MSG);
		} catch (MalformedURLException e) {
			FileNotFoundException report=new FileNotFoundException("File cannot be opened.");
			report.initCause(e);/*(so sad sonar shuns initCause!)*/
			throw report;//NOSONAR
		}

	}
	
	/**
	 * Create a binary file instance from an URL.
	 * @param url the url to read from.
	 * @throws IOException if an error occurs while opening or reading from the URL.
	 */
	public BinaryData(URL url) throws IOException{
		this(url,DEFAULT_CLOSE_ERROR_MSG);
	}
	
	/**
	 * Create a binary file instance from an URL.
	 * @param url the url to read from.
	 * @param closeErrorMsg custom error message for the warning in case the closing fails.
	 * @throws IOException if an error occurs while opening or reading from the URL.
	 */
	public BinaryData(URL url,String closeErrorMsg) throws IOException{
		this(url,DEFAULT_BUFFER_SIZE,closeErrorMsg);
	}
	
	/**
	 * Create a binary file instance from an URL.
	 * @param url the url to read from.
	 * @param closeErrorMsg custom error message for the warning in case the closing fails.
	 * @param bufferSize custom buffer size for data fetching.
	 * @throws IOException if an error occurs while opening or reading from the URL.
	 */
	public BinaryData(URL url, int bufferSize, String closeErrorMsg) throws IOException {
		load(url, bufferSize, closeErrorMsg);
	}

	private void load(URL url, int bufferSize, String closeErrorMsg) throws IOException {
		byte[] buffer = new byte[bufferSize];
		InputStream inStream=null;
		try {
			inStream=url.openStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream(bufferSize);
			int readSize = inStream.read(buffer);
			while (readSize >= 0) {
				baos.write(buffer, 0, readSize);
				readSize = inStream.read(buffer);
			}
			data=baos.toByteArray();
		} finally {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (IOException ioe) {
					LOGGER.warn(
							closeErrorMsg,
							ioe);
				}
			}
		}
	}
	
	public BinaryData(byte[] content){
		this.data=content.clone();
	}
	
	private void write(OutputStream os) throws IOException{
		try{
			os.write(data);
		}finally{
			os.close();
		}
	}
	
	public void write(File destination) throws IOException{
		OutputStream os=new FileOutputStream(destination);
		write(os);
	}
	
	public void write(URL url){
	}
	
	public byte[] toByteArray(){
		return data;
	}
}
