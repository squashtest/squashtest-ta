/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.tools.JavaFileObject.Kind;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.java.BundleFilePlacementAlgorithm;
import org.squashtest.ta.commons.library.java.CompilationReport;
import org.squashtest.ta.commons.library.java.CompilerConnector;
import org.squashtest.ta.commons.library.java.SourceEnumerator;
import org.squashtest.ta.commons.resources.JavaCodeBundle;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

@EngineComponent("compile")
public class FileToJavaCodeBundle implements
		ResourceConverter<FileResource, JavaCodeBundle> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileToJavaCodeBundle.class);
	private static final BundleFilePlacementAlgorithm BUNDLE_FILE_PLACEMENT_ALGORITHM = new BundleFilePlacementAlgorithm();
	private static CompilerConnector COMPILER_CONNECTOR = new CompilerConnector();
	private static final FileTree FILE_TREE = new FileTree();

	private List<String>options=new ArrayList<String>();
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		
		for(Resource<?> element:configuration){
			if(element instanceof FileResource){
				try {
					SimpleLinesData elementContent=new SimpleLinesData(((FileResource) element).getFile().getAbsolutePath());
					for(String optionDefinition:elementContent.getLines()){
						/*
						 * options here follow the command line convention: a
						 * list of space separated elements, with quotes to
						 * protect space in values
						 */
						String[] optionComponents=optionDefinition.split("[\"]");
						for(int i=0;i<optionComponents.length;i++){
							if(i%2==0){//even elements were not protected by double quotes => hack along the spaces
								String[] optionFlags=optionComponents[i].trim().split(" ");
								for(String flag:optionFlags){
									options.add(flag);
								}
							}else{//odd elements were protected => integrate as is (putting double quotes back)
								options.add("\""+optionComponents[i]+"\"");
							}
						}
					}
				} catch (IOException e) {
					throw new InstructionRuntimeException("Failed to load configuration file",e);
				}
			}else{
				LOGGER.warn("Only resources of type file are supported in configuration. Ignored.");
			}
		}
	}

	@Override
	public JavaCodeBundle convert(FileResource sourceTree) {
		File sourceTreeFile=sourceTree.getFile();
		SourceEnumerator enumerator=new SourceEnumerator(sourceTreeFile);
		Set<File> sourceFiles=enumerator.getJavaSourceFiles();
		Set<File> resourceFiles=enumerator.getResourceFiles();
		
		try {
			File bundleBase=FILE_TREE.createTempDirectory("bundle", ".basedir");
			CompilationReport compileReport = COMPILER_CONNECTOR.compile(sourceFiles, options, bundleBase);
			if(compileReport.isSuccess()){
				for(File resourceFile:resourceFiles){
					String relativePath=enumerator.getResourceRelativePath(resourceFile);
					String resourceName=relativePath.replaceAll("\\\\", "/");
					File resourceDestination=BUNDLE_FILE_PLACEMENT_ALGORITHM.getElementFile(bundleBase, resourceName, Kind.OTHER);
					File resourceParent=resourceDestination.getParentFile();
					if(resourceParent!=null && !resourceParent.exists()){
						boolean created=resourceParent.mkdirs();
						if(!created){
							throw new InstructionRuntimeException("Failed to create resource destination in java code bundle: "+resourceParent.getAbsolutePath());
						}
					}
					BinaryData resourceData=new BinaryData(resourceFile);
					resourceData.write(resourceDestination);
				}
				return new JavaCodeBundle(bundleBase,compileReport.getCompiledClassNames());
			}else{
				throw new BadDataException("Java code compilation failed:\n"+compileReport.getCompilerMessages());
			}
		} catch (IOException e) {
			throw new InstructionRuntimeException("JavaCodeBundle compile failed during I/O operation.",e);
		}
	}

	@Override
	public void cleanUp() {
		//noop
	}
}
