/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.components.unary.assertion

import org.squashtest.ta.commons.resources.SQLQuery
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.legacy.components.unary.assertion.ValidateCountSQLQuery
import static org.junit.Assert.*

import spock.lang.Specification

class ValidateCountSQLQueryTest extends Specification {

	def "query should validate the test"(){
		given :
			SQLQuery mySQLQuery = Mock(SQLQuery)
			mySQLQuery.getQuery() >> "SELECT COUNT(*) AS actual, 123 AS expected, 'DESC' AS description FROM A_CERTAIN_TABLE WHERE 1=1"
			ValidateCountSQLQuery validation = new ValidateCountSQLQuery()
		when :
			validation.setActualResult(mySQLQuery)
			validation.test()
		then :
			notThrown AssertionFailedException
	}
	
	def "query should not validate the test"(){
		given :
			SQLQuery mySQLQuery = Mock(SQLQuery)
			mySQLQuery.getQuery() >> "SELECT COUNT(*) AS real, 123 AS awaited, 'DESC' AS description FROM A_CERTAIN_TABLE WHERE 1=1"
			ValidateCountSQLQuery validation = new ValidateCountSQLQuery()
		when :
			validation.setActualResult(mySQLQuery)
			validation.test()
		then :
			thrown AssertionFailedException
	}
}
