/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.verify.cubic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.cubictest.common.utils.ErrorHandler;
import org.cubictest.model.parameterization.Parameter;
import org.cubictest.model.parameterization.ParameterList;
import org.cubictest.persistence.CubicTestXStream;
import org.cubictest.persistence.TestPersistance;
import org.squashtest.ta.core.tools.io.SimpleLinesData;

/**
 * Similar to Cubic {@link org.cubictest.persistence.ParameterPersistance} but operates from {@link File} objects
 * instead of {@link IFile}.
 * This code was repackaged from the CATS squashTA pre-version, and changed for headless use.
 * FIXME uses Eclipse licensed code, not compatible with GPL
 * 
 * @author gf
 * 
 */
public class ParameterPersistance {
	/**
	 * Utility class : hidden constructor.
	 */
	private ParameterPersistance() {
		super();
	}

	/**
	 * Reads a paramaterList from IFile.
	 * 
	 * @param iFile
	 *            The file containing the test.
	 * @return The parameterList.
	 */
	public static ParameterList loadFromFile(File pFile) {
		String xml = "";
		try {
			String charset = TestPersistance.getCharset(pFile);
			xml = FileUtils.readFileToString(pFile, charset);
		} catch (IOException e) {
			ErrorHandler.logAndRethrow(e);
		}
		try {
			ParameterList list = (ParameterList) new CubicTestXStream().fromXML(xml);
			list.setFileName(pFile.getAbsolutePath());
			return list;
		} catch (Exception e) {
			return fromFile(pFile);
		}
	}

	private static ParameterList fromFile(File pFile) {
		ParameterList list = new ParameterList();
		String absolutePath = pFile.getAbsolutePath();
		list.setFileName(absolutePath);
		try {
			SimpleLinesData data=new SimpleLinesData(absolutePath);
			Iterator<String> lineIterator=data.getLines().iterator();
			String line;
			if(lineIterator.hasNext()){
				line = lineIterator.next();
			}else{
				return list;
			}
			
			for (String token : line.split(";")) {
				Parameter param = new Parameter();
				param.setHeader(token.trim());
				list.addParameter(param);
			}
			while (lineIterator.hasNext()) {
				line=lineIterator.next();
				int i = 0;
				for (String token : line.split(";")) {
					list.getParameters().get(i++).addParameterInput(token.trim());
				}
			}
		} catch (FileNotFoundException e) {
			ErrorHandler.logAndRethrow(e);
		} catch (IOException e) {
			ErrorHandler.logAndRethrow(e);
		}
		return list;
	}
}
