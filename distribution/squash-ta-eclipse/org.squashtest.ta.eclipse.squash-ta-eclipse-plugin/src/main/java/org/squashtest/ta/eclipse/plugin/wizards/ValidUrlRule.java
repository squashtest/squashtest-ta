/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Text;

/**
 * This class implements the URL validation rule.
 * 
 * @author edegenetais
 * 
 */
public class ValidUrlRule implements ModifyListener {
	private Page page;
	private Text field;
	private List<String> protocols = Collections.emptyList();
	private String ruleId;

	/**
	 * Full initialization constructor.
	 * 
	 * @param targetPage
	 *            wizard page to check
	 * @param targetField
	 *            field to check
	 * @param targetProtocol
	 *            one or more protocols accepted by the URL field
	 */
	public ValidUrlRule(Page targetPage, Text targetField,
			String... targetProtocol) {
		this.page = targetPage;
		this.field = targetField;
		this.protocols = Arrays.asList(targetProtocol);
		ruleId = toString();
	}

	@Override
	public void modifyText(ModifyEvent event) {
		if (event.getSource() == field) {
			String value = field.getText();
			String[] parts=value.split(":");
			if(parts.length<2){
				page.updateStatus(ruleId, value+" is no valid URL (no scheme)");
			}else if(protocols!=null && protocols.size()>0 && !protocols.contains(parts[0])){
				page.updateStatus(ruleId,
						parts[0] + " is not one of the expected URL schemes ("
								+ Arrays.toString(protocols.toArray()) + ")");
			}else if(parts[1]==null || parts[1].trim().length()==0){
				page.updateStatus(ruleId, value+" is not valid URL: missing shema-specific part");
			}else{
				page.updateStatus(ruleId, null);
			}
		}
	}

}
