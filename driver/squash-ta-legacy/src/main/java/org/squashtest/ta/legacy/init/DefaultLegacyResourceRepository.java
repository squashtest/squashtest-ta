/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.init;

import java.io.File;
import java.util.Properties;

import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;

@ResourceType("legacy-project")
public class DefaultLegacyResourceRepository implements ResourceRepository {

	@Override
	public void init() {
		// XXX check this should not do some stuff...
	}

	@Override
	public Properties getConfiguration() {
		return new Properties();
	}

	@Override
	public FileResource findResources(String name) {
		File absoluteFile = new File(name);
		if(absoluteFile.exists()){
			return new FileResource(absoluteFile);
		}else{
			//XXX see if no other locations may be required...
			return null;
		}
	}

	@Override
	public void cleanup() {
		//XXX check if this should not do some stuff...
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}
