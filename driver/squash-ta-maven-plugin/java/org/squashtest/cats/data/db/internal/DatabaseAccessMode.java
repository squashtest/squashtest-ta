/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.data.db.internal;

/**
 * Database access mode. This code from the CATS squash version was taken to the
 * squash-ta-legacy module to provide an integration facade for legacy CATS test
 * classes.
 * 
 * @author gf
 * 
 */
public enum DatabaseAccessMode {
	LOCAL_ACCESS("local"), REMOTE_ACCESS("remote");

	private final String code;

	private DatabaseAccessMode(String pCode) {
		assert pCode != null;
		this.code = pCode;
	}

	public static final DatabaseAccessMode findByCode(String pCode) {
		for (DatabaseAccessMode mode : values()) {
			if (mode.getCode().equals(pCode)) {
				return mode;
			}
		}

		throw new IllegalArgumentException("DatabaseAccessMode '" + pCode + "' is unknown");
	}

	public String getCode() {
		return code;
	}
}
