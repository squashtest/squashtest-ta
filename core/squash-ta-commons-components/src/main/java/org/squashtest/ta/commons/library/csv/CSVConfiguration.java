/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.csv;

import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;

/**
 * A library containing the separation character of a CSV file
 * The default is "," as in Comma Separated Values
 * @author FOG
 *
 */
@ResourceType("csv.conf")
public class CSVConfiguration implements Resource<CSVConfiguration>{
	
	private CSVSeparator separationCharacter = CSVSeparator.COMMA;
	private CSVNullEntry nullEntry = CSVNullEntry.EMPTY;
	private CSVQuoteCharacter quoteCharacter = CSVQuoteCharacter.DOUBLE_QUOTE;
	private CSVEscapeCharacter esacapeCharacter = CSVEscapeCharacter.BACK_SLASH;
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public CSVConfiguration() {}
	
	/**
	 * There are many constructors possible depending what you want to configure at instanciation
	 * <p> You can configure up to 3 things :<p>
	 * <ul>
	 * 	<li>The separation character</li>
	 * 	<li>The Entry Escape character, in case you want to write an entry containing the separation character</li>
	 *  <li>The Null Entry</li>
	 * </ul>
	 * 
	 * You Can cal these constructors with the different Enum in the order you wish
	 */
	
	/**
	 * Constructor with one config attribute
	 */
	public CSVConfiguration(ConfigEnum configEnum) {
		setConfig(configEnum);
	}
	
	/**
	 * Constructor with two config attributes
	 */
	public CSVConfiguration(ConfigEnum configEnum, ConfigEnum configEnum2) {
		setConfig(configEnum);
		setConfig(configEnum2);
	}
	
	/**
	 * Constructor with three config attributes
	 */
	public CSVConfiguration(ConfigEnum configEnum, ConfigEnum configEnum2, ConfigEnum configEnum3) {
		setConfig(configEnum);
		setConfig(configEnum2);
		setConfig(configEnum3);
	}
	
	/**
	 * Constructor with three config attributes
	 */
	public CSVConfiguration(ConfigEnum configEnum, ConfigEnum configEnum2, ConfigEnum configEnum3, ConfigEnum configEnum4) {
		setConfig(configEnum);
		setConfig(configEnum2);
		setConfig(configEnum3);
		setConfig(configEnum4);
	}

	@Override
	public CSVConfiguration copy() {
		// noop
		return null;
	}

	@Override
	public void cleanUp() {
		//noop	
	}

	/**
	 * Getters of the different configuration parameters
	 * @return
	 */
	public CSVSeparator getSeparationCharacter(){
		return separationCharacter;
	}
	
	public CSVQuoteCharacter getQuoteCharacter(){
		return quoteCharacter;
	}
	
	public CSVNullEntry getNullEntry(){
		return nullEntry;
	}
	
	public CSVEscapeCharacter getEscapeCharacter(){
		return esacapeCharacter;
	}
	
	private void setConfig (ConfigEnum configEnum){
		if (configEnum instanceof CSVQuoteCharacter){
			quoteCharacter = (CSVQuoteCharacter)configEnum;
		} else if (configEnum instanceof CSVNullEntry){
			nullEntry = (CSVNullEntry)configEnum;
		} else if (configEnum instanceof CSVSeparator){
			separationCharacter = (CSVSeparator)configEnum;
		} else if (configEnum instanceof CSVEscapeCharacter){
			esacapeCharacter = (CSVEscapeCharacter)configEnum;
		}
	}
}
