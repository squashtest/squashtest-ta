/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import java.util.Properties;

public class NewSshTargetWizard extends AbstractTargetWizard {


	public static final String PASSWORD_KEY = "squashtest.ta.ssh.password";
	public static final String USERNAME_KEY = "squashtest.ta.ssh.username";
	public static final String PORT_KEY = "squashtest.ta.ssh.port";
	public static final String HOSTNAME_KEY = "squashtest.ta.ssh.hostname";
	private SshTargetPropertiesWizardPage targetProperties;
	
	@Override
	protected String getShebangMark() {
		return "ssh";
	}

	@Override
	protected Properties extractTargetProperties() {
		final Properties sshProperties=new Properties();
		sshProperties.setProperty(HOSTNAME_KEY, targetProperties.getHostname());
		if(targetProperties.isPortFieldEnable())
		{
			sshProperties.setProperty(PORT_KEY, targetProperties.getPort());
		}
		sshProperties.setProperty(USERNAME_KEY, targetProperties.getUsername());
		sshProperties.setProperty(PASSWORD_KEY, targetProperties.getPassword());
		return sshProperties;
	}

	@Override
	protected void addSpecificPages() {
		targetProperties=new SshTargetPropertiesWizardPage();
		addPage(targetProperties);
	}

}
