/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.param;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.SimpleLinesData;

/**
 * This class isolates replacement elements from raw data. Sections between arobases <pre>@...@</pre> protected against substitution. 
 * @author edegenetais
 *
 */
public class ReplacementTokenizer {
	private static final LitteralExpression LINE_END_EXPRESSION = new LitteralExpression("\n");
	private static final Pattern LITTERAL_STRINGS=Pattern.compile("@(\\$\\{.*?\\})@");
	private static final Pattern PLACE_HOLDERS=Pattern.compile("\\$\\{([^}]+)\\}");
	private static final Logger LOGGER=LoggerFactory.getLogger(ReplacementTokenizer.class);
	
	private List<ExpressionParser>expressionParsers=new ArrayList<ExpressionParser>();
	
	/**
	 * Convenience version for a single parser.
	 * @param singleParser the parser to use.
	 */
	public ReplacementTokenizer(ExpressionParser singleParser){
		expressionParsers.add(singleParser);
	}
	
	/**
	 * Full initialization constructor.
	 * @param expressionParsers parsers to compute expression values in the expression evaluation process.
	 */
	public ReplacementTokenizer(List<ExpressionParser> expressionParsers) {
		this.expressionParsers.addAll(expressionParsers);
	}
	
	/**
	 * Parse some data.
	 * @param data data to parse.
	 * @return the data as a list of expressions.
	 */
	public List<Expression>tokenize(SimpleLinesData data){
		List<Expression> list=new ArrayList<Expression>();
		LOGGER.debug("Begin parsing");
		for(String line:data.getLines()){
			LOGGER.debug("Parsing line: "+line);
			Matcher litteralStringsMatcher=LITTERAL_STRINGS.matcher(line);
			int parseableBegin=0;
			while(litteralStringsMatcher.find()){
				int parseableEnd=litteralStringsMatcher.start();
				String litteralStringDef=litteralStringsMatcher.group(1);
				String parseable=line.substring(parseableBegin,parseableEnd);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("parseable content: "+parseable+"\nlitteral: "+litteralStringDef);
				}
				parse(list, parseable);
				parseableBegin=litteralStringsMatcher.end();
				list.add(new LitteralExpression(litteralStringDef));
			}
			if(parseableBegin<line.length()){
				String lastParseable=line.substring(parseableBegin);
				LOGGER.debug("Last parseable part: "+lastParseable);
				parse(list,lastParseable+"\n");
			}else{
				list.add(LINE_END_EXPRESSION);
			}
		}
		return list;
	}
	
	private void parse(List<Expression> list, String parseable) {
		Matcher parseableMatcher=PLACE_HOLDERS.matcher(parseable);
		int litteralBegin=0;
		LOGGER.debug("Begin parsing: "+parseable);
		while(parseableMatcher.find()){
			litteralBegin = tokenizeParseableMatch(list, parseable,
					parseableMatcher, litteralBegin);
		}
		if(litteralBegin<parseable.length()){
			String litteralString = parseable.substring(litteralBegin);
			list.add(new LitteralExpression(litteralString));
			LOGGER.debug("Last string litteral in parseable: "+litteralString);
		}
	}

	private int tokenizeParseableMatch(List<Expression> list, String parseable,
			Matcher parseableMatcher, int litteralBegin) {
		int litteralEnd=parseableMatcher.start();
		String expression=parseableMatcher.group(1);
		String fixedPart = parseable.substring(litteralBegin,litteralEnd);
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Found fixed part: "+fixedPart+"\nExpression: "+expression);
		}
		list.add(new LitteralExpression(fixedPart));
		litteralBegin=parseableMatcher.end();
		Iterator<ExpressionParser>parserIterator=expressionParsers.iterator();
		boolean searching=true;
		while(searching && parserIterator.hasNext()){
			ExpressionParser parser=parserIterator.next();
			if(parser.accept(expression)){
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Parser "+parser.getClass().getName()+" parses "+expression);
				}
				list.add(parser.parse(expression));
				searching=false;
			}
		}
		if(searching){
			list.add(new LitteralExpression("${"+expression+"}"));
		}
		return litteralBegin;
	}
}
