/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Collection;
import java.util.LinkedList;

import org.squashtest.ta.backbone.engine.AssertionFindSettings;

public class AssertionFindSettingsImpl implements AssertionFindSettings {

	private String actualResultNatureName;
	private String expectedResultNatureName;
	private String assertionIdentifier;
	private Collection<String> assertionConfiguration = new LinkedList<String>();
	
	
	public String getActualResultNatureName() {
		return actualResultNatureName;
	}
	public String getExpectedResultNatureName() {
		return expectedResultNatureName;
	}
	public String getAssertionCategory() {
		return assertionIdentifier;
	}
	public Collection<String> getAssertionConfiguration() {
		return assertionConfiguration;
	}
	public void setActualResultNatureName(String actualResultNatureName) {
		this.actualResultNatureName = actualResultNatureName;
	}
	public void setExpectedResultNatureName(String expectedResultNatureName) {
		this.expectedResultNatureName = expectedResultNatureName;
	}
	public void setAssertionCategory(String assertionIdentifier) {
		this.assertionIdentifier = assertionIdentifier;
	}
	public void addAssertionConfiguration(Collection<String> assertionConfiguration) {
		this.assertionConfiguration.addAll(assertionConfiguration);
	}
	

	
	
	

}
