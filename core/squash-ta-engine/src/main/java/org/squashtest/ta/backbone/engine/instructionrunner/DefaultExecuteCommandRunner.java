/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.squashtest.ta.backbone.engine.CommandFindSettings;
import org.squashtest.ta.backbone.engine.impl.CommandFindSettingsImpl;
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.exception.IncompatibleNaturesException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.backbone.exception.TargetNotFoundException;
import org.squashtest.ta.backbone.test.DefaultResourceMetadata;
import org.squashtest.ta.framework.exception.CannotApplyCommandException;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.ResourceMetadata.ResourceRole;

class DefaultExecuteCommandRunner extends AbstractDefaultInstructionRunner {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultConvertResourceRunner.class); 
	
	private static final String MISSING_NAME_ERROR = "the resource name is missing";
	private static final String MISSING_RESULT_NAME_ERROR = "the result must be given a name";
	private static final String MISSING_COMMAND_NAME_ERROR = "the name of the command is missing";
	private static final String MISSING_TARGET_ERROR = "the target was not specified";
	private static final String GENERIC_ERROR_MESSAGE = "Cannot execute command : ";	

	private ExecuteCommandInstruction instruction;
	
	DefaultExecuteCommandRunner(ExecuteCommandInstruction instruction){
		this.instruction = instruction;
	}
	
	
	@Override	
	protected void doRun() {
	
		//step 1 : the easy part
		ResourceName resourceName = instruction.getResourceName();
		ResourceWrapper resource = fetchResourceOrFail(resourceName);
		
		String targetName = instruction.getTargetName();
		TargetWrapper target = fetchTarget(targetName);
		
		//set the command up
		CommandHandler command = setupCommand(resource, target);
		
		//execute
		try{
			ResourceWrapper result = command.apply();
			
			//store the new resource if any
			if (result != null){
				ResourceName alias = instruction.getResultName();
				result.setName(alias);
				testRunner.addResourceToCache(result);
			}
		}finally{		
			//cleanup
			command.cleanUp();
		}
	}
	
	@Override
	protected void addInputToFailureReport(ExecutionDetails executionDetails) {
		
		ResourceWrapper input=testRunner.getResourceFromCache(instruction.getResourceName());
		if(input!=null){
			ResourceAndContext inputRac=new ResourceAndContext();
			DefaultResourceMetadata inputMetadata=new DefaultResourceMetadata(input);
			inputMetadata.setRole(ResourceRole.INPUT);
			inputRac.metadata=inputMetadata;
		}
	}
	
	@Override
	protected void checkSettings(){
		if (instruction.getResourceName()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_NAME_ERROR);
		}
		if (instruction.getResultName()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_RESULT_NAME_ERROR);
		}
		
		if (instruction.getCommandCategory()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_COMMAND_NAME_ERROR);
		}
		
		if (instruction.getTargetName()==null){
			logAndThrow(MISSING_TARGET_ERROR);
		}
	}

	
	@Override
	protected TestInstruction getInstruction() {
		return instruction;
	}
	
	protected TargetWrapper fetchTarget(String targetName){
		TargetWrapper target = testRunner.getContextManager().getTarget(targetName);
		if (target == null){
			TargetNotFoundException ex = new TargetNotFoundException("Cannot execute instruction : target "+
					targetName+" does not exist in this test context : check your configuration again.");
			LOGGER.error(ex.getMessage());
			throw ex;
			
		}
		
		return target;
	}

	
	private CommandFindSettings buildSettings(ResourceWrapper resource, TargetWrapper target){
		CommandFindSettings settings = new CommandFindSettingsImpl();
		settings.setResourceNatureName(resource.getNature().getName());
		settings.setTargetNatureName(target.getNature().getName());
		settings.setCommandIdentifier(instruction.getCommandCategory());
		return settings;
	}

	//if a command is not found because the settings contains unknown natures or identifier,
	//if doesn't mean the operation fail. The diagnostic will be run later.
	private CommandHandler safeFindCommand(CommandFindSettings settings){
		try{
			Collection<CommandHandler> found = testRunner.getContextManager().getCommand(settings);
			if (found.size()==1){
				return found.iterator().next();
			}else{
				return null;
			}
		}catch(ResourceNotFoundException ex){
			return null;
		}
	}
	
	
	//will attempt to get the command. In case it's impossible because of incompatible natures, if the testrunner is set to autoconversion
	//it will retry with optimistic conversion
	private CommandHandler setupCommand(ResourceWrapper resource, TargetWrapper target){
		CommandFindSettings settings = buildSettings(resource, target);
		CommandHandler command = null;
		
		command = safeFindCommand(settings);
		
		if (command!=null){
			command.setResource(resource);
			command.setTarget(target);
			Collection<ResourceWrapper> conf = fetchConfiguration(instruction.getCommandConfiguration());
			command.addConfiguration(conf);
			return command;
		}else{
			BrokenTestException error = diagnoseError(resource, target, instruction.getCommandCategory());
			throw error;
		}
			
	}
	
	/* ************************************ error handling **************************************** */
	
	//basically, the question is : was absolutely no command found, or was the user command definition ambiguous ? 
	private BrokenTestException diagnoseError(ResourceWrapper resource, TargetWrapper target, String commandName){
		
		CommandFindSettings settings = buildSettings(resource, target);
		settings.setResourceNatureName(null);
		
		Collection<CommandHandler> commands = null;
		
		commands = testRunner.getContextManager().getCommand(settings);
		
		if (commands.size()!=0){
			return throwIncompatibleNatureException(resource, target, commandName);
		}
		else{
			return throwCannotApplyCommandException(commandName);
		}
	}
	
	
	private BrokenTestException throwIncompatibleNatureException(ResourceWrapper resource, TargetWrapper target, String commandName){
		ResourceName resourceName = resource.getName();
		String targetName = target.getName();
		String resNatureName = resource.getNature().getName();
		String tarNatureName = target.getNature().getName();
		
		FormattingTuple tuple = MessageFormatter.arrayFormat("Cannot apply command : command '{}' found but cannot handle resource '{}' and " +
				"target '{}' together. This is probably due to incompatible natures : '{}' is of nature '{}' and '{}' is of nature '{}'. If needed, consider explicit conversion and deactivate" +
				" auto conversion.", new Object[]{commandName, resourceName, targetName, resourceName, resNatureName, targetName, tarNatureName});
		
		
		LOGGER.error(tuple.getMessage());
		
		return new IncompatibleNaturesException(tuple.getMessage());		
		
	}
	
	
	private BrokenTestException throwCannotApplyCommandException(String commandName){
		CannotApplyCommandException ex = new CannotApplyCommandException("Cannot apply command : command '"+commandName+"' not found.");
		LOGGER.error(ex.getMessage());
		return ex;		
	}


	@Override
	protected String getInstructionGenericFailureMessage() {
		return GENERIC_ERROR_MESSAGE;
	}

}
