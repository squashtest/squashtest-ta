/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools


import org.squashtest.ta.core.library.properties.DefaultPropertiesSource;
import org.squashtest.ta.core.library.properties.NamespacedPropertiesSource;
import org.squashtest.ta.core.library.properties.PropertiesKeySet;
import org.squashtest.ta.core.tools.PropertiesUtils;

import spock.lang.Specification

class PropertiesUtilsTest extends Specification {

	
	def "should get the aggregated properties (1)"(){
		
		given :
			def config = initPropertiesConfig()
			def defaut = initPropertiesDefault()
		
		and :
			def configSource = new DefaultPropertiesSource(config)
			def defaultSource = new DefaultPropertiesSource(defaut)
		
		and :
			def keySet = new PropertiesKeySet(["ta.key1", "ta.key2", "ta.key3", "ta.key4"])
			
		when :
			def effective = PropertiesUtils.staticGetEffectiveProperties(keySet, configSource, defaultSource)
		
		then :
			effective.getProperty("ta.key1") == "value1"
			effective.getProperty("ta.key2") == "value2"
			effective.getProperty("ta.key3") == "value3"
			effective.getProperty("ta.key4") == "value4a"
			effective.getProperty("ta.key5") == null
	}
	
	def "should get the aggregated properties (2)"(){
		
		given :
			def config = initPropertiesConfig()
			def defaut = initPropertiesDefault()
		
		and :
			def configSource = new DefaultPropertiesSource(config)
			def defaultSource = new DefaultPropertiesSource(defaut)
		
		and :
			def keySet = new PropertiesKeySet(["ta.key1", "ta.key2", "ta.key3", "ta.key4"])
			
		when :
			def effective = PropertiesUtils.staticGetEffectiveProperties(keySet, defaultSource, configSource)
		
		then :
			effective.getProperty("ta.key1") == "value1"
			effective.getProperty("ta.key2") == "value2a"
			effective.getProperty("ta.key3") == "value3"
			effective.getProperty("ta.key4") == "value4a"
			effective.getProperty("ta.key5") == null
		
	}
	
	
	def "should get the aggregated properties(3)"(){
		
		given :
			def config = initPropertiesConfig()
			def defaut = initPropertiesDefault()
			def namespaced = initPropertiesNamespaced()
		
		and :
			def configSource = new DefaultPropertiesSource(config)
			def defaultSource = new DefaultPropertiesSource(defaut)
			def namespacedSource = new NamespacedPropertiesSource(namespaced, "bob")
		
		and :
			def keySet = new PropertiesKeySet(["ta.key1", "ta.key2", "ta.key3", "ta.key4"])
			
		when :
			def effective = PropertiesUtils.staticGetEffectiveProperties(keySet, namespacedSource, configSource, defaultSource)
		
		then :
			effective.getProperty("ta.key1") == "value1"
			effective.getProperty("ta.key2") == "value2b"
			effective.getProperty("ta.key3") == "value3"
			effective.getProperty("ta.key4") == "value4b"
			effective.getProperty("ta.key5") == null
	}

	def "should get the aggregated properties(4)"(){
		
		given :
			def config = initPropertiesConfig()
			def defaut = initPropertiesDefault()
			def namespaced = initPropertiesNamespaced()
		
		and :
			def configSource = new DefaultPropertiesSource(config)
			def defaultSource = new DefaultPropertiesSource(defaut)
			def namespacedSource = new NamespacedPropertiesSource(namespaced, "bob")
		
		and :
			def keySet = new PropertiesKeySet(["ta.key1","ta.key3", "ta.key4", "ta.key5"])
			
		when :
			def effective = PropertiesUtils.staticGetEffectiveProperties(keySet, namespacedSource, configSource, defaultSource)
		
		then :
			effective.getProperty("ta.key1") == "value1"
			effective.getProperty("ta.key3") == "value3"
			effective.getProperty("ta.key4") == "value4b"
			effective.getProperty("ta.key5") == "value5"
	}

	
	def "should find keys matching a pattern in a bunch of sources and return the corresponding PropertiesKeySet (1)"(){
		
		given :			
			def config = initPropertiesConfig()
			def defaut = initPropertiesDefault()
			def namespaced = initPropertiesNamespaced()
			
		and :
			def configSource = new DefaultPropertiesSource(config)
			def defaultSource = new DefaultPropertiesSource(defaut)
			def namespacedSource = new NamespacedPropertiesSource(namespaced, "bob")
			
		when :
			def keySet = PropertiesUtils.staticFindKeysByPattern("ta.key.*", namespacedSource, configSource, defaultSource)
		
		then :
			def iterator = keySet.iterator()
			def res = {
					def result = []; 
					while(iterator.hasNext()) result.add(iterator.next()); 
					return result;
				}();
			
			res as Set == ["ta.key1", "ta.key2", "ta.key3", "ta.key4", "ta.key5", "ta.key6"] as Set
			
		
	}
	
	
	
	/* ****************************** utils ****************************** */
	
	def initPropertiesConfig = {
				
		def ppts = new Properties()
		
		//file
		ppts.setProperty("ta.key1","value1")
		ppts.setProperty("ta.key2","value2")
		ppts.setProperty("ta.key3","value3")
		
		return ppts
		
	}
	
	def initPropertiesDefault = {
		
		def ppts = new Properties()
		
		//default
		ppts.setProperty("ta.key2", "value2a")
		ppts.setProperty("ta.key4", "value4a")
		ppts.setProperty("ta.key5", "value5")
		
		return ppts
		
	}
	
	def initPropertiesNamespaced = {
		
		def ppts = new Properties()
		
		//namespaced
		ppts.setProperty("bob.ta.key2", "value2b")
		ppts.setProperty("bob.ta.key4", "value4b")
		ppts.setProperty("bob.ta.key6", "value6")
		
		return ppts;
	}
	
	
	
	def "should return stripped properties"(){
		given :
			def url = getClass().getClassLoader().getResource("repositorytest/some.random.properties");
			File file = new File(url.toURI())
		
			Properties properties = new Properties()
			file.withReader { properties.load(it) }
			
		when :
			Properties result = PropertiesUtils.staticGetStrippedProperties(properties, "ta.random.append.");
		
		then :
			result.containsKey "data1"
			result.containsKey "stuff2"
			! (result.containsKey("albert"))
	}
	
	def "should also return stripped INHERITED properties"(){
		given :
		def url = getClass().getClassLoader().getResource("repositorytest/some.random.properties");
		File file = new File(url.toURI())
	
		Properties properties = new Properties()
		file.withReader { properties.load(it) }
		Properties testedProperties=new Properties(properties)
	when :
		Properties result = PropertiesUtils.staticGetStrippedProperties(testedProperties, "ta.random.append.");
	
	then :
		result.containsKey "data1"
		result.containsKey "stuff2"
		! (result.containsKey("albert"))
	}
}
