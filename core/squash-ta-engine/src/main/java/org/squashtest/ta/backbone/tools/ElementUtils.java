/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.tools;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.exception.NotUniqueEntryException;

public class ElementUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElementUtils.class);
	
	public <R> void checkUniqueness(Set<R> keySet, R element){
		if (keySet.contains(element)){
			NotUniqueEntryException ex = new NotUniqueEntryException("cannot add element "+element.toString()+" to "+
					"the cache : identifier already exists");
			LOGGER.error(ex.getMessage());
			throw ex;
		}
	}
	
	
}
