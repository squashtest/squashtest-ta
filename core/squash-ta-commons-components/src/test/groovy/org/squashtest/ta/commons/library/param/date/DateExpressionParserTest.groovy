/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.param.date

import java.text.SimpleDateFormat;

import spock.lang.Specification

class DateExpressionParserTest extends Specification {
	def testee
	def setup(){
		testee=new DateExpressionParser()
	}
	
	def "now().format(MMDD) is valid"(){
		given:
			def expression="now().format(MMDD)"
		when:
			def resultat=testee.accept(expression)
		then:
			resultat==true
	}
	
	def "with more functions in between is valid"(){
		given:
		def expression="now().addDay(3).format(MMDD)"
	when:
		def resultat=testee.accept(expression)
	then:
		resultat==true
	}
	
	def "with false function in between is NOT valid"(){
		given:
		def expression="now().addDay.format(MMDD)"
	when:
		def resultat=testee.accept(expression)
	then:
		resultat==false
	}
	
	def "not now is not valid"(){
	given:
		def expression="anything().format(MMDD)"
	when:
		def resultat=testee.accept(expression)
	then:
		resultat==false
	}
	
	def "only format is not valid"(){
	given:
		def expression="format(MMDD)"
	when:
		def resultat=testee.accept(expression)
	then:
		resultat==false
	}
	
	def "simplest expression should work"(){
		given:
			def expression="now().format(yy-MM-dd)"
		and:
			def expected=new SimpleDateFormat("yy-MM-dd").format(Calendar.getInstance().getTime())
		when:
			def dateString=testee.parse(expression)
		then:
			expected.equals(dateString.evaluate())
	}
	
	def "should find injected function by name and given args and date"(){
		given:
			def function=Mock(DateFunction)
			testee=new DateExpressionParser(["mock":function])
		and:
			def expected=new SimpleDateFormat("yy-MM-dd").format(new Date(0))
		and:
			def expression="now().mock(toto).format(yy-MM-dd)"
		when:
			def dateString=testee.parse(expression)
		then:
			1 * function.evaluate(_,"toto") >> {Calendar cal=Calendar.getInstance(); cal.setTimeInMillis(0); return cal}
			expected.equals(dateString.evaluate())
	}
	
	def "should find implemented function by name and given args and date"(){
		given:
			DummyFunction.called=false;
		and:
			def expression="now().dummyFunction().format(yy-MM-dd)"
		when:
			def dateString=testee.parse(expression)
		then:
			DummyFunction.called
			"70-01-01".equals(dateString.evaluate())
	}
}
