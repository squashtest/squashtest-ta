/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.link;

import org.squashtest.ta.api.execution.client.StatusUpdate;
import org.squashtest.ta.framework.test.event.EcosystemStatusUpdate;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;

/**
 * Event listener to post {@link TestStatusUpdate} events back to a Squash TM API- compliant RESTful service.
 * @author edegenetais
 *
 */
public class RestTestStatusUpdateListener extends RestSquashTMUpdateCallback implements StatusUpdateListener{
	
	public RestTestStatusUpdateListener(StatusUpdate updateClient,
			String executionHostname, String jobName, String externalId) {
		super(updateClient, executionHostname, jobName, externalId);
	}

	@Override
	public void handle(TestSuiteStatusUpdate event) {/* This listener only manages test level updates. */}

	@Override
	public void handle(EcosystemStatusUpdate event) {/* This listener only manages test level updates. */}

	@Override
	public void handle(TestStatusUpdate event) {
		TestExecutionStatus status=new TestExecutionStatus();
		TestResult testResult = event.getPayload();
		status.setTestName(testResult.getName());
		status.setTestGroupName(event.getEcosystemName());
		
		setStatusData(status, testResult);
		
		status.setStartTime(testResult.startTime());
		status.setEndTime(testResult.endTime());
		
		getUpdateClient().updateTestExecutionStatus(getExecutionHostname(), getJobName(), getExternalId(), event.getEcosystemName(), event.getPayload().getName(), status);
	}

	private void setStatusData(TestExecutionStatus status, TestResult testResult) {
		GeneralStatus taStatus = testResult.getStatus();
		TaTmStatusMapping statusMapping=TaTmStatusMapping.getMapping(taStatus);
		status.setStatus(statusMapping.getTmStatus());
		if(taStatus==GeneralStatus.WARNING || !taStatus.isPassed()){
			ExecutionDetails details=testResult.getFailureReport();
			if(details!=null && details.caughtException()!=null){
				status.setStatusMessage(details.caughtException().getMessage());
			}
		}
	}
	
}
