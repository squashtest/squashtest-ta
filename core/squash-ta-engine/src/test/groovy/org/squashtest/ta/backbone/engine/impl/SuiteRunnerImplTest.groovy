/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl


import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.SuiteRunnerSettings
import org.squashtest.ta.backbone.engine.impl.SuiteRunnerImpl
import org.squashtest.ta.framework.test.definition.Test
import org.squashtest.ta.framework.test.definition.TestSuite
import org.squashtest.ta.framework.test.result.TestResult
import org.squashtest.ta.framework.test.result.GeneralStatus

import spock.lang.Specification

class SuiteRunnerImplTest extends Specification {
	/**

TODO: this test should be rewritten as a test of the SuiteResult object
	
	def "should correctly build the results"(){
		
		given :
			def runner = new SuiteRunnerImpl();
		and :
			def test1 = buildTest1();
			def test2 = buildTest2();
			def test3 = buildTest3();
			def settings = buildSettings();
		and :
			def suite = assemble([test1, test2, test3]);
			runner.setSuite(suite)
			runner.configure(settings)
	
		when :
			def suiteResult = runner.buildResult()
			
		then :
			suiteResult.getName() == "suite"
			suiteResult.getStatus() == GeneralStatus.SETUP_ERROR;
			suiteResult.getTotalTests() == 3
			suiteResult.getTotalCommands() == 6
			suiteResult.getTotalAssertions() == 4
			suiteResult.getTotalNotRun() == 1
			suiteResult.getTotalSuccess() == 1
			suiteResult.getTotalFailure() == 0
			suiteResult.getTotalError() == 1
			
			suiteResult.getTestResults().size() == 3 
	}
	
	
	def buildSettings = {
		def settings = Mock(SuiteRunnerSettings)
		
		settings.isAutoCache() >> false
		settings.isAutoConvert() >> false
		
		def manager = Mock(ContextManager)
		
		settings.getContextManager() >> manager
		
		return settings;
		
	}
	
	
	def buildTest1 = {
		def test1 = Mock(Test)
		
		def iter = Mock(Iterator)
		iter.hasNext() >> false
		
		test1.iterator() >> iter
		
		def res = Mock(TestResult)
		res.getTestStatus() >> GeneralStatus.SUCCESS
		res.getCommandCounter() >> 2
		res.getAssertionCounter() >> 3
		
		test1.getResult() >> res
		
		return test1;		
	}
	
	def buildTest2 = {
		def test2 = Mock(Test)
		
		def iter = Mock(Iterator)
		iter.hasNext() >> false
		
		test2.iterator() >> iter
		
		def res = Mock(TestResult)
		res.getTestStatus() >> GeneralStatus.NOT_RUN
		res.getCommandCounter() >> 0
		res.getAssertionCounter() >> 0
		
		test2.getResult() >> res
		return test2;		
	}
	
	def buildTest3 = {
		def test3 = Mock(Test)
		
		def iter = Mock(Iterator)
		iter.hasNext() >> false
		
		test3.iterator() >> iter
		
		def res = Mock(TestResult)
		res.getTestStatus() >> GeneralStatus.ERROR
		res.getCommandCounter() >> 4
		res.getAssertionCounter() >> 1
		
		test3.getResult() >> res
		
		
		return test3;		
	}
	
	def assemble = { tests ->
		def suite = Mock(TestSuite)
		
		suite.getName() >> "suite"
		
		tests.each{suite.addTest(it)}
		
		def iter = Mock(Iterator)
		
		def bools = []
		(tests.size()).times{bools << true}
		bools << false
		
		iter.hasNext() >>> bools
		iter.next() >>> tests
		
		suite.iterator() >> iter
		
		return suite;
		
	}
*/
}
