/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.test;

import java.util.ArrayList;
import java.util.Collection;

import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.ResourceMetadata;

public class DefaultExecutionDetails implements ExecutionDetails {
	
	private GeneralStatus status=GeneralStatus.NOT_RUN;
	private InstructionType type;
	private String instructionAsText;
	
	private int phasePosition=0;
	private int absolutePosition=0;
	
	private Exception caughtException;
	
	private Collection<ResourceAndContext> resourcesInContext=new ArrayList<ResourceAndContext>();

	
	
	/* *********************************** API part ************************* */
	
	public GeneralStatus getStatus() {
		return status;
	}

	public InstructionType instructionType() {
		return type;
	}

	public int instructionNumberInPhase() {
		return phasePosition;
	}

	public int instructionNumber() {
		return absolutePosition;
	}

	public String instructionAsText() {
		return instructionAsText;
	}

	public Exception caughtException() {
		return caughtException;
	}

	public Collection<ResourceAndContext> getResourcesAndContext() {
		return resourcesInContext;
	}
	
	
	/* ***************************** Setters part ****************************** */
	
	public void setStatus(GeneralStatus status){
		this.status=status;
	}
	
	public void setInstructionType(InstructionType type){
		this.type=type;
	}
	
	public void setInstructionNumberInPhase(int number){
		this.phasePosition=number;
	}
	
	public void setInstructionNumber(int number){
		this.absolutePosition=number;
	}
	
	public void setInstructionAsText(String text){
		this.instructionAsText=text;
	}
	
	public void setCaughtException(Exception ex){
		this.caughtException = ex;
	}
	
	public void addResourceAndContext(ResourceAndContext contextResource){
		this.resourcesInContext.add(contextResource);
	}
	
	/**
	 * This method will build a ResourceAndContext object from a ResourceWrapper, add it to the collection
	 * then return it. So later on we can modify its role using {@link DefaultResourceMetadata#setRole(org.squashtest.ta.framework.test.result.ResourceMetadata.ResourceRole)}.
	 * 
	 * 
	 * @param wrapper
	 */
	public ResourceAndContext addPartialResourceAndContext(ResourceWrapper wrapper){
		Resource<?> resource = wrapper.unwrap();
		ResourceMetadata meta = new DefaultResourceMetadata(wrapper);
		ResourceAndContext rac = new ResourceAndContext();
		rac.resource=resource;
		rac.metadata=meta;
		resourcesInContext.add(rac);
		return rac;
	}
}
