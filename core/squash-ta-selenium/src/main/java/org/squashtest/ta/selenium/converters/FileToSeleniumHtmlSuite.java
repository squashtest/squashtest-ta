/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.converters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.BundleComponent;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.selenium.resources.SeleniumHtmlTestSuite;

/**
 * Converter to create {@link SeleniumHtmlTestSuite} resources from
 * {@link FileResource} resources.
 * 
 * @author edegenetais
 * 
 */
@EngineComponent("script")
public class FileToSeleniumHtmlSuite extends BundleComponent implements
		ResourceConverter<FileResource, SeleniumHtmlTestSuite> {

	private static final FileTree FILE_TREE = new FileTree();
	private static final Logger LOGGER = LoggerFactory
			.getLogger(FileToSeleniumHtmlSuite.class);
	
	private Collection<Resource<?>> configuration=new ArrayList<Resource<?>>();
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	@Override
	public SeleniumHtmlTestSuite convert(FileResource resource) {
		try {
			boolean ownsBundleTree=false;
			File resourceFile = resource.getFile();
			File bundleBase;
			File suiteFile;
			if (resourceFile.isDirectory()) {
				bundleBase = resourceFile;

				suiteFile = extractMainFileFromConfiguration(configuration,
						bundleBase);

			} else {
				ownsBundleTree=true;
				bundleBase = FILE_TREE.createTempDirectory("HTML",
						".selenium");
				suiteFile=new File(bundleBase,resourceFile.getName());
				BinaryData suiteContent=new BinaryData(resourceFile);
				suiteContent.write(suiteFile);
			}
			String browserStringOption=getOptions(configuration).get("browser");
			return new SeleniumHtmlTestSuite(bundleBase, suiteFile,ownsBundleTree,browserStringOption);
		} catch (IOException e) {
			throw new InstructionRuntimeException(
					"Selenium HTML suite conversion failed.", e);
		}
	}

	@Override
	public void cleanUp() {
		//noop
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

}
