/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.squashtest.ta.framework.test.instructions.BinaryAssertionInstruction;
import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction;
import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.instructions.UnaryAssertionInstruction;

/**
 * This class is a facade to integrate legacy CATS batch tests classes into the
 * new squashTA execution framework.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractBatchTestCase extends AbstractCatsTestCase {

	private static final String ASSERT_CATEGORY_CONTAINS="contains";
	
	private static final String RESOURCE_TYPE_SCRIPT_SHELL = "script.shell";

	/**
     * Runs a SSH command, reading the connection parameters from the specified
     * configuration file. If the exit status is 0 (= "success"), checks whether
     * the contents of the output and error streams match the specified patterns.
     * 
     * @param configFile SSH configuration file (contains host, port, username
     *            and password)
     * @param command Command to execute
     * @param outputStreamPattern Pattern to match against the output stream. If
     *            <code>null</code> or empty, the output stream is not checked.
     * @param errorStreamPattern Pattern to match against the error stream. If
     *            <code>null</code> or empty, the error stream is not checked.
     * @throws IOException
     */
    protected void runAndCheckBatchTestCase(String configFile, String command, String outputStreamPattern,
                    String errorStreamPattern) throws URISyntaxException, IOException, InterruptedException {
    	ResetLocalContextInstruction context=createLocalContext("runAndCheckBatchTestCase()");
    	getCallListener().addInstruction(context);
    	
    	ResourceName scriptFile=new ResourceName(Scope.SCOPE_TEMPORARY,"script.file");
    	DefineResourceInstruction defineScript=createDefineInstruction(scriptFile, command);
    	getCallListener().addInstruction(defineScript);
    	
    	ResourceName script=new ResourceName(Scope.SCOPE_TEMPORARY,RESOURCE_TYPE_SCRIPT_SHELL);
		ConvertResourceInstruction castToShell = createConvertInstruction(
				scriptFile, script, "runAndCheckBatchTestCase()",
				CONVERT_CATEGORY_SCRIPT, RESOURCE_TYPE_SCRIPT_SHELL);
		getCallListener().addInstruction(castToShell);
		
		List<ResourceName> executeConfig = Collections.emptyList();
		ResourceName resultName = new ResourceName(Scope.SCOPE_TEMPORARY,
				"flux");
		ExecuteCommandInstruction commandInstruction = createExecuteCommandInstruction(
				configFile, script, resultName, "runAndCheckBatchTestCase()", executeConfig,
				COMMAND_CATEGORY_EXECUTE);
		getCallListener().addInstruction(commandInstruction);
		
		UnaryAssertionInstruction successAssertion=createUnaryAssertion(resultName, "success");
		getCallListener().addInstruction(successAssertion);
		
		if(outputStreamPattern!=null && outputStreamPattern.length()>0){
			//checking standard output pattern
			createStreamAssert(outputStreamPattern, resultName, "out.");
		}
		
		if(errorStreamPattern!=null && errorStreamPattern.length()>0){
			//checking error stream pattern
			createStreamAssert(errorStreamPattern, resultName, "err.");
		}
    }

	private void createStreamAssert(String regex,
			ResourceName resultName, String namePrefix) {
		ResourceName targetStream=new ResourceName(Scope.SCOPE_TEMPORARY,namePrefix+"target.stream");
		DefineResourceInstruction defineTargetOutStream=createDefineInstruction(targetStream, "STDOUT");
		getCallListener().addInstruction(defineTargetOutStream);
		
		ResourceName pattern=new ResourceName(Scope.SCOPE_TEMPORARY,namePrefix+"pattern");
		DefineResourceInstruction defineExpectedPattern=createDefineInstruction(pattern, regex);
		getCallListener().addInstruction(defineExpectedPattern);
		
		List<ResourceName>assertStdoutConfig=Arrays.asList(new ResourceName[]{targetStream});
		BinaryAssertionInstruction streamAssert = createBinaryAssertion(
				pattern, resultName, ASSERT_CATEGORY_CONTAINS,
				"runAndCheckBatchTestCase()", assertStdoutConfig);
		getCallListener().addInstruction(streamAssert);
	}

}
