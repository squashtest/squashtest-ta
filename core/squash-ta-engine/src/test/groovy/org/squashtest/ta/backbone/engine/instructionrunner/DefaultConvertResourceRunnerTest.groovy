/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.instructionrunner

import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultConvertResourceRunner
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.backbone.exception.MalformedInstructionException
import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource
import org.squashtest.ta.framework.components.ResourceConverter

import spock.lang.Specification


class DefaultConvertResourceRunnerTest extends Specification {

	DefaultConvertResourceRunner runner
	InternalTestRunner testRunner
	ConvertResourceInstruction instruction
	ContextManager manager
	
	
	def setup(){
		instruction = new ConvertResourceInstruction()
		runner = new DefaultConvertResourceRunner(instruction)
		
		testRunner = Mock(InternalTestRunner)
		
		runner.setTestRunner testRunner
		
		manager = Mock(ContextManager)
		
	}
	
	
	def "should rant because the settings doesn't supply the resource name"(){		
		when :
			runner.checkSettings()
			
		then :
			thrown MalformedInstructionException
		
	}
	
	def "should rant because the settings doesn't supply an alias"(){
		given :
			instruction.getResourceName() >> new ResourceName(Scope.SCOPE_TEST,"test")
		
		when :
			runner.checkSettings()
			
		then :
			thrown MalformedInstructionException
	}
	
	def "should rant because the settings doesn't supply the type"(){
		given :
			instruction.getResourceName() >> new ResourceName(Scope.SCOPE_TEST,"test")
			instruction.getResultName() >> new ResourceName(Scope.SCOPE_TEST,"alias")
		
		when :
			runner.checkSettings()
		then :
			thrown MalformedInstructionException
	}
	
	def "should pass the check"(){
		given :
			mockInstruction()
		
		when :
			runner.checkSettings()
		then :
			true
	}
	
	def "should return the instruction"(){
		expect :
			runner.getInstruction() == instruction
		
	}
	
	
	def "should fail the run because the resource wasn't loaded in the first place"(){
		given :
			mockInstruction()
			
		when :
			def resultat=runner.run()
		then :
			resultat.getStatus()==GeneralStatus.ERROR
			resultat.caughtException() instanceof ResourceNotFoundException
	}
	
	def "should run normally"(){
		given :
			mockInstruction()
		
		and :
			def resource = Mock(ResourceWrapper)
			def nature1 = Mock(Nature)
			resource.getNature() >> nature1
			testRunner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"test")) >> resource
			
		and :
			testRunner.getContextManager() >> manager 
			
		and :
			def resultResource = Mock(ResourceWrapper)
			def converter = Mock(ResourceConverterHandler) 
			converter.convert(resource) >> resultResource
			manager.getConverters(_) >> [converter]

		when :
			runner.run()
		
		then :
			1 * testRunner.addResourceToCache(resultResource)
			1 * resultResource.setName(new ResourceName(Scope.SCOPE_TEST,"alias"))
		
	}
	
	def "should fail because no conversion was possible"(){
		
		given :
			mockInstruction()
		
		and :
			def resource = Mock(ResourceWrapper)
			def nature1 = Mock(Nature)
			resource.getNature() >> nature1
			resource.unwrap() >> Mock(Resource)
			testRunner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"test")) >> resource
			
		and :
			testRunner.getContextManager() >> manager 
			
		and :
			def resultResource = Mock(Resource)
			def converter = Mock(ResourceConverter) 
			converter.convert(resource) >> resultResource
			manager.getConverters(_) >> []
			
		when :
			def resultat=runner.run()
			
		then :
			resultat.getStatus()==GeneralStatus.ERROR
			resultat.caughtException() instanceof ImpossibleConversionException
	}
	
	def mockInstruction = {
		instruction.setResourceName(new ResourceName(Scope.SCOPE_TEST, "test"))
		instruction.setResultName(new ResourceName(Scope.SCOPE_TEST, "alias"))
		instruction.setResultType("nature")
	}
	
}
