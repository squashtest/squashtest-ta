/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction;
import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.LoadResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

public abstract class AbstractSahiTestCase extends AbstractCatsTestCase {

    private static final String RESOURCE_TYPE_SCRIPT_SAHI = "script.sahi";

	/**
     * @see SquashSahiRunner#run(String)
     */
    protected void runAndCheckSahiTestCase(String suiteName) throws URISyntaxException, IOException,
                    InterruptedException {
    	runAndCheckSahiTestCase(suiteName, null, null);
    }

    /**
     * @see SquashSahiRunner#run(String, String, String)
     */
    protected void runAndCheckSahiTestCase(String suiteName, String browserType, String baseUrl)
                    throws URISyntaxException, IOException, InterruptedException {
    	ResetLocalContextInstruction context=createLocalContext("runAndCheckSahiTestCase()");
    	getCallListener().addInstruction(context);
    	
		ResourceName suiteFile = new ResourceName(Scope.SCOPE_TEMPORARY, suiteName);
		LoadResourceInstruction load = createLoadInstruction(suiteName,
				suiteFile,
				"runAndCheckSahiTestCase()", "SAHI test script");
		getCallListener().addInstruction(load);
		
		ResourceName suite=new ResourceName(Scope.SCOPE_TEMPORARY,"suite");
    	ConvertResourceInstruction convert=createConvertInstruction(suiteFile, suite, "runAndCheckSahiTestCase()", CONVERT_CATEGORY_SCRIPT, RESOURCE_TYPE_SCRIPT_SAHI);
    	getCallListener().addInstruction(convert);
    	
    	List<ResourceName>configuration=new ArrayList<ResourceName>();
    	if(browserType!=null){
    		ResourceName browserTypeName = new ResourceName(Scope.SCOPE_TEMPORARY,"browserType");
			DefineResourceInstruction define=createDefineInstruction(browserTypeName, "browserType="+browserType+"\n");
    		getCallListener().addInstruction(define);
    		
    		configuration.add(browserTypeName);
    	}
    	if(baseUrl==null){//will have to be handled by some special TargetCreator...
    		baseUrl="sahi:default";
    	}
    	ExecuteCommandInstruction command=createExecuteCommandInstruction(baseUrl, suite, IGNORED_RESULT_NAME, "", configuration, "execute");
    	getCallListener().addInstruction(command);
    }

}
