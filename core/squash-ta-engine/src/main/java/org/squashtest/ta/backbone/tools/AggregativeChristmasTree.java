/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.squashtest.ta.backbone.exception.NotUniqueEntryException;

/**
 * Named so because it's implemented as a Tree and today is Christmas. 
 * 
 * @author bsiri
 *
 * @param <T>
 */
public class AggregativeChristmasTree<T> implements MultiDimensionMap<T> {

	private int dimension;
	
	private Node root = new Node(null);
	
	public AggregativeChristmasTree(int dimension){
		
		if (dimension<=0){
			throw new IllegalArgumentException("dimension cannot be null");
		}
		
		this.dimension=dimension;		
	}
	
	public int getDimension() {
		return dimension;
	}

	public void put(T element, Object... key) {
		
		//check the dimension
		if (key.length != dimension){
			throw new IllegalArgumentException("key of wrong dimension : expected dimension "+dimension);
		}
		
		//check that ANY is not part of the key set
		for (Object o : key){
			if (o==ANY){
				throw new IllegalArgumentException("ANY is not a valid key element for this operation");
			}
		}
		
		//is the object already in that map ? sorry for the overhead btw.
		Collection<T> elts= get(key);
		if (elts.size()>0){
			throw new NotUniqueEntryException("cannot insert new element : an element corresponding to the same coordinates already exists");
		}
		
		//let's insert the element
		Node currentNode = root;
		for (Object k : key){
			Node child = currentNode.findChildren(k);
			if (child == null){
				child = new Node(k);
				currentNode.children.add(child);
			}
			currentNode=child;
		}
		//at this point we finished our walk down the tree
		Node newElement = new Node(element);
		currentNode.children.add(newElement);
		
	}

	public void remove(Object... key) {
		//check the dimension
		if (key.length != dimension){
			throw new IllegalArgumentException("key of wrong dimension : expected dimension "+dimension);
		}
		
		SearchAction<T> action = new RemoveAction<T>();
		
		nextNode(root, action, 0, key);
		
	}

	public Collection<T> get(Object... key) {
		//check the dimension
		if (key.length != dimension){
			throw new IllegalArgumentException("wrong key dimension ("+key.length+"): expected dimension "+dimension);
		}
		
		SearchAction<T> action = new CollectAction<T>();
		
		return nextNode(root, action, 0, key);		
	}
	
	
	/* *************** private methods *********************** */
	
	/**
	 * that interface tells what to do when we finished to explore the map 
	 * 
	 * @author bsiri
	 *
	 */
	private static interface SearchAction<T>{
		Collection<T> doWith(Node node);
	}
	
	private static class CollectAction<T> implements SearchAction<T>{

		public Collection<T> doWith(Node node) {
			List<T> result = new ArrayList<T>(1);
			if (node.children.size()>0){
				Node elt = node.children.get(0);
				result.add((T)elt.key);
			}
			return result;
		}	
	}
	
	private static class RemoveAction<T> implements SearchAction<T>{
		public Collection<T> doWith(Node node) {
			if (node.children.size()>0){
				node.children.remove(0);
			}
			return Collections.emptyList();
		}	
	}
	
	/**
	 * it will walk down the tree, eating chunks of the key for each step down.
	 * 
	 * @param currentNode the node currently being explored
	 * @param action what to do once the node was found.
	 * @param indexInMultiKey the index of the current part of the whole key we are considering now 
	 * @param wholeMultiKey the multikey itself.
	 * @return
	 */
	private Collection<T> nextNode(Node currentNode, SearchAction<T> action, int indexInMultiKey, Object... wholeMultiKey){		
		if (indexInMultiKey==wholeMultiKey.length){
			return action.doWith(currentNode);
		}else{
			Collection<T> resultList = new LinkedList<T>();
			Object currentKey = wholeMultiKey[indexInMultiKey];
			
			if ( currentKey == MultiDimensionMap.ANY){
				
				for (Node child : currentNode.children){
					resultList.addAll(nextNode(child, action, indexInMultiKey+1, wholeMultiKey));
				}
			}else{
				Node child = currentNode.findChildren(currentKey);
				if (child!=null){
					resultList =nextNode(child, action, indexInMultiKey+1, wholeMultiKey); 
				}
			}
			return resultList;
			
		}
	}
	
	
	/* ************** internal structures ********************* */
	
	private static class Node{
		private Object key;
		private ArrayList<Node> children = new ArrayList<Node>(1);
		
		public Node(Object key){
			this.key=key;
		}
		
		public Node findChildren(Object key){
			for (Node child : children){
				if ((key==null && child.key==null)||
						(child.key!=null && child.key.equals(key))){
					return child;
				}
			}
			return null;
		}
		
		
	}
	

}
