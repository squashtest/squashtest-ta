test :
LOAD selenium/html AS htmlbundle
CONVERT htmlbundle TO script.html.selenium USING $(mainpath:integration-suite.html,browser:*chrome) AS suite
EXECUTE execute WITH suite ON webSUT AS result
ASSERT result IS success