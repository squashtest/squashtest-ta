/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.command.ta;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import org.squashtest.installer.command.ProcessCommand;
import org.squashtest.installer.context.Context;
import org.squashtest.installer.tools.FileLocatorUtils;


/**
 * Importing Eclipse user preferences requires to run Eclipse in headless mode and call its Ant Runner.
 * 
 * The Ant build will run a task defined in an Eclipse plugin written by IBM.
 * The said plugin is com.ibm.rational.support.anttasks.epf.preferencesmanager_<version>.jar. Be 
 * sure that this jar is present in the plugins/ folder in Eclipse. 
 * For convenience the said jar is embedded in this installer tool but it's up to the calling class
 * to move that jar in plugins/
 * 
 * @param preferenceFile the preferences file we want to import.
 * @param userWorkspaceFile the workspace we want to configure.
 * @param squashDirectoryFile the directory were the Squash TA toolbox lies in.
 * @return a command object that will perform the import.
 * @throws IOException 
 * @throws URISyntaxException 
 */
public class ImportPreferencesCommand extends ProcessCommand {

	File antBuildfile;
	File userWorkspaceFile; 
	File eclipseDirectory;
	
	FileLocatorUtils locatorUtils;
	
	public ImportPreferencesCommand(){
		
	}
	
	public ImportPreferencesCommand(Context context) throws URISyntaxException, IOException{
		
		locatorUtils = new FileLocatorUtils(context);
		
		this.antBuildfile=new File(context.getWorkingDirectory(), context.getProperty("CONFIGURED_ANT_BUILD"));
		this.userWorkspaceFile=context.getOutputDirectory();
		this.eclipseDirectory=locatorUtils.findEclipseDirectory();
	}
	
	@Override
	public void execute(){
		try{
			init();
			super.execute();
		}catch(IOException ioe){
			throw new RuntimeException("TA workspace installer : unreachable or malformed eclipse.ini", ioe);
		}
	}


	public void setAntBuildfile(File antBuildfile) {
		this.antBuildfile = antBuildfile;
	}

	public void setUserWorkspaceFile(File userWorkspaceFile) {
		this.userWorkspaceFile = userWorkspaceFile;
	}

	public void setEclipseDirectory(File eclipseDirectory) {
		this.eclipseDirectory = eclipseDirectory;
	}


	private void init() throws MalformedURLException, IOException{
		//find the jar we need to start	
		File launcher = findEclipseLauncher();
		
		
		List<String> command = new LinkedList<String>();
		
		command.add("java");
		command.add("-classpath");
		command.add(launcher.getCanonicalPath());
		command.add("org.eclipse.core.launcher.Main");
		command.add("-data");
		command.add(userWorkspaceFile.getCanonicalPath());
		command.add("-application");
		command.add("org.eclipse.ant.core.antRunner");
		command.add("-buildfile");
		command.add(antBuildfile.getCanonicalPath());
		
		addAllTokens(command);
		
		setSynchronous(true);

	}
	
	/* ******************* private code, no need to include them in the interface. **************** */
	

	/*
	 * a convenient way to find the said jar is to parse eclipse.ini
	 * 
	 */
	protected File findEclipseLauncher() throws IOException{
		
		File eclipseIni = new File(eclipseDirectory, "eclipse.ini");
		BufferedReader reader = new BufferedReader(new FileReader(eclipseIni));
		

		String buffer;
		String jarName;
		StringBuilder iniContent = new StringBuilder();
		
		//we'll first modify the structure of the file to a canonical form
		while ((buffer=reader.readLine())!=null){
			iniContent.append(" "+buffer);
		}
		reader.close();
		
		String[] tokenizedContent = iniContent.toString().split("\\s+");
		
		//now we can parse it
		ListIterator<String> iterator = Arrays.asList(tokenizedContent).listIterator();
		
		try{
			jarName=null;
			while (iterator.hasNext()){
				String next = iterator.next();
				if (next.equals("-startup")){
					jarName=iterator.next();
					break;
				}
			}
			if (jarName==null) throw new NoSuchElementException();
		}catch(NoSuchElementException nsee){
			throw new RuntimeException("TA workspace installer : user preferences import failed : launcher jar not specified in eclipse.ini");
		}
		
		//now let's open a file on that jar.
		return new File(eclipseDirectory, jarName);
	}


}
