/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.factory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.legacy.exception.MalformedCatsClassException;

/**
 * This component generates tests with their phases and instructions. Please
 * note that this object is stateful, and is therefore <strong>NOT
 * thread-safe</strong>.
 * 
 * @author edegenetais
 * 
 */
public class LegacyClassTestFactory {
	
	private static final Object[] NOARGS=new Object[0];
	
	/** Instruction listener as private member for testability */
	private LegacyCatsCallListener listener=new LegacyCatsCallListener();
	
	/**
	 * This methods build a test using a method and test case instance.
	 * @param method the method to translate.
	 * @param instance the test case instance.
	 * @return the built test.
	 * @throws MalformedCatsClassException if the method is some way not a legal CATS test method
	 */
	public Test buildTest(Method method, AbstractCatsTestCase instance){
		if(method.getReturnType()!=Void.TYPE){
			throw new MalformedCatsClassException("Test method "+method.toGenericString()+" should have void return type.");
		}
		if(method.getParameterTypes().length>0){
			throw new MalformedCatsClassException("Test method "+method.toGenericString()+" should take no arguments.");
		}
		Test test=new Test();
		test.setName(method.getName());
		
		instance.setCallListener(listener);
		
		List<TestInstruction> testInstructions = translate(method, instance);
		
		for(Method phaseCandidate:instance.getClass().getMethods()){
			if(phaseCandidate.isAnnotationPresent(Before.class)){
				test.addToSetup(translate(phaseCandidate, instance));
			}else if(phaseCandidate.isAnnotationPresent(After.class)){
				test.addToTeardown(translate(phaseCandidate, instance));
			}
		}
		
		test.addToTests(testInstructions);
		
		test.addToSetup(instance.getImplicitSetup());
		test.addToTeardown(instance.getImplicitTearDown());
		
		return test;
	}

	private List<TestInstruction> translate(Method method,
			AbstractCatsTestCase instance) {
		List<TestInstruction> testInstructions=new ArrayList<TestInstruction>();
		
		try {
			method.invoke(instance, NOARGS);
		} catch (IllegalArgumentException e) {
			//should never happen, since we only take noarg methods.
			throw e;
		} catch (IllegalAccessException e) {
			throw new MalformedCatsClassException("Cats test methods have to be public methods", e);
		} catch (InvocationTargetException e) {
			throw new MalformedCatsClassException("Some error occurred during test method translation", e);
		}
		
		testInstructions.addAll(listener.getGeneratedInstructions());
		listener.reset();
		return testInstructions;
	}
}
