/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.framework.components;

import java.util.Properties;

import org.squashtest.ta.framework.annotations.ResourceType;


/**
 * An instance of VoidTarget will be automatically created. The name of that special Target is <em>builtin:void</em>, as defined by {@link #INSTANCE_NAME}.
 * 
 * @author bsiri
 */
@ResourceType("void.target")
public class VoidTarget implements Target {
	
	public static final String INSTANCE_NAME = "builtin:void";

	@Override
	public void init() {}

	@Override
	public void reset() {}

	@Override
	public void cleanup() {}

	@Override
	public Properties getConfiguration() {
		return new Properties();
	}

}
