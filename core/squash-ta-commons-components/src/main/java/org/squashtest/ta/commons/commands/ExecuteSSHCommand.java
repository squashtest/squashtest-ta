/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.commands;

import java.io.IOException;
import java.util.Collection;

import org.squashtest.ta.commons.library.shell.ShellOptionReader;
import org.squashtest.ta.commons.library.shell.ShellResult;
import org.squashtest.ta.commons.library.ssh.SquashSSHClient;
import org.squashtest.ta.commons.resources.CommandLineResource;
import org.squashtest.ta.commons.resources.ShellResultResource;
import org.squashtest.ta.commons.targets.SSHTarget;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * Command to execute a command line over SSH.
 * @author edegenetais
 *
 */
@EngineComponent("execute")
public class ExecuteSSHCommand implements
		Command<CommandLineResource, org.squashtest.ta.commons.targets.SSHTarget> {
	
	private SSHTarget target;
	private CommandLineResource commandLine;
	private ShellOptionReader optionReader=new ShellOptionReader("SSHexecuteCommand");
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		optionReader.addConfiguration(configuration);
	}

	@Override
	public void setTarget(SSHTarget target) {
		this.target=target;
	}

	@Override
	public void setResource(CommandLineResource resource) {
		this.commandLine=resource;
	}

	@Override
	public ShellResultResource apply() {
		ShellResultResource result;
		SquashSSHClient client=target.getClient();
		String command=commandLine.getCommand();
		Integer timeout=optionReader.getTimeout();
		if(timeout==null){
			timeout=commandLine.getTimeout();
		}
		ShellResult output;
		try {
			output = client.runSSHCommand(command,timeout);
		} catch (IOException e) {
			throw new InstructionRuntimeException("Executing command "+command+" over SSH failed", e);
		}
		result=new ShellResultResource(output);
		return result;
	}

	@Override
	public void cleanUp() {
		//noop: no resource held
	}

}
