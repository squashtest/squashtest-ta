/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Delegate java file manager to direct output files to our decided storage place and register them on the fly.
 * @author edegenetais
 *
 */
public class PlacementJavaFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {

	private File outputLocation;
	
	private static final Logger LOGGER=LoggerFactory.getLogger(PlacementJavaFileManager.class);
	
	private static final BundleFilePlacementAlgorithm FILE_PLACEMENT_ALGORITHM=new BundleFilePlacementAlgorithm();
	
	private Set<String> registeredClassNames=new HashSet<String>();
	
	/**
	 * @return an unmodifiable Set with registered class names.
	 */
	public Set<String> getRegisteredClassNames() {
		return Collections.unmodifiableSet(registeredClassNames);
	}

	/**
	 * Create a java file manager instance to override fileManager class files placing behavior.
	 * @param fileManager the delegate fileManager to use.
	 * @param bundleClassLocation the class file location.
	 */
	public PlacementJavaFileManager(StandardJavaFileManager fileManager, File bundleClassLocation) {
		super(fileManager);
		if(bundleClassLocation.isDirectory()){
			outputLocation=bundleClassLocation;
		}else{
			throw new IllegalArgumentException(bundleClassLocation.getAbsolutePath()+" isn't a valid directory.");
		}
	}

	@Override
	public JavaFileObject getJavaFileForOutput(Location location,
			String className, Kind kind, FileObject sibling)
			throws IOException {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Java file for output: "+location.getName()+" kind: "+kind+" class name: "+className+" sibling: "+sibling.getName());
		}
		
		JavaFileObject objectFile=new PlacementJavaFileObject(FILE_PLACEMENT_ALGORITHM.getElementURI(outputLocation, className, kind), Kind.CLASS);
		registeredClassNames.add(className);
		return objectFile;
	}
}
