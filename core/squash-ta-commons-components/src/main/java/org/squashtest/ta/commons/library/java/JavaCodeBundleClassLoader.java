/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.java;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.tools.JavaFileObject.Kind;

import org.squashtest.ta.core.tools.io.BinaryData;

public class JavaCodeBundleClassLoader extends ClassLoader {
	private static final List<URL> EMPTY_URL_LIST = Collections.emptyList();
	private static final BundleFilePlacementAlgorithm BUNDLE_FILE_PLACEMENT_ALGORITHM = new BundleFilePlacementAlgorithm();
	/** Base file for the code bundle. */
	private File bundleBase;

	/**
	 * @param bundleBase
	 */
	public JavaCodeBundleClassLoader(File bundleBase) {
		super(Thread.currentThread().getContextClassLoader());
		this.bundleBase = bundleBase;
	}

	/**
	 * We want to isolate this from the engine environment, so we load the
	 * classes from our repository BEFORE searching for them in our parent.
	 */
	@Override
	protected synchronized Class<?> loadClass(String name, boolean resolve)
			throws ClassNotFoundException {
		Class<?> foundClass = findLoadedClass(name);
		if (foundClass == null) {
			try {
				foundClass = findClass(name);
			} catch (ClassNotFoundException e) {
				ClassLoader parent = getParent();
				if (parent == null) {
					foundClass = findSystemClass(name);
				} else {
					foundClass = parent.loadClass(name);
				}
			}
		}
		if (resolve) {
			resolveClass(foundClass);
		}
		return foundClass;
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		File file = BUNDLE_FILE_PLACEMENT_ALGORITHM.getElementFile(
				bundleBase, name, Kind.CLASS);
		if (file.exists()) {
			try {
				BinaryData classData=new BinaryData(file);
				byte[] classDataBytes = classData.toByteArray();
				return defineClass(name, classDataBytes,0,classDataBytes.length);
			}catch(ClassFormatError e){
				//we want this to get the problem reported, not to kill the engine thread...
				throw new JavaBundleClassLoadingException("Corrupt class data in JavaCodeBundle", e);
			} catch (IOException e) {
				// should never happen
				throw new JavaBundleClassLoadingException("Class file for '"+name+"' exists, but cannot be loaded.", e);
			}
		} else {
			throw new ClassNotFoundException(name);
		}
	}
	
	@Override
	protected URL findResource(String name) {
		URL resourceURL;
		File resourceFile=BUNDLE_FILE_PLACEMENT_ALGORITHM.getElementFile(bundleBase, name, Kind.OTHER);
		if(resourceFile.exists()){
			try {
				resourceURL=resourceFile.toURI().toURL();
			} catch (MalformedURLException e) {
				throw new JavaBundleClassLoadingException("Placement algorithm generated a malformed URL", e);
			}
		}else{
			resourceURL=null;
		}
		return resourceURL;
	}
	
	@Override
	protected Enumeration<URL> findResources(String name) throws IOException {
		URL foundResource = findResource(name);
		List<URL> foundResourceSet;
		if(foundResource==null){
			foundResourceSet=EMPTY_URL_LIST;
		}else{
			foundResourceSet = new ArrayList<URL>(1);
			foundResourceSet.add(foundResource);
		}
		return Collections.enumeration(foundResourceSet);
	}
}
