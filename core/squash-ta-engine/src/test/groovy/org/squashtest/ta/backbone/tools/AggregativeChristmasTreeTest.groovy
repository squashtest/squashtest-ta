/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.tools

import java.util.Collection

import org.squashtest.ta.backbone.tools.AggregativeChristmasTree;
import org.squashtest.ta.backbone.tools.MultiDimensionMap;
import org.squashtest.ta.framework.components.ResourceRepository
import org.squashtest.ta.backbone.engine.wrapper.Nature
import org.squashtest.ta.backbone.exception.NotUniqueEntryException;

import spock.lang.Specification

class AggregativeChristmasTreeTest extends Specification {

	
	def key1 = "key1"
	def key2 = "key2"
	def key3 = "key3"
	def key4 = "key4"
	def key5 = "key5"
	def key6 = "key6"

	def elt1 = "elt1"
	def elt2 = "elt2"
	def elt3 = "elt3"
	def elt4 = "elt4"
	def elt5 = "elt5"

	def "should rant if dimension at construction is <=0"(){
		
		when :
			new AggregativeChristmasTree(0);
			
		then :
			thrown IllegalArgumentException;
		
	}
	
	def "should return the dimension"(){
		
		when :
			def map = new AggregativeChristmasTree(10);
			def res = map.getDimension();
			
		then :
			res == 10
		
	}
	
	
	def "should rant because we try to insert an element with a key of wrong dimension"(){
		
		when :
			def map = new AggregativeChristmasTree(3);
			map.put(new Object(), "test");
			
		then :
			thrown IllegalArgumentException
		
	}
	
	def "should rant because ANY is not valid in insert key"(){
		
		when :
			def map = new AggregativeChristmasTree(3);
			map.put(new Object(), "test", MultiDimensionMap.ANY, "reretest");
		then :
			thrown IllegalArgumentException
	}
	
	
	def "should insert an element"(){
		
		given :
			def key1 = new Object()
			def key2 = new Integer(2)
			def key3 = new String("test")
			
		and :
			def element = Mock(ResourceRepository);
			def map = new AggregativeChristmasTree(3);
		
		when :
			map.put(element, [key1, key2, key3] as Object[])
		then :
			
			def elt = map.root.children[0].children[0].children[0].children[0].key
						 
			elt == element
	}
	
	def "should get an inserted element"(){
		
		given :
			def key1 = new Object()
			def key2 = new Integer(2)
			def key3 = new String("test")
		
		and :
			def element = Mock(ResourceRepository);
			def map = new AggregativeChristmasTree(3);
			
		when :
			map.put(element, key1, key2, key3)
			def res = map.get(key1, key2, key3)
			
		then :
			res.contains element
		
	}
		
	def "snould refuse to insert two element at the same coordinates"(){
		
		given :
			def key1 = new Object()
			def key2 = new Integer(2)
			def key3 = new String("test")
			
		and :
			def elt1 = Mock(ResourceRepository);
			def elt2 = new Nature("test");
			def map = new AggregativeChristmasTree(3)
		
		when :
			map.put(elt1, key1, key2, key3)
			map.put(elt2, key1, key2, key3)
		
		then :
			thrown NotUniqueEntryException
	}
	
	def "should return all the elements matching key using ANY wildcard"(){

		
		given :
			def map = new AggregativeChristmasTree(4);
			map.put(elt1, key1, key3, key2, key5)
			map.put(elt2, key2, key6, key3, key4)
			map.put(elt3, key6, key2, key1, key5)
			map.put(elt4, key3, key1, key5, key3)
			map.put(elt5, key2, key6, key2, key1)
			
		when :
			def res = map.get(key2, key6, MultiDimensionMap.ANY, , MultiDimensionMap.ANY)
			
		then :
			res.size() == 2
			res.containsAll([elt2, elt5])
				
	}
	
	def "should return all the element matching key using ANY wildcard (2)"(){
		
		given :
			def map = new AggregativeChristmasTree(4);
			map.put(elt1, key1, key3, key2, key5)
			map.put(elt2, key2, key6, key3, key4)
			map.put(elt3, key6, key3, key1, key5)
			map.put(elt4, key3, key1, key5, key3)
			map.put(elt5, key2, key6, key2, key1)
			
		when :
			def result = map.get(MultiDimensionMap.ANY, key3,
				MultiDimensionMap.ANY, key5)
			
		then :
			result.size() == 2
			result.containsAll([elt1, elt3])
	}
	
	def "should return the complete map becayse we used ANY everywhere"(){
		given :
			def map = new AggregativeChristmasTree(4);
			map.put(elt1, key1, key3, key2, key5)
			map.put(elt2, key2, key6, key3, key4)
			map.put(elt3, key6, key3, key1, key5)
			map.put(elt4, key3, key1, key5, key3)
			map.put(elt5, key2, key6, key2, key1)
			
		when :
			def result = getAll(map)
		then :
			result.size() == 5
			result.containsAll([elt1, elt2, elt3, elt4, elt5])
		
	}
	
	def "should remove one element"(){
		
		given :
			def map = new AggregativeChristmasTree(4);
			map.put(elt1, key1, key3, key2, key5)
			map.put(elt2, key2, key6, key3, key4)
			map.put(elt3, key6, key3, key1, key5)
			map.put(elt4, key3, key1, key5, key3)
			map.put(elt5, key2, key6, key2, key1)
			
		when :
			map.remove(key3, key1, key5, key3);
			def res = getAll(map)
			
		then :
			res.size()==4
			res.containsAll([elt1, elt2, elt3, elt5])
	}
	
	def "should remove a several element because of ANY"(){
		
		given :
			def map = new AggregativeChristmasTree(4);
			map.put(elt1, key2, key3, key2, key5)
			map.put(elt2, key2, key6, key3, key4)
			map.put(elt3, key6, key3, key3, key5)
			map.put(elt4, key3, key1, key3, key3)
			map.put(elt5, key2, key4, key3, key1)
			
		when :
			map.remove(key2, MultiDimensionMap.ANY, key3, MultiDimensionMap.ANY)
			def res = getAll(map)
			
		then :
			res.size() == 3
			res.containsAll([elt1, elt3, elt4])
		
	}
	
	def "should remove all the elements due to ANY"(){
		given :
			def map = new AggregativeChristmasTree(4);
			map.put(elt1, key2, key3, key2, key5)
			map.put(elt2, key2, key6, key3, key4)
			map.put(elt3, key6, key3, key3, key5)
			map.put(elt4, key3, key1, key3, key3)
			map.put(elt5, key2, key4, key3, key1)
		
		when :
			map.remove(MultiDimensionMap.ANY, MultiDimensionMap.ANY, MultiDimensionMap.ANY, MultiDimensionMap.ANY)
			def res = getAll(map)
			
		then :
			res.size()==0
			
	}

	private Collection getAll(MultiDimensionMap map){
		return 	map.get(MultiDimensionMap.ANY,
					 MultiDimensionMap.ANY,
					 MultiDimensionMap.ANY,
					 MultiDimensionMap.ANY)
	}
	
}

