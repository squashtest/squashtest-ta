/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.repositories;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.squashtest.ta.commons.library.ftp.SimpleFTPClient;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;


@ResourceType("ftp.repository")
public class FTPRepository implements ResourceRepository{

	private SimpleFTPClient client;
	private boolean useCache = false;
	
	public  static final String FTP_USE_CACHE = "squashtest.ta.ftp.useCache";
	
	private Map<String, File> cache = new HashMap<String, File>();
	
	public FTPRepository(){
		super();
	}
	
	public FTPRepository(SimpleFTPClient client, boolean useCache){
		super();
		this.client=client;
		this.useCache=useCache;
	}
	
	@Override
	public void init() {
		client.init();
	}

	@Override
	public void reset() {
		client.reset();
	}

	@Override
	public void cleanup() {
		client.cleanup();
		for (File file : cache.values()){
			if (file.exists()){
				file.delete();
			}
		}
		cache.clear();
	}

	@Override
	public Properties getConfiguration() {
		Properties properties = client.getConfiguration();
		Properties newProperties = (Properties)properties.clone();
		newProperties.setProperty(FTP_USE_CACHE, Boolean.toString(useCache));
		return newProperties;
	}

	@Override
	public FileResource findResources(String name) {
		File file;
		
		if (useCache && cache.containsKey(name)){
			file = cache.get(name);
		}else{
			file = download(name);
		}
		
		return (file!=null) ? new FileResource(file) : null;	//valid per contract
		
	}
	
	private File download(String name){

		File local = client.getFile(name);

    	if (useCache){
    		cache.put(name, local);
    	}
		return local;
	}

}
