/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.tools;

import java.util.Collection;

/**
 * 
 * <p>This multi key map allows wilcards in the keys to match any known value of it. </p>
 * 
 * <p>Say you have an instance using 3 keys. The get() operation will then accept three keys as parameters. 
 * 	But when you provide only two keys and null for the third, a standard implementation will look up for an object
 * 	having null as its key. 
 * </p>
 * 
 * <p>Consider this map as a multidimensional structure, and each key represents a dimension. In this context, you may
 * supply wildcards instead of some keys : any entry partially mapped by the non-wildcards key will match the key just like
 * if the wildcard keys had an actual value. In other words, if the Map is a space, we can query for hyperplans of it.</p>
 * 
 * <p>For instance, if you consider a map of dimension 3 (x are wildcards)</p>
 * 
 * <ul>
 * 	<li>map.get(A, B, C) returns one element or null.</li>
 * 	<li>map.get(A, x, C) returns a collection of elements belonging to the map and that are mapped by A, B and a third coordinate.
 * 	<li>map.get(x, x, x) returns a flatten collection of all the elements of the map. 
 * </ul>
 *  
 *  <p>The elements are returned in any order. The dimension is specified as Constructor argument.</p>
 *  
 * @author bsiri
 *
 */
public interface MultiDimensionMap<T> {
	
	public static final Object ANY = new Object();

	/**
	 * 
	 * @return the dimension this map was initialized to.
	 */
	int getDimension();
	
	/**
	 * Will insert T at the specified location. No type safety regarding the keys, and they must be provided
	 * in the correct order. If the number of keys does not fit the map dimension exactly, 
	 * an exception will be thrown. Does not accept ANY as a valid argument for keys.
	 * 
	 * @param element
	 * @param key
	 */
	void put(T element, Object... key );
	
	/**
	 * remove all the elements that would be returned by get(...) with the same key set.
	 * 
	 * @param key
	 */
	void remove(Object... key);
	
	/**
	 * read class specification for more details.
	 * 
	 */
	Collection<T> get(Object... key);
}
