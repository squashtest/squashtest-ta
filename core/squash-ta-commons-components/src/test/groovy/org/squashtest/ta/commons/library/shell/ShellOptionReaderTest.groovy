/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.shell

import org.squashtest.ta.commons.resources.DirectoryResource;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;

import spock.lang.Specification;

class ShellOptionReaderTest extends Specification{
	ShellOptionReader optionReader;
	
	def setup(){
		optionReader=new ShellOptionReader("")
	}
	
	def "no configuration means no timeout"(){
		when:
			def timeout=optionReader.getTimeout()
		then:
			timeout==null
	}
	
	def "should find option"(){
		given:
			SimpleLinesData conf=new SimpleLinesData(["timeout:26000"])
			File file=File.createTempFile("test", ".conf")
			file.deleteOnExit()
			conf.write(file)
		and:
			FileResource confResource=new FileResource(file)
		when:
			optionReader.addConfiguration([confResource])
			Integer timeout=optionReader.getTimeout()
		then:
			timeout==26000
	}
	
	def "should find options in other resources"(){
		given:
			SimpleLinesData conf=new SimpleLinesData(["timeout:26000"])
			File file=File.createTempFile("test", ".conf")
			file.deleteOnExit()
			conf.write(file)
		and:
			FileResource confResource=new FileResource(file)
		when:
			optionReader.addConfiguration([
				new DirectoryResource(file.getParentFile()),
				confResource,
				new DirectoryResource(file.getParentFile())
				])
			Integer timeout=optionReader.getTimeout()
		then:
			timeout==26000
	}
	
	def "ignore irrelevant options, but find timeout"(){
		given:
			SimpleLinesData conf=new SimpleLinesData(["toto:nill,timeout:26000,use:no"])
			File file=File.createTempFile("test", ".conf")
			file.deleteOnExit()
			conf.write(file)
		and:
			FileResource confResource=new FileResource(file)
		when:
			optionReader.addConfiguration([confResource])
			Integer timeout=optionReader.getTimeout()
		then:
			timeout==26000
	}
	
	def "illegal (<0) timeout value triggers IllegalConfigurationException"(){
		given:
			SimpleLinesData conf=new SimpleLinesData(["toto:nill,timeout:-26000,use:no"])
			File file=File.createTempFile("test", ".conf")
			file.deleteOnExit()
			conf.write(file)
		and:
			FileResource confResource=new FileResource(file)
		when:
			optionReader.addConfiguration([confResource])
			Integer timeout=optionReader.getTimeout()
		then:
			thrown(IllegalConfigurationException)
	}
	
	def "illegal (0) timeout value triggers IllegalConfigurationException"(){
		given:
			SimpleLinesData conf=new SimpleLinesData(["toto:nill,timeout:0,use:no"])
			File file=File.createTempFile("test", ".conf")
			file.deleteOnExit()
			conf.write(file)
		and:
			FileResource confResource=new FileResource(file)
		when:
			optionReader.addConfiguration([confResource])
			Integer timeout=optionReader.getTimeout()
		then:
			thrown(IllegalConfigurationException)
	}
	
	
}
