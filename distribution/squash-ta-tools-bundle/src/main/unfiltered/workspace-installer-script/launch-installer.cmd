@echo off

SET JAR=%~1
SET TA_DIRECTORY=%~2
SET WKS=%~3

echo
echo JAR : %JAR%
echo
echo TA_DIRECTORY : %TA_DIRECTORY%
echo
echo WORKSPACE : %WKS% 
echo
echo Start installation
echo

java -jar "%JAR%" -ta-directory "%TA_DIRECTORY%" -dest "%WKS%"

echo
echo Installation done