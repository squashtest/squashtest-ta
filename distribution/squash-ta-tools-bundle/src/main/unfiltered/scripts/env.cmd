@echo off

cd ..

:: Positionnement des variables d'environnement

set SQUASH_TA_HOME=%cd%
set SQUASH_TA_DRIVE=%SQUASH_TA_HOME:~0,2%

set JAVA_HOME=%SQUASH_TA_HOME%\${tools.jdk.name}
set MVN_HOME=%SQUASH_TA_HOME%\${tools.maven.name}
set SAHI_HOME=%SQUASH_TA_HOME%\${tools.sahi.name}\userdata\bin
set ECLIPSE_HOME=%SQUASH_TA_HOME%\${tools.eclipse.name}
set JAILER_HOME=%SQUASH_TA_HOME%\${tools.jailer.name}
set SELENIUM_HOME=%SQUASH_TA_HOME%\${tools.selenium.name}

:: Ajout des variables d'environnement au path
path=%JAVA_HOME%\bin;%MVN_HOME%\bin;%ECLIPSE_HOME%;%path%

:: Impression
echo [INFO] ------------------------------------------------------------------------
echo [INFO] Mise en place des variables d'environnement
echo [INFO] ------------------------------------------------------------------------
echo [INFO] SQUASH_TA_HOME = %SQUASH_TA_HOME%
echo [INFO] JAVA_HOME = %JAVA_HOME%
echo [INFO] MVN_HOME = %MVN_HOME%
echo [INFO] SAHI_HOME = %SAHI_HOME%
echo [INFO] ECLIPSE_HOME = %ECLIPSE_HOME%
echo [INFO] JAILER_HOME = %JAILER_HOME%