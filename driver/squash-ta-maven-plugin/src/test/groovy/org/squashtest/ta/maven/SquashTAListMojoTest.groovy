/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Assert;

import org.cubictest.export.exceptions.ExporterException;
import org.dbunit.dataset.datatype.BigIntegerDataType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.commons.exporter.surefire.SurefireSuiteResultExporter;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.TempUnzipper;
import org.squashtest.ta.maven.SquashTATestListMojo;

import spock.lang.Specification;

public class SquashTAListMojoTest extends Specification{
	SquashTATestListMojo testee
	Pattern testPattern
	Pattern suitePattern
	
	def setup(){
		testee=new SquashTATestListMojo()
		testPattern=Pattern.compile("<testcase time=\"0\\.0\" classname=\"([^\"]*)\" name=\"([^\"]*)\" />")
		suitePattern=Pattern.compile("<testsuite time=\"0\\.0\" failures=\"([0-9]*)\" tests=\"([0-9]*)\" name=\"([^\"]*)\" >");
	}
	
	def "get expected list"(){
		given:
			testee.exporters=[new SurefireSuiteResultExporter()]
		and:
			File testTemp=new TempUnzipper().unzipInTemp(getClass().getResourceAsStream("sampleTests.zip"));
			testee.setTestsDirectory(new File(testTemp,"tests"))
		and:
			File outputDir=new FileTree().createTempDirectory()
			testee.setOutputDirectory(outputDir);
		when:
			testee.execute()
		then:
			//two output files
			new File(outputDir,"surefire-reports").listFiles(new FilenameFilter(){
				boolean accept(File arg0, String arg1) {
					arg1.endsWith(".xml")
				};
			}).length==2
		
			BinaryData actual=new BinaryData(new File(outputDir,"surefire-reports/TEST-tests.xml"))
			getSuiteName(actual)=="tests"
			getFailures(actual)==0
			getTests(actual)==1
			getClassNames(actual)==new HashSet(["tests"])
			getTestNames(actual)==new HashSet(["ssh.txt"])
						
			BinaryData actualInner=new BinaryData(new File(outputDir,"surefire-reports/TEST-tests.ssh.xml"))
		getSuiteName(actualInner)=="tests.ssh"
		getFailures(actualInner)==0
		getTests(actualInner)==7
		getClassNames(actualInner)==new HashSet(["tests.ssh"])
		getTestNames(actualInner)==new HashSet([
			"ssh.txt",
			"ssh_errorcode_check.txt",
			"ssh_macro.txt",
			"ssh_stream_contains.txt",
			"ssh_stream_not_contains.txt",
			"ssh_timout.txt",
			"ssh_timout_macro.txt"
		])
		
		cleanup:
			new TempUnzipper().clean(testTemp)
			new FileTree().clean(outputDir)
	}
	
	def Set getTestNames(BinaryData data){
		Matcher matcher=testPattern.matcher(normalizedNewlineStringContent(data))
		Set<String> testNames=new HashSet<String>()
		while(matcher.find()){
			testNames.add(matcher.group(2))
		}
		return testNames
	}
	
	def Set getClassNames(BinaryData data){
		Matcher matcher=testPattern.matcher(normalizedNewlineStringContent(data))
		Set<String> classNames=new HashSet<String>()
		while(matcher.find()){
			classNames.add(matcher.group(1))
		}
		return classNames
	}
	
	def int getFailures(BinaryData data){
		Matcher matcher=suitePattern.matcher(normalizedNewlineStringContent(data))
		Integer failures
		if(matcher.find()){
			failures=Integer.parseInt(matcher.group(1))
		}else{
			Assert.fail("Missing failure attribute")
		}
		if(matcher.find()){
			Assert.fail("Too many suite elements:"+matcher.group())
		}
		return failures
	}
	
	def int getTests(BinaryData data){
		Matcher matcher=suitePattern.matcher(normalizedNewlineStringContent(data))
		Integer tests
		if(matcher.find()){
			tests=Integer.parseInt(matcher.group(2))
		}else{
			Assert.fail("Missing test attribute")
		}
		if(matcher.find()){
			Assert.fail("Too many suite elements:"+matcher.group())
		}
		return tests
	}
	
	
	
	def String getSuiteName(BinaryData data){
		Matcher matcher=suitePattern.matcher(normalizedNewlineStringContent(data))
		String name		
		if(matcher.find()){
			name=matcher.group(3)
		}else{
			Assert.fail("Missing name attribute")
		}
		if(matcher.find()){
			Assert.fail("Too many suite elements:"+matcher.group())
		}
		return name
	}
	
	def String normalizedNewlineStringContent(BinaryData data){
		byte[] rawData=data.toByteArray()
		String wildString=new String(rawData)
		String normalizedString=wildString.replace("\\r", "")
		return normalizedString;
	}
}
