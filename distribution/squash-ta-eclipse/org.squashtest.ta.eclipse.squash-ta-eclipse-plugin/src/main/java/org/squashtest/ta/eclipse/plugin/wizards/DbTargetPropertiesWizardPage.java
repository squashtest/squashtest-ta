/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * This wizard page class lets the user enter the supported database properties.
 * @author edegenetais
 *
 */
public class DbTargetPropertiesWizardPage extends Page {
	
	private Text driverName;
	private Text url;
	private Text userName;
	private Text password;
	private Text schemaName;
	private Button enableSchemaField;
	
	public DbTargetPropertiesWizardPage() {
		super(DbTargetPropertiesWizardPage.class.getSimpleName());
		setTitle("Enter datasource properties");
		setDescription("This form lets you define the datasource properties.");
	}
	
	@Override
	public void createControl(Composite parent) {
		Composite uiContainer=createPageUIContainer(parent, 3);
		
		addLabel(uiContainer, "Database driver",2);
		driverName=createInput(uiContainer);
		addPageRule(new MandatoryRule(driverName, "Database driver", this));
		
		addLabel(uiContainer, "JDBC Url",2);
		url=createInput(uiContainer);
		url.addModifyListener(new ValidUrlRule(this, url, "jdbc"));
		addPageRule(new MandatoryRule(url, "JDBC Url", this));
		
		addLabel(uiContainer, "Username",2);
		userName=createInput(uiContainer);
		addPageRule(new MandatoryRule(userName, "Username", this));
		
		addLabel(uiContainer, "Password",2);
		password=createInput(uiContainer);
		addPageRule(new MandatoryRule(password, "Password", this));
		
		enableSchemaField = createCheck(uiContainer);
		addLabel(uiContainer, "Schema name");
		schemaName=createInput(uiContainer);
		final MandatoryRule schemaMandatoryRule = new MandatoryRule(schemaName, "Schema name", this); 
		addPageRule(schemaMandatoryRule);
		setControl(uiContainer);
		
		enableSchemaField.setSelection(false);
		schemaName.setEnabled(false);
		
		/* Add the listeners */
		enableSchemaField.addSelectionListener( new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				schemaName.setEnabled(enableSchemaField.getSelection());
				if(enableSchemaField.getSelection())	{
					schemaMandatoryRule.activate();
				} else {
					schemaMandatoryRule.deactivate();
				}
			}
			
		});
	}

	public String getDbDriverName(){
		return driverName.getText();
	}
	
	public String getDbUserName(){
		return userName.getText();
	}
	
	public String getDbPassword(){
		return password.getText();
	}
	
	public String getDbSchemaName(){
		return schemaName.getText();
	}
	
	public String getDbUrl(){
		return url.getText();
	}
	
	public boolean isSchemaNameEnable() {
		return enableSchemaField.getSelection();
	}
	
}
