/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.wrapper;

import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.test.result.ResourceGenerator;


public class TargetWrapper {

	private Nature targetNature;
	private String name;
	private Target target;
	
	public TargetWrapper(String name, Nature targetNature, Target target){
		this.name=name;
		this.targetNature=targetNature;
		this.target=target;
	}
	
	public Nature getNature() {
		return targetNature;
	}
	
	public void setTargetNature(Nature targetNature) {
		this.targetNature = targetNature;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Target unwrap() {
		return target;
	}
	
	public void wrap(Target target) {
		this.target = target;
	}
	
	public ResourceGenerator toGenerator(){
		return new GenericResourceGenerator(name, targetNature.getName(), target.getConfiguration());
	}
	
	public void cleanup(){
		target.cleanup();
	}
	
	public void reset(){
		target.reset();
	}
	
	public void init(){
		target.init();
	}
}
