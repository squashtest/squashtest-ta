/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * This class ORes filters together: if one of them accepts, the file gets in!
 * @author edegenetais
 *
 */
class CompositeFilenameFilter implements FilenameFilter {
	private List<FilenameFilter> elements=new ArrayList<FilenameFilter>();
	public CompositeFilenameFilter(List<FilenameFilter> filters) {
		this.elements.addAll(filters);
	}
	@Override
	public boolean accept(File dir, String name) {
		for(FilenameFilter element:elements){
			if(element.accept(dir, name)){
				//da cool ones get opted in
				return true;
			}
		}
		//no pity for the Ugly Duckling...
		return false;
	}
}