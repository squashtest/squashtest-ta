/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerPrintWriter extends PrintStream {
	private final Logger LOGGER;
	/**
	 * Creates a {@link PrintWriter} that directs its input into a logger.
	 * @param filename(fileName) logger name (also the (dummy) filename transmitted to the logger.
	 * @throws FileNotFoundException
	 */
	public LoggerPrintWriter(String fileName) throws FileNotFoundException {
		super(filename(fileName));//completely dummy as all output is to be redirected to the logger.
		LOGGER=LoggerFactory.getLogger(fileName);
	}

	/**
	 * Try to create a temporary filename in case some outputs leaks to the PrintWriter layer.
	 * @param fileName the requested filename.
	 * @return a temporary file build like this: "filename"+generated name part+".sysout"
	 */
	protected static String filename(String fileName) {
		String effectiveName;
		try {
			File tempFilename=File.createTempFile(fileName, ".sysout");
			tempFilename.deleteOnExit();
			effectiveName=tempFilename.getAbsolutePath();
		} catch (IOException e) {
			effectiveName=fileName;
		}
		return effectiveName;
	}

	@Override
	public void println(String s) {
		LOGGER.debug(s);
	}
}