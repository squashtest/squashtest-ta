/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter.wildcard

import org.squashtest.ta.maven.testfilter.wildcard.WildcardTestFilter;

import spock.lang.Specification;

class WildCardTestFilterTest extends Specification{
	WildcardTestFilter testee;

	//series one: explicit matches
	def "absolute explicit match"(){
		given:
			String pattern="/base/features/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def accept=testee.accept(new File(pattern).getParentFile(), new File(pattern).getName())
		then:
			accept
	}

	def "absolute explicit mismatch"(){
		given:
			String pattern="/base/features/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("/base/unwanted/file.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			!accept
	}

	def "relative explicit match"(){
		given:
			String pattern="features/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def accept=testee.accept(new File(pattern).getParentFile(), new File(pattern).getName())
		then:
			accept
	}

	//series 2: * matches
	def "absolute * match"(){
		given:
			String pattern="/base/*/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("/base/features/litteral.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			accept
	}

	def "absolute * mismatch"(){
		given:
			String pattern="/*/features/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("/base/unwanted/file.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			!accept
	}

	def "relative * match"(){
		given:
			String pattern="features/*/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("features/advanced/litteral.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			accept
	}

	def "partial * ending match"(){
		given:
			String pattern="base/feat*/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("base/features/litteral.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			accept
	}

	def "partial * beginning match"(){
		given:
			String pattern="base/*tures/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("base/features/litteral.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			accept
	}

	def "partial inner * match"(){
		given:
			String pattern="base/f*ures/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("base/features/litteral.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			accept
	}

	//series 3: ** matches
	def "absolute ** match"(){
		given:
			String pattern="/**/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("/base/features/litteral.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			accept
	}

	def "absolute ** mismatch"(){
		given:
			String pattern="/**/features/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("/base/unwanted/file.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			!accept
	}

	def "relative ** match"(){
		given:
			String pattern="features/**/litteral.txt"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def file = new File("features/advanced/litteral.txt")
			def accept=testee.accept(file.getParentFile(), file.getName())
		then:
			accept
	}

	def "test case from issue 1014 (revamped)"(){
		given:
			String pattern="foo/**/*.*"
		and:
			testee=new WildcardTestFilter(null, pattern)
		when:
			def fileShallow = new File("foo/toto.test")
			def acceptShallow=testee.accept(fileShallow.getParentFile(), fileShallow.getName())
			def fileDeep = new File("foo/bar/tutu.test")
			def acceptDeep =testee.accept(fileDeep.getParentFile(), fileDeep.getName())
		then:
			acceptShallow
	}
}
