/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.ResultPart;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;

/**
 * Default implementation for the {@link SuiteResult} interface. 
 * @author bsiri
 **/
public class DefaultSuiteResult implements SuiteResult {

	private String suiteName;
	private List<EcosystemResult> ecosystemResults = new ArrayList<EcosystemResult>();

	/**
	 * Constructor with mandatory testSuiteName initialization.
	 * 
	 * @param testSuiteName
	 */
	public DefaultSuiteResult(String testSuiteName) {
		if (testSuiteName == null) {
			throw new IllegalArgumentException("null suite name forbidden");
		}
		suiteName = testSuiteName;
	}

	/**
	 * Add a {@link TestResult} to this {@link DefaultSuiteResult}.
	 * 
	 * @param result
	 *            the {@link TestResult} to add.
	 */
	public void addTestEcosystemResult(EcosystemResult result) {
		ecosystemResults.add(result);
	}

	@Override
	public String getName() {
		return suiteName;
	}

	@Override
	public Date startTime() {
		Date minDate = new Date();
		for (EcosystemResult result : ecosystemResults) {
			if (minDate.compareTo(result.startTime()) > 0) {
				minDate = result.startTime();
			}
		}
		return minDate;
	}

	@Override
	public Date endTime() {
		Date maxDate = new Date();
		for (EcosystemResult result : ecosystemResults) {
			if (maxDate.compareTo(result.endTime()) < 0) {
				maxDate = result.endTime();
			}
		}
		return maxDate;
	}

	@Override
	public int getTotalTests() {
		int i=0;
		for(EcosystemResult r:ecosystemResults){
			i+=r.getTotalTests();
		}
		return i;
	}

	@Override
	public int getTotalSuccess() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalSuccess();
		}
		return total;
	}

	@Override
	public int getTotalWarning() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalWarning();
		}
		return total;
	}

	@Override
	public int getTotalFailures() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalFailures();
		}
		return total;
	}

	@Override
	public int getTotalErrors() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalErrors();
		}
		return total;
	}

	@Override
	public int getTotalNotRun() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalNotRun();
		}
		return total;
	}

	public GeneralStatus getStatus() {
		GeneralStatus status = GeneralStatus.SUCCESS;
		for (EcosystemResult result : ecosystemResults) {
			status = status.mostSevereStatus(result.getStatus());
		}
		return status;
	}

	@Override
	public void cleanUp() {
		for (ResultPart part : ecosystemResults){
			part.cleanUp();
		}
	}

	@Override
	public List<? extends EcosystemResult> getSubpartResults() {
		return ecosystemResults;
	}

	@Override
	public int getTotalPassed() {
		int totalPassed=0;
		for(EcosystemResult result:ecosystemResults){
			totalPassed+=result.getTotalPassed();
		}
		return totalPassed;
	}

	@Override
	public int getTotalNotPassed() {
		int totalNotPassed=0;
		for(EcosystemResult result:ecosystemResults){
			totalNotPassed+=result.getTotalNotPassed();
		}
		return totalNotPassed;
	}

}
