/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine;

import org.squashtest.ta.framework.test.instructions.ResourceName;



/**
 * 
 * <p>This object will carry additional informations on how to load a resource, but see it's use in
 * {@link ContextManager#getResource(ResourceLoadingSettings)} for more details.</p>
 * 
 *  @author bsiri
 *
 */
public interface ResourceLoadingSettings {

	
	/**
	 * sets the resource name you want to load. This is mandatory prior any use.
	 * 
	 * @param name the name of the resource.
	 */
	public void setResourceName(ResourceName name);
	
	public ResourceName getResourceName();
	
	/**
	 * if you know where to look for your resource (if not cached yet), you may set this attribute.
	 * If not set, the {@link ContextManager} will simply browse all available repositories.
	 * 
	 * @param name
	 */
	public void setRepositoryName(String name);
	
	public String getRepositoryName();


}
