/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import java.util.Properties;

/**
 * This is the wizard to create database targets for Squash TA.
 */

public class NewDbTargetWizard extends AbstractTargetWizard {
	private static final String SCHEMA_KEY = "squashtest.ta.database.schema";
	private static final String PASSWORD_KEY = "squashtest.ta.database.password";
	private static final String USERNAME_KEY = "squashtest.ta.database.username";
	private static final String URL_KEY = "squashtest.ta.database.url";
	private static final String DRIVER_KEY = "squashtest.ta.database.driver";
	private DbTargetPropertiesWizardPage targetProperties;
	
	@Override
	protected void addSpecificPages() {
		targetProperties=new DbTargetPropertiesWizardPage();
		addPage(targetProperties);
	}

	protected Properties extractTargetProperties() {
		final Properties dbProperties=new Properties();
		dbProperties.setProperty(DRIVER_KEY, targetProperties.getDbDriverName());
		dbProperties.setProperty(URL_KEY, targetProperties.getDbUrl());
		dbProperties.setProperty(USERNAME_KEY, targetProperties.getDbUserName());
		dbProperties.setProperty(PASSWORD_KEY, targetProperties.getDbPassword());
		String dbSchemaName = targetProperties.getDbSchemaName();
		if(targetProperties.isSchemaNameEnable()){
			dbProperties.setProperty(SCHEMA_KEY,dbSchemaName);
		}
		return dbProperties;
	}

	@Override
	protected String getShebangMark() {
		return null;//shebang not used for this kind of target
	}
		
}