/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.wrapper;

import java.util.Collection;

import org.squashtest.ta.backbone.tools.ReflectionUtils;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;


/**
 * An UnaryAssertionHandler will wrap an actual instance of {@link UnaryAssertion} and manage the metadata. 
 * 
 * @author bsiri
 *
 */
public class UnaryAssertionHandler extends ConfiguredComponentHandler{
	
	private static final String SETTER_METHOD = "setActualResult";
		
	private ReflectionUtils reflector = new ReflectionUtils();
	
	private Nature actualNature;
	private Nature assertionCategory;
	
	private UnaryAssertion<?> wrappedAssertion;
	
	/* ************** ctors ******************** */
	
	public UnaryAssertionHandler(){
		super();
	}
	
	public UnaryAssertionHandler(Nature actualNature, Nature assertionCategory, UnaryAssertion<?> assertion){
		this.actualNature=actualNature;
		this.assertionCategory=assertionCategory;
		this.wrappedAssertion=assertion;
	}
	
	/* ************** metadata ***************** */
	
	public Nature getActualResultNature(){
		return actualNature;
	}
	
	public void setActualResultNature(Nature nature){
		this.actualNature = nature;
	}
	
	public Nature getAssertionCategory(){
		return assertionCategory;
	}
	
	public void setAssertionCategory(Nature category){
		this.assertionCategory = category;
	}
	
	public void wrapAssertion(UnaryAssertion<?> assertion){
		this.wrappedAssertion=assertion;
	}
	
	/* ******************* wrapped methods *************** */
	
	public void setActualResult(ResourceWrapper actual){
		Resource<?> resource = actual.unwrap();
		reflector.invoke(wrappedAssertion, SETTER_METHOD, resource);		
	}
	
	public void test() throws AssertionFailedException{
		wrappedAssertion.test();
	}

	@Override
	protected void addConfigurationToWrapped(
			Collection<Resource<?>> unwrappedConfig) {
		wrappedAssertion.addConfiguration(unwrappedConfig);
	}
	
	
}
