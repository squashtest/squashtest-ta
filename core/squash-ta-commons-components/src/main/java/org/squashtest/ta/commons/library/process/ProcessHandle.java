/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is a process handle. It allows process streams monitoring and process termination management.
 * @author edegenetais
 *
 */
public class ProcessHandle {
	
	private static final int DEFAULT_STDERR_LENGTH = 200;

	private static final Logger LOGGER=LoggerFactory.getLogger(ProcessHandle.class);
	
	private String processName;
	private Process process;
	private Thread shutdownHook;
	private StreamMuncher errorStreamMonitor;
	
	/**
	 * Create a process handle without shutdown hook.
	 * @param process process to manage.
	 */
	public ProcessHandle(Process process) {
		this("Anonymous",process,false, false, 200);
	}
	
	/**
	 * Create a process handle.
	 * @param processName name for the process
	 * @param process process to manage.
	 * @param registerShutdownHook whether to register a process shutdown hook or not.
	 * @param monitorErrorStream if the error stream should be monitored.
	 */
	public ProcessHandle(String processName, Process process, boolean registerShutdownHook, boolean monitorErrorStream) {
		this(processName,process,registerShutdownHook,monitorErrorStream,DEFAULT_STDERR_LENGTH);
	}
	/**
	 * Create a process handle.
	 * @param processName name for the process
	 * @param process process to manage.
	 * @param registerShutdownHook whether to register a process shutdown hook or not.
	 * @param monitorErrorStream if the error stream should be monitored.
	 * @param maxLength max stderr record length to keep
	 */
	public ProcessHandle(String processName, Process process, boolean registerShutdownHook, boolean monitorErrorStream, int maxLength) {
		this.process=process;
		this.processName=processName;
		if(registerShutdownHook){
			shutdownHook=new Thread(new ProcessKiller(process));
			Runtime.getRuntime().addShutdownHook(new Thread(shutdownHook));
		}
		if(monitorErrorStream){
			errorStreamMonitor=new StreamMuncher(processName+"error stream", process.getErrorStream(), maxLength);
			/* This thread is a completely independant thread...thus it is out of the scope
			 * of the rule invoked by sonar to flag this... */
			new Thread(errorStreamMonitor).start();//NOSONAR
		}
	}
	
	/**
	 * Kill the underlying process.
	 */
	public void killProcess(){
		if(shutdownHook!=null){
			LOGGER.debug("removing killing shutdown hook");
			Runtime.getRuntime().removeShutdownHook(shutdownHook);
			LOGGER.debug("killing shutdown hook removed");
			shutdownHook=null;
		}
		
		if(errorStreamMonitor!=null){
			LOGGER.debug("killing "+processName+" error stream monitor.");
			errorStreamMonitor.requestStop();
			LOGGER.debug(processName+" error stream monitor killed");
		}
		
		process.destroy();
	}
	
	/**
	 * Check if the underlying process is still alive.
	 * @return <code>true</code> if alive, <code>false</code> if dead.
	 */
	public boolean isProcessAlive() {
		try{
			process.exitValue();
			return false;
		}catch(IllegalThreadStateException  ex){
			return true;
		}
	}
	
	/**
	 * @return process's return value.
	 */
	public Integer returnValue(){
		Integer value;
		try{
			value=process.exitValue();
		}catch(IllegalThreadStateException itse){
			value=null;
		}
		return value;
	}
	
	/**
	 * Get the last 200 lines of the process error stream.
	 * @return
	 */
	public String getErrorStream(){
		if(errorStreamMonitor!=null){
			return errorStreamMonitor.getStreamContent();
		}else{
			return "stderr was not monitored";
		}
	}

	public String getProcessName() {
		return processName;
	}
}
