/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.soapui.converters;

import java.io.IOException;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.soapui.resources.SoapUIScript;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * File To XML Converter
 * Converts a File entry (XML File obviously) into an XMLResource Type 
 * (encapsulated XML File which proves that the XML is "well formed")
 * <p> The file must a correct XML implementation<p>
 * 
 * @author fgaillard
 *
 */
@EngineComponent("structured")
public class FileToSoapUI implements ResourceConverter<FileResource, SoapUIScript> {

	/**
	 * Default constructor for Spring enumeration only.
	 */
	public FileToSoapUI(){}
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		//noop
	}

	@Override
	public SoapUIScript convert(FileResource resource) {
		SoapUIScript resultResource = null;
		try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
		 
			DefaultHandler handler = new DefaultHandler() {};
			
			saxParser.parse(resource.getFile(), handler);
		} catch (SAXException se) {
			throw new BadDataException("Wrongly configured SoapUI File\n",se);
		} catch (IOException ioe) {
			throw new BadDataException("Wrongly configured SoapUI File\n",ioe);
		} catch (ParserConfigurationException pce) {
			throw new BadDataException("Wrongly configured SoapUI File\n",pce);
		}
		resultResource = new SoapUIScript(resource.getFile());
		return resultResource;
	}

	@Override
	public void cleanUp() {
		
	}
}
