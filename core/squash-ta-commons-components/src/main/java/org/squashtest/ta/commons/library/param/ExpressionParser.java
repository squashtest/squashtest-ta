/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.param;

/**
 * Interface of non-litteral expression parsers added as plugins to the tokenizer. 
 * @author edegenetais
 *
 */
public interface ExpressionParser {
	/**
	 * If this expression is an acceptable expression for this parser.
	 * @param expression the expression to validate.
	 * @return <code>true</code> if the expression is a valid expression for this parser, <code>false</code> otherwise.
	 */
	boolean accept(String expression);
	/**
	 * Parse this expression and create the corresponding {@link Expression} instance.
	 * @param expression
	 * @return
	 */
	Expression parse(String expression);
}
