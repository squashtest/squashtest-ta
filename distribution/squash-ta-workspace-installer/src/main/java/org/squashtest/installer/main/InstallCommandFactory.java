/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.main;

import java.io.IOException;
import java.net.URISyntaxException;

import org.squashtest.installer.command.Command;
import org.squashtest.installer.command.CommandRunner;
import org.squashtest.installer.command.InstallFileCommand;
import org.squashtest.installer.command.ta.ImportPreferencesCommand;
import org.squashtest.installer.context.Context;


/*
 * Well I should have learned more about Ant before implementing that but I don't have time for this.
 * 
 * TODO : test that class
 * 
 */
public class InstallCommandFactory {
	
	Context context;

	public CommandRunner makeRunner() throws IOException, URISyntaxException{
		CommandRunner runner = new CommandRunner();
		runner.add(newImportPreferencesCommand());
		runner.add(newConfigJDKCommand());
		runner.add(newConfigRunConf1Command());

		//the runner 2 is not supported for now so we disable it for now
		runner.add(newConfigRunConf2Command());
		
		return runner;
	}
	
	public Command newImportPreferencesCommand() throws IOException, URISyntaxException{
		return new ImportPreferencesCommand(context);		
	}
	
	public Command newConfigJDKCommand(){
		return new InstallFileCommand(context,get("JDK_CONFIG_TEMPLATE"), get("CONFIGURED_JDK_CONFIG"), get("RELATIVE_PREF_DIRECTORY") );
	}
	
	public Command newConfigRunConf1Command(){
		return new InstallFileCommand(context,get("RUN_CONF_1_TEMPLATE"), get("CONFIGURED_RUN_CONF_1"), get("RELATIVE_RUN_CONF_DIRECTORY") );
	}
	
	public Command newConfigRunConf2Command(){
		return new InstallFileCommand(context, get("RUN_CONF_2_TEMPLATE"), get("CONFIGURED_RUN_CONF_2"), get("RELATIVE_RUN_CONF_DIRECTORY") );
	}


	public InstallCommandFactory(Context context){
		this.context=context;
	}
	
	
	private String get(String key){
		return context.getProperty(key);
	}
}
