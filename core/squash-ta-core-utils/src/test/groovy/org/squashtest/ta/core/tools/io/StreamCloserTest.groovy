/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools.io

import spock.lang.Specification

class StreamCloserTest extends Specification {

	def "should create a buggy stream"(){
		
		given :
			def streams = [] 
			10.times { streams << Mock(Closeable) }
		
		when :
			StreamCloser.close(streams)
		
		then :
			1 * streams[0].close()
			1 * streams[2].close()
			1 * streams[4].close()
			1 * streams[6].close()
			1 * streams[8].close()
			
			1 * streams[1].close() >> {throw new IOException()}
			1 * streams[3].close() >> {throw new IOException()}
			1 * streams[5].close() >> {throw new IOException()}
			1 * streams[7].close() >> {throw new IOException()}
			1 * streams[9].close() >> {throw new IOException()}
			
		
			thrown IOException
			
			
			
	}
	

	
}
