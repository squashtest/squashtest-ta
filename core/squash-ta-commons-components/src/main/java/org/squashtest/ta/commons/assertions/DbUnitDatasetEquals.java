/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.dbunit.DatabaseUnitException;
import org.dbunit.assertion.DiffCollectingFailureHandler;
import org.dbunit.assertion.Difference;
import org.dbunit.assertion.FailureHandler;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.SortedTable;
import org.squashtest.ta.commons.helpers.DiffReportBuilder;
import org.squashtest.ta.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.commons.library.dbunit.helper.LowerCasedTable;
import org.squashtest.ta.commons.resources.DbUnitDatasetResource;
import org.squashtest.ta.commons.resources.DbUnitPPKFilter;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.TestAssertionFailure;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

/**
 * Binary assertion that checks that effective dataset contains expected
 * dataset.
 * 
 * @author edegenetais
 * 
 */
@EngineComponent("equal")
public class DbUnitDatasetEquals extends AbstractDbUnitDatasetCompare implements
		BinaryAssertion<DbUnitDatasetResource, DbUnitDatasetResource> {

	private static final String DIFF_RESOURCE_NAME = "diff";

	private static final Column[] NO_PK_VALUE = new Column[0];

	private DbUnitDatasetResource expected;

	private DbUnitDatasetResource actual;

	private DbUnitPPKFilter pseudoPrimaryKeys;

	protected void compare(IDataSet pExpected, IDataSet pActual) {
		try {
			String[] tableNames = pExpected.getTableNames();

			// Handler which will eventually contain the differences found
			// between
			// Expected dataset and Actual dataset
			DiffCollectingFailureHandler myHandler = new DiffCollectingFailureHandler();
			
			List<String> failedTableNames = new ArrayList<String>();

			// Comparison is made between sorted tables
			for (int i = 0; i < tableNames.length; i++) {
				try{
					performTableCompare(tableNames[i], pExpected, pActual, myHandler);
				} catch (TestAssertionFailure taf) {
					failedTableNames.add(tableNames[i]);
				}
			}
			
			@SuppressWarnings("unchecked")//check forced by dbunit API...
			List<Difference> diffList = myHandler.getDiffList();
			if (diffList.size() > 0 || failedTableNames.size() > 0) {
				throwAssertionFailure(diffList, failedTableNames);
			}
		} catch (DataSetException bde) {
			throw new BadDataException("Dataset comparison threw dbunit error",
					bde);
		} catch (DatabaseUnitException e) {
			throw new BadDataException("Dataset comparison threw dbunit error",
					e);
		}
	}

	private void performTableCompare(String currentTableName, IDataSet pExpected, IDataSet pActual, FailureHandler myHandler) throws DatabaseUnitException{
		ITable expTable = new LowerCasedTable(pExpected.getTable(currentTableName));
		ITable actTable = new LowerCasedTable(pActual.getTable(currentTableName));
		
		ITableMetaData expTableMetaData = expTable.getTableMetaData();
		
		Map<String, Column[]> pkMap = new HashMap<String, Column[]>();
		
		SortedTable expSortedTable = null;
		SortedTable actSortedTable = null;
		
		//We retrieve the primary keys from the expected table (or from the configuration if they are difined by properties)
		Column[] primaryKeys = extractPrimaryKeys(pkMap, expTable);

		//If they don't exist, we retrieve them from the actual table
		if (primaryKeys != null && primaryKeys.length == 0) {
			primaryKeys = extractPrimaryKeys(pkMap, actTable);
		} 
		
		//If they are still not defined, we take the first collumn of the expected table (we have to take something)
		if (primaryKeys != null && primaryKeys.length == 0 && expTableMetaData.getColumns().length > 0){
			primaryKeys = new Column[]{expTableMetaData.getColumns()[0]};
		} 
		//If the expected table has no columns, we it as the empty array it is
		
		if (primaryKeys != null && primaryKeys.length > 0){
			expSortedTable = new SortedTable(expTable, primaryKeys);
			actSortedTable = new SortedTable(actTable, primaryKeys);	
		} else {
			expSortedTable = new SortedTable(expTable);
			actSortedTable = new SortedTable(actTable, expTableMetaData);	
		} 
		
		assertConnector.assertEquals(expSortedTable, actSortedTable,
				myHandler);
	}

	private void throwAssertionFailure(List<Difference> diffList, List<String> failedTables) {
		List<ResourceAndContext> context = new ArrayList<ResourceAndContext>();

		try {/*
			 * exceptions during the reporting building process should
			 * not block assertion failure reporting or change the FAIL
			 * state into an ERROR state
			 */
			DiffReportBuilder builder = buildComparisonReport(diffList);

			File tempFile = File.createTempFile("binaryDataSet",
					".diff");
			tempFile.deleteOnExit();
			
			FileResource diff=new FileResource(tempFile);
			StringBuilder reportBuilder=new StringBuilder(builder.toString());
			if (failedTables.size() > 0){
				reportBuilder.append("The Following tables had compare issues (most likely size differences)\n");
				for (String tableName : failedTables) {
					reportBuilder.append(tableName).append("\n");
				}
			}
			BinaryData diffData=new BinaryData(reportBuilder.toString().getBytes("UTF-8"));
			diffData.write(tempFile);
			ResourceAndContext diffContext=new ResourceAndContext();
			diffContext.resource=diff;
			diffContext.metadata=new ExecutionReportResourceMetadata(getClass(), new Properties(), FileResource.class, DIFF_RESOURCE_NAME);
			context.add(diffContext);
		} catch (IOException e) {
			logFailureReportingError(e);
		} catch (DataSetException e) {
			logFailureReportingError(e);
		}

		throw new BinaryAssertionFailedException(
				"Actual resultset did not contain expected dataset",
				expected, actual, context);
	}

	public DiffReportBuilder buildComparisonReport(List<Difference> diffList)
			throws DataSetException {
		Map<String, Column[]> pkMap = new HashMap<String, Column[]>();
		DiffReportBuilder builder = diffReportBuilderFactory
				.newInstance();
		for (Difference diff : diffList) {
			ITable table = diff.getExpectedTable();

			Column[] primaryKeys = extractPrimaryKeys(pkMap, table);
			int rowId = diff.getRowIndex();
			Map<String, String> pkSet;
			if (primaryKeys != null) {
				//if the expected Table does not have primary keys, we try with the actual table
				if (primaryKeys.length == 0) {
					primaryKeys = extractPrimaryKeys(pkMap, diff.getActualTable());
				}
				pkSet = new HashMap<String, String>(
						primaryKeys.length);
				for (Column c : primaryKeys) {
					Object valueObject = diff.getExpectedTable()
							.getValue(rowId, c.getColumnName());
					String valueString = valueObject.toString();
					pkSet.put(c.getColumnName(), valueString);
				}
			} else {
				pkSet = null;
			}
			builder.addDiffElement(pkSet, rowId, table
					.getTableMetaData().getTableName(), diff
					.getColumnName(), diff.getExpectedValue(), diff
					.getActualValue());
		}
		return builder;
	}

	/**
	 * This method tries to compute the set of PK(s) for a given dataset table.
	 * 
	 * @param pkMap
	 *            cache to reuse results from previous diff lines
	 * @param table
	 *            the target table
	 * @return an array holding the table primary key column definitions if the
	 *         information was available. If no primary key information exists,
	 *         an empty array is cached and returned to speed up the next look
	 *         up.
	 * @throws DataSetException
	 *             if some error occurs while probing the dataset.
	 */
	private Column[] extractPrimaryKeys(Map<String, Column[]> pkMap,
			ITable table) throws DataSetException {
		// first, try from Map to use cache from previous iteration
		ITableMetaData tableMetaData = table.getTableMetaData();
		String tableName = tableMetaData.getTableName();
		Column[] primaryKeys = pkMap.get(tableName);

		// if cache miss, try from dataset
		if (primaryKeys == null) {
			primaryKeys = tableMetaData.getPrimaryKeys();
		}

		// if no PK in dataset, see if some PKs where defined in PPKFilter
		if ((primaryKeys == null || primaryKeys.length == 0) && pseudoPrimaryKeys != null) {
			List<Column> pkList = new ArrayList<Column>();
			for (Column c : tableMetaData.getColumns()) {
				if (pseudoPrimaryKeys.accept(tableName, c)) {
					pkList.add(c);
				}
			}
			primaryKeys = pkList.toArray(new Column[pkList.size()]);

			// if not, create empty array and cache it to make next lookup
			// quicker for this table.
			if (primaryKeys == null) {
				primaryKeys = NO_PK_VALUE;
			}

			pkMap.put(tableName, primaryKeys);
		}

		return primaryKeys;
	}

}
