/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl

import java.util.Iterator

import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.impl.InstructionRunner
import org.squashtest.ta.backbone.engine.impl.InstructionRunnerFactory
import org.squashtest.ta.backbone.engine.impl.TestRunnerImpl
import org.squashtest.ta.backbone.engine.wrapper.LocalContext;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.NotUniqueEntryException;
import org.squashtest.ta.backbone.test.DefaultExecutionDetails;
import org.squashtest.ta.framework.test.definition.Test
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.instructions.TestInstruction
import org.squashtest.ta.framework.components.Resource
import org.squashtest.ta.framework.exception.TestAssertionFailure
import org.squashtest.ta.framework.exception.BrokenTestException
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.TestResult
import org.squashtest.ta.framework.test.result.GeneralStatus

import spock.lang.Specification

class TestRunnerImplTest extends Specification {

	TestRunnerImpl runner;
	ContextManager manager;
	Test test;
	InstructionRunnerFactory runnerFactory;
	
	Iterator setupIter;
	Iterator testIter;
	Iterator teardownIter;
	
	TestInstruction failedInst
	TestInstruction inst1
	TestInstruction inst2
	TestInstruction inst3
	
	InstructionRunner failRun
	InstructionRunner run1
	InstructionRunner run2
	InstructionRunner run3
	
	ExecutionDetails exeFailed
	ExecutionDetails exe1
	ExecutionDetails exe2
	ExecutionDetails exe3
	
	TestResult result
	
	def setup(){
		/* runner setup */
		runner = new TestRunnerImpl();
		manager = Mock(ContextManager);
		runnerFactory = Mock(InstructionRunnerFactory)
		test = Mock(Test)
		
		runner.setContextManager manager
		runner.setInstructionRunnerFactory runnerFactory;
		runner.setTest test
		
		/* other mock setups */
		setupIter = Mock(Iterator)
		testIter = Mock(Iterator)
		teardownIter = Mock(Iterator)
		
		failedInst = Mock(TestInstruction)
		inst1 = Mock(TestInstruction)
		inst2 = Mock(TestInstruction)
		inst3 = Mock(TestInstruction)
			
		failRun = Mock(InstructionRunner)
		run1 = Mock(InstructionRunner)
		run2 = Mock(InstructionRunner)
		run3 = Mock(InstructionRunner)
		
		exeFailed = Mock(ExecutionDetails)
		exe1 = Mock(ExecutionDetails)
		exe2 = Mock(ExecutionDetails)
		exe3 = Mock(ExecutionDetails)
		
		test.getSetup() >> setupIter
		test.getTests() >> testIter
		test.getTeardown() >> teardownIter
	}
		
	def "should run some instructions and expect normal result"(){
		given :	
			setupIter.hasNext() >>> [false]
		
			testIter.hasNext() >>> [true, true, true, false]
			testIter.next() >>> [inst1, inst2, inst3]
			
			teardownIter.hasNext() >>> [false]
			
		and:
			run1.run() >> exe1
			run2.run() >> exe2
			run3.run() >> exe3
		
		and:	
			exe1.instructionType() >> InstructionType.ASSERT
			exe1.getStatus() >> GeneralStatus.SUCCESS
			
			exe2.instructionType() >> InstructionType.COMMAND
			exe2.getStatus() >> GeneralStatus.SUCCESS
			
			exe3.instructionType() >> InstructionType.ASSERT
			exe3.getStatus() >> GeneralStatus.SUCCESS
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == GeneralStatus.SUCCESS
			1 * runnerFactory.getRunner(inst1) >> run1
			1 * runnerFactory.getRunner(inst2) >> run2
			1 * runnerFactory.getRunner(inst3) >> run3
			
			1 * run1.setTestRunner(runner)
						
			1 * run2.setTestRunner(runner)
						
			1 * run3.setTestRunner(runner)
			
	}
		
	def "should skip test, execute teardown and end up NOT_RUN if error during setup"(){
		given :
			setupIter.hasNext() >>> [true,true,false]
			setupIter.next() >>> [failedInst,inst1]
		and:
			failRun.run() >> exeFailed
			run1.run() >> exe1
		and:
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [inst2]
			
		and:
			run2.run() >> exe2
			
		and:
			teardownIter.hasNext() >>> [true,false]
			teardownIter.next() >>> [inst3]
		and:
			run3.run() >> exe3

		and:
			exeFailed.instructionType() >> InstructionType.COMMAND
			exeFailed.getStatus() >> GeneralStatus.ERROR	
		
			exe1.instructionType() >> InstructionType.ASSERT
			exe1.getStatus() >> GeneralStatus.SUCCESS
			
			exe2.instructionType() >> InstructionType.COMMAND
			exe2.getStatus() >> GeneralStatus.SUCCESS
			
			exe3.instructionType() >> InstructionType.ASSERT
			exe3.getStatus() >> GeneralStatus.SUCCESS
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == GeneralStatus.NOT_RUN
			
			1 * runnerFactory.getRunner(failedInst) >> failRun
			0 * runnerFactory.getRunner(inst1) >> run1
			
			0 * runnerFactory.getRunner(inst2) >> run2
			
			1 * runnerFactory.getRunner(inst3) >> run3
			
			0 * run1.setTestRunner(runner)
			0 * run2.setTestRunner(runner)
			
			1 * run3.setTestRunner(runner)
	}

	def "should skip test, execute teardown and end up NOT_RUN if fail during setup"(){
		given :
			setupIter.hasNext() >>> [true,true,false]
			setupIter.next() >>> [failedInst,inst1]
		and:
			failRun.run() >> exeFailed
			run1.run() >> exe1
		and:
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [inst2]
			
		and:
			run2.run() >> exe2
			
		and:
			teardownIter.hasNext() >>> [true,false]
			teardownIter.next() >>> [inst3]
		and:
			run3.run() >> exe3

		and:
			exeFailed.instructionType() >> InstructionType.ASSERT
			exeFailed.getStatus() >> GeneralStatus.FAIL
		
			exe1.instructionType() >> InstructionType.ASSERT
			exe1.getStatus() >> GeneralStatus.SUCCESS
			
			exe2.instructionType() >> InstructionType.COMMAND
			exe2.getStatus() >> GeneralStatus.SUCCESS
			
			exe3.instructionType() >> InstructionType.ASSERT
			exe3.getStatus() >> GeneralStatus.SUCCESS
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == GeneralStatus.NOT_RUN
			
			1 * runnerFactory.getRunner(failedInst) >> failRun
			0 * runnerFactory.getRunner(inst1) >> run1
			
			0 * runnerFactory.getRunner(inst2) >> run2
			
			1 * runnerFactory.getRunner(inst3) >> run3
			
			0 * run1.setTestRunner(runner)
			0 * run2.setTestRunner(runner)
			
			1 * run3.setTestRunner(runner)
	}
	
	def "should do setup, end test, execute teardown and end up ERROR if error during test"(){
		given :
			setupIter.hasNext() >>> [true,false]
			setupIter.next() >>> [inst1]
		and:
			failRun.run() >> exeFailed
			run1.run() >> exe1
		and:
			testIter.hasNext() >>> [true, true, false]
			testIter.next() >>> [failedInst,inst2]
			
		and:
			run2.run() >> exe2
			
		and:
			teardownIter.hasNext() >>> [true,false]
			teardownIter.next() >>> [inst3]
		and:
			run3.run() >> exe3

		and:
			exeFailed.instructionType() >> InstructionType.COMMAND
			exeFailed.getStatus() >> GeneralStatus.ERROR
		
			exe1.instructionType() >> InstructionType.ASSERT
			exe1.getStatus() >> GeneralStatus.SUCCESS
			
			exe2.instructionType() >> InstructionType.COMMAND
			exe2.getStatus() >> GeneralStatus.SUCCESS
			
			exe3.instructionType() >> InstructionType.ASSERT
			exe3.getStatus() >> GeneralStatus.SUCCESS
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == GeneralStatus.ERROR
			
			
			1 * runnerFactory.getRunner(inst1) >> run1
			
			1 * runnerFactory.getRunner(failedInst) >> failRun
			0 * runnerFactory.getRunner(inst2) >> run2
			
			1 * runnerFactory.getRunner(inst3) >> run3
			
			1 * run1.setTestRunner(runner)
			
			0 * run2.setTestRunner(runner)
			
			1 * run3.setTestRunner(runner)
	}
	
	def "should do setup, end test, execute teardown and end up FAIL if error during test"(){
		given :
			setupIter.hasNext() >>> [true,false]
			setupIter.next() >>> [inst1]
		and:
			failRun.run() >> exeFailed
			run1.run() >> exe1
		and:
			testIter.hasNext() >>> [true, true, false]
			testIter.next() >>> [failedInst,inst2]
			
		and:
			run2.run() >> exe2
			
		and:
			teardownIter.hasNext() >>> [true,false]
			teardownIter.next() >>> [inst3]
		and:
			run3.run() >> exe3

		and:
			exeFailed.instructionType() >> InstructionType.COMMAND
			exeFailed.getStatus() >> GeneralStatus.FAIL
		
			exe1.instructionType() >> InstructionType.ASSERT
			exe1.getStatus() >> GeneralStatus.SUCCESS
			
			exe2.instructionType() >> InstructionType.COMMAND
			exe2.getStatus() >> GeneralStatus.SUCCESS
			
			exe3.instructionType() >> InstructionType.ASSERT
			exe3.getStatus() >> GeneralStatus.SUCCESS
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == GeneralStatus.FAIL
			
			
			1 * runnerFactory.getRunner(inst1) >> run1
			
			1 * runnerFactory.getRunner(failedInst) >> failRun
			0 * runnerFactory.getRunner(inst2) >> run2
			
			1 * runnerFactory.getRunner(inst3) >> run3
			
			1 * run1.setTestRunner(runner)
			
			0 * run2.setTestRunner(runner)
			
			1 * run3.setTestRunner(runner)
	}
	
	def "should do setup, do test, stop teardown and end up WARNING if error during teardown"(){
		given :
			setupIter.hasNext() >>> [true,false]
			setupIter.next() >>> [inst1]
		and:
			run1.run() >> exe1
		and:
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [inst2]
				
		and:
			failRun.run() >> exeFailed
			run2.run() >> exe2
			
		and:
			teardownIter.hasNext() >>> [true,true,false]
			teardownIter.next() >>> [failedInst,inst3]
		and:
			run3.run() >> exe3

		and:
			exeFailed.instructionType() >> InstructionType.COMMAND
			exeFailed.getStatus() >> GeneralStatus.ERROR
		
			exe1.instructionType() >> InstructionType.ASSERT
			exe1.getStatus() >> GeneralStatus.SUCCESS
			
			exe2.instructionType() >> InstructionType.COMMAND
			exe2.getStatus() >> GeneralStatus.SUCCESS
			
			exe3.instructionType() >> InstructionType.ASSERT
			exe3.getStatus() >> GeneralStatus.SUCCESS
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == GeneralStatus.WARNING
			
			
			1 * runnerFactory.getRunner(inst1) >> run1
			
			
			1 * runnerFactory.getRunner(inst2) >> run2
			
			1 * runnerFactory.getRunner(failedInst) >> failRun
			0 * runnerFactory.getRunner(inst3) >> run3
			
			1 * run1.setTestRunner(runner)
			
			1 * run2.setTestRunner(runner)
			
			0 * run3.setTestRunner(runner)
	}
	
	def "should do setup, test, and teardown but end up WARNING if fail during teardown"(){
		given :
			setupIter.hasNext() >>> [true,false]
			setupIter.next() >>> [inst1]
		and:
			run1.run() >> exe1
		and:
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [inst2]
				
		and:
			failRun.run() >> exeFailed
			run2.run() >> exe2
			
		and:
			teardownIter.hasNext() >>> [true,true,false]
			teardownIter.next() >>> [failedInst,inst3]
		and:
			run3.run() >> exe3

		and:
			exeFailed.instructionType() >> InstructionType.COMMAND
			exeFailed.getStatus() >> GeneralStatus.FAIL
		
			exe1.instructionType() >> InstructionType.ASSERT
			exe1.getStatus() >> GeneralStatus.SUCCESS
			
			exe2.instructionType() >> InstructionType.COMMAND
			exe2.getStatus() >> GeneralStatus.SUCCESS
			
			exe3.instructionType() >> InstructionType.ASSERT
			exe3.getStatus() >> GeneralStatus.SUCCESS
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == GeneralStatus.WARNING
			
			
			1 * runnerFactory.getRunner(inst1) >> run1
			
			
			1 * runnerFactory.getRunner(inst2) >> run2
			
			1 * runnerFactory.getRunner(failedInst) >> failRun
			1 * runnerFactory.getRunner(inst3) >> run3
			
			1 * run1.setTestRunner(runner)
			
			1 * run2.setTestRunner(runner)
			
			1 * run3.setTestRunner(runner)
	}
	
	def "should add then get a resource to/from the cache"(){
		
		given :
			def res = Mock(ResourceWrapper)
			res.getName() >> new ResourceName("resource")
			
		when :
			runner.addResourceToCache(res)
			def result = runner.getResourceFromCache(new ResourceName("resource"))
			
		then :
			result == res		
		
	}
	
	def "should rant because another resource with that name already exists"(){
		
		given :
			def res = Mock(ResourceWrapper)
			res.getName() >> new ResourceName("resource")
			
		and :
			def res2 = Mock(ResourceWrapper)
			res2.getName() >> new ResourceName("resource")
		when :
			runner.addResourceToCache(res)
			runner.addResourceToCache(res2)
		
		then :
			thrown NotUniqueEntryException
		
	}
	
	def "should not return a resource that is not cached"(){
		
		expect :
			runner.getResourceFromCache(new ResourceName("test")) == null
		
	}
	
	def "should clean the place up once the test is over"(){
		
		given :
			def res1 = Mock(ResourceWrapper)
			def res2 = Mock(ResourceWrapper)
			def res3 = Mock(ResourceWrapper)
		
			res1.getName() >> new ResourceName("resource1")
			res2.getName() >> new ResourceName("resource2")
			res3.getName() >> new ResourceName("resource3")
			
		and :
			runner.addResourceToCache(res1)
			runner.addResourceToCache(res2)
			runner.addResourceToCache(res3)
		and :
			runner.setLocalContext(Mock(LocalContext))
		when :
			runner.cleanUp()
			
		then :
			runner.testResources.isEmpty()==true
			1 * res1.cleanUp()
			1 * res2.cleanUp()
			1 * res3.cleanUp()
			1 * runner.localContext.cleanUp()
	}
	
	def "should sucessfully clean up without the optional local context"(){
		
		given :
			def res1 = Mock(ResourceWrapper)
			def res2 = Mock(ResourceWrapper)
			def res3 = Mock(ResourceWrapper)
		
			res1.getName() >> new ResourceName("resource1")
			res2.getName() >> new ResourceName("resource2")
			res3.getName() >> new ResourceName("resource3")
			
		and :
			runner.addResourceToCache(res1)
			runner.addResourceToCache(res2)
			runner.addResourceToCache(res3)
		when :
			runner.cleanUp()
		then :
			runner.testResources.isEmpty()==true
			1 * res1.cleanUp()
			1 * res2.cleanUp()
			1 * res3.cleanUp()
	}
	
	def "should not conflict resources with same short name in two namespaces"(){
		given:
			runner.setLocalContext(new DefaultLocalContextImpl(0))
		and:
			def res1 = Mock(ResourceWrapper)
			def res2 = Mock(ResourceWrapper)
			
			res1.getName() >> new ResourceName(Scope.SCOPE_TEST,"samename")
			res2.getName() >> new ResourceName(Scope.SCOPE_TEMPORARY,"samename")
			
		and:
			runner.addResourceToCache(res1)
			runner.addResourceToCache(res2)
		when:
			def found1=runner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEMPORARY,"samename"))
			def found2=runner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"samename"))
		then:
			found1 == res2
			found2 == res1
	}
	
	def "should lose temporary but not test scope resources when reset local context"(){
		given:
			runner.setLocalContext(new DefaultLocalContextImpl(0))
		and:
			def res1 = Mock(ResourceWrapper)
			def res2 = Mock(ResourceWrapper)
			
			res1.getName() >> new ResourceName(Scope.SCOPE_TEST,"samename")
			res2.getName() >> new ResourceName(Scope.SCOPE_TEMPORARY,"samename")
		and:
			runner.addResourceToCache(res1)
			runner.addResourceToCache(res2)
		when:
			runner.setLocalContext(Mock(LocalContext))
			def found1=runner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEMPORARY,"samename"))
			def found2=runner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"samename"))
		then:
			found1==null
			found2==res1
	}
	
	def "should throw cleanly when using a local context before initialisation"(){
		given:
			def res = Mock(ResourceWrapper)
		and:
			res.getName() >> new ResourceName(Scope.SCOPE_TEMPORARY,"name")
		when:
			runner.addResourceToCache(res)
		then:
			thrown BrokenTestException
	}
}
