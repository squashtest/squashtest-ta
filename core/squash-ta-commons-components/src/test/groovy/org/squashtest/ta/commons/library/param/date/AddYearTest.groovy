/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.library.param.date

import java.text.SimpleDateFormat;

import org.squashtest.ta.commons.library.param.IllegalExpressionException;

import spock.lang.Specification

class AddYearTest extends Specification {
	def testee
	def setup(){
		testee=new AddYear();
	}
	
	def "forward 2 years OK"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/05/11"))
		and:
			def expected=Calendar.getInstance()
			expected.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2014/05/11"))
		when:
			def actual=testee.evaluate(input,"2")
		then:
			expected.equals(actual)
	}
	
	def "back 2 years OK"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/05/11"))
		and:
			def expected=Calendar.getInstance()
			expected.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2010/05/11"))
		when:
			def actual=testee.evaluate(input,"-2")
		then:
			expected.equals(actual)
	}
	
	def "null argument gives IllegalArgumentException"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/01/01"))
		when:
			def actual=testee.evaluate(input,null)
		then:
			thrown(IllegalExpressionException)
	}
	
	def "empty argument gives IllegalArgumentException"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/01/01"))
		when:
			def actual=testee.evaluate(input,"")
		then:
			thrown(IllegalExpressionException)
	}
}
