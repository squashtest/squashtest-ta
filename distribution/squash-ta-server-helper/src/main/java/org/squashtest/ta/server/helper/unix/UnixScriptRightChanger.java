/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.server.helper.unix;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class executes a script to make scripts executable in unix.
 * 
 * @author edegenetais
 * 
 */
public class UnixScriptRightChanger {
	
	private File serverDir;

	private static final class FileOnlyFilter implements FileFilter {
		@Override
		public boolean accept(File pathname) {
			return pathname.isFile();
		}
	}

	private class ProcessFactory {
		private ProcessBuilder builder;

		public ProcessFactory(String... command) {
			builder = new ProcessBuilder(command);
		}

		public List<String> command() {
			return builder.command();
		}

		public void directory(File dir) {
			builder.directory(dir);
		}

		public File directory() {
			return builder.directory();
		}

		public Process start() throws IOException {
			return builder.start();
		}
	}

	public UnixScriptRightChanger(File base) {
		serverDir = base;
	}

	void markScriptsAsExecutableFiles(List<File> scriptlist) throws IOException {
		ProcessFactory builder = new ProcessFactory("chmod", "u+x");
		for (File script : scriptlist) {
			List<String> commandLine = builder.command();
			String path;
			if (script.isAbsolute()) {
				path = script.getPath();
			} else {
				path = script.getAbsolutePath();
			}
			System.out.println("Listing file " + path + " for execution right.");
			commandLine.add(path);
		}
		
		StringBuilder commandLog = new StringBuilder(
				"Launching execution rights commandline:\n");
		commandLog.append(builder.directory()).append("# ");
		for (String arg : builder.command()) {
			commandLog.append(arg).append(" ");
		}
		System.out.println(commandLog.toString().trim());
	
		builder.directory(serverDir);
		Process chmod = builder.start();
		int code;
		try {
			code = chmod.waitFor();
		} catch (InterruptedException ie) {
			System.err.println("chmod was interrupted!");
			code = chmod.exitValue();
		}
		if (code != 0) {
			System.err.println("chmod failed with code " + code);
		}
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out
					.println("Usage: UnixScriptRightChanger (<scriptdir> | <scriptfile>)*\n"
							+ "scriptdir: a directory where all files are scripts. Only the first level is treated: "
							+ "if needed, add relevant subdirectories to argument list.");
			System.exit(1);
		}
		for (String scriptSpec : args) {
			try {
				File scriptPath = new File(scriptSpec);
				if (scriptPath.isDirectory()) {
					File[] scripts = scriptPath.listFiles(new FileOnlyFilter());
					new UnixScriptRightChanger(scriptPath)
							.markScriptsAsExecutableFiles(Arrays
									.asList(scripts));
				} else {
					List<File> list = new ArrayList<File>();
					list.add(scriptPath);
					new UnixScriptRightChanger(scriptPath.getParentFile()).markScriptsAsExecutableFiles(list);
				}
			} catch (IOException e) {
				System.err.println("Failed to set " + scriptSpec
						+ " as executable: " + e.getMessage());
			}
		}
	}
}
