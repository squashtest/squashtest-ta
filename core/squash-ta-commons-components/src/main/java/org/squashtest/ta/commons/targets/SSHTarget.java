/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.targets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.ssh.SSHConfigurationException;
import org.squashtest.ta.commons.library.ssh.SquashSSHClient;
import org.squashtest.ta.core.tools.PropertiesUtils;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Target;

@ResourceType("ssh.target")
public class SSHTarget implements Target {

	public static final String NAMESPACE = "squashtest.ta.ssh";

	public static final String PASSWORD_KEY = NAMESPACE+".password";

	public static final String USERNAME_KEY = NAMESPACE+".username";

	public static final String PORT_KEY = NAMESPACE+".port";

	public static final String HOSTNAME_KEY = NAMESPACE+".hostname";

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SSHTarget.class);

	private List<SquashSSHClient> clients = new LinkedList<SquashSSHClient>();

	private String hostname;
	private Integer port;
	private String username;
	private String password;
	
	/**noarg constructor for Spring*/
	public SSHTarget() {}
	
	/**
	 * @param hostname
	 * @param port
	 * @param username
	 * @param password
	 */
	public SSHTarget(Properties configuration) {
		this.hostname = configuration.getProperty(HOSTNAME_KEY);
		this.port = new PropertiesUtils().getIntegerValue(configuration, PORT_KEY);
		this.username = configuration.getProperty(USERNAME_KEY);
		this.password = configuration.getProperty(PASSWORD_KEY);
	}

	@Override
	public void init() {
		SquashSSHClient client = new SquashSSHClient(hostname, port, username,
				password);
		try {
			client.connect();
			client.authenticate();
			synchronized (clients) {
				clients.add(client);
			}
		} catch (IOException e) {
			throw new SSHConfigurationException(e, "Client connection failed.",
					new Object[] {});
		}
	}

	@Override
	public void reset() {
		synchronized (clients) {
			cleanup();
			init();
		}
	}

	@Override
	public void cleanup() {
		synchronized (clients) {
			for (SquashSSHClient client : clients) {
				try {
					client.dispose();
				} catch (IOException e) {
					LOGGER.warn("Filed to dispose an SSH connection to host "
							+ hostname
							+ (port == null ? "" : ":" + port.toString()));
				}
			}
			clients.clear();
		}
	}

	public SquashSSHClient getClient(){
		synchronized(clients){
			if(clients.isEmpty()){
				init();
			}
			return clients.remove(0);
		}
	}
	
	@Override
	public Properties getConfiguration() {
		Properties pro=new Properties();
		pro.setProperty(HOSTNAME_KEY, hostname);
		if(port!=null){
			pro.setProperty(PORT_KEY, port.toString());
		}
		pro.setProperty(USERNAME_KEY, username);
		pro.setProperty(PASSWORD_KEY, password);
		return pro;
	}
}
