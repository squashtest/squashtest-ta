/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.repositories;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.utils.authentication.AuthenticatorChain;
import org.squashtest.ta.commons.utils.authentication.EngineAuthenticator;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;

@ResourceType("url")
public class URLRepository implements ResourceRepository {
	

	private static final Logger LOGGER = LoggerFactory.getLogger(URLRepository.class);

	public static final String BASE_URL_KEY = "squashtest.ta.url.base";
	public static final String LOGIN_KEY = "squashtest.ta.url.login";
	public static final String PASSWORD_KEY = "squashtest.ta.url.password";
	public static final String USE_CACHE_KEY = "squashtest.ta.url.cache";
	private static final FileTree FILE_TREE=new FileTree();
	
	private Properties effectiveConfiguration;
	
	private URL baseURL;
	private EngineAuthenticator authenticator;
	private boolean useCache=false;
	
	
	private Map<String, File> cache = new HashMap<String, File>();
	
	public URLRepository() {}
	
	public URLRepository(Properties properties, URL baseURL, boolean useCache, EngineAuthenticator authenticator){
		super();
		this.baseURL=baseURL;
		this.useCache=useCache;
		this.authenticator = authenticator;
		this.effectiveConfiguration = properties; 
	}
	
	
	
	
	public URL getBaseURL() {
		return baseURL;
	}

	public boolean isUseCache() {
		return useCache;
	}

	
	@Override
	public void init() {
		
		AuthenticatorChain.getInstance().add(authenticator);
		
	}
	

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanup() {
		
		AuthenticatorChain.getInstance().remove(authenticator);
		
		for (File file : cache.values()){
			boolean deleted=file.delete();
			if(!deleted && file.exists()){
				LOGGER.warn("Failed to delete file "+file.getAbsolutePath()+" during cleanup.");
			}
		}
		cache.clear();
	}

	@Override
	public Properties getConfiguration() {
		return effectiveConfiguration;
	}

	@Override
	public FileResource findResources(String name) {
		File file;
		
		if (useCache && cache.containsKey(name)){
			file = cache.get(name);
		}else{
			file = download(name);
		}
		
		return (file!=null) ? new FileResource(file) : null;	//valid per contract
	}
	
	private File download(String name){
             	
		try {
			
			URL	url = new URL(baseURL.toString()+"/"+name);
        	File local = FILE_TREE.createTempCopyDestination("ta.url.repo"+name);
        	FileUtils.copyURLToFile(url, local);
        	
        	if (useCache){
        		cache.put(name, local);
        	}
        	
        	return local;
		} catch (MalformedURLException e) {
			
			LOGGER.info("URLRepository : could not load resource '"+name+"' from repository '"+baseURL+"' : malformed url ");
			return null;	
			
		} catch (IOException e) {
			
			LOGGER.info("URLRepository : could not load resource '"+name+"' from repository '"+baseURL+"' : malformed url ");
			return null; 	
			
		}
        
	}

	
	
	
	
}
