/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.instructionrunner

import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultExecuteCommandRunner
import org.squashtest.ta.backbone.exception.CannotApplyAssertionException
import org.squashtest.ta.backbone.exception.setup.IncompatibleNaturesException
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.backbone.exception.TargetNotFoundException
import org.squashtest.ta.backbone.exception.MalformedInstructionException
import org.squashtest.ta.framework.test.instructions.BinaryAssertionInstruction
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.Command
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.element.Resource
import org.squashtest.ta.framework.components.Target
import org.squashtest.ta.framework.exception.CannotApplyCommandException;
import org.squashtest.ta.framework.exception.BrokenTestException
import org.squashtest.ta.backbone.exception.IncompatibleNaturesException
import org.squashtest.ta.framework.components.Resource
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner;
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;


import spock.lang.Specification

class DefaultBinaryAssertionRunnerTest extends Specification {
	
	DefaultBinaryAssertionRunner runner
	InternalTestRunner testRunner
	BinaryAssertionInstruction instruction
	ContextManager manager
	
	
	def setup(){
		instruction = new BinaryAssertionInstruction()
		runner = new DefaultBinaryAssertionRunner(instruction)
		
		testRunner = Mock(InternalTestRunner)
		
		runner.setTestRunner testRunner
				
		manager = Mock(ContextManager)
		
	}
	
	def "should rant because the actual result name is missing"(){
		when :
			runner.checkSettings()
		
		then :
			thrown MalformedInstructionException
	}
	
	def "should rant because the expected result name is missing"(){
		given :
			instruction.getActualResultName() >> "actual"
		
		when :
			runner.checkSettings()
		then :
			thrown MalformedInstructionException
	}
	
	
	
	def "should rant because the assertion name is missing"(){
		given :
			instruction.getActualResultName() >> "actual"
			instruction.getExpectedResultName() >> "expected"
		
		when :
			runner.checkSettings()
		then :
			thrown MalformedInstructionException;
	}
	
	

	def "should be clean"(){
		given :
			mockBinaryAssertionInstruction()
			
		when :
			runner.checkSettings()
			
		then :
			true
	}
	

	
	def "should return the instruction"(){
		expect :
			runner.getInstruction() == instruction
		
	}
	
	def "should rant because the actual result is not found"(){
		given :
			mockBinaryAssertionInstruction()
			
		when :
			runner.getActualResult()
			
		then :
			thrown ResourceNotFoundException
		
	}

	
	def "should rant if the expected result is not found"(){
		given :
			mockBinaryAssertionInstruction()
						
		when :
			runner.getExpectedResult();
			
		then :
			thrown ResourceNotFoundException
		
	}
	
	
	def "should work normally"(){
		
		given :
			mockBinaryAssertionInstruction()
			def actual = mockActualResult()
			def expected = mockExpectedResult()
			
		and :
		
			def assertion = Mock(BinaryAssertionHandler)
			
		and :
			testRunner.getContextManager() >> manager
			manager.getBinaryAssertion(_) >> [assertion]
			
			testRunner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"actual")) >> actual
			testRunner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"expected")) >> expected
				
			
		when :
			runner.run()
			
		then :
			
			1 * assertion.setActualResult(actual)
			1 * assertion.setExpectedResult(expected)

		
	}


	def "should diagnose error and return incompatible nature exception"(){
		
		given :
			mockBinaryAssertionInstruction()
			def actual = mockActualResult()
			def expected = mockExpectedResult()
		
		and :
			testRunner.getContextManager() >> manager
			manager.getBinaryAssertion(_) >> [Mock(BinaryAssertionHandler)]
			
		when :
			def res = runner.diagnoseError(actual, expected, "test")
			
		then :
			res instanceof IncompatibleNaturesException
		
	}
	
	

	def "should diagnose error and return a CannotApplyAssertionException "(){
		
		given :
			mockBinaryAssertionInstruction()
			def actual = mockActualResult()
			def expected = mockExpectedResult()
			
		and :
			testRunner.getContextManager() >> manager
			manager.getBinaryAssertion(_) >> []
			
		when :
			def res = runner.diagnoseError(actual, expected, "test")
			
		then :
			res instanceof CannotApplyAssertionException
		
		
	}
	
	def "should fail because operand natures are not compatible with this binary assertion handler"(){
		
		given :
			mockBinaryAssertionInstruction()
			def actual = mockActualResult()
			def expected = mockExpectedResult()
			
		and :
			
			testRunner.getContextManager() >> manager
			testRunner.getResourceFromCache(_) >>> [actual, expected]
		and :
			
			//first try it returns no result, second time it returns something
			//since constraints are relaxed
			manager.getBinaryAssertion(_) >>> [ [], [1] ]
			
		when :
			runner.setupAssertion(actual, expected)
			
		then :
			thrown IncompatibleNaturesException
	}
	/* *************************** ************************** */
	
	def mockBinaryAssertionInstruction = {
		instruction.setActualResultName(new ResourceName(Scope.SCOPE_TEST,"actual"))
		instruction.setExpectedResultName(new ResourceName(Scope.SCOPE_TEST,"expected"))
		instruction.setAssertionCategory("assertion")
		//we don't want configuration for now ==> no need to assign one, as an empty configuration is created by default 
	}
	
	def mockActualResult = {
		def actual = Mock(ResourceWrapper)
		def actNature = Mock(Nature)
		actNature.getName() >> "nature13"
		actual.getNature() >> actNature
		actual.getName() >> new ResourceName(Scope.SCOPE_TEST,"actual")
		return actual		
	}
	
	def mockExpectedResult = {
		def expected = Mock(ResourceWrapper)
		def expNature = Mock(Nature)
		expNature.getName() >> "nature23"
		expected.getNature() >> expNature
		expected.getName() >> new ResourceName(Scope.SCOPE_TEST,"expected")
		return expected		
		
	}
	
}
