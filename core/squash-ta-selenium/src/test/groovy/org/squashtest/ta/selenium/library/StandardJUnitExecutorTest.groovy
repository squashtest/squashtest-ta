/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.library

import org.junit.runner.Result;

import spock.lang.Specification

class StandardJUnitExecutorTest extends Specification {

	
	def "should run a JUnit 3 test"(){
		
		given :
			
			def mainClass = Class.forName("org.squashtest.ta.selenium.stuffs.SampleJUnit3")
		
			def res = Mock(JavaExecutableUnit)
			res.getMainClass() >> mainClass;
			
			def command = new StandardJUnitExecutor();
			
			
		when :
			
			command.setResource(res)
			def result = command.apply()
		
		
		then :
			
			result instanceof Result
			result.getFailureCount() == 1
			result.fFailures[0].getMessage() == "expected:<Howdy [Mike] !> but was:<Howdy [Bob] !>"
		
	}
	
	def "should run a JUnit 4 test"(){
		
		given :
			
			def mainClass = Class.forName("org.squashtest.ta.selenium.stuffs.SampleJUnit4")
		
			def res = Mock(JavaExecutableUnit)
			res.getMainClass() >> mainClass;
			
			def command = new StandardJUnitExecutor();
			
			
		when :
			
			command.setResource(res)
			def result = command.apply()
		
		
		then :
			
			result instanceof Result
			result.getFailureCount() == 1
			
			result.fFailures[0].getMessage()  == "expected:<Howdy [Mike] !> but was:<Howdy [Bob] !>"
		
	}
	
	
	
   def "should run a run a JU3 test class loaded at runtime by a different classloader"(){
	   
	   given :
		   def className = "org.squashtest.ta.selenium.stuffs.RuntimeJUnit3Class"
	   and :
		   def isInitiallyNotFound = currentClassloaderDontKnowClass(className)
		   
	   and :
	   
		   def classBytes = loadBytes("runtime-classes/org/squashtest/ta/selenium/stuffs/RuntimeJUnit3Class.class")
		   
	   and :
		   def classloader = prepareClassLoader(className, classBytes)
	   
	   and :
		   def test = Mock(JavaExecutableUnit)
		   test.getMainClass() >> { classloader.loadClass(className) }
		   
	   and :
		   def command = new StandardJUnitExecutor();
	   
	   when :
		   command.setResource(test)
		   def result = command.apply();
	   
	   then :
		   true == isInitiallyNotFound
		   result.getFailureCount() == 1
	   
   }
   
	
	
	def "should run a run a JU4 test class loaded at runtime by a different classloader"(){
		
		given :
			def className = "org.squashtest.ta.selenium.stuffs.RuntimeJUnit4Class"
		and :
			def isInitiallyNotFound = currentClassloaderDontKnowClass(className)
			
		and :
		
			def classBytes = loadBytes("runtime-classes/org/squashtest/ta/selenium/stuffs/RuntimeJUnit4Class.class")
			
		and :
			def classloader = prepareClassLoader(className, classBytes)
		
		and :
			def test = Mock(JavaExecutableUnit)
			test.getMainClass() >> { classloader.loadClass(className) }
			
		and :
			def command = new StandardJUnitExecutor();
		
		when :
			command.setResource(test)
			def result = command.apply();
		
		then :
			true == isInitiallyNotFound
			result.getFailureCount() == 1
		
	}
	
	
	
	
	
	// **************************************** scaffolding code *******************************
	
	
	def currentClassloaderDontKnowClass(String className){
		try{
			this.getClass().getClassLoader().loadClass(className)
			return false;
		}catch(ClassNotFoundException ex ){
			return true;
		}
	}
	
	def loadBytes(String fileName){
		def URL bytesURL = this.getClass().getClassLoader()
		.getResource(fileName);
	
		return new File(bytesURL.toURI()).getBytes();
	}
	
	def prepareClassLoader(className, classBytes){
		
		def loader = new ClassLoader(this.getClass().getClassLoader()){
			public Class<?> addClass(String name, byte[] bytes, int off, int len){
				return super.defineClass(name, bytes, off, len);
			}
		};
	
		def debug = loader.addClass(className, classBytes, 0, classBytes.length)
		
		return loader;
		
	}
	
}
