/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.test;

import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.result.ResourceGenerator;
import org.squashtest.ta.framework.test.result.ResourceMetadata;

public class DefaultResourceMetadata implements ResourceMetadata {

	private ResourceName name;
	private ResourceGenerator origin;
	private String resourceType;
	private ResourceRole role;
	
	/* ******************* API ****************** */
	
	public ResourceName getName() {
		return name;
	}

	public ResourceGenerator getOrigin() {
		return origin;
	}

	public String getResourceType() {
		return resourceType;
	}

	public ResourceRole getRole() {
		return role;
	}

	
	/* ********************* setters *************** */

	public void setName(ResourceName name) {
		this.name = name;
	}

	public void setOrigin(ResourceGenerator origin) {
		this.origin = origin;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public void setRole(ResourceRole role) {
		this.role = role;
	}
	
	/* ********************* ctors ******************* */
	
	public DefaultResourceMetadata(ResourceWrapper wrapper){
		this.name = wrapper.getName();
		this.origin = wrapper.getGenerator();
		this.resourceType = wrapper.getNature().getName();
		//we cannot infer the r�le from here
	}
}
