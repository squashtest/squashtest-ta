/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import org.squashtest.ta.backbone.exception.NotUniqueEntryException;

public class MultiDimensionMapImpl<T> implements MultiDimensionMap<T>{
	

	private ArrayList<Entry> objectMap = new ArrayList<Entry>(1);
	
	
	private int dimension;
	
	public MultiDimensionMapImpl(int dimension) {
		
		if (dimension<=0){
			throw new IllegalArgumentException("dimension cannot be null");
		}
		
		this.dimension=dimension;
	}

	public int getDimension() {
		return dimension;
	}

	public void put(T element, Object... key) {
		
		//check the dimension
		if (key.length != dimension){
			throw new IllegalArgumentException("key of wrong dimension : expected dimension "+dimension);
		}
		
		//check that ANY is not part of the key set
		for (Object o : key){
			if (o==ANY){
				throw new IllegalArgumentException("ANY is not a valid key element for this operation");
			}
		}
		
		//is the object already in that map ? sorry for the overhead btw.
		Collection<T> elts= get(key);
		if (elts.size()>0){
			throw new NotUniqueEntryException("cannot insert new element : an element corresponding to the same coordinates already exists");
		}
		
		//let's insert the element
		ArrayList<Entry> currentDimension = objectMap;
		
		for (Object k : key){
			Entry e = getEntry(currentDimension, k);
			if (e==null){
				e = extendMap(currentDimension, k);
			}
			currentDimension=e.getDimension();
		}
		
		//at this point we reached what should be an entry with empty entry, we can put the element there
		Entry eltEntry= new Entry(element);
		currentDimension.add(eltEntry);
		
	}

	public void remove(Object... key) {		
		
		//check the dimension
		if (key.length != dimension){
			throw new IllegalArgumentException("key of wrong dimension : expected dimension "+dimension);
		}
		SearchAction<T> action = new RemoveAction<T>();
		exploreSubDimensions(objectMap,action,key);		
	}

	public Collection<T> get(Object... key) {
		if (key.length!=dimension){
			throw new IllegalArgumentException("key of wrong dimension : expected dimension "+dimension);
		}
		SearchAction<T> action = new CollectAction<T>();
		return exploreSubDimensions(objectMap, action,key);		
	}
	
	private Collection<T> exploreSubDimensions(ArrayList<Entry> dimension, SearchAction<T>  action, Object... subKey){
		if (subKey.length==0){
			//that's were we end our search
			return action.doWith(dimension);

		}else{		
			Collection<T> result = new LinkedList<T>();
			
			//create the new subKey for the next recursive call
			Object[] subSubKey = createSubKey(subKey);
			
			//now let's call this method recursively
			Object currentKey = subKey[0];
			if (currentKey==ANY){
				for (Entry e : dimension){
					result.addAll(exploreSubDimensions(e.getDimension(), action, subSubKey));
				}
			}else{
				Entry e = getEntry(dimension, currentKey);
				if (e!=null){
					result = exploreSubDimensions(e.getDimension(), action, subSubKey);
				}
			}
			
			return result;
		}		
	}
	

	
	
	
	private Object[] createSubKey(Object[] key){
		int keyLength = key.length;
		Object[] subKey = new Object[keyLength-1];
		
		for (int i=1;i<key.length;i++){
			subKey[i-1]=key[i];
		}		
		
		return subKey;
	}
	
	/**
	 * 
	 * @param dimension
	 * @param dimensionValue
	 * @return the Entry if the corresponding dimensionValue is found, null if not found.
	 */
	private Entry getEntry(ArrayList<Entry> dimension, Object dimensionValue){
		for (Entry e : dimension){
			if (e.getKey().equals(dimensionValue)){
				return e;
			}
		}
		return null;
	}
	
	/**
	 * <p>That method will extend the current dimension with a new entry. Basically, it extends the list corresponding to that dimension
	 * by one, so that we can hold in there a new ArrayList for newKey as a new value for this dimension.</p>
	 * 
	 * <p>The newKey will be added regardless of it being already present, so be sure to check first that this value was not already there in the 
	 * first place.</>
	 * 
	 * @param newKey
	 * @return newly created entry
	 */
	private Entry extendMap(ArrayList<Entry> dimension, Object dimensionValue){
		Entry newEntry = new Entry(dimensionValue);
		dimension.add(newEntry);
		return newEntry;
	}

	/**
	 * An entry pairs a n-th key with it's content representing what's coming from n+1 to map.dimension.
	 * <p>A special case if for the last dimension, where the field 'key' will hold the element T to store
	 * and the list should stay empty.</p>
	 * @author bsiri
	 *
	 */
	private static class Entry{
		private Object key;
		private ArrayList<Entry> dimension = new ArrayList<Entry>(1);
		
		public Entry(Object key){
			this.key = key;
		}
		
		public Object getKey(){
			return key;
		}
		
		public ArrayList<Entry> getDimension(){
			return dimension;
		}
	}
	
	/**
	 * that interface tells what to do when we finished to explore the map 
	 * 
	 * @author bsiri
	 *
	 */
	private static interface SearchAction<T>{
		Collection<T> doWith(ArrayList<Entry> dimension);
	}
	
	private static class CollectAction<T> implements SearchAction<T>{

		public Collection<T> doWith(ArrayList<Entry> dimension) {
			if (dimension.size()>0){
				Entry e = dimension.get(0);
				Collection<T> res = new ArrayList<T>();
				res.add((T)e.getKey());
				return res;
			}else{
				return Collections.emptyList();
			}
		}	
	}
	
	private static class RemoveAction<T> implements SearchAction<T>{
		public Collection<T> doWith(ArrayList<Entry> dimension) {
			if (dimension.size()>0){
				dimension.remove(0);
			}
			return Collections.emptyList();
		}
	}
	
}
