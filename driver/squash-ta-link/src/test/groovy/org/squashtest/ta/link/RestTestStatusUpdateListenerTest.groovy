/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.link

import org.squashtest.ta.api.execution.client.StatusUpdate
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus

import spock.lang.Specification;

class RestTestStatusUpdateListenerTest extends Specification{
	StatusUpdate mockService
	RestTestStatusUpdateListener testee
	TestStatusUpdate event
	TestResult mockResult
	Date startTime
	
	final String MOCK_TEST="mockTest"
	final String MOCK_TEST_GROUP="mockTestGroup"
	
	def setup(){
		mockService=Mock()
		testee=new RestTestStatusUpdateListener(mockService,"testHost","testJob", "testId")
		event=Mock()
		
		startTime=new Date(2385)
		
		mockResult=Mock()
		mockResult.getName()>>MOCK_TEST
		mockResult.getStatus()>>GeneralStatus.SUCCESS
		mockResult.startTime()>>startTime
		
		event.getPayload() >> mockResult
		event.getEcosystemName()>>MOCK_TEST_GROUP
	}
	
	def "should transmit test host name"(){
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus("testHost", _, _, _, _, _)
	}
	
	def "should transmit job name"(){
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_, "testJob", _, _, _, _)
	}

	def "should transmit external ID"(){
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,_,"testId", _, _, _)
	}
	
	def "should transmit test group name"(){
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,_,_,"mockTestGroup", _, _)
	}
	
	def "should transmit test name"(){
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,_,_,_,"mockTest",_)
	}
	
	def "should transmit SUCCESS event"(){
		given:
			Date endTime=new Date(3392)
		and:
			mockResult.endTime()>>endTime
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,_,_,_,_,{
				it instanceof TestExecutionStatus
				TestExecutionStatus status=it
				startTime.equals(status.getStartTime())
				endTime.equals(status.getEndTime())
				status.getStatus()==ExecutionStatus.SUCCESS
				status.getStatusMessage()==null
				status.getTestGroupName()==MOCK_TEST_GROUP
				status.getTestName()==MOCK_TEST
			})
	}
	
	def "should transmit FAILURE event"(){
		given:
			Date endTime=new Date(3325)
		and:
			ExecutionDetails detail=Mock()
			String expectdMessage="expected error message"
			detail.caughtException()>>new Exception(expectdMessage)
		and:
			mockResult.endTime()>>endTime
			mockResult.getStatus()>>GeneralStatus.FAIL
			mockResult.getFailureReport()>>detail
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,_,_,_,_,{
				it instanceof TestExecutionStatus
				TestExecutionStatus status=it
				startTime.equals(status.getStartTime())
				endTime.equals(status.getEndTime())
				status.getStatus()==ExecutionStatus.FAILURE
				status.getStatusMessage()==expectdMessage
				status.getTestGroupName()==MOCK_TEST_GROUP
				status.getTestName()==MOCK_TEST
			})
	}
	
	def "should transmit WARNING event"(){
		given:
			Date endTime=new Date(3325)
		and:
			ExecutionDetails detail=Mock()
			String expectdMessage="expected error message"
			detail.caughtException()>>new Exception(expectdMessage)
		and:
			mockResult.endTime()>>endTime
			mockResult.getStatus()>>GeneralStatus.WARNING
			mockResult.getFailureReport()>>detail
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,_,_,_,_,{
				it instanceof TestExecutionStatus
				TestExecutionStatus status=it
				startTime.equals(status.getStartTime())
				endTime.equals(status.getEndTime())
				status.getStatus()==ExecutionStatus.WARNING
				status.getStatusMessage()==expectdMessage
				status.getTestGroupName()==MOCK_TEST_GROUP
				status.getTestName()==MOCK_TEST
			})
	}
	
	def "should transmit RUNNING event"(){
		given:
			mockResult.getStatus()>>GeneralStatus.RUNNING
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,_,_,_,_,{
				it instanceof TestExecutionStatus
				TestExecutionStatus status=it
				startTime.equals(status.getStartTime())
				status.getEndTime()==null
				status.getStatus()==ExecutionStatus.RUNNING
				status.getStatusMessage()==null
				status.getTestGroupName()==MOCK_TEST_GROUP
				status.getTestName()==MOCK_TEST
			})
	}
}
