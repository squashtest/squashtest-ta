/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.resources

import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.exception.BadDataException;

import spock.lang.Specification

class CommandLineResourceTest extends Specification {
	CommandLineResource testee
	File file
	
	def setup(){
		file=File.createTempFile("cmd", ".txt")
		file.deleteOnExit()
	}
	
	def "Must be able to read single line"(){
		given:
			BinaryData data=new BinaryData(getClass().getResource("shellCommandTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
	
	def "Must be able to read first non-comment line"(){
		given:
			BinaryData data=new BinaryData(getClass().getResource("shellCommandCommentTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
	
	def "Ignore excess code lines (take first)"(){
		given:
			BinaryData data=new BinaryData(getClass().getResource("shellCommandMultilineTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
	
	def "should hang on empty"(){
		given:
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getCommand()
		then:
			thrown(BadDataException)
	}
	
	def cleanup(){
		file.delete()
	}
}
