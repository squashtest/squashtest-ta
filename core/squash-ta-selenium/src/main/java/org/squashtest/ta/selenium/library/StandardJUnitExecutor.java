/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.library;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.squashtest.ta.core.tools.ExceptionLogger;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;

public class StandardJUnitExecutor {
	private static final ExceptionLogger logger = new ExceptionLogger(StandardJUnitExecutor.class, IllegalConfigurationException.class);
	
	public static final String ARG_MAIN_CLASS = "mainclass";
	
	private JavaExecutableUnit javaTest;
	
	private Collection<Resource<?>> configuration = new ArrayList<Resource<?>>(); 
	
	
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	
	public void setTarget(VoidTarget target) {
		//no target used
	}

	
	public void setResource(JavaExecutableUnit resource) {
		this.javaTest = resource;
	}

	
	public Result apply() {
		
		Class<?> main = getMainClass();
		
		if (main==null){
			logger.errAndThrow("junit test : no main class defined neither as in command configuration nor in the resource configuration", null);
		}
		
		JUnitCore junit = new JUnitCore();
		return junit.run(main);
		
	}

	
	public void cleanUp() {
		// TODO Auto-generated method stub
	}
	
	
	// ****************** private stuffs ********************
	
	private Class<?> getMainClass(){
		Map<String, String> options = readOptions();
		
		if (options != null){
			return getClassFromDedicatedCL(options.get(ARG_MAIN_CLASS));
		}
		else{
			return javaTest.getMainClass();
		}
		
	}
	
	private Map<String, String> readOptions(){
		for (Resource<?> resource : configuration){
			if (FileResource.class.isAssignableFrom(resource.getClass())){
				File file = ((FileResource)resource).getFile();
				return extractOptions(file);
			}
		}
		return null;
	}

	private Map<String, String> extractOptions(File file) {
		try {
			return OptionsReader.BASIC_READER.getOptions(file);
		} catch (IllegalArgumentException e) {
			throw logger.errAndThrow("junit test : supplied configuration has an invalid format", e);
		} catch (IOException e) {
			throw logger.errAndThrow("junit test : supplied configuration ", e);
		}
	}

	private Class<?> getClassFromDedicatedCL(String fullClassName){
		
		if (fullClassName == null){
			throw logger.errAndThrow("junit test : a configuration was supplied, however it does not specify '"+ARG_MAIN_CLASS+"'", null);
		}
		
		ClassLoader loader = javaTest.getDedicatedClassloader();
		
		try 
		{
			return loader.loadClass(fullClassName);
		} 
		catch (ClassNotFoundException e) {
			throw logger.errAndThrow("junit test : main class '"+fullClassName+"' does not exist for the supplied test", e);
		}
	}

}
