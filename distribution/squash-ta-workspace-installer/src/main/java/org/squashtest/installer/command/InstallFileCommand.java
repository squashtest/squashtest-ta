/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.command;

import java.io.File;
import java.io.IOException;

import org.squashtest.installer.context.Context;
import org.squashtest.installer.tools.FileUtils;
import org.squashtest.installer.tools.PlaceHolderFiller;


/**
 * 
 * That class is meant to 
 * <ol>
 * <li>process a file via the PlaceHolderFiller and </li>
 * <li>copy the result in a given directory</li>
 * </ol>
 * 
 * Note : The class will detect if the filenames are absolute or relative. If relative path
 * 		are given, they will be considered relative to context.getWorkingDirectory(). In the case of the installPath,
 * 		relative paths are considered relative to the context.outputDirectory
 *  
 * @author bsiri
 *
 */
public class InstallFileCommand implements Command{

	private Context context;
	
	
	File templateFile;
	File resultFile;
	File installDirectory;
	
	PlaceHolderFiller filler;
	
	
	/**
	 * sets the template file we need to fill with the placeholderfiller.
	 * 
	 *  If the filename is relative, it will be assumed relative to the context.workingDirectory.
	 * 
	 * @param templateFile
	 */
	public void setTemplateFile(String templateFile) {
		this.templateFile = attachIfNeeded(templateFile).to(context.getWorkingDirectory());
	}

	/**
	 * sets the file resulting from the placeholderifller process.
	 * 
	 *  If the filename is relative, it will be assumed relative to the context.workingDirectory.
	 * @param resultFile
	 */
	public void setResultFile(String resultFile) {
		this.resultFile = attachIfNeeded(resultFile).to(context.getWorkingDirectory());
	}

	/**
	 * In this case, relative paths are considered relative to the context.outputDirectory (and not context.workingDirectory)
	 * @param installDirectory
	 */
	public void setInstallDirectory(String installDirectory) {
		this.installDirectory = attachIfNeeded(installDirectory).to(context.getOutputDirectory());
	}

	@Override
	public void setContext(Context context) {
		this.context=context;
		filler = new PlaceHolderFiller(context);
	}

	public InstallFileCommand(){
		
	}
	
	public InstallFileCommand(Context context,String template, String output, String installDir){

		setContext(context);
		
		setTemplateFile(template);
		setResultFile(output);
		setInstallDirectory(installDir);
	}
		
	
	@Override
	public Context getContext() {
		return context;
	}

	@Override
	public void execute() {
		try{
			prepareFile();
			copyInWorkspace();
		}catch(IOException ioe){
			throw new RuntimeException(ioe);
		}
	}
	
	
	protected void prepareFile(){
		try{			
			resultFile.createNewFile();
			
			filler.setInputFile(templateFile);
			filler.setOutputFile(resultFile);
			
			filler.process();
	
		}catch(IOException ioe){
			throw new RuntimeException(ioe);
		}			
	}
	
	
	protected void copyInWorkspace() throws IOException{
		
		//build the destination file
		if (! installDirectory.exists()){
			installDirectory.mkdirs();
		}
		
		File destFile = new File(installDirectory, resultFile.getName());
		
		
		if (destFile.exists()){
			destFile.delete();
		}
		destFile.createNewFile();
		FileUtils.copyFile(resultFile, destFile);
		
	}
	
	/* ************************************************************************************************ */
	
	/* The filename will be treated as a path. If it's actually an absolute path it will be kept as is.
	 * if it's not, then it will be considered as relative to the defaultRoot.
	 * It's ALWAYS used with the class defined right below, in order to make it more DSL-like. It's so
	 * because I couldn't find an appropriate method name.
	 */
	private RootAttacher attachIfNeeded(String filename){
		return new RootAttacher(filename);
	}
	
	
	private class RootAttacher{
		String filename;
		RootAttacher(String s){filename=s;}
		public File to(File defaultRoot){
			File result;
			File test = new File(filename);
			if (! test.isAbsolute()){
				result=new File(defaultRoot, filename);
			}else{
				result=test;
			}
			return result;			
		}
	}
	
}
