// this test validates the replacement of primary keys by pseudo-primary keys.
test :
LOAD datasets/nopk_dataset.xml AS dataset.file
CONVERT dataset.file TO xml(structured) AS dataset.xml
CONVERT dataset.xml TO dataset.dbunit(dataset) AS dataset

LOAD conf/multi_column_pk.properties AS ppkfilter.properties
CONVERT ppkfilter.properties TO conf.dbunit.ppk AS ppkfilter

EXECUTE insert ON mydb WITH dataset AS doncare

EXECUTE delete ON mydb WITH dataset USING $(operation:delete),ppkfilter AS forget