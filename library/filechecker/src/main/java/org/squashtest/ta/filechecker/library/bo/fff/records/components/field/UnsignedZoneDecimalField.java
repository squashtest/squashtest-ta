/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.UserInput;

public final class UnsignedZoneDecimalField extends AbstractVariableFixedField<Double> implements UserInput {

    private int decNb;

    // création initiale
    public UnsignedZoneDecimalField(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength, int pDecNb) {
        label = pLabel;
        description = pDescription;
        start = pStart;
        length = pLength;
        decNb = pDecNb;
    }

    @Override
    public Object clone() {
        return new UnsignedZoneDecimalField(label, description, start, length, decNb);
    }

    /**
     * @return La valeur du champ, formattée de la manière suivante :<br>
     *         --> les décimales sont explicites<br>
     *         --> les zéros non significatifs n'apparaissent pas<br>
     *         --> le séparateur, lorsqu'il est présent, est un point<br>
     */
    @Override
    public String toString() {
        return getFunctionalValue().toString();
    }

    /**
     * @return La valeur du champ, formattée de la manière suivante :<br>
     *         --> les décimales sont explicites<br>
     *         --> les zéros non significatifs n'apparaissent pas<br>
     *         --> le séparateur, lorsqu'il est présent, est un point<br>
     */
    @Override
    public StringBuffer getValue() {
        return getFunctionalValue();
    }

    /**
     * @return La valeur du champ, formattée de la manière suivante :<br>
     *         --> les décimales sont explicites<br>
     *         --> les zéros non significatifs n'apparaissent pas<br>
     *         --> le séparateur, lorsqu'il est présent, est un point<br>
     */
    private StringBuffer getFunctionalValue() {
        StringBuffer formattedValue = null;
        if (null != value) {
            formattedValue = new StringBuffer();
            Double numValue = Double.parseDouble(value.toString());
            Double remainder = numValue % Math.pow(10, decNb);
            numValue /= Math.pow(10, decNb);
            if (remainder.equals(0d)) {
                formattedValue.append(numValue.longValue());
            } else {
                formattedValue.append(numValue);
            }
        }
        return formattedValue;
    }

    @Override
    public void setValue(Double pValue) throws InvalidSyntaxException {
        String longValue = "" + (long ) Math.floor(pValue.doubleValue() * Math.pow(10, decNb));
        value = new StringBuffer(longValue);
    }

    @Override
    public void validate() throws InvalidSyntaxException {
    }

    /*@Override
    public String toFileFormat() {
        throw new UnsupportedOperationException();
    }*/

	@Override
	public FixedFieldType getFieldType() {
		return FixedFieldType.unsignedZoneDecimal;
	}
    
    

}
