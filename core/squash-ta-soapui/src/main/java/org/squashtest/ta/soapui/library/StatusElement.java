/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.soapui.library;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eviware.soapui.model.testsuite.TestRunner.Status;

final class StatusElement implements Element {
	private static final Logger LOGGER = LoggerFactory.getLogger(StatusElement.class);
	private Status status;
	@Override
	public Element add(String input, Map<String, Status> statusMap,
			Map<String, String> messageMap) {
		Element el;
		if(status==null){
			status=Status.valueOf(input);
			el=this;
			LOGGER.debug("Status: "+status);
		}else{
			statusMap.put(input, status);
			el=new IntervalElement();
			LOGGER.debug("Test name: "+input);
		}
		return el;
	}
}