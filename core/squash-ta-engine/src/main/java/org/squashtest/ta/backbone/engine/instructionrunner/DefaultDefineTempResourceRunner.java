/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.ResourceGenerator;

public class DefaultDefineTempResourceRunner extends
		AbstractDefaultInstructionRunner {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DefaultDefineTempResourceRunner.class);

	private static final String MISSING_TEMP_RESOURCE_NAME = "the name of the resource was not supplied";
	private static final String MISSING_TEMP_RESOURCE_CONTENT = "the content of the new resource was not supplied";
	private static final String GENERIC_ERROR_MESSAGE = "Cannot create temporary resource : ";

	private DefineResourceInstruction instruction;

	public DefaultDefineTempResourceRunner(DefineResourceInstruction instruction) {
		this.instruction = instruction;
	}

	@Override
	protected void checkSettings() {
		if (instruction.getResourceContent() == null) {
			logAndThrow(GENERIC_ERROR_MESSAGE + MISSING_TEMP_RESOURCE_CONTENT);
		}

		if (instruction.getResourceName() == null) {
			logAndThrow(GENERIC_ERROR_MESSAGE + MISSING_TEMP_RESOURCE_NAME);
		}
	}

	@Override
	protected TestInstruction getInstruction() {
		return instruction;
	}

	@Override
	protected void doRun() {
		try {
			List<String> lines = instruction.getResourceContent();
			File file = File.createTempFile("temp.resource", "instruction");
			/*
			 * SimpleLinesData seems to fit the bill, however we don't want to
			 * add the final EOL marker because it triggers [Issue #1061], so we
			 * do it the hard way. Users will have to add the \n marker
			 * explicitly to terminate the file by an EOL marker.
			 */
			StringBuilder resourceContent=new StringBuilder();
			for(String line:lines){
				resourceContent.append(line).append("\n");
			}
			if(resourceContent.length()>0){
				resourceContent.setLength(resourceContent.length()-"\n".length());
			}
			BinaryData resourceData=new BinaryData(resourceContent.toString().getBytes("UTF-8"));
			resourceData.write(file);
			file.deleteOnExit();

			ResourceWrapper resource = new ResourceWrapper(
					Nature.FILE_RESOURCE_NATURE, instruction.getResourceName(),
					new FileResource(file), new TemporaryResourceGenerator());
			testRunner.addResourceToCache(resource);

		} catch (IOException ex) {
			BrokenTestException exception = new BrokenTestException(
					"cannot create temp resource of content ("
							+ instruction.getResourceContent()
							+ ") due to physical errors from the test platform",
					ex);
			LOGGER.warn(exception.getMessage());
			throw exception;
		}
	}

	@Override
	protected void addInputToFailureReport(ExecutionDetails executionDetails) {
		//NOOP: not so useful here!
	}
	
	private static class TemporaryResourceGenerator implements ResourceGenerator {

		public String getName() {
			return "temp.resource.generator";
		}

		public Properties getConfiguration() {
			return new Properties();
		}

		@Override
		public String getGeneratorType() {
			return getName();
		}

	}

	@Override
	protected String getInstructionGenericFailureMessage() {
		return GENERIC_ERROR_MESSAGE;
	}

}
