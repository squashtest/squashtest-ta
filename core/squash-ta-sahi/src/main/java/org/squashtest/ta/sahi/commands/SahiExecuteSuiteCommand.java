/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.sahi.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.library.BundleComponent;
import org.squashtest.ta.commons.resources.PropertiesResource;
import org.squashtest.ta.commons.targets.WebTarget;
import org.squashtest.ta.core.tools.ExceptionLogger;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.sahi.library.BasicSahiCommandHelper;
import org.squashtest.ta.sahi.resources.SahiSuiteResource;
import org.squashtest.ta.sahi.resources.SahiSuiteResultResource;

/**
 * This command executes a Sahi test suite given as input resource ({@link SahiSuiteResource}) against its target web site (passed as a {@link WebTarget}).
 * <p><strong>Returns : </strong> {@link #apply()} returns a {@link SahiSuiteResultResource}</p>
 * 
 * @author edegenetais
 *
 */
@EngineComponent("execute")
public class SahiExecuteSuiteCommand extends BundleComponent implements Command<SahiSuiteResource, WebTarget> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SahiExecuteSuiteCommand.class);

	private static ExceptionLogger LOGGER_WRAPPER = new ExceptionLogger(SahiExecuteSuiteCommand.class, IllegalConfigurationException.class);

	private BasicSahiCommandHelper helper = new BasicSahiCommandHelper();
	
	private Collection<Resource<?>> confResources = new ArrayList<Resource<?>>();
	
	private SahiSuiteResource suite;
	
	/** Noarg constructor for Spring enumeration. */
	public SahiExecuteSuiteCommand(){}
	
	/**
	 * This command expects sahi configuration from one or more resources
	 * injected as configuration. Each configuration element can be
	 * either a {@link FileResource}, or a
	 * {@link PropertiesResource}. The configuration resources can be given in any order.
	 * <p>There are two types of parameters:</p>
	 * <ul><li><p>Sahi engine parameters.</p>
	 * These parameters are defined using the properties format (on each line, a key/value pair in the <code>key</code>=<code>value</code> format).
	 * Default values are taken for any parameter except <code>browserType</code>, which MUST be injected. See the Sahi documentation for more information on its parameters.
	 * </li>
	 * <li><p>Execute Sahi Command parameters.</p>
	 * The sahi command parameter files contain a comma separated list of key/value pairs following the <code>key</code><strong>:</strong><code>value</code> format. 
	 * There is currently only one defined parameter: <code>mainpath</code>. 
	 * As a SahiSuiteResource may contain several sahi scripts, the file to execute must be defined. 
	 * The <code>mainpath</code> parameter defines the {@link SahiSuiteResource} file to execute by giving its path relative to the {@link SahiSuiteResource} base directory. 
	 * This parameter is optional because the Sahi suite may define its main file. 
	 * If both the suite and the command have a main file definition, the parameters of the command override the main file definition of the {@link SahiSuiteResource}. 
	 * If neither define a main file, an {@link IllegalConfigurationException} occurs. Please note that if the {@link SahiSuiteResource} is defined from a single file, this file is automatically defined as main file.
	 * </li>
	 * </ul>
	 */
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.confResources.addAll(configuration);
	}


	@Override
	public void setTarget(WebTarget target) {
		helper.setTarget(target);
	}

	@Override
	public void setResource(SahiSuiteResource resource) {
		suite=resource;
	}

	@Override
	public SahiSuiteResultResource apply() {
		configure();
		
		return helper.apply();
	}
	
	@Override
	public void cleanUp() {
		helper.cleanUp();
	}

	
	
	private void configure() {
		//first, read all options data
		File mainFile=null;
		
		List<Resource<?>>commandConfiguration=new ArrayList<Resource<?>>();
		for (Resource<?> resource : confResources) {
			if (isOptions(resource)) {
				commandConfiguration.add(resource);
			} else {
				helper.readPropertiesFrom(resource);
			}
		}
		try{
			mainFile=extractMainFileFromConfiguration(commandConfiguration, null);
		}catch (IOException ex) {
			throw LOGGER_WRAPPER.errAndThrow(
					"execute sahi bundle : failed to read configuration.", ex);
		}
		
		//then, decide main file
		if(mainFile==null){
			mainFile=suite.getMain();
		}
		if (mainFile == null){
			throw LOGGER_WRAPPER.errAndThrow("execute sahi bundle : cannot run bundle, main script wasn't defined", null);
		}else{
			helper.setSuite(mainFile);
		}
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}
	
}
