/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.filechecker.assertions;

import java.util.Collection;

import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.InvalidStructureException;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.Records;
import org.squashtest.ta.filechecker.resources.FFFResource;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;

@EngineComponent("valid")
public class FffIsValid implements UnaryAssertion<FFFResource> {

	private Records records;
	
	@Override
	public void setActualResult(FFFResource actual) {
		records = actual.getRecords();
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		// No op
	}

	@Override
	public void test() throws AssertionFailedException {
		
        try {
			records.validate();
		} catch (InvalidSyntaxException e) {
			throw new AssertionFailedException(e.getMessage(),null,null);
		} catch (InvalidStructureException e) {
			throw new AssertionFailedException(e.getMessage(),null,null);
		}
    	
	}

}
