/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools

import java.io.File;
import java.util.Properties;

import org.squashtest.ta.core.library.properties.DefaultPropertiesSource;
import org.squashtest.ta.core.library.properties.NamespacedPropertiesSource;
import org.squashtest.ta.core.library.properties.PropertiesSource;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;

import spock.lang.Specification;

class PropertiesBasedCreatorHelperTest extends Specification {

	
	def "should make a default source using the system properties"(){
		given :
			def tester = new PropertiesBasedCreatorHelper();
		
		when :
			def source = tester.newDefaultSource();
			
		then :
			source instanceof DefaultPropertiesSource
			source.ppts == System.getProperties()
	}
	
	def "should make a namespaced source using the system properties"(){
		given :
			def tester = new PropertiesBasedCreatorHelper();
		
		when :
			def source = tester.newNamespacedSource("bob");
			
		then :
			source instanceof NamespacedPropertiesSource
			source.ppts == System.getProperties()
			source.namespace == "bob"
	}
	
	
	def "should get the effective keyset"(){
		
		given :
			def url = getClass().getClassLoader().getResource("repositorytest/some.random.properties");
			File file = new File(url.toURI())
		
		and :
			//"default level" properties
			System.setProperty "ta.random.append.thing3", "thing3"
			
			//"priority level" properties
			System.setProperty "some.random.ta.random.append.moar4", "moar4"
			
		and :
			def tester = new PropertiesBasedCreatorHelper()
			tester.setKeys(["ta.random.albert", "ta.random.bob"] as String[])
			tester.setKeysRegExp(["ta.random.append.*"] as String[])
			
		and :
			def properties = tester.toProperties(file)
			def configSource = new DefaultPropertiesSource(properties)
			def defaultSource = tester.newDefaultSource()
			def namespacedSource = tester.newNamespacedSource("some.random")
		
		when :
			def keySet = tester.buildEffectiveKeySet(namespacedSource, configSource, defaultSource)
			
		
		then :
			keySet.containsExactly(["ta.random.albert", "ta.random.bob", "ta.random.append.data1", 
									"ta.random.append.stuff2", "ta.random.append.thing3", 
									"ta.random.append.moar4"])
		
		cleanup :
			System.clearProperty "ta.random.append.thing3"
			System.clearProperty "ta.random.append.moar4"
		
	}
	
	
	def "should get effective properties"(){
		
		given :
			def url = getClass().getClassLoader().getResource("repositorytest/some.random.properties");
			File file = new File(url.toURI())
			
		and :
			//"default level" properties
			System.setProperty "ta.random.albert", "Monaco" 
			System.setProperty "ta.random.darry", "Cowl"
			
			//"override level" properties
			System.setProperty "some.random.ta.random.bob", "Paige"
			
		and :
			def tester = new PropertiesBasedCreatorHelper();
			tester.setKeys(["ta.random.albert", "ta.random.bob", "ta.random.charlie", "ta.random.darry"] as String[])
					
		when :
			def ppts = tester.getEffectiveProperties(file)
		
		then :
			ppts.getProperty("ta.random.albert") == "Camus" 	//from config : above the default, and not overriden
			ppts.getProperty("ta.random.bob") == "Paige"		//from namespaced system properties : override
			ppts.getProperty("ta.random.charlie") == "Brown"	//from config : that key was defined by no other source
			ppts.getProperty("ta.random.darry") == "Cowl"		//from default: that key was defined by no other source
		
		cleanup :
			System.clearProperty "ta.random.albert"
			System.clearProperty "ta.random.darry"
			System.clearProperty "ta.random.bob"
		
	}
	
	def "should make a Properties with that file"(){
		given :
			def tester = new PropertiesBasedCreatorHelper()
			def url = getClass().getClassLoader().getResource("repositorytest/my.classpath.properties");
			File file = new File(url.toURI())
		when :
			Properties result = tester.toProperties(file)
		then :
			result.containsKey "squashtest.ta.classpath"		
	}
	
	public "should return the shortened name"(){
		expect :
			def tester = new PropertiesBasedCreatorHelper()
			tester.stripExtension(name) == "name.which.suffix.is"
		where :
			name = "name.which.suffix.is.properties"
		
	}
	
	def "should return the name as is"(){
		expect :
			def tester = new PropertiesBasedCreatorHelper()
			tester.stripExtension(name) == "here.properies.is.not.the.suffix"
		where :
			name = "here.properies.is.not.the.suffix"
	}

}
