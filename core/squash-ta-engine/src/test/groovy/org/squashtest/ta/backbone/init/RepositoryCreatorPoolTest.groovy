/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.init;

import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.init.RepositoryCreatorPool;
import org.squashtest.ta.framework.components.RepositoryCreator;
import org.squashtest.ta.framework.components.ResourceRepository;

import spock.lang.Specification;

public class RepositoryCreatorPoolTest extends Specification {
	
	RepositoryCreatorPool pool;
	EngineComponentDefinitionManager manager;
	
	def setup(){
		pool = new RepositoryCreatorPool();
		manager = Mock(EngineComponentDefinitionManager)
		
		pool.definitionManager = manager;
	}
	
	
	//note : we don't bother with the enginecomponentdefinitionmanager here
	def "should wrap a resource repository"(){
		given :
			def repository = Mock(ResourceRepository)
		
		when :
			def result = pool.wrapRepository(repository)
		
		then :
			result instanceof RepositoryWrapper
			result.repository == repository
			result.name == "default"
	}
	
	//note : we don't bother with the enginecomponentdefinitionmanager here
	def "should invoke the second creator"(){
		
		given :
			def creator1 = Mock(RepositoryCreator)
			def creator2 = Mock(RepositoryCreator)
			def creator3 = Mock(RepositoryCreator)
		
		and :
			def repository = Mock(ResourceRepository)
			
		and :
			URL file = new URL("file:bob")
		
		and :
			creator1.canInstantiate(file) >> false
			creator2.canInstantiate(file) >> true
			creator2.createRepository(file) >> repository
			
		and :
			pool.creators = [creator1, creator2, creator3]
			
		
		when :
			def result = pool.createRepository(file)
		
		then :
			result.repository == repository
			result.name=="bob"
		
	}
	
	
}
