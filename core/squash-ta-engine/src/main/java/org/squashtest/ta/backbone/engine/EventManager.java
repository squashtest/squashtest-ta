/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine;

import org.squashtest.ta.framework.test.event.StatusUpdateEvent;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;

/**
 * Interface for the event managing component.
 * 
 * @author edegenetais
 * 
 */
public interface EventManager {
	/**
	 * Event producers call this method to post an event.
	 * @param event
	 */
	void postEvent(StatusUpdateEvent<?> event);

	/**
	 * Add a listener for the event. Please note: listeners don't consume
	 * events, so that all listeners will receive all events (and at their
	 * convenience do something about it - or not)
	 * 
	 * @param listener
	 */
	void addEventListener(StatusUpdateListener listener);

	/**
	 * Remove an event listener.
	 * @param listener the listener to remove.
	 * @return <code>true</code> if the listener was registered, <code>false</code> if it was not.
	 */
	boolean removeEventListener(StatusUpdateListener listener);
	
	/**
	 * Method to start event handling.
	 */
	void start();
	
	/**
	 * Method to know if event handling is active. Allows to know if the manager is already active, in case you want to reset it (by calling shutdown(), then start()).
	 * @return <code>true</code> if active.
	 */
	boolean active();
	
	/**
	 * Method to shutdown the event handling thread. This attempts a <strong>clean</strong> shutdown: pending events will be treated.
	 */
	void shutdown();
}
