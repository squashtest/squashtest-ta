/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.escaper

import org.squashtest.installer.escaper.Escaper;

import spock.lang.Specification;

class EscaperTest extends Specification {
	
	def "expect standard characters unmodified"(){

		given :
			def standardString = "that string shall not be modified"
				
		expect :
			Escaper.escapeUnicode(standardString) == standardString
	}
	
	def "expect special characters to be escaped"(){
		given :
			def weirdString = "ça va tomber à côté"
			def escapedString = "\\u00e7a va tomber \\u00e0 c\\u00f4t\\u00e9"
			
		expect :	
			Escaper.escapeUnicode(weirdString) == escapedString
	}
	
	
	def "should double backslashes that are not refering to escaped unicode characters "(){
		
		given :
			def path = "C:\\test\\with\\accents like \\u00E9"
			def expectedResult = "C:\\\\test\\\\with\\\\accents like \\u00E9"
			
		expect : 
			Escaper.doubleBackslashes(path) == expectedResult			
		
	}
	
}
