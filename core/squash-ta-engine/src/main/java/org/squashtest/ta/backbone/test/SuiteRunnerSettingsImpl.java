/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.test;

import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.SuiteRunnerSettings;

public class SuiteRunnerSettingsImpl implements SuiteRunnerSettings {

	private boolean isAutoCache=false;
	private boolean isAutoConvert=false;
	private ContextManager contextManager;
	
	public SuiteRunnerSettingsImpl(){
		
	}
	
	public SuiteRunnerSettingsImpl(boolean isAutoCache, boolean isAutoConvert,
			ContextManager contextManager) {
		super();
		this.isAutoCache = isAutoCache;
		this.isAutoConvert = isAutoConvert;
		this.contextManager = contextManager;
	}
	
	public void setAutoCache(boolean autoCache){
		this.isAutoCache=autoCache;
	}
	
	public void setAutoConvert(boolean autoConvert){
		this.isAutoConvert=autoConvert;
	}
	
	public void setContextManager(ContextManager manager){
		this.contextManager=manager;
	}

	public boolean isAutoCache() {
		return isAutoCache;
	}

	public boolean isAutoConvert() {
		return isAutoConvert;
	}
	
	public ContextManager getContextManager(){
		return contextManager;
	}

}
