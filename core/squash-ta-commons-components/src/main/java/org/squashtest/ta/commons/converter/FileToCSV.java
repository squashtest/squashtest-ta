/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;

import org.squashtest.ta.commons.library.csv.CSVConfiguration;
import org.squashtest.ta.commons.library.csv.CSVSeparator;
import org.squashtest.ta.commons.resources.CSVResource;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;

import au.com.bytecode.opencsv.CSVReader;

/**
 * File To XML Converter
 * Converts a File entry (XML File obviously) into an XMLResource Type 
 * (encapsulated XML File which proves that the XML is "well formed")
 * <p> The file must a correct XML implementation<p>
 * 
 * @author fgaillard
 *
 */
@EngineComponent("structured")
public class FileToCSV implements ResourceConverter<FileResource, CSVResource> {

	private char separationChar = ',';
	private char quoteChar = '\"'; 
	private char escapeChar = '\\'; 
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public FileToCSV(){}
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}
	
	/**
	 * You can pass a configuration along to change the separation character 
	 */
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		for (Object object : configuration) {
			if (object instanceof CSVConfiguration) {
				CSVConfiguration character = (CSVConfiguration) object;
				if (character.getSeparationCharacter() == CSVSeparator.PIPE){
					//we keep only the pipe "|" as a separator instead of the whole "\t|\t" and not the tabs enclosing it 
					separationChar = character.getSeparationCharacter().getCharacter().toCharArray()[1];
				} else {
					separationChar = character.getSeparationCharacter().getCharacter().toCharArray()[0];
				}
				quoteChar = character.getQuoteCharacter().getCharacter().toCharArray()[0];
				escapeChar = character.getEscapeCharacter().getCharacter().toCharArray()[0];
			}
		}
	}

	@Override
	public CSVResource convert(FileResource resource) {
		CSVResource resultResource = null;
		try
		{
			CSVReader reader = new CSVReader(new FileReader(resource.getFile()), separationChar, quoteChar, escapeChar);
			int nbColumns = -1;
			String [] nextLine;
		    while ((nextLine = reader.readNext()) != null) {
		        if (nextLine.length > 1){
		        	if (nbColumns == -1){
						//The first line defines the number of columns in the table
						nbColumns = nextLine.length;
					} else {
						if (nextLine.length != nbColumns){
							throw new BadDataException("Wrongly configured CSV File\nThe column number is variable");
						}
					}
		        } else {
					//If not we throw an exception
					throw new BadDataException("Wrongly configured CSV File\nThe separation character \'" + separationChar + "\' is not present");
				}
		    }
		} catch (FileNotFoundException fnfe) {
			throw new BadDataException("Wrongly configured CSV File\n",fnfe);
		} catch (IOException ioe) {
			throw new BadDataException("Wrongly configured CSV File\n",ioe);
		}
		resultResource = new CSVResource(resource.getFile());
		return resultResource;
	}

	@Override
	public void cleanUp() {
		
	}
}
