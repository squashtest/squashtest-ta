@echo off

:: Positionnement des variables d'environnement et du path
call env.cmd

::Demarrage du serveur selenium
%SQUASH_TA_DRIVE%
cd %SELENIUM_HOME%
java -jar selenium-server-standalone-2.24.1.jar