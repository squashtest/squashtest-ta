/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.init

import org.squashtest.ta.legacy.test.catsclasses.LegacyTestBatchTestCase
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestCubictestTestCase
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestSahiTestCase
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestSoapuiTestCase

import spock.lang.Specification

class LegacyTestClassesLoaderTest extends Specification {
	def testee
	def setup(){
		testee=new LegacyTestClassesLoader()
	}
	def "should find test classes"(){
		given:
			Set<Class<?>> expectedClassList=[LegacyTestBatchTestCase.class,LegacyTestCubictestTestCase.class,LegacyTestSahiTestCase.class,LegacyTestSoapuiTestCase.class]
		and:
			Set<Class<?>> packageSet=["org.squashtest.ta.legacy.test.catsclasses"]
		when:
			def resultat=testee.enumerateTestClasses(packageSet)
		then:
			def resultSet=new HashSet<Class<?>>(resultat)
			expectedClassList.equals(resultSet)
	}
}
