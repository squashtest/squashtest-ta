/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.wizards;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * This class implements the web target - specific wizard page to set up a web target definition.
 * @author edegenetais
 *
 */
public class WebTargetPropertiesWizardPage extends Page {
	private Text sutUrl;
	public WebTargetPropertiesWizardPage() {
		super(WebTargetPropertiesWizardPage.class.getSimpleName());
		setTitle("Enter web SUT properties");
		setDescription("This form lets you define the web SUT properties.");
	}
	
	@Override
	public void createControl(Composite parent) {
		Composite uiContainer=createPageUIContainer(parent, 2);
		addLabel(uiContainer, "SUT Url");
		
		sutUrl=createInput(uiContainer);
		sutUrl.addModifyListener(new ValidUrlRule(this, sutUrl, "http","https"));
		
		addPageRule(new MandatoryRule(sutUrl, "SUT Url", this));
		
		setControl(uiContainer);
	}
	
	public String getSutUrl(){
		return sutUrl.getText();
	}
}
