/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Collection;
import java.util.Map;

import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.AssertionFindSettings;
import org.squashtest.ta.backbone.engine.CommandFindSettings;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.ResourceConversionSettings;
import org.squashtest.ta.backbone.engine.ResourceLoadingSettings;
import org.squashtest.ta.backbone.engine.event.ContextualStatusUpdateEvent;
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler;
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler;
import org.squashtest.ta.framework.test.instructions.ResourceName;

/**
 * This class allows creating delegate-based context managers with just a few
 * overriden methods to add the required behavior tweaks.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractContextManagerDecorator implements ContextManager{
	
	private ContextManager delegate;
	
	public AbstractContextManagerDecorator(ContextManager provider) {
		delegate=provider;
	}
	
	public Map<ResourceName, ResourceWrapper> getEcosystemResources() {
		return delegate.getEcosystemResources();
	}
	public void initAll() {
		delegate.initAll();
	}
	public void resetAll() {
		delegate.resetAll();
	}
	public void cleanupAll() {
		delegate.cleanupAll();
	}
	public void registerRepository(RepositoryWrapper repository) {
		delegate.registerRepository(repository);
	}
	public void registerTarget(TargetWrapper target) {
		delegate.registerTarget(target);
	}
	public void registerConverterDefinition(ConverterDefinition definition) {
		delegate.registerConverterDefinition(definition);
	}
	public void registerCommandDefinition(CommandDefinition definition) {
		delegate.registerCommandDefinition(definition);
	}
	public void registerAssertionDefinition(BinaryAssertionDefinition definition) {
		delegate.registerAssertionDefinition(definition);
	}
	public void registerAssertionDefinition(UnaryAssertionDefinition definition) {
		delegate.registerAssertionDefinition(definition);
	}
	public void unregisterRepository(RepositoryWrapper repository) {
		delegate.unregisterRepository(repository);
	}
	public void unregisterRepository(String repositoryName) {
		delegate.unregisterRepository(repositoryName);
	}
	public void unregisterTarget(TargetWrapper target) {
		delegate.unregisterTarget(target);
	}
	public void unregisterTarget(String targetName) {
		delegate.unregisterTarget(targetName);
	}
	public void unregisterConverterDefinition(ConverterDefinition definition) {
		delegate.unregisterConverterDefinition(definition);
	}
	public void unregisterConverterDefinition(Nature input, Nature output,
			Nature category) {
		delegate.unregisterConverterDefinition(input, output, category);
	}
	public void unregisterCommandDefinition(CommandDefinition definition) {
		delegate.unregisterCommandDefinition(definition);
	}
	public void unregisterCommandDefinition(Nature resourceNature,
			Nature targetNature, Nature category) {
		delegate.unregisterCommandDefinition(resourceNature, targetNature,
				category);
	}
	public void unregisterAssertionDefinition(
			BinaryAssertionDefinition definition) {
		delegate.unregisterAssertionDefinition(definition);
	}
	public void unregisterAssertionDefinition(Nature actualNature,
			Nature expectedNature, Nature category) {
		delegate.unregisterAssertionDefinition(actualNature, expectedNature,
				category);
	}
	public void unregisterAssertionDefinition(
			UnaryAssertionDefinition definition) {
		delegate.unregisterAssertionDefinition(definition);
	}
	public void unregisterAssertionDefinition(Nature actualNature,
			Nature category) {
		delegate.unregisterAssertionDefinition(actualNature, category);
	}
	public TargetWrapper getTarget(String targetName) {
		return delegate.getTarget(targetName);
	}
	public Collection<ResourceConverterHandler> getConverters(
			ResourceConversionSettings settings) {
		return delegate.getConverters(settings);
	}
	public ResourceWrapper convertResource(ResourceWrapper resource,
			ResourceConversionSettings settings) {
		return delegate.convertResource(resource, settings);
	}
	public Collection<CommandHandler> getCommand(CommandFindSettings settings) {
		return delegate.getCommand(settings);
	}
	public Collection<BinaryAssertionHandler> getBinaryAssertion(
			AssertionFindSettings settings) {
		return delegate.getBinaryAssertion(settings);
	}
	public Collection<UnaryAssertionHandler> getUnaryAssertion(
			AssertionFindSettings settings) {
		return delegate.getUnaryAssertion(settings);
	}
	public ResourceWrapper getResource(ResourceName resourceName) {
		return delegate.getResource(resourceName);
	}
	public ResourceWrapper getResource(ResourceName resourceName,
			String repositoryName) {
		return delegate.getResource(resourceName, repositoryName);
	}
	public ResourceWrapper getResource(ResourceLoadingSettings settings) {
		return delegate.getResource(settings);
	}
	public void postEvent(ContextualStatusUpdateEvent<?> event) {
		delegate.postEvent(event);
	}
}
