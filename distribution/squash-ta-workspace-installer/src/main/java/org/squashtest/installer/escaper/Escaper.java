/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.installer.escaper;


/**
 * 
 * That class has one unique purpose : take a String and escape all non-ansi character to their literal unicode escape sequence.
 * That is, "à"="\u00E0", "é"="\u00E9" etc.
 * 
 * @author bsiri
 *
 */
public class Escaper {

	public static String escapeUnicode(String escapeThis){
		
		StringBuffer buffer = new StringBuffer();
		
		char[] chars = new char[escapeThis.length()];
		escapeThis.getChars(0, escapeThis.length(), chars, 0);
		
		for (char a : chars){
			buffer.append(UnicodeFormatter.escape(a));
		}
		
		return buffer.toString();
	}
	
	
	/**
	 * That method will double backslashes ('\'), with the exception of those refering to explicit unicode-escaped characters (the '\' in '\u1234' for instance). 
	 * Why would we do that ? Because sometimes you need to put a windows path in a quote-enclosed output like "path=\"method-output\"". In short, a string that represents a string.
	 *  
	 * @param escapeMe the String that need escaping.
	 * 
	 */
	static public String doubleBackslashes(String escapeMe){	
		/* well due to java String specs, backslashes must themselves be escaped.
		 * due to java Pattern specs, both the backslash and the one escaping that backslash must be escaped
		 * that's why we end with 4 \ to match a single one.
		 */
		return escapeMe.replaceAll("\\\\(?!u[0-9a-fA-F]{4})", "\\\\\\\\");
	}	
	
}
