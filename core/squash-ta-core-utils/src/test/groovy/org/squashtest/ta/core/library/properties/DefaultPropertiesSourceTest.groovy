/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.library.properties

import spock.lang.Specification

class DefaultPropertiesSourceTest extends Specification {

	def "should find some keys matching a pattern"(){
		
		given :
			def ppts = initProperties()
			def source = new DefaultPropertiesSource(ppts);
		
		when :
			def res = source.findKeysMatchingPattern("ta.key[13]") as Set
		
		then :
			res == ["ta.key1", "ta.key3"] as Set
		
	}
	
	
	def initProperties={
		
		def ppts = new Properties();
		
		ppts.setProperty("ta.key1", "value1")
		ppts.setProperty("ta.key2", "value2")
		ppts.setProperty("ta.key3", "value3")
		
		return ppts	
		
	}
}
