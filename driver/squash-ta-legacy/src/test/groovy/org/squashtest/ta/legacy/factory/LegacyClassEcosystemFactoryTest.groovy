/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.factory

import org.squashtest.cats.runner.test.AbstractCatsTestCase;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.legacy.test.catsclasses.LegacyTestBatchTestCase;
import org.squashtest.ta.legacy.test.factorydummies.EnvironmentClassTestCase;
import org.squashtest.ta.legacy.test.factorydummies.NoSetupTeardownTestCase;

import spock.lang.Specification;

class LegacyClassEcosystemFactoryTest extends Specification{
	def testee
	def mockTestFactory
	def noparm
	
	def setup(){
		testee=new LegacyClassEcosystemFactory()
		mockTestFactory=Mock(LegacyClassTestFactory)
		testee.factory=mockTestFactory
		noparm=new Class<?>[0]
	}
	
	def "ecosystem name is class name"(){
		given:
			def testClass=LegacyTestBatchTestCase.class
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			testClass.getName().equals(result.getName())
	}
	
	def "ecosystem tests are class @Test methods"(){
		given:
			def testClass=LegacyTestBatchTestCase.class
		and:
			def method=testClass.getMethod("thatIsATestCase", noparm)
		and:
			def snare=testClass.getMethod("howeverThatIsNot", noparm)
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			1 * mockTestFactory.buildTest(method,_) >> new Test()
			0 * mockTestFactory.buildTest(snare,_) >> new Test()
	}
	
	def "ecosystem tests are NOT class @BeforeClass methods"(){
		given:
			def testClass=EnvironmentClassTestCase.class
		and:
			def setupMethod1=testClass.getMethod("setup1", noparm)
			def setupMethod2=testClass.getMethod("setup2", noparm)
			def suT=Mock(Test)
			suT.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(setupMethod1,_) >> suT
			mockTestFactory.buildTest(setupMethod2,_) >> suT
		and:
			def anyTest=Mock(Test)
			anyTest.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(_,_) >> anyTest
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			def beforeClassIncludedInTest=false
			def testList=result.getTestPopulation()
			for(Test currTest:testList){
				if(currTest==suT){
					beforeClassIncludedInTest=true
				}
			}
			beforeClassIncludedInTest==false
	}
	
	def "ecosystem tests are NOT class @AfterClass methods"(){
		given:
			def testClass=EnvironmentClassTestCase.class
		and:
			def teardownMethod1=testClass.getMethod("teardown1", noparm)
			def teardownMethod2=testClass.getMethod("teardown2", noparm)
			def suT=Mock(Test)
			suT.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(teardownMethod1,_) >> suT
			mockTestFactory.buildTest(teardownMethod2,_) >> suT
		and:
			def anyTest=Mock(Test)
			anyTest.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(_,_) >> anyTest
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			def afterClassIncludedInTest=false
			def testList=result.getTestPopulation()
			for(Test currTest:testList){
				if(currTest==suT){
					afterClassIncludedInTest=true
				}
			}
			afterClassIncludedInTest==false
	}
	
	def "ecosystem tests are NOT class @Before methods"(){
		given:
			def testClass=EnvironmentClassTestCase.class
		and:
			def testSuMethod=testClass.getMethod("setUpSnare", noparm)
			def suT=Mock(Test)
			suT.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(testSuMethod,_) >> suT
		and:
			def anyTest=Mock(Test)
			anyTest.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(_,_) >> anyTest
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			def beforeClassIncludedInTest=false
			def testList=result.getTestPopulation()
			for(Test currTest:testList){
				if(currTest==suT){
					beforeClassIncludedInTest=true
				}
			}
			beforeClassIncludedInTest==false
	}
	
	def "ecosystem tests are NOT class @After methods"(){
		given:
			def testClass=EnvironmentClassTestCase.class
		and:
			def testTdMethod=testClass.getMethod("tearDownSnare", noparm)
			def tdT=Mock(Test)
			tdT.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(testTdMethod,_) >> tdT
		and:
			def anyTest=Mock(Test)
			anyTest.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(_,_) >> anyTest
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			def afterClassIncludedInTest=false
			def testList=result.getTestPopulation()
			for(Test currTest:testList){
				if(currTest==tdT){
					afterClassIncludedInTest=true
				}
			}
			afterClassIncludedInTest==false
	}
	
	def "ecosystem must have an environment"(){
		given:
			def testClass=LegacyTestBatchTestCase.class
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			result.getEnvironment()!=null
	}
	
	def "environment must have default setup/teardown if no setup or teardown in class"(){
		given:
			def testClass=NoSetupTeardownTestCase.class
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			result.getEnvironment()!=null
			result.getEnvironment().getSetUp()!=null
			result.getEnvironment().getTearDown()!=null
	}
	
	def "all beforeClass methods and only'em in setup"(){
		given:
			def testClass=EnvironmentClassTestCase.class
		and:
			def setup1=testClass.getMethod("setup1",noparm)
			def setup2=testClass.getMethod("setup2",noparm)
		and:
			def test1=new Test()
			def ins1=Mock(TestInstruction)
			test1.addToTests(ins1)
			def test2=new Test()
			def ins2=Mock(TestInstruction)
			test2.addToTests(ins2)
		and:
			def mockTest = Mock(Test)
			mockTest.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(_,_) >> mockTest
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			1 * mockTestFactory.buildTest(setup1,_) >> test1
			1 * mockTestFactory.buildTest(setup2,_) >> test2
			result.getEnvironment()!=null
			result.getEnvironment().getSetUp()!=null
			def setupIterator=result.getEnvironment().getSetUp().getTests()
			setupIterator.hasNext()
			setupIterator.next()==ins1
			setupIterator.hasNext()
			setupIterator.next()==ins2
			setupIterator.hasNext()==false
	}
	
	def "all afterClass methods and only'em in teardown"(){
		given:
			def testClass=EnvironmentClassTestCase.class
		and:
			def teardown1=testClass.getMethod("teardown1",noparm)
			def teardown2=testClass.getMethod("teardown2",noparm)
		and:
			def mockTest = Mock(Test)
			mockTest.getTests() >> Collections.emptyList().iterator()
			mockTestFactory.buildTest(_,_) >> mockTest
		and:
			def test1=new Test()
			def ins1=Mock(TestInstruction)
			test1.addToTests(ins1)
			def test2=new Test()
			def ins2=Mock(TestInstruction)
			test2.addToTests(ins2)
		when:
			def result=testee.buildEcosystem(testClass)
		then:
			result!=null
			1 * mockTestFactory.buildTest(teardown1,_) >> test1
			1 * mockTestFactory.buildTest(teardown2,_) >> test2
			result.getEnvironment()!=null
			result.getEnvironment().getSetUp()!=null
			def setupIterator=result.getEnvironment().getTearDown().getTests()
			setupIterator.hasNext()
			setupIterator.next()==ins1
			setupIterator.hasNext()
			setupIterator.next()==ins2
			setupIterator.hasNext()==false
	}
}
