#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2012 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses/>.
#

OLDCD=`pwd`
cd "`dirname "$0"`/.."
export SQUASH_TA_HOME=`pwd`
cd "$OLDCD"

export JAVA_HOME=$SQUASH_TA_HOME/openjdk1.6.0_25-unix-i586
export MAVEN_HOME=$SQUASH_TA_HOME/apache-maven-3.0.4
export MVN_HOME=$MAVEN_HOME
export MAVEN_OPTS="-Xms512m -Xmx512m -XX:PermSize=256m -XX:MaxPermSize=512m"
export SAHI_HOME=$SQUASH_TA_HOME/sahi_v35_20110719
export PATH=$SQUASH_TA_HOME/openjdk1.6.0_25-unix-i586/bin:$MVN_HOME/bin:$PATH

# protect the execution server tomcat environment against system-wide variable settings
export CATALINA_HOME=
export CATALINA_OPTS=

#switch to yes to start the Sahi proxy
export START_SAHI_PROXY=%SAHI_SELECTED

export JENKINS_HOME=$SQUASH_TA_HOME/execution_home
export HOSTNAME=`hostname`

echo Squash TA Server environment $SQUASH_TA_HOME@$HOSTNAME
