/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.converter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collection;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlProducer;
import org.squashtest.ta.commons.resources.DbUnitDatasetResource;
import org.squashtest.ta.commons.resources.XMLResource;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.xml.sax.InputSource;

/**
 * XML To Dataset Converter
 * Converts a XMLFile entry into a DBUnit Dataset 
 * The conversion is mainly done by DBunit parsers and converters
 * 
 * @author fgaillard
 *
 */
@EngineComponent("dataset")
public class XmlToDataset implements ResourceConverter<XMLResource, DbUnitDatasetResource> {

	/**
	 * Default constructor for Spring enumeration only.
	 */
	public XmlToDataset(){}
	
	@Override
	public float rateRelevance(XMLResource input) {
		return 0.5f;
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		//noop
	}

	@Override
	public DbUnitDatasetResource convert(XMLResource resource) {
		DbUnitDatasetResource dbUnitDataResource = null;
		try
		{
			InputSource iSource = new InputSource(new FileReader(resource.getXMLFile()));
			FlatXmlProducer producer = new FlatXmlProducer(iSource);
		    //IDataSet dataSet = new StreamingDataSet(producer);
			IDataSet dataSet = new FlatXmlDataSet(producer);
			
			ReplacementDataSet rDataSet;
			rDataSet = new ReplacementDataSet(dataSet);
			rDataSet.setSubstringDelimiters("${", "}");
			rDataSet.setStrictReplacement(false);
			rDataSet.addReplacementObject(new String("[NULL]"), null);
			rDataSet.addReplacementObject(new String("[null]"), null);
			
			//we set hasMetadata to false here because the xml dataset includes no metadata
		    dbUnitDataResource = new DbUnitDatasetResource(rDataSet,false);
		} catch (FileNotFoundException fnfe) {
			throw new BadDataException("file not found!!!!!\n", fnfe);
		} catch (DataSetException e) {
			throw new BadDataException("Cannot create dataset, something must be wrong with the underlying xml\n", e);
		}
		return dbUnitDataResource;
	}

	@Override
	public void cleanUp() {
		
	}
}
