/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter.wildcard;

import java.util.List;

/**
 * Implementation of the ** wildcard element.
 * @author edegenetais
 *
 */
class MultiDirectoryMatcher extends AbstractMatcherBase {

	/**
	 * Next matcher in the chain: the MultiDirectoryMatcher is meaningless if
	 * not followed by a file or directory name matcher.
	 */
	private PathPartMatcher nextMatcher;

	/**
	 * Full initialization constructor.
	 * @param nextMatcher
	 */
	public MultiDirectoryMatcher(PathPartMatcher nextMatcher) {
		if(nextMatcher==null){
			throw new IllegalArgumentException("Next matcher cannot be null.");
		}
		this.nextMatcher=nextMatcher;
	}

	@Override
	public MatchState matches(List<String> path) {
		MatchState state=nextMatcher.matches(path);
		List<String>leftOver=path;
		while(Boolean.FALSE==state.getMatches() && state.getLeftOver().size()>0){
			leftOver=state.getLeftOver();
			state=nextMatcher.matches(leftOver);
		}
		return new MatchState(state.getMatches()==Boolean.FALSE?false:null, leftOver);
	}

	@Override
	public String toPattern() {
		return "**";
	}

}
