/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.assertions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.DatabaseUnitException;
import org.dbunit.assertion.DbComparisonFailure;
import org.dbunit.assertion.FailureHandler;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.SortedTable;
import org.dbunit.dataset.filter.DefaultTableFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.helpers.DiffReportBuilder;
import org.squashtest.ta.commons.library.dbunit.ByTableIncludeExcludeColumnFilter;
import org.squashtest.ta.commons.library.dbunit.FilteredStructureDataSet;
import org.squashtest.ta.commons.library.dbunit.assertion.DbUnitAssertExtension;
import org.squashtest.ta.commons.resources.DbUnitDatasetResource;
import org.squashtest.ta.commons.resources.DbUnitFilterResource;
import org.squashtest.ta.commons.resources.DbUnitPPKFilter;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.TestAssertionFailure;

public abstract class AbstractDbUnitDatasetCompare {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDbUnitDatasetCompare.class);
	protected DbUnitDatasetResource expected;
	protected DbUnitDatasetResource actual;
	protected DbUnitPPKFilter pseudoPrimaryKeys;
	
	/** Filters to apply before comparison. */
	private List<DbUnitFilterResource> filters = new ArrayList<DbUnitFilterResource>();

	protected AssertConnector assertConnector = new AssertConnector();
	
	protected abstract void compare(IDataSet pExpected, IDataSet pActual);
	
	protected DiffReportBuilderFactory diffReportBuilderFactory = new DiffReportBuilderFactory();
	
	public AbstractDbUnitDatasetCompare() {
		super();
	}

	public DiffReportBuilderFactory getDiffReportBuilderFactory() {
		return diffReportBuilderFactory;
	}

	public void setDiffReportBuilderFactory(DiffReportBuilderFactory diffReportBuilderFactory) {
		this.diffReportBuilderFactory = diffReportBuilderFactory;
	}

	public void setActualResult(DbUnitDatasetResource actual) {
		this.actual = actual;
	}

	public void setExpectedResult(DbUnitDatasetResource expected) {
		this.expected = expected;
	}

	/**
	 * This assertion needs no configuration, but will apply any injected
	 * {@link DbUnitFilterResource} to both the expected and actual dataset
	 * before comparing them.
	 */
	public void addConfiguration(Collection<Resource<?>> configuration) {
		for (Resource<?> confElement : configuration) {
			if (confElement instanceof DbUnitFilterResource) {
				filters.add((DbUnitFilterResource) confElement);
			} else if (confElement instanceof DbUnitPPKFilter) {
				if (pseudoPrimaryKeys == null) {
					pseudoPrimaryKeys = (DbUnitPPKFilter) confElement;
				} else {
					LOGGER.warn("Redundant DbUnitPPKFilter configuration will be ignored.");
				}
			} else {
				LOGGER.warn("Unrecognized configuration resource will be ignored (type: "
						+ confElement.getClass().getName() + ")");
			}
		}
	}

	/**
	 * @see BinaryAssertion#test()
	 * @throws BinaryAssertionFailedException if the asserted condition is false.
	 */
	public void test() throws BinaryAssertionFailedException {
		try {
			// extract datasets
			IDataSet expectedDataset;
			IDataSet actualDataset;
			expectedDataset = expected.getDataset();
			actualDataset = actual.getDataset();
	
			// apply filters, if any
			if (filters.size() > 0) {
				for (DbUnitFilterResource filter : filters) {
					expectedDataset = filter.apply(expectedDataset);
					actualDataset = filter.apply(actualDataset);
				}
			}
	
			DefaultTableFilter containsTableFilter = new DefaultTableFilter();
			ByTableIncludeExcludeColumnFilter containsColumnFilter = new ByTableIncludeExcludeColumnFilter();
			String[] tableNames = expectedDataset.getTableNames();
			for (String name : tableNames) {
				containsTableFilter.includeTable(name);
				ITableMetaData metaData = expectedDataset.getTableMetaData(name);
				for (Column column : metaData.getColumns()) {
					containsColumnFilter.addColumnIncludeFilter(name,
							column.getColumnName());
				}
			}
			IDataSet actualViewInExpectedStructure = new FilteredStructureDataSet(
					actualDataset, containsTableFilter, containsColumnFilter);
	
			compare(expectedDataset, actualViewInExpectedStructure);
	
		} catch (DataSetException dse) {
			throw new BadDataException("Dataset assert error.", dse);
		}
	}

	protected void logFailureReportingError(Exception e) {
		LOGGER.error(
				"Error while reporting assertion failure. Failure details won't be available.",
				e);
	}
	
	/** package accessible for testability */
	class AssertConnector {
		public void assertEquals(SortedTable expected, SortedTable actual,
				FailureHandler fh) throws DatabaseUnitException {
			try{
				Assertion.assertEquals(expected, actual, fh);
			}catch(AssertionError err){
				throw new TestAssertionFailure(err.getMessage(), err);
			}
		}
		
		public void assertContains(ITable expected, ITable actual, FailureHandler fh, List<String> primaryKeysName )throws DatabaseUnitException
		{
			try{
				DbUnitAssertExtension assertion = new DbUnitAssertExtension();
				assertion.assertContains(expected, actual, fh, primaryKeysName);
			}catch (DbComparisonFailure dbcf) {
				throw new BinaryAssertionFailedException("Dataset comparison threw dbunit error : "+dbcf.getMessage(),AbstractDbUnitDatasetCompare.this.expected,AbstractDbUnitDatasetCompare.this.actual,null);
			}catch(AssertionError err){
				throw new TestAssertionFailure(err.getMessage(), err);
			}
		}
		
	}
	
	/**
	 * Accessible mainly for testability purpose. May be used to change
	 * reporting behavior.
	 */
	public class DiffReportBuilderFactory {
		public DiffReportBuilder newInstance() {
			return new DiffReportBuilder();
		}
	}

}