/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.editors.completion;

import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;

/**
 * Minimal {@link IContextInformation} implementation for the Squash TA plugin.
 * @author edegenetais
 *
 */
class ContextInformation implements IContextInformation {
	private String content;

	/**
	 * Simple context information implementation with String content.
	 * @param content
	 */
	public ContextInformation(String content) {
		this.content = content;
	}

	@Override
	public String getInformationDisplayString() {
		return content;
	}

	@Override
	public Image getImage() {
		return null;
	}

	@Override
	public String getContextDisplayString() {
		return content;
	}
}