/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.editors.completion;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension5;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

/**
 * Implementation of the {@link ICompletionProposal} eclipse API interface for the Squash TA plugin.
 * @author edegenetais
 *
 */
class CompletionProposal implements ICompletionProposal,ICompletionProposalExtension5 {
	private final int replacementBegin;
	private final int toReplaceLength;
	private String newContent;
	private String additionalInfo;
	private String shortDescription;

	/**
	 * Full initialization constructor.
	 * 
	 * @param beginReplaceOffset
	 *            begin index of the replaced string.
	 * @param toReplaceLength
	 *            en index of the replaced string. If
	 *            beginReplaceOffset=endReplaceOffset, the completion is purely
	 *            inserted, otherwise it will replace the part of the content
	 *            between beginReplaceIndex and endReplaceIndex (ex: replacing the beginning of a keyword by the whole keyword).
	 * @param insertedContent the content to insert.
	 * @param additionalInformation additional information to display beside the completion proposal to help the completion choice.
	 */
	CompletionProposal(int beginReplaceOffset,int toReplaceLength, String insertedContent, String shortDescription, String additionalInformation) {
		this.newContent = insertedContent;
		this.replacementBegin = beginReplaceOffset;
		this.toReplaceLength = toReplaceLength;
		this.additionalInfo=additionalInformation;
		this.shortDescription = shortDescription;
	}

	@Override
	public Point getSelection(IDocument arg0) {
		return new Point(replacementBegin+newContent.length(), 0);
	}

	@Override
	public Image getImage() {
		return null;
	}

	@Override
	public String getDisplayString() {
		return shortDescription;
	}

	@Override
	public IContextInformation getContextInformation() {
		return null;//return new ContextInformation(newContent);
	}

	@Override
	public String getAdditionalProposalInfo() {
		return additionalInfo;
	}

	@Override
	public void apply(IDocument editedDocument) {
		try {
			editedDocument.replace(replacementBegin, toReplaceLength, newContent);
		} catch (BadLocationException e) {
			// nothing happen
		}
		
	}

	@Override
	public Object getAdditionalProposalInfo(IProgressMonitor arg0) {
		arg0.beginTask("Returning precomputed additional info", 0);
		arg0.done();
		return additionalInfo;
	}
}