/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.test;

import java.io.IOException;

import org.apache.xmlbeans.XmlException;

import com.eviware.soapui.support.SoapUIException;

/**
 * This class is a facade to integrate legacy CATS SoapUi test classes into the
 * new squashTA execution framework.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractSoapUiTestCase extends AbstractCatsTestCase{

	protected void runAndCheckSoapUiTestCase(String projectName, String testSuiteName, String testCaseName)
			throws XmlException, IOException, SoapUIException {
		throw new UnsupportedOperationException("Legacy test suite factory implementation in progress!");
	}
}
