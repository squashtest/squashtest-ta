/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.eclipse.plugin.model;

import java.util.ArrayList;
import java.util.List;

public class DSLInstruction {

	private DSLHeadClause headerClause;
	
	private List<DSLClause> possibleClause = new ArrayList<DSLClause>();

	protected DSLInstruction(DSLHeadClause headerClause,
			List<DSLClause> possibleClause) {
		super();
		this.headerClause = headerClause;
		if(possibleClause != null){
			this.possibleClause = possibleClause;
		}
	}

	public DSLHeadClause getHeaderClause() {
		return headerClause;
	}

	public boolean isPossibleClause(DSLClause clause){
		return possibleClause.contains(clause);
	}
	
	public List<DSLClause> getPossibleClause() {
		return possibleClause;
	}
	
}
