/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.RepositoryCreator;
import org.squashtest.ta.framework.components.ResourceRepository;


/**
 * <p>EngineComponent annotates classes that will be managed by the engine (eg Command or BinaryAssertion). Note that Resource or Target use @ResourceType instead.</p>
 * 
 *  <p>Classes that use that annotation will be acknowledged by the engine and new instances created when relevant to do so. The engine will create one 
 *  new instance per use, therefore the implementing object can be stateful.</p>
 *  
 *  <p>There are some major differences between @ResourceType and @EngineComponent.</p>
 *  
 *  <p>First, the difference is semantic :</p>
 *  
 *  <ul>
 *  	<li>ResourceType means that instances will be created by user code explicitly : for instance a {@link ResourceRepository} 
 *  is annotated @ResourceType because it is meant to be created by a {@link RepositoryCreator}. </li>
 *  	<li>EngineComponent means that instances will be created by the engine itself <strong>when it needs it</strong>: for instance a {@link RepositoryCreator} 
 *  is annotated @EngineComponent then the engine will know it must create one when it needs it.</li>  
 *  </ul>
 *  
 *  <p>It ensue that classes @EngineComponent MUST declare a constructor accepting zero parameters or the engine won't know how to 
 *  handle them. @ResourceType classes, however, need not to do so.</p>
 *  
 *  <p>Second, they have different rules for occurrence of a single value : </p> 
 *  
 *  <ul>
 *  	<li>the value of each ResourceType must be unique (in other words each Resource or Target implementation have their own, unique ResourceType)</li>
 *  	<li>the value of EngineComponent may be shared among multiple classes implementing ResourceConverter, Command, BinaryAssertion etc.
 *  </ul>
 *  
 *  <p>Addendum : do not assume that @EngineComponent means that the annotated class will be a singleton. For instance a {@link RepositoryCreator} will actually be 
 *  created once, while a new {@link BinaryAssertion} will be created anytime the engine process an instruction saying it needs one. What classes will be singletons and 
 *  what will not is a matter of which interfaces the said classes implement.</p>
 *                                         
 * 
 * @author bsiri
 *
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface EngineComponent {
	String value();
}
