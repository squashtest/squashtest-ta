/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven;


import java.io.File;
import java.util.Date;
import java.util.Iterator;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.squashtest.ta.backbone.test.DefaultEcosystemResult;
import org.squashtest.ta.backbone.test.DefaultSuiteResult;
import org.squashtest.ta.commons.factories.TestSuiteDescription;
import org.squashtest.ta.commons.factories.dsl.list.DSLTestListFactory;
import org.squashtest.ta.commons.init.FileSystemTestSuiteDefinition;
import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.maven.list.ListTestResult;

/**
 * <p>
 * That mojo creates a surefire-like list of the Squash TA tests in the project.
 * It is used to integrate the engine to client tools and give them a list of
 * available tests for a given project.
 * </p>
 * 
 * @author bsiri
 * 
 * @goal test-list
 * @requiresDependencyResolution test
 * @phase integration-test
 * 
 */
public class SquashTATestListMojo extends AbstractSquashTaMojo {
	
	
	/**
	 * The base directory where the test project is defined.
	 * @parameter expressions="${ta.src}" default-value="src/squashTA"
	 */
	private File mainDirectory;
	
	/**
	 * The base directory where tests are found.
	 * @parameter expressions="${ta.src.tests}"
	 */
	private File testsDirectory;
	
	@Override
	public void execute() throws MojoExecutionException,
			MojoFailureException {
		initLogging();
		
		SuiteResult result=executeImpl();
		
		exportResults(result);
	}
	
	@Override
	protected SuiteResult executeImpl() throws MojoExecutionException,
			MojoFailureException {
		init();
		
		logPhase("listing tests...");
		TestSuiteDescription description = new FileSystemTestSuiteDefinition(testsDirectory);
		TestSuite testSuite = new DSLTestListFactory().buildTestSuite(description);
		
		logPhase("exporting list.");
		SuiteResult result=buildListingResult(testSuite);
		
		return result;
	}

	protected void init() {
		if(testsDirectory==null){
			testsDirectory=new File(mainDirectory,"/tests");
		}
	}

	protected SuiteResult buildListingResult(TestSuite testSuite) {
		
		Date now = new Date();
		DefaultSuiteResult result=new DefaultSuiteResult(testSuite.getName());

		Iterator<Ecosystem> ecosystemIterator = testSuite.iterator();
		while(ecosystemIterator.hasNext()){
			Ecosystem ecosystem=ecosystemIterator.next();
			DefaultEcosystemResult ecosystemResult=new DefaultEcosystemResult();
			ecosystemResult.setName(ecosystem.getName());
			ecosystemResult.setStartTime(now);
			ecosystemResult.setEndTime(now);
			ecosystemResult.setStatus(GeneralStatus.SUCCESS);
			for(Test test:ecosystem.getTestPopulation()){
				TestResult description=new ListTestResult(test); 
				ecosystemResult.addTestResult(description);
			}
			result.addTestEcosystemResult(ecosystemResult);
		}
		return result;
	}

	protected File getTestsDirectory() {
		return testsDirectory;
	}

	protected void setTestsDirectory(File testsDirectory) {
		this.testsDirectory = testsDirectory;
	}

	protected File getMainDirectory() {
		return mainDirectory;
	}

	protected void setMainDirectory(File mainDirectory) {
		this.mainDirectory = mainDirectory;
	}

}
