/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.engine.impl

import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.TestRunner;
import org.squashtest.ta.backbone.engine.impl.EcosystemRunnerImpl.TestRunnerFactory;
import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.Environment;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;

import spock.lang.Specification

class EcosystemRunnerImplTest extends Specification {
	def testee
	
	def testRunnerFactory
	
	def setupRunner
	def setupResult
	ContextManager contextManager
	
	def tearDownRunner
	def tearDownResult
	
	def testRunner1
	def testRunner2
	def testRunner3
	def result1
	def result2
	def result3
	
	def setup(){
		testee=new EcosystemRunnerImpl();
		def ecosystem = Mock(Ecosystem)
		testee.setEcosystem(ecosystem)
		
		contextManager=Mock()
		testee.contextManager = contextManager
		
		def environment = Mock(Environment)
		def setup=Mock(Test)
		def tearDown=Mock(Test)
		environment.getSetUp() >> setup
		environment.getTearDown() >> tearDown
		ecosystem.getEnvironment() >> environment
		
		testRunnerFactory=Mock(TestRunnerFactory)
		testee.runnerFactory=testRunnerFactory
		
		setupRunner = Mock(TestRunner)
		testRunnerFactory.configureTestRunner(setup,_) >> setupRunner
		setupResult = Mock(TestResult)
		
		tearDownRunner = Mock(TestRunner)
		testRunnerFactory.configureTestRunner(tearDown,_) >> tearDownRunner
		tearDownResult = Mock(TestResult)
		
		def test1=Mock(Test)
		def test2=Mock(Test)
		def test3=Mock(Test)
		
		def tests=[test1,test2,test3]
		ecosystem.getTestPopulation() >> tests
		
		testRunner1 = Mock(TestRunner)
		result1 = Mock(TestResult)
		testRunnerFactory.configureTestRunner(test1,_) >> testRunner1
		
		testRunner2 = Mock(TestRunner)
		result2 = Mock(TestResult)
		testRunnerFactory.configureTestRunner(test2,_) >> testRunner2
		
		testRunner3 = Mock(TestRunner)
		result3 = Mock(TestResult)
		testRunnerFactory.configureTestRunner(test3,_) >> testRunner3
		
		contextManager.getEcosystemResources() >> new HashMap()
	}
	
	def "should excute setup, tests and tear down and end with SUCCESS status"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.SUCCESS
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.SUCCESS
			1 * setupRunner.runTest() >> setupResult 
			
			1 * testRunner1.runTest() >> result1
			1 * testRunner2.runTest() >> result2
			1 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult			
	}
	
	def "should fail with NOT_RUN status, without executing test, but trying to teardown (setup ERROR)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.ERROR
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.SUCCESS
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.NOT_RUN
			1 * setupRunner.runTest() >> setupResult
			
			0 * testRunner1.runTest() >> result1
			0 * testRunner2.runTest() >> result2
			0 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should fail with NOT_RUN status, without executing test, but trying to teardown (setup FAIL)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.ERROR
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.SUCCESS
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.NOT_RUN
			1 * setupRunner.runTest() >> setupResult
			
			0 * testRunner1.runTest() >> result1
			0 * testRunner2.runTest() >> result2
			0 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should fail with ERROR status, executing setup, all tests and teardown (test2 ERROR)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.ERROR
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.ERROR
			1 * setupRunner.runTest() >> setupResult
			
			1 * testRunner1.runTest() >> result1
			1 * testRunner2.runTest() >> result2
			1 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should fail with FAIL status, executing setup, all tests and teardown (test2 FAIL)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.FAIL
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.FAIL
			1 * setupRunner.runTest() >> setupResult
			
			1 * testRunner1.runTest() >> result1
			1 * testRunner2.runTest() >> result2
			1 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should fail with ERROR status, executing setup, all tests and teardown (test1 FAIL, 2 ERROR)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.FAIL
			result2.getStatus()>>GeneralStatus.ERROR
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.ERROR
			1 * setupRunner.runTest() >> setupResult
			
			1 * testRunner1.runTest() >> result1
			1 * testRunner2.runTest() >> result2
			1 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should fail with ERROR status, executing setup, all tests and teardown (test1 ERROR, 2 FAIL)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.ERROR
			result2.getStatus()>>GeneralStatus.FAIL
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.ERROR
			1 * setupRunner.runTest() >> setupResult
			
			1 * testRunner1.runTest() >> result1
			1 * testRunner2.runTest() >> result2
			1 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should succeed with WARNING status, executing setup, all tests and teardown (teardown FAIL)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.SUCCESS
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.FAIL
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.WARNING
			1 * setupRunner.runTest() >> setupResult
			
			1 * testRunner1.runTest() >> result1
			1 * testRunner2.runTest() >> result2
			1 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should succeed with WARNING status, executing setup, all tests and teardown (teardown ERROR)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.SUCCESS
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.ERROR
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.WARNING
			1 * setupRunner.runTest() >> setupResult
			
			1 * testRunner1.runTest() >> result1
			1 * testRunner2.runTest() >> result2
			1 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should fail with ERROR status, executing setup, all tests and teardown (test2 ERROR, teardown ERROR)"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.ERROR
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.ERROR
		when:
			def result = testee.run()
		then:
			result.getStatus()==GeneralStatus.ERROR
			1 * setupRunner.runTest() >> setupResult
			
			1 * testRunner1.runTest() >> result1
			1 * testRunner2.runTest() >> result2
			1 * testRunner3.runTest() >> result3
			
			1 * tearDownRunner.runTest() >> tearDownResult
	}
	
	def "should have setup, test and teardown status after execution"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.SUCCESS
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
			setupRunner.runTest() >> setupResult
			testRunner1.runTest() >> result1
			testRunner2.runTest() >> result2
			testRunner3.runTest() >> result3
			tearDownRunner.runTest() >> tearDownResult
		when:
			def result = testee.run()
		then:
			result.getSubpartResults()!=null
			result.getSubpartResults().size()==3
			
			result.getSetupResult()==setupResult
			result.getTearDownResult()==tearDownResult
	}
}
