/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.sahi.library;

import org.squashtest.ta.commons.resources.PropertiesResource;
import org.squashtest.ta.commons.targets.WebTarget;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.sahi.library.BasicSahiCommandHelper
import org.squashtest.ta.sahi.library.SquashSahiRunner
import org.squashtest.ta.sahi.library.BasicSahiCommandHelper.SahiRunnerFactory

import spock.lang.Specification;

public class BasicSahiCommandHelperTest extends Specification{
	def testee
	def browserTypeResource
	def genConfResource
	def runnerFactory
	
	def setup(){
		runnerFactory=Mock(SahiRunnerFactory)
		testee=new BasicSahiCommandHelper()
		
		testee.runnerFactory=runnerFactory
		runnerFactory.getRunner(_)>>Mock(SquashSahiRunner)
		
		testee.setTarget(Mock(WebTarget))
		
		def browserType=new Properties()
		def browserTypeIn=getClass().getResourceAsStream("/org/squashtest/ta/sahi/commands/browserType.properties")
		browserType.load(browserTypeIn)
		browserTypeResource= new PropertiesResource(browserType)

		
		def genConfig=new Properties()
		def genConfigIn=getClass().getResourceAsStream("/org/squashtest/ta/sahi/commands/genConfig.properties")
		genConfig.load(genConfigIn)
		genConfResource=new PropertiesResource(genConfig)
	}
	
	def "should throw IllegalConfiguration if double configuration"(){
		when:
			testee.readPropertiesFrom(genConfResource)
			testee.readPropertiesFrom(browserTypeResource)
		then:
			thrown IllegalConfigurationException
	}
	
	def "should take parameters into account"(){
		given:
			def script=new File(".")
		and:
			testee.readPropertiesFrom(genConfResource)
			testee.setSuite(script)
		when:
			testee.apply()
		then:
			1 * runnerFactory.getRunner({
				"squashtest.org".equals(it.getProperty("sahi.proxy.host"))
				"256".equals(it.getProperty("sahi.proxy.port"))
				"2".equals(it.getProperty("sahi.thread.nb"))
				"firefox".equals(it.getProperty("browserType"))
			}) >> Mock(SquashSahiRunner)
	}
}
