/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.framework.test.instructions;

import java.util.LinkedList;
import java.util.List;

import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceConverter;

/**
 * <p>A DefineResourceInstruction allows the definition of a resource directly in the test context, thus that need not to be loaded from a repository nor 
 * resulting from a {@link Command} or {@link ResourceConverter} job. Those are called 'script-defined instruction'.</p>
 * 
 * <p>A script-defined resource will be stored in the test context as a {@link FileResource}, which in this case will be a text file - including binary data 
 * this way is discouraged because the data will be scrambled by the decoding.</p>
 * 
 * 
 * @author bsiri
 *
 */

public class DefineResourceInstruction  implements TestInstruction{

	/**
	 * OPTIONAL : The content of the file, as a list of String. Though optional, leaving it empty would make no sense. 
	 */
	private List<String> resourceContent = new LinkedList<String>();
	
	/**
	 * OPTIONAL : May be used to store useful informations like a human readable representation of the instruction, comments etc.
	 */
	private String text;
	
	/**
	 * MANDATORY : The name of the script-defined resource, under which it will be stored in the test context.
	 */
	private ResourceName name;
	
	/**
	 * OPTIONAL : The scope of the newly defined resource.
	 */
	private ResourceName.Scope scope = ResourceName.Scope.SCOPE_TEST;
	
	
	public List<String> getResourceContent() {
		return resourceContent;
	}
	
	public String toText() {
		return text;
	}
	
	public void addLine(String line){
		resourceContent.add(line);
	}
	
	public void setText(String text){
		this.text=text;
	}
	
	public void setResourceName(ResourceName name){
		this.name=name;
	}

	public ResourceName getResourceName(){
		return name;
	}
	
	public ResourceName.Scope getScope(){
		return scope;
	}
	
	public void setScope(ResourceName.Scope scope){
		this.scope=scope;
	}
	
	public void visit(TestInstructionVisitor visitor) {
		visitor.accept(this);
	}

}
