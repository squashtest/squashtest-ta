/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.cats.runner.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.xmlbeans.XmlException;
import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction;
import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.LoadResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

import com.eviware.soapui.support.SoapUIException;

/**
 * This class is a facade to integrate legacy CATS SoapUi test classes into the
 * new squashTA execution framework.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractSoapUiTestCase extends AbstractCatsTestCase{

	private static final String RESOURCE_TYPE_SCRIPT_SOAPUI = "script.soapui";

	protected void runAndCheckSoapUiTestCase(String projectName, String testSuiteName, String testCaseName)
			throws XmlException, IOException, SoapUIException {
		ResetLocalContextInstruction context=createLocalContext("runAndCheckSoapUiTestCase()");
		getCallListener().addInstruction(context);
		
		ResourceName projectFile = new ResourceName(Scope.SCOPE_TEMPORARY,projectName);
		LoadResourceInstruction load=createLoadInstruction(projectName, projectFile, "runAndCheckSoapUiTestCase()", "soapUI script");
		getCallListener().addInstruction(load);
		
		ResourceName project = new ResourceName(Scope.SCOPE_TEMPORARY, "project");
		ConvertResourceInstruction convert = createConvertInstruction(
				projectFile,
				project,
				"runAndCheckSoapUiTestCase()", CONVERT_CATEGORY_SCRIPT,
				RESOURCE_TYPE_SCRIPT_SOAPUI);
		getCallListener().addInstruction(convert);
		
		ResourceName testSuiteResourceName = new ResourceName(Scope.SCOPE_TEMPORARY,"tsName");
		DefineResourceInstruction defineTSName=createDefineInstruction(testSuiteResourceName, "testSuiteName:"+testSuiteName);
		getCallListener().addInstruction(defineTSName);		
		
		ResourceName testCaseResourceName = new ResourceName(Scope.SCOPE_TEMPORARY,"tcName");
		DefineResourceInstruction defineTCName=createDefineInstruction(testCaseResourceName, "testCaseName:"+testCaseName);
		getCallListener().addInstruction(defineTCName);
		
		List<ResourceName>configuration=Arrays.asList(new ResourceName[]{testSuiteResourceName,testCaseResourceName});
		//the soapui:default target name will have to be managed by a special legacy target creator.
		ExecuteCommandInstruction command = createExecuteCommandInstruction("soapui:default",
				project, IGNORED_RESULT_NAME, "runAndCheckSoapUiTestCase()",
				configuration, COMMAND_CATEGORY_EXECUTE);
		getCallListener().addInstruction(command);
	}
}
