/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.maven.testfilter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Factory class to create a filename filter from a file filter specification
 * string.
 * 
 * @author edegenetais
 * 
 */
public class FileFilterFactory {
	
	/** Token interpreters used to create filters from specification tokens. */
	private static final TokenInterpreter[] INTERPRETERS;
	static{
		//here we link the tokenizing global factory to the effectively used token interpreters
		INTERPRETERS=new TokenInterpreter[]{
			new IncludeTestFactory(),
			new WildcardFilterFactory(),
			new RegexFilterFactory()
		};
	}
	
	/**
	 * Create a filter from a filter specification string.
	 * @param baseDirectory the base directory for relative path resolution.
	 * @param specification the filter specification string. The specification is a comma separated filter configuration token list. The exact grammar of the tokens depends on the used sub - filter factories (implementations of {@link TokenInterpreter}) - see {@link #INTERPRETERS}. In this version, {@value #INTERPRETERS}
	 * @return a filter implementing the filter specification.
	 */
	public FilenameFilter createFilter(File baseDirectory, String specification) {
		
		List<String> tokens = tokenizeSpecification(specification);
		
		List<FilenameFilter> filters = new ArrayList<FilenameFilter>();
		while (tokens.size() > 0) {
			TokenInterpreter interpreter = findEligibleInterpreter(
					baseDirectory, tokens);
			if(interpreter==null){
				throw new IllegalArgumentException(
						"No token interpreter wants token '" + tokens.get(0)
								+ "'");
			}else{
				filters.add(interpreter.interprete(baseDirectory, tokens));
			}
		}
		
		return createGlobalFilter(filters);
	}

	protected List<String> tokenizeSpecification(String specification) {
		List<String> tokens;
		if (specification == null) {
			tokens=Collections.emptyList();
		}else{
			//split the specification into tokens and remove any empty token
			tokens = new LinkedList<String>(
				Arrays.asList(specification.split(",")));
			boolean tokenRemoved;
			do {
				tokenRemoved = tokens.remove("");
			} while (tokenRemoved);
		}
		return tokens;
	}

	private FilenameFilter createGlobalFilter(List<FilenameFilter> filters) {
		FilenameFilter filter;
		if (filters.size() == 0) {
			filter=new YesFilenameFilter();
		} else if (filters.size() == 1) {
			filter=filters.get(0);
		} else {
			filter=new CompositeFilenameFilter(filters);
		}
		return filter;
	}

	private TokenInterpreter findEligibleInterpreter(File baseDirectory,
			List<String> tokens) {
		TokenInterpreter interpreter=null;
		for(TokenInterpreter candidate:INTERPRETERS){
			if(candidate.isEligible(baseDirectory, tokens)){
				if(interpreter==null){
					interpreter=candidate;
				}else{
					throw new IllegalStateException("Ambiguity: interpreters "
							+ interpreter.getClass().getName() + " and "
							+ candidate.getClass().getName()
							+ " both want token " + tokens.get(0));
				}
			}
		}
		return interpreter;
	}
}
