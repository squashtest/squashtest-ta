/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools.io;

import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

public class StreamCloser {

	public static void close(Closeable... closeables) throws IOException{
		close(new LinkedList<Closeable>(Arrays.asList(closeables)));
	}
	
	public static void close(Collection<Closeable> closeables) throws IOException{
		close(new LinkedList<Closeable>(closeables));
	}
	
	private static void close(LinkedList<Closeable> closeables) throws IOException{
		
		if (closeables.size()>0){		
		Closeable toClose = closeables.removeFirst();
		
		try {
			
			if (toClose!=null){
				toClose.close();
			}
		}
		finally{
			close(closeables);
		}
		}
	}
}
