/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.resources;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * CSV resource implementation.
 * @author fgaillard
 *
 */
@ResourceType("csv")
public class CSVResource implements Resource<CSVResource> {

	private static final FileTree FILE_TREE = new FileTree();
	private File csvFile;
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public CSVResource(){}
	
	public CSVResource(File file){
		csvFile = file;
	}
	
	@Override
	public CSVResource copy() {
		try {
			File newFile = FILE_TREE.createTempCopyDestination(csvFile);
			FileUtils.copyFile(csvFile, newFile);
			return new CSVResource(newFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException("CSV (resource) : could not copy file path '"+csvFile.getPath()+"', an error occured :", e);
		}
	}

	@Override
	public void cleanUp() {
		if (csvFile.exists()){
			boolean delete=csvFile.delete();
			if(!delete){
				LoggerFactory.getLogger(CSVResource.class).warn("Failed to delete file "+csvFile.getAbsolutePath()+" on cleanup.");
			}
		}
	}

	public File getCSVFile() {
		return csvFile;
	}
}
