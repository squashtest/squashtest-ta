/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.commons.helpers;

import java.util.Properties;

import org.squashtest.ta.core.library.properties.UnmodifiableProperties;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.result.ResourceGenerator;
import org.squashtest.ta.framework.test.result.ResourceMetadata;

/**
 * {@link ResourceMetadata} implementation for engine component which generate execution report resources.
 * @author edegenetais
 *
 */
public final class ExecutionReportResourceMetadata implements ResourceMetadata {
	
	/**
	 * {@link ResourceGenerator} descriptor implementation for this {@link ResourceMetadata} implementation.
	 * @author edegenetais
	 *
	 */
	private final class ResourceGeneratorImpl implements ResourceGenerator {
		private Properties unmodifiableMap;
		public ResourceGeneratorImpl(Properties configuration){
			unmodifiableMap=new UnmodifiableProperties(configuration);
		}
		
		@Override
		public String getName() {
			return generatorClassName;
		}

		@Override
		public String getGeneratorType() {
			return GENERATOR_TYPE;
		}

		@Override
		public Properties getConfiguration() {
			return unmodifiableMap;
		}
	}

	/**
	 * Resource generator type string (value of the 
	 */
	private final String GENERATOR_TYPE;
	private final String FILE_RESOURCE_RESOURCE_TYPE;
	private final ResourceGeneratorImpl resourceGeneratorDescriptor;
	private final ResourceName resourceName;
	private final String generatorClassName;
	
	public ExecutionReportResourceMetadata(Class<?> generatorType, Properties generatorConfiguration,Class<? extends Resource<?>>resourceType, String resourceName){
		generatorClassName=generatorType.getName();
		//resource generator description
		EngineComponent generatorAnnotation = generatorType.getAnnotation(EngineComponent.class);
		if(generatorAnnotation==null){
			throw new IllegalArgumentException(generatorType.getName()+" is not a valid engine component (no @EngineComponent annotation).");
		}
		GENERATOR_TYPE=generatorAnnotation.value();
		resourceGeneratorDescriptor = new ResourceGeneratorImpl(generatorConfiguration);
		
		//resource description
		ResourceType resourceTypeAnnotation = resourceType.getAnnotation(ResourceType.class);
		if(resourceTypeAnnotation==null){
			throw new IllegalArgumentException(resourceType.getName()+" is not a valid resource type (no @ResourceType annotation).");
		}
		FILE_RESOURCE_RESOURCE_TYPE = resourceTypeAnnotation.value();
		this.resourceName = new ResourceName(Scope.FAILURE_REPORT,resourceName);
	}
	
	@Override
	public ResourceRole getRole() {
		return ResourceRole.EXECUTION_REPORT;
	}

	@Override
	public String getResourceType() {
		return FILE_RESOURCE_RESOURCE_TYPE;
	}

	@Override
	public ResourceGenerator getOrigin() {
		return resourceGeneratorDescriptor;
	}

	@Override
	public ResourceName getName() {
		return resourceName;
	}
}