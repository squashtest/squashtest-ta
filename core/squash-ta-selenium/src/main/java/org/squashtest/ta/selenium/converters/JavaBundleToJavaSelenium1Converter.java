/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.selenium.converters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.resources.JavaCodeBundle;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.selenium.library.JUnitTester;
import org.squashtest.ta.selenium.resources.SeleniumOneJavaTest;

@EngineComponent("script")
public class JavaBundleToJavaSelenium1Converter  implements ResourceConverter<JavaCodeBundle, SeleniumOneJavaTest> {

	private static final Logger logger = LoggerFactory.getLogger(JavaBundleToJavaSelenium1Converter.class);
	
	private String mainSeleniumClass;
	private JUnitTester junitTester = JUnitTester.getInstance();
	
	/**
	 * empty constructor for Spring instanciation purposes
	 */
	public JavaBundleToJavaSelenium1Converter(){}

	@Override
	public float rateRelevance(JavaCodeBundle input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		// The main class of the selenium test suite can be passed through configuration
		try{
			for (Object object : configuration) {
				if (object instanceof FileResource) {
					FileResource fileConf = (FileResource) object;
					SimpleLinesData confLines=new SimpleLinesData(fileConf.getFile().getAbsolutePath());
					for(String conf:confLines.getLines()) {
						setMainSeleniumClass(conf);
					}
				}
			}
		} catch (FileNotFoundException fnfe) {
			throw new BadDataException("Configuration not found\n",fnfe);
		} catch (IOException ioe) {
			throw new BadDataException("Configuration not found\n",ioe);
		}
	}

	@Override
	public SeleniumOneJavaTest convert(JavaCodeBundle resource) {
		//We test if the code Bundle (compiled classes) have at least one class of Junit type
		boolean jUnit = false;
		ClassLoader bundleClassLoader = resource.getDedicatedClassloader();
		Set<String> classesInBundle = resource.getBundleClassNames();
		try {
			if (mainSeleniumClass != null){
				//first if the mainSeleniumClass has been defined we check to see if it is a Junit Type
				if (junitTester.isJunitClass(bundleClassLoader.loadClass(mainSeleniumClass))){
					jUnit = true;
				}
			} else {
				for (String className : classesInBundle) {
					Class<?> clazz = bundleClassLoader.loadClass(className);
					if (junitTester.isJunitClass(clazz)){
						jUnit = true;
					}
				}
			}
		} catch (ClassNotFoundException cnfe) {
			throw new BadDataException("Class not found", cnfe);
		}
		if (jUnit){
			if (mainSeleniumClass != null){
				return new SeleniumOneJavaTest(bundleClassLoader, mainSeleniumClass);
			} else {
				return new SeleniumOneJavaTest(bundleClassLoader);
			}
		} else {
			logError("Java Code Bundle does not have any Junit type class");
			throw new BadDataException("Java Code Bundle does not have any Junit type class");
		}
	}

	@Override
	public void cleanUp() {
		// no cleanup necessary
	}

	public void setMainSeleniumClass(String mainSeleniumClass) {
		this.mainSeleniumClass = mainSeleniumClass;
	}
	
	private void logError(String message){
		if (logger.isErrorEnabled()){
			logger.error(message);
		}
	}

}

