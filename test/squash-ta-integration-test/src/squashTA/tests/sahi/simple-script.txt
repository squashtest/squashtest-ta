SETUP :
LOAD sahi-scripts/main/simple-script.sah AS sahiFile
CONVERT sahiFile TO script.sahi AS suite

TEST :
EXECUTE execute WITH suite ON c3p0_doc USING $(browserType=firefox) AS sahiresult
ASSERT sahiresult IS success
