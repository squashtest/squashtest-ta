/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.core.tools;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.library.properties.DefaultPropertiesSource;
import org.squashtest.ta.core.library.properties.NamespacedPropertiesSource;
import org.squashtest.ta.core.library.properties.PropertiesKeySet;
import org.squashtest.ta.core.library.properties.PropertiesSource;
import org.squashtest.ta.core.tools.io.PropertiesLoader;

/**
 * Allows creating property sets with automatic overloading by system properties with namespace support.
 * @author edegenetais
 *
 */
public class PropertiesBasedCreatorHelper{

	private static final PropertiesLoader PROPERTIES_LOADER=new PropertiesLoader();
	
	private Logger logger = LoggerFactory.getLogger(PropertiesBasedCreatorHelper.class);
	
	private String[] keys = new String[0];
	private String[] keysRegExp = new String[0];
	
	
	public void setKeys(String... keys){
		this.keys = Arrays.copyOf(keys, keys.length);
	}
	
	public void setKeysRegExp(String... regExps){
		this.keysRegExp = Arrays.copyOf(regExps, regExps.length);
	}
		
	public Properties getEffectiveProperties(File file) throws IOException{
		
		//prologue
		String name = FilenameUtils.getName(file.getName());
		String shortName = stripExtension(name);
		
		Properties config = toProperties(file);
		
		//build the properties sources
		PropertiesSource configSource = new DefaultPropertiesSource(config) ;
		PropertiesSource defaultSource = newDefaultSource();
		PropertiesSource namespacedSource = newNamespacedSource(shortName);
		
		//build the key set
		PropertiesKeySet keySet = buildEffectiveKeySet(namespacedSource, configSource, defaultSource);
		
		//get the properties
		Properties properties = PropertiesUtils.staticGetEffectiveProperties(keySet, namespacedSource, configSource, defaultSource );
		
		return properties;
		
	}
	
	private PropertiesSource newDefaultSource(){
		try{
			Properties properties = System.getProperties();
			return new DefaultPropertiesSource(properties);
		}catch(SecurityException ex){
			logger.warn("PropertiesBasedRepositoryCreator : the jvm security manager forbids access to the system properties. The system properties will be ignored.");
			return new DefaultPropertiesSource(new Properties());
		}
	}
	
	private PropertiesSource newNamespacedSource(String namespace){
		try{
			Properties properties = System.getProperties();
			return new NamespacedPropertiesSource(properties, namespace);
		}catch(SecurityException ex){
			logger.warn("PropertiesBasedRepositoryCreator : the jvm security manager forbids access to the system properties. The system properties will be ignored.");
			return new NamespacedPropertiesSource(new Properties(), namespace);
		}		
	}
	
	private PropertiesKeySet buildEffectiveKeySet(PropertiesSource... sources){
		
		PropertiesKeySet keySet = new PropertiesKeySet(keys);
		
		for (String regexp : keysRegExp){
			PropertiesKeySet moreKeys = PropertiesUtils.staticFindKeysByPattern(regexp, sources);
			keySet.addUnique(moreKeys);
		}
		
		return keySet;
	}
	
	
	/**
	 * From 'properties' will produce a new Properties object, for which some value will be truncated to 'size' characters then ellipsed if the corresponding
	 * key is within the list 'keys'. The original object will remain unchanged.
	 * 
	 * @param properties the properties to anonymize
	 * @param keys the keys we want to shadow
	 * @param size : the number of character shown before ellipsis.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Properties anonymize(Properties properties, int size, String... keys){

		Properties result = new Properties();
		List<String> bannedKeys = Arrays.asList(keys);
			
		Enumeration<String> keySet = (Enumeration<String>)properties.propertyNames();
		
		while(keySet.hasMoreElements()){
			
			String key = keySet.nextElement();
			String value = properties.getProperty(key);
			
			if (value!=null && bannedKeys.contains(key)){ 
				value=value.substring(0,Math.min(size, value.length()))+"..." ;
			}
					
			result.setProperty(key, value);
		}
		
		return result;
	}
	

	
	protected Properties toProperties(File file) throws IOException{
			return PROPERTIES_LOADER.load(file);
	}
	
	protected String stripExtension(String name){
		int suffixIndex = name.lastIndexOf(".properties");
		if (suffixIndex==-1){
			return name;
		}else{
			return name.substring(0, suffixIndex);
		}
	}
	

}
