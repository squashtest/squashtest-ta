/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.init

import java.net.URL;

import spock.lang.Specification

class ComponentPackageEnumeratorTest extends Specification {
	def testee
	def parser
	
	def setup(){
		//isolating class loader to feed the enumerator a controled resource list. It should try to load both proposed resources.
		def loader=new URLClassLoader([null]){
			@Override
			public URL findResource(String name) {
				return super.findResource(name);
			}
			public Enumeration<URL> getResources(String name){
				Enumeration<URL> resources=new Enumeration<URL>(){
					private int nb=2;
					
					@Override
					boolean hasMoreElements(){
						return nb>0;
					}
					
					@Override
					URL nextElement(){
						if(hasMoreElements()){
							nb--;
							return new File("src/test/resources/"+nb+"/META-INF/squashTA-components.mf").toURI().toURL();
						}else{
							throw new NoSuchElementException("No more elements");
						}
					}
				};
				return resources;
			}
			
			@Override
			public Class<?> loadClass(String name)
					throws ClassNotFoundException {
						Class<?> clazz=findLoadedClass(name);
						if(clazz==null){
							File classFile=new File("target/classes/"+name.replaceAll("\\.", "/")+".class")
							if(classFile.exists()){
								byte[] buffer=new byte[4096];
								InputStream is=new FileInputStream(classFile);
								ByteArrayOutputStream baos=new ByteArrayOutputStream();
								int nb=is.read(buffer);
								while(nb>=0){
									baos.write(buffer, 0, nb);
									nb=is.read(buffer);
								}
								is.close();
								baos.close();
								clazz=defineClass(name, baos.toByteArray(), 0, baos.size());
								resolveClass(clazz);
							}
						}
						if(clazz==null){
							clazz=parent.loadClass(name);
						}
						return clazz;
			}
		};
		testee=loader.loadClass("org.squashtest.ta.backbone.init.ComponentPackagesEnumerator").newInstance();
		def parserClass=loader.loadClass("org.squashtest.ta.backbone.init.ComponentManifestParser");
		parser=Mock(parserClass)
		testee.parser=parser
	}
	
	def "should enumerate the two presented manifests"(){
		when:
			def config=testee.getComponentConfiguration()
		then:
			1 * parser.loadManifest(_,new File("src/test/resources/0/META-INF/squashTA-components.mf").toURI().toURL())
			1 * parser.loadManifest(_,new File("src/test/resources/1/META-INF/squashTA-components.mf").toURI().toURL())
	}
}
