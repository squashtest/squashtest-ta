@echo off
@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2012 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses/>.
@REM

hostname > .\.hostname.tmp.txt
set /p HOSTNAME=<.\.hostname.tmp.txt
del .\.hostname.tmp.txt

set OLDCD=%CD%
cd ..
set SQUASH_TA_HOME=%CD%
cd "%OLDCD%"

set JAVA_HOME=%SQUASH_TA_HOME%\openjdk1.6.0_24-win32
set MAVEN_HOME=%SQUASH_TA_HOME%\apache-maven-3.0.4
set MVN_HOME=%MAVEN_HOME%
set MAVEN_OPTS=-Xms512m -Xmx512m -XX:PermSize=256m -XX:MaxPermSize=512m
set SAHI_HOME=%SQUASH_TA_HOME%\sahi_v35_20110719
set PATH="%JAVA_HOME%\bin";"%MVN_HOME%\bin";%PATH%
set START_SAHI_PROXY=$SAHI_SELECTED

@REM protect the execution server tomcat environment against system-wide variable settings
set CATALINA_HOME=
set CATALINA_OPTS=

set JENKINS_HOME=%SQUASH_TA_HOME%\execution_home

echo "Squash TA Server environment %SQUASH_TA_HOME%@%HOSTNAME%"