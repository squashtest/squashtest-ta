/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.framework.test.instructions;


/**
 * <p>Conceptually an instruction is an atomic operation that will use available resources, targets etc and make something with them.
 * Basically here, an instruction just describes who are the said resources, targets etc but makes nothing : it merely carry String-based 
 * informations to the engine, and the engine will run it by fetching getting the objects corresponding to those informations.</p>
 * 
 * <p>The set of instructions is finite and they are all published in this same package, please refer to their definition for details about their purpose. Note that some arguments are mandatory while some other are not :
 * <ul>
 * 	<li>the mandatory arguments are javadoced MANDATORY followed by a short explanation of what argument is. If the argument is missing the engine will 
 * throw an exception and will terminate the test with the status (the rest of the test suite will be executed normaly). </li>
 * 	<li>the optional arguments are javadoced OPTIONAL followed by a short explanation of what that argument is. These arguments may be left to null.</li> 
 * <ul>
 * </p>
 * 
 * <p>Remember that an Instruction, while being a java object, should be attached a textual representation for reporting purpose (see {@link #toText()})</p>
 * 
 * @author bsiri
 *
 */

public interface TestInstruction {
	
	/**
	 * Gets a textual representation of the instruction. May be used as a commentary for instance, and has no impact on the job itself.
	 * 
	 * @return
	 */
	String toText();
	
	/**
	 * Sets the textual representation.
	 * 
	 * @param text
	 */
	void setText(String text);
	
	
	/**
	 * Visitor hook.
	 * 
	 * @param visitor
	 */
	void visit(TestInstructionVisitor visitor);
	
}
