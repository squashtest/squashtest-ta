/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2012 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.api.execution.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

/**
 * Factory class to instantiate the execution client.
 * 
 * @author edegenetais
 * 
 */
public class ClientFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientFactory.class);
	/* marshalling configuration under there! */
	private static final List<Object> providers=new ArrayList<Object>();
	static{
		JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
		providers.add(provider);
	}
	
	/**
	 * Get a configured client for the specified endpoint. Questions?
	 * @param endPoint endpoint URL to connect to.
	 * @return the client.
	 */
	public StatusUpdate getClient(URL endPoint, File endpointLoginConfFile) {
		StatusUpdate client = null;
		String endpointURLString = endPoint.toExternalForm();
		client=JAXRSClientFactory.create(endpointURLString, StatusUpdateBinding.class,providers);
		
		String authorizationHeader = getCredentials(endPoint, endpointLoginConfFile);
		if(authorizationHeader != null)
		{
			WebClient.client(client).header("Authorization", authorizationHeader);
		}

		return client;
	}
	
	private String getCredentials(URL endpoint, File endpointLoginConfFile)
	{
		String authorizationHeader=null;
		if(endpointLoginConfFile!=null)
		{
			Properties prop = new Properties();
			FileInputStream inStream;
			try {
				inStream = new FileInputStream(endpointLoginConfFile);
				prop.load(inStream);
				inStream.close();
				String key = endpoint.toExternalForm()+".login";
				String login = prop.getProperty(key);
				if(login == null){
					LOGGER.warn("Rest client configurer : key {} not found, no credentials will be used", key);
				} else if(login.equals(""))	{
					LOGGER.warn("Rest client configurer : Login credentials is empty, no credentials will be used");
				}else{
					key = endpoint.toExternalForm()+".password";
					String pwd = prop.getProperty(key);
					if(pwd == null)	{
						LOGGER.warn("Rest client configurer : key {} not found, no credentials will be used", key);
					} else {
						String concat = login+":"+pwd;
						authorizationHeader = "Basic " + org.apache.cxf.common.util.Base64Utility.encode(concat.getBytes());
					}
				}
			} catch (FileNotFoundException e) {
				LOGGER.warn("Rest client configurer : Squash TA configuration file for TM credentials was not found at : {}, no credentials will be used ",endpointLoginConfFile.getPath());
			} catch (IOException e) {
				LOGGER.warn("Rest client configurer : An IO error occured while using Squash TA configuration file for TM credentials, no credentials will be used.");
				LOGGER.warn(e.getMessage());
			}
			
		}else{
			LOGGER.warn("Rest client configurer : No Squash TA configuration file for TM credentials was given in maven command line argument. No credentials will be used");
		}
		return authorizationHeader;
	}
	
}
